/*
 Navicat Premium Data Transfer

 Source Server         : E.D.I.T.H
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : localhost:3306
 Source Schema         : sikeren

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 28/06/2021 12:24:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for biaya_siswas
-- ----------------------------
DROP TABLE IF EXISTS `biaya_siswas`;
CREATE TABLE `biaya_siswas` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nis` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rincian_biaya_id` int NOT NULL,
  `jenis_biaya_id` int NOT NULL,
  `satuan` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `prioritas` int NOT NULL,
  `nominal` int NOT NULL,
  `dibayar` int NOT NULL DEFAULT '0',
  `bulan` int DEFAULT NULL,
  `tahun_id` int NOT NULL,
  `tahun_ajaran` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=229708 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for core_infos
-- ----------------------------
DROP TABLE IF EXISTS `core_infos`;
CREATE TABLE `core_infos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'CV Alphabet',
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0853 2013 2014',
  `copyright_before_login` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `copyright_after_login` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `color_primary` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#00695C',
  `color_secondary` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#4CAF50',
  `metode_bri` tinyint(1) NOT NULL DEFAULT '0',
  `metode_bri_syariah` tinyint(1) NOT NULL DEFAULT '0',
  `metode_payment_gateway` tinyint(1) NOT NULL DEFAULT '0',
  `logo` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo_after_login` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `left_logo` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `left_logo_status` tinyint(1) NOT NULL DEFAULT '0',
  `right_logo` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `right_logo_status` tinyint(1) NOT NULL DEFAULT '0',
  `walpaper_image` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `core_infos` (`id`, `nama`, `alamat`, `no_telp`, `copyright_before_login`, `copyright_after_login`, `color_primary`, `color_secondary`, `metode_bri`, `metode_bri_syariah`, `metode_payment_gateway`, `logo`, `logo_after_login`, `favicon`, `left_logo`, `left_logo_status`, `right_logo`, `right_logo_status`, `walpaper_image`, `created_at`, `updated_at`) VALUES (1, 'Pesantren', 'Kp. Tasikmalaya', '(0265) 000', 'Copyright © 2019 - 2021. SANTREN.ID', 'Copyright © 2019 - 2021. Made with <i class=\"fa fa-coffee\"></i> <a href=\"http://santren.id/\" target=\"_blank\">SANTREN.ID</a>', '#00695c', '#4caf50', 0, 1, 0, NULL, NULL, NULL, 'l_logo.png', 1, NULL, 0, NULL, '2021-06-07 14:18:29', '2021-06-07 14:38:15');


-- ----------------------------
-- Table structure for districts
-- ----------------------------
DROP TABLE IF EXISTS `districts`;
CREATE TABLE `districts` (
  `id` char(7) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `regency_id` char(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `districts_id_index` (`regency_id`),
  CONSTRAINT `districts_regency_id_foreign` FOREIGN KEY (`regency_id`) REFERENCES `regencies` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for ihsan_dan_potongans
-- ----------------------------
DROP TABLE IF EXISTS `ihsan_dan_potongans`;
CREATE TABLE `ihsan_dan_potongans` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipe` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_jabatan` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for jenis_biayas
-- ----------------------------
DROP TABLE IF EXISTS `jenis_biayas`;
CREATE TABLE `jenis_biayas` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for jenis_pegawais
-- ----------------------------
DROP TABLE IF EXISTS `jenis_pegawais`;
CREATE TABLE `jenis_pegawais` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for jenjang
-- ----------------------------
DROP TABLE IF EXISTS `jenjang`;
CREATE TABLE `jenjang` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for jenjangs
-- ----------------------------
DROP TABLE IF EXISTS `jenjangs`;
CREATE TABLE `jenjangs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for jurnals
-- ----------------------------
DROP TABLE IF EXISTS `jurnals`;
CREATE TABLE `jurnals` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kode_trx` varchar(100) CHARACTER SET latin1 NOT NULL,
  `nis` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `ref` int NOT NULL,
  `biaya_id` int DEFAULT NULL,
  `nama_biaya` varchar(100) CHARACTER SET latin1 NOT NULL,
  `id_jenis_biaya` int NOT NULL,
  `type` int NOT NULL,
  `jumlah` int NOT NULL,
  `tgl_bayar` date NOT NULL,
  `saldo_akhir` int DEFAULT NULL,
  `ket` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=209581 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for jurusan
-- ----------------------------
DROP TABLE IF EXISTS `jurusan`;
CREATE TABLE `jurusan` (
  `kode` varchar(3) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `jurusan` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `program_studi_ids` varchar(100) CHARACTER SET latin1 NOT NULL,
  UNIQUE KEY `unik_kode` (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for kelas
-- ----------------------------
DROP TABLE IF EXISTS `kelas`;
CREATE TABLE `kelas` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `jenjang_id` int NOT NULL,
  `tingkat` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for master_biayas
-- ----------------------------
DROP TABLE IF EXISTS `master_biayas`;
CREATE TABLE `master_biayas` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tingkat` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tingkat` (`tingkat`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for master_penggajians
-- ----------------------------
DROP TABLE IF EXISTS `master_penggajians`;
CREATE TABLE `master_penggajians` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tipe` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `itp_id` int NOT NULL,
  `tipe_pegawai_id` int DEFAULT NULL,
  `jenis_pegawai_id` int DEFAULT NULL,
  `jumlah_ihsan` double(11,2) NOT NULL,
  `is_persen` tinyint(1) NOT NULL DEFAULT '0',
  `dikalikan_masa_kerja` tinyint(1) NOT NULL,
  `dikalikan_jam_kerja` tinyint(1) NOT NULL,
  `pegawai_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `berlaku_sampai` date DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=441 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urutan` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (1, 'Master', '#', 'fa fa-user-cog', NULL, 1, '2019-10-02 00:08:08', '2019-10-02 00:08:08');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (2, 'Referensi', '#', 'fa fa-list-ul', NULL, 2, '2019-10-02 00:08:26', '2019-10-02 00:08:26');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (3, 'Manajemen', '#', 'fa fa-object-ungroup', NULL, 3, '2019-10-02 00:08:53', '2019-10-02 00:08:53');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (4, 'Laporan', '#', 'fa fa-archive', NULL, 4, '2019-10-02 00:09:34', '2019-11-27 06:59:11');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (5, 'Aplikasi Siswa', '/tabletSiswa', 'fa fa-search', NULL, 4, '2019-10-02 00:10:46', '2019-12-06 08:46:46');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (6, 'Siswa', '/siswa', 'fa fa-users', '1', NULL, '2019-10-02 00:13:34', '2019-10-02 00:13:34');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (7, 'Kelas', '/kelas', 'fa fa-user', '1', 1, '2019-11-21 00:00:00', '2019-11-21 00:00:00');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (9, 'Jenjang', '/jenjang', 'fa fa-user-cog', '1', NULL, '2019-10-02 00:19:33', '2019-10-02 00:19:33');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (10, 'Keuangan', '/keuangan', 'fa fa-money-check', '1', NULL, '2019-10-02 00:21:31', '2020-03-05 08:46:53');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (11, 'Alamat', '/alamat', 'fa fa-money-check', '2', NULL, '2019-10-02 00:22:37', '2019-10-02 00:22:37');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (12, 'Sekolah', '/sekolah', 'fa fa-users', '2', NULL, '2019-10-02 00:32:10', '2019-10-02 00:32:10');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (13, 'Pekerjaan', '/pekerjaan', 'fa fa-coins', '2', NULL, '2019-10-02 00:33:04', '2019-10-02 00:33:30');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (14, 'Penghasilan Ortu', '/penghasilan-ortu', 'fa fa-wallet', '2', NULL, '2019-10-02 00:35:49', '2019-10-02 00:35:49');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (15, 'Input Cash / Cicil', '/input-cash-cicil', 'fa fa-archive', '3', NULL, '2019-10-02 00:37:43', '2019-10-02 00:40:06');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (16, 'Input Tabungan Siswa', '/input-tabungan-siswa', 'fa fa-wallet', '3', NULL, '2019-10-02 00:38:58', '2019-10-02 00:38:58');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (17, 'Input Pengeluaran', '/input-pengeluaran', 'fa fa-archive', '3', NULL, '2019-10-02 00:39:43', '2019-10-02 00:39:43');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (19, 'per POS / jenis biaya', '/laporan/per-jenis', 'fa fa-users', '4', NULL, '2019-10-02 00:44:33', '2019-11-18 20:49:49');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (20, 'Per siswa/kelas', '/kelas', 'fa fa-users', '4', NULL, '2019-10-02 00:45:16', '2019-11-09 06:03:44');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (23, 'Melihat tagihan/tunggakan', '/melihat-tagihan', 'fa fa-list-ul', '5', NULL, '2019-10-02 00:49:09', '2019-10-02 00:52:43');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (24, 'Melihat tabungan', '/melihat-tabungan', 'fa fa-list-ul', '5', NULL, '2019-10-02 00:49:55', '2019-10-02 00:49:55');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (25, 'Menu', '/menu', 'fa fa-list-ul', '1', NULL, '2019-10-02 01:05:50', '2019-10-02 01:05:50');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (26, 'Users', '/users', 'fa fa-user-cog', '1', NULL, '2019-10-02 01:06:17', '2019-10-02 01:06:17');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (27, 'Hak Akses (Role)', '/role', 'fa fa-object-ungroup', '1', NULL, '2019-10-02 01:07:43', '2021-06-23 09:16:14');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (28, 'Jenis Biaya', '/keuangan/jenis-biaya', 'fa fa-money-check', '10', NULL, '2019-10-05 09:35:01', '2019-10-05 09:36:46');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (29, 'Generate Biaya', '/keuangan/generate-biaya', 'fa fa-money-check', '10', NULL, '2019-10-12 09:26:48', '2019-10-12 09:26:48');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (30, 'Biaya Siswa', '/keuangan/biaya-siswa', 'fa fa-money-check', '10', NULL, '2019-10-17 13:04:42', '2019-10-17 13:04:42');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (31, 'Jurnal harian/bulan/tahun', '/laporan/jurnal', 'fa fa-print', '4', NULL, '2019-11-18 21:00:28', '2019-11-18 21:00:28');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (32, 'Laporan LK', '/laporan/lk', 'fa fa-save', '4', NULL, '2019-11-21 14:16:58', '2019-11-21 14:16:58');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (33, 'Uang Setoran', '/laporan/pemasukan', 'fa fa-print', '4', NULL, '2019-11-26 20:55:45', '2019-11-26 20:55:45');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (34, 'Laporan Saldo Tabsan', '/laporan-saldo-tabungan-siswa', 'fa fa-print', '4', NULL, '2019-11-26 20:56:15', '2021-06-22 13:55:17');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (35, 'Laporan Daftar Ulang', '/laporan/daftar-ulang', 'fa fa-list', '4', NULL, '2020-02-21 10:40:52', '2020-02-21 10:40:52');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (36, 'Laporan Daftar Ulang Kelas', '/laporan/daftar-ulang/kelas', 'fa fa-list', '4', NULL, '2020-02-25 09:09:20', '2020-02-25 11:34:40');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (37, 'Siswa Lulus', '/siswa-lulus', 'fa fa-users', '1', NULL, '2020-06-16 09:59:47', '2020-06-17 14:21:09');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (38, 'Laporan Tagihan Siswa Lulus', '/laporan-tagihan-siswa-lulus?filter=tagihan', 'fa fa-users', '4', NULL, '2020-06-16 10:03:30', '2020-06-16 10:03:30');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (39, 'Pegawai', '#', 'fa fa-users', NULL, NULL, '2020-10-08 18:24:37', '2020-10-08 18:24:37');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (40, 'Tipe Pegawai', '/tipe-pegawai', 'fa fa-list', '39', NULL, '2020-10-08 18:25:18', '2020-10-08 18:25:18');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (41, 'Jenis Pegawai', '/jenis-pegawai', 'fa fa-list', '39', NULL, '2020-10-08 18:25:44', '2020-10-08 18:25:44');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (42, 'Tunjangan & Potongan', '/itp-pegawai', 'fa fa-list', '39', NULL, '2020-10-08 18:26:28', '2020-10-08 18:26:50');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (43, 'Master Penggajian', '/master-penggajian', 'fa fa-list', '39', NULL, '2020-10-08 18:27:21', '2020-10-08 18:27:21');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (44, 'Penggajian', '/penggajian', 'fa fa-list', '39', NULL, '2020-10-08 18:27:58', '2020-10-08 18:27:58');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (45, 'Data Pegawai', '/pegawai', 'fa fa-users', '39', NULL, '2020-10-24 22:05:19', '2020-10-24 22:05:19');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (46, 'Laporan Tagihan Keuangan Siswa', '/laporan/tagihan', 'fa fa-user', '4', NULL, '2021-02-27 22:10:13', '2021-02-28 21:25:50');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (47, 'Setting', '/settings', 'fa fa-cog', '1', NULL, '2021-06-07 14:27:29', '2021-06-07 14:27:29');
INSERT INTO `menus` (`id`, `nama`, `url`, `icon`, `sub`, `urutan`, `created_at`, `updated_at`) VALUES (48, 'Laporan Saldo Tabsan', '/laporan-saldo-tabungan-siswa', 'fa fa-print', '4', NULL, '2021-06-22 13:54:47', '2021-06-22 15:34:13');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for pegawai_import
-- ----------------------------
DROP TABLE IF EXISTS `pegawai_import`;
CREATE TABLE `pegawai_import` (
  `niy` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `tahun` int DEFAULT NULL,
  `sertifikasi` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for pegawais
-- ----------------------------
DROP TABLE IF EXISTS `pegawais`;
CREATE TABLE `pegawais` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nik` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgl_masuk` date NOT NULL,
  `tipe_pegawai_id` int NOT NULL,
  `jenis_pegawai_id` int NOT NULL,
  `no_rek` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_hp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sertifikasi` tinyint(1) NOT NULL DEFAULT '0',
  `pin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for pekerjaan
-- ----------------------------
DROP TABLE IF EXISTS `pekerjaan`;
CREATE TABLE `pekerjaan` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for penggajians
-- ----------------------------
DROP TABLE IF EXISTS `penggajians`;
CREATE TABLE `penggajians` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `pegawai_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `masa_kerja` int NOT NULL,
  `ihsan_pokok_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ihsan_jk_jp` int NOT NULL,
  `ihsan_jk_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tunjangan_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `potongan_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bulan` int NOT NULL,
  `tahun` int NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1128 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for penghasilan_ortus
-- ----------------------------
DROP TABLE IF EXISTS `penghasilan_ortus`;
CREATE TABLE `penghasilan_ortus` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `kategori` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `min` int NOT NULL,
  `max` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for provinces
-- ----------------------------
DROP TABLE IF EXISTS `provinces`;
CREATE TABLE `provinces` (
  `id` char(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for referensi
-- ----------------------------
DROP TABLE IF EXISTS `referensi`;
CREATE TABLE `referensi` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for regencies
-- ----------------------------
DROP TABLE IF EXISTS `regencies`;
CREATE TABLE `regencies` (
  `id` char(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `province_id` char(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `regencies_province_id_index` (`province_id`),
  CONSTRAINT `regencies_province_id_foreign` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for rincian_biayas
-- ----------------------------
DROP TABLE IF EXISTS `rincian_biayas`;
CREATE TABLE `rincian_biayas` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `master_biaya_id` int NOT NULL,
  `jenis_biaya_id` int NOT NULL,
  `kelas` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `satuan` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `prioritas` int NOT NULL,
  `jk` int NOT NULL,
  `nominal` int NOT NULL,
  `status` int NOT NULL,
  `jenis_siswa` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for riwayat_kelas
-- ----------------------------
DROP TABLE IF EXISTS `riwayat_kelas`;
CREATE TABLE `riwayat_kelas` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nis` varchar(15) NOT NULL,
  `id_kelas` int NOT NULL,
  `kelas_tujuan` int NOT NULL,
  `tgl_lulus` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5221 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `roles` (`id`, `nama`, `menu`, `created_at`, `updated_at`) VALUES (1, 'Administrator', '[\"1\",\"6\",\"7\",\"9\",\"10\",\"28\",\"29\",\"30\",\"25\",\"26\",\"27\",\"37\",\"47\",\"3\",\"15\",\"16\",\"17\",\"4\",\"19\",\"20\",\"31\",\"32\",\"33\",\"34\",\"35\",\"36\",\"38\",\"46\",\"48\",\"5\",\"39\",\"40\",\"41\",\"42\",\"43\",\"44\",\"45\"]', '2019-10-02 01:15:38', '2021-06-22 13:56:15');
INSERT INTO `roles` (`id`, `nama`, `menu`, `created_at`, `updated_at`) VALUES (2, 'Keuangan', '[\"1\",\"6\",\"10\",\"28\",\"29\",\"30\",\"37\",\"3\",\"15\",\"16\",\"17\",\"4\",\"19\",\"20\",\"31\",\"32\",\"33\",\"34\",\"35\",\"36\",\"38\",\"46\",\"48\",\"39\",\"40\",\"41\",\"42\",\"43\",\"44\",\"45\"]', '2019-10-02 01:36:09', '2021-06-22 15:35:13');
INSERT INTO `roles` (`id`, `nama`, `menu`, `created_at`, `updated_at`) VALUES (3, 'Operator', '[\"1\",\"6\",\"7\",\"9\",\"10\",\"28\",\"29\",\"30\",\"37\",\"3\",\"15\",\"16\",\"17\",\"4\",\"19\",\"20\",\"32\",\"33\",\"34\",\"35\",\"38\"]', '2019-11-26 09:11:07', '2021-06-23 09:38:58');
INSERT INTO `roles` (`id`, `nama`, `menu`, `created_at`, `updated_at`) VALUES (4, 'PSDM', '[\"39\",\"45\"]', '2021-01-21 21:29:16', '2021-01-21 21:29:16');
INSERT INTO `roles` (`id`, `nama`, `menu`, `created_at`, `updated_at`) VALUES (5, 'DAPODIK', '[\"1\",\"6\"]', '2021-03-04 10:38:42', '2021-03-04 10:38:42');
INSERT INTO `roles` (`id`, `nama`, `menu`, `created_at`, `updated_at`) VALUES (6, 'Tabsan', '[\"3\",\"16\"]', '2021-05-29 09:37:05', '2021-05-30 09:13:33');
INSERT INTO `roles` (`id`, `nama`, `menu`, `created_at`, `updated_at`) VALUES (7, 'Ka. Keuangan', '[\"1\",\"6\",\"7\",\"9\",\"10\",\"28\",\"29\",\"30\",\"37\",\"3\",\"15\",\"16\",\"17\",\"4\",\"19\",\"20\",\"31\",\"32\",\"33\",\"34\",\"35\",\"36\",\"38\",\"46\",\"48\",\"39\",\"40\",\"41\",\"42\",\"43\",\"44\",\"45\"]', '2021-06-07 14:25:54', '2021-06-23 09:39:54');

-- ----------------------------
-- Table structure for sekolah
-- ----------------------------
DROP TABLE IF EXISTS `sekolah`;
CREATE TABLE `sekolah` (
  `id` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `kabupaten_id` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `crawling` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for siswas
-- ----------------------------
DROP TABLE IF EXISTS `siswas`;
CREATE TABLE `siswas` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nis` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nipd` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kelas` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_kelas` int NOT NULL,
  `nisn` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jk` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat_ortu` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_hp` varchar(14) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenjang` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int NOT NULL DEFAULT '1',
  `pin` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `du_dk` tinyint(1) NOT NULL DEFAULT '0',
  `baru_lama` tinyint(1) NOT NULL DEFAULT '0',
  `laundry` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `avatar` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `siswas_nis_unique` (`nis`)
) ENGINE=InnoDB AUTO_INCREMENT=4536 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for tagihan_h2hs
-- ----------------------------
DROP TABLE IF EXISTS `tagihan_h2hs`;
CREATE TABLE `tagihan_h2hs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nis` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenjang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kelas` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref` int NOT NULL DEFAULT '2',
  `biaya_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nominal` int NOT NULL,
  `status` int NOT NULL DEFAULT '1',
  `tgl_bayar` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1463 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for tipe_pegawais
-- ----------------------------
DROP TABLE IF EXISTS `tipe_pegawais`;
CREATE TABLE `tipe_pegawais` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for total_bayar_siswas
-- ----------------------------
DROP TABLE IF EXISTS `total_bayar_siswas`;
CREATE TABLE `total_bayar_siswas` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `kode_trx` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nis` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref` int NOT NULL,
  `biaya_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah` int NOT NULL,
  `tgl_bayar` date NOT NULL,
  `id_tagihan_h2h` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45458 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `role_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `avatar`, `username`, `name`, `email`, `email_verified_at`, `password`, `active`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES (1, NULL, 'sikeren', 'Admin Sikeren', 'admin@sikeren.com', NULL, '$2y$10$Kuj1ZJpTxR8dI4XuiBOckeo.XMJb2p9CLnYtxr7Pxgi6dCG5T.imS', 1, '1', 'M8hWGLMjWEYCaaIo97UBf90mlnzlaTuhplrah2eL2Dy4T7fNMqoQdi03IPSX', '2019-09-30 13:38:33', '2019-11-18 07:46:50');

-- ----------------------------
-- Table structure for villages
-- ----------------------------
DROP TABLE IF EXISTS `villages`;
CREATE TABLE `villages` (
  `id` char(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `district_id` char(7) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `villages_district_id_index` (`district_id`),
  CONSTRAINT `villages_district_id_foreign` FOREIGN KEY (`district_id`) REFERENCES `districts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for wa
-- ----------------------------
DROP TABLE IF EXISTS `wa`;
CREATE TABLE `wa` (
  `nisn` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nohp` varchar(14) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`nisn`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


TRUNCATE siswas;
TRUNCATE biaya_siswas;
TRUNCATE jenjang;
TRUNCATE ihsan_dan_potongans;
TRUNCATE jenis_biayas;
TRUNCATE jenis_pegawais;
TRUNCATE jurnals;
TRUNCATE jurusan;
TRUNCATE kelas;
TRUNCATE master_penggajians;
TRUNCATE master_biayas;
TRUNCATE pegawai_import;
TRUNCATE pegawais;
TRUNCATE pekerjaan;
TRUNCATE penggajians;
TRUNCATE penghasilan_ortus;
TRUNCATE rincian_biayas;
TRUNCATE riwayat_kelas;
TRUNCATE sekolah;
TRUNCATE tagihan_h2hs;
TRUNCATE tipe_pegawais;
TRUNCATE total_bayar_siswas;
TRUNCATE wa;

-- ----------------------------
-- Function structure for fRupiah
-- ----------------------------
DROP FUNCTION IF EXISTS `fRupiah`;
delimiter ;;
CREATE FUNCTION `fRupiah`(number BIGINT)
 RETURNS varchar(255) CHARSET latin1
  DETERMINISTIC
BEGIN  
DECLARE hasil VARCHAR(255);  
SET hasil = REPLACE(REPLACE(REPLACE(FORMAT(number, 0), '.', '|'), ',', '.'), '|', ',');  
RETURN (hasil);  
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
