<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Api BRIS
Route::prefix('v1')->group(function() {
    // BANK BRIS
    // http://web/v1/apiDataTagihanMhs/{REG}/{API_KEY}
    Route::get('/apiDataTagihan/{nis}/{api_key}', 'ApiBrisController@DataTagihanH2h');
    Route::get('/apiUpdate/{nis}/{noRef}/{idTagihan}', 'ApiBrisController@apiUpdateH2h');
    Route::get('/apiReversal/{nis}/{noRef}/{idTagihan}', 'ApiBrisController@apiReversalH2h');

    // APP WA
    Route::get('/wa/{api_key}', 'ApiBrisController@DataTagihanWA');
});

Route::prefix('api/v1')->group(function() {
    // APP SISWA
    Route::get('dataTagihan/{nis}/{pin}', 'ApiBrisController@DataTagihan');

    Route::get('buatTagihanBris/{nis}/{pin}/{tagihanReq}', 'ApiBrisController@buatTagihan');
    Route::get('buatTabunganBris/{nis}/{pin}/{uang}', 'ApiBrisController@buatTabungan');

    Route::get('batalkanTagihanBris/{nis}/{pin}/{id}', 'ApiBrisController@batalkanTagihan');
    
});


