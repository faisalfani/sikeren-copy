<?php

namespace Modules\ApiBris\Entities;

use Illuminate\Database\Eloquent\Model;

class TagihanH2h extends Model
{
    protected $table = 'tagihan_h2hs';
    protected $guarded = [];
}
