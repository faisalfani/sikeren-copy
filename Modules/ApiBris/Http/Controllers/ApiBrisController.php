<?php

namespace Modules\ApiBris\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Keuangan\Entities\BiayaSiswa;
use Modules\Siswa\Entities\Siswa;
use Modules\Keuangan\Entities\Jurnal;
use Modules\Keuangan\Entities\TotalBayarSiswa;
use Modules\ApiBris\Entities\TagihanH2h;
use Modules\Keuangan\Entities\JenisBiaya;

class ApiBrisController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    // API SISWA
    public function buatTagihan($nis, $pin, $tagihanReq)
    {


        $tagihan = explode("-", $tagihanReq);


        // cek NISN dan PIN
        if (!$tagihan) {
            return response()->json([
                "status" => "error",
                "info" => "Tagihan tidak ditemukan"
            ]);
        }

        $siswa = Siswa::where("nis", $nis)->where("pin", $pin)->first();
        // Tess
        if ($siswa) {

            $totalTagihan = 0;

            foreach ($tagihan as $tagihanId) {
                $ta = BiayaSiswa::where("id", $tagihanId)
                    ->whereRaw("dibayar != nominal")
                    ->first();
                if ($ta) {
                    $jmlTagihan = $ta->nominal - $ta->dibayar;
                    $totalTagihan += $jmlTagihan;
                }
            }


            if ($totalTagihan > 0) {

                $cek = TagihanH2h::where("nis", $siswa->nis)->first();
                if (!$cek) {
                    $tagihanH2h = new TagihanH2h;
                    $tagihanH2h->nis = $siswa->nis;
                    $tagihanH2h->nama = $siswa->nama;
                    $tagihanH2h->jenjang = $siswa->jenjang;
                    $tagihanH2h->kelas = $siswa->kelas;
                    $tagihanH2h->ref = 2;
                    $tagihanH2h->biaya_id = implode(",", $tagihan);
                    $tagihanH2h->nominal = $totalTagihan;
                    // $tagihanH2h->dibayar = 0;
                    $tagihanH2h->status = 1;
                    $tagihanH2h->save();
                } else {
                    return response()->json([
                        "status" => "error",
                        "info" => "Tagihan sebelumnya belum dibayar"
                    ]);
                }
            } else {
                return response()->json([
                    "status" => "error",
                    "info" => "Tagihan tidak ditemukan"
                ]);
            }



            return response()->json([
                "status" => "success",
                "info" => "Tagihan BSI berhasil dibuat",
                "data" => $tagihanH2h
            ]);
            // 
        } else {
            return response()->json([
                "status" => "error",
                "info" => "Maaf NIS dan PIN tidak cocok"
            ]);
        }
    }
    public function buatTabungan($nis, $pin, $uang)
    {


        // $tagihan = explode("-",$tagihanReq);
        // cek NISN dan PIN
        if ($uang < 1) {
            return response()->json([
                "status" => "error",
                "info" => "Uang Harus di isi"
            ]);
        }

        $siswa = Siswa::where("nis", $nis)->where("pin", $pin)->first();
        // Tess
        if ($siswa) {
            $idTabungan = DB::table('jenis_biayas')->where('nama', 'TABUNGAN')->first();
            // return DB::table('jenis_biayas')->get();
            $cek = TagihanH2h::where("nis", $siswa->nis)->first();
            if (!$cek) {
                $tagihanH2h = new TagihanH2h;
                $tagihanH2h->nis = $siswa->nis;
                $tagihanH2h->nama = $siswa->nama;
                $tagihanH2h->jenjang = $siswa->jenjang;
                $tagihanH2h->kelas = $siswa->kelas;
                $tagihanH2h->ref = 2;
                $tagihanH2h->biaya_id = $idTabungan->id;
                $tagihanH2h->nominal = $uang;
                // $tagihanH2h->dibayar = 0;
                $tagihanH2h->status = 1;
                $tagihanH2h->type = 4;
                $tagihanH2h->save();
            } else {
                return response()->json([
                    "status" => false,
                    "errDesc" => "Tagihan sebelumnya belum dibayar"
                ]);
            }



            return response()->json([
                "status" => true,
                "info" => "Tagihan BSI berhasil dibuat",
                "data" => $tagihanH2h
            ]);
            // 
        } else {
            return response()->json([
                "status" => false,
                "errDesc" => "Maaf NIS dan PIN tidak cocok"
            ]);
        }
    }

    public function batalkanTagihan($nis, $pin, $id)
    {
        // cek NISN dan PIN

        $siswa = Siswa::where("nis", $nis)->where("pin", $pin)->first();
        if ($siswa) {
            $batal = TagihanH2h::find($id);
            if ($batal) {
                $batal->delete();
                return response()->json([
                    "status" => "success",
                    "info" => "Tagihan BSI berhasil di batalkan"
                ]);
            } else {
                return response()->json([
                    "status" => "error",
                    "info" => "Tagihan tidak ditemukan"
                ]);
            }

            // 
        } else {
            return response()->json([
                "status" => "error",
                "info" => "Maaf NIS dan PIN tidak cocok"
            ]);
        }
    }

    public function DataTagihan($nis, $pin)
    {
        $siswa = Siswa::where("nis", $nis)->where("pin", $pin)->first();
        if ($siswa) {
            $tagihan = TagihanH2h::where("nis", $nis)->where('status', 1)->orderBy('created_at', 'DESC')->first();

            if ($tagihan) {
                $items = [];
                if ($tagihan->biaya_id == 0) {
                    array_push($items, [
                        "id" => 1, // ID Tagihan
                        "nama" => "Top-Up (Menabung)", // Nama Jenis Biaya
                        "nominal" => $tagihan->nominal, // Nama Jenis
                    ]);
                } else {
                    foreach (explode(",", $tagihan->biaya_id) as $ta_id) {
                        $isTabungan = DB::table('jenis_biayas')->where('id', $ta_id)->where('nama', 'TABUNGAN')->first();
                        if ($isTabungan) {
                            array_push($items, [
                                "id" => $isTabungan->id,
                                "nama" => $isTabungan->nama, // Nama Jenis Biaya
                                "nominal" => $tagihan->nominal // Jumlah
                            ]);
                        } else {
                            $item = BiayaSiswa::find($ta_id);

                            array_push($items, [
                                "id" => $item->id,
                                "nama" => $item->nama, // Nama Jenis Biaya
                                "nominal" => $item->nominal - $item->dibayar  // Jumlah
                            ]);
                        }
                    }
                }


                $tagihan->items = $items;


                return response()->json([
                    "status" => "success",
                    "info" => "Tagihan ada",
                    "data" => $tagihan
                ]);
            } else {
                return response()->json([
                    "status" => "null",
                    "info" => "Tagihan tidak ditemukan"
                ]);
            }
        } else {
            return response()->json([
                "status" => "error",
                "info" => "Maaf NIS dan PIN tidak cocok"
            ]);
        }
    }
    // API BRIS
    public function DataTagihanH2h($nis, $apikey)
    {



        $key = '0PrCdBKfOTpgo1Zhqw6N3v9LbnAIJXHzGukDxt2e';
        if ($apikey !== $key) {
            return  response()->json([
                "response" => [
                    "code" => "99"
                ]
            ]);
        }


        $tagihanH2h1 = TagihanH2h::where("nis", $nis)->first();


        if (!$tagihanH2h1) {
            // Generate Tagihan H2h
            $siswa = Siswa::where("nis", $nis)->first();
            if ($siswa) {

                $tagihanId =  BiayaSiswa::where("nis", $siswa->nis)
                    ->whereRaw("dibayar != nominal")
                    ->pluck("id")->toArray();

                $this->buatTagihan($siswa->nis, $siswa->pin, implode("-", $tagihanId));
            }
        }

        $tagihanH2h = TagihanH2h::where("nis", $nis)->first();


        if ($tagihanH2h) {
            $data = [
                "nomorPembayaran" => $tagihanH2h->nis, // NISN
                "idTagihan" =>  $tagihanH2h->id, // ID Tagihan H2H
                "nomorInduk" =>  $tagihanH2h->nis, // NISN
                "nama" =>  $tagihanH2h->nama, // Nama SIswa
                "jenjang" => $tagihanH2h->jenjang, // JENJANG
                "kelas" =>  $tagihanH2h->kelas, // KELAS
                "totalNominal" => $tagihanH2h->nominal, // Nominal
            ];


            $items = [];

            foreach (explode(",", $tagihanH2h->biaya_id) as $ta_id) {
                $isTabungan = DB::table('jenis_biayas')->where('id', $ta_id)->where('nama', 'TABUNGAN')->first();
                if ($isTabungan) {
                    array_push($items, [
                        "idItem" => $isTabungan->id, // ID Tagihan
                        "namaItem" => $isTabungan->nama, // Nama Jenis Biaya
                        "nominal" => $tagihanH2h->nominal // Jumlah
                    ]);
                } else {
                    $item = BiayaSiswa::find($ta_id);

                    array_push($items, [
                        "idItem" => $item->id, // ID Tagihan
                        "namaItem" => $item->nama, // Nama Jenis Biaya
                        "nominal" => $item->nominal - $item->dibayar  // Jumlah
                    ]);
                }
            }

            $items = [
                "item" => $items
            ];
            return  response()->json([
                "response" => [
                    "code" => "00",
                    "data" => $data,
                    "details" => $items
                ]
            ]);
        } else {

            return  response()->json([
                "response" => [
                    "code" => "88",
                    "info" => "Tagihan tidak ditemukan"
                ]
            ]);
        }
    }

    public function apiUpdateH2h($nis, $noRef, $idTagihan)
    {

        $err = [
            "response" => [
                "code" => "99",
                "info" => "Tagihan sudah dibayarkan"
            ]
        ];

        $tagihanH2h = TagihanH2h::find($idTagihan);


        if (!$tagihanH2h) {
            return response()->json($err);
        }

        $biayaId = [];

        // Update Jurnal
        foreach (explode(",", $tagihanH2h->biaya_id) as $ta_id) {
            $tabungan = JenisBiaya::where('id', $ta_id)->where('nama', 'TABUNGAN')->first();

            if ($tabungan) {
                $saldoAsal = 0;
                $saldoAkhir = Jurnal::where('nis', $nis)->where(function ($q) {
                    $q->where('nama_biaya', 'TABUNGAN')->orWhere('biaya_id', NULL);
                })->orderBy('created_at', 'DESC')->first();
                if ($saldoAkhir) {
                    $saldoAsal = $saldoAkhir->saldo_akhir;
                }
                // return response()->json($saldoAsal + $tagihanH2h->nominal, 200);
                $new = new Jurnal;
                $new->nis = $tagihanH2h->nis;
                $new->kode_trx =  "B-" . $noRef;
                $new->ref = 2;
                $new->jumlah = $tagihanH2h->nominal;
                $new->tgl_bayar = date("Y-m-d", strtotime(now()));
                $new->nama_biaya = $tabungan->nama;
                $new->id_jenis_biaya = $tabungan->id;
                $new->type = 1;
                $new->saldo_akhir = $saldoAsal + $tagihanH2h->nominal;
                $new->save();

                array_push($biayaId, $new->id);
            } else {
                $item = BiayaSiswa::find($ta_id);

                if ($item) {
                    $new = new Jurnal;
                    $new->nis = $tagihanH2h->nis;
                    $new->kode_trx =  "B-" . $noRef;
                    $new->ref = 2;
                    $new->jumlah = $item->nominal - $item->dibayar;
                    $new->tgl_bayar = date("Y-m-d", strtotime(now()));
                    $new->biaya_id = $item->id;
                    $new->nama_biaya = $item->nama;
                    $new->id_jenis_biaya = $item->jenis_biaya_id;
                    $new->type = 1;
                    $new->save();

                    array_push($biayaId, $new->id);

                    $item->dibayar = $item->nominal;
                    $item->save();
                } else {
                    return response()->json($err);
                }
            }
        }


        $tbs = new TotalBayarSiswa;
        $tbs->kode_trx = "B-" . $noRef;
        $tbs->nis =  $tagihanH2h->nis;
        $tbs->ref = 2;
        $tbs->biaya_id = implode(",", $biayaId);
        $tbs->jumlah =  $tagihanH2h->nominal;
        // 
        $tbs->id_tagihan_h2h =  $tagihanH2h->id;
        // 
        $tbs->tgl_bayar = date("Y-m-d", strtotime(now()));
        $tbs->save();

        $res = [
            "response" => [
                "code" => "00",
                "data" => [
                    "nomorPembayaran" => $tagihanH2h->nis, // NISN
                    "idTagihan" =>  $tagihanH2h->id // ID Tagihan H2H
                ]
            ]
        ];

        $tagihanH2h->delete();

        return  response()->json($res);
    }

    public function apiReversalH2h($nis, $noRef, $idTagihan)
    {

        $err = [
            "response" => [
                "code" => "99"
            ]
        ];


        $Total = TotalBayarSiswa::where("id_tagihan_h2h", $idTagihan)->first();

        if (!$Total) {
            return response()->json($err);
        }
        $jurnal = explode(",", $Total->biaya_id);
        foreach ($jurnal as $j) {
            $jj = Jurnal::find($j);

            $updateBiaya = BiayaSiswa::find($jj->biaya_id);
            $updateBiaya->dibayar = $updateBiaya->dibayar - $jj->jumlah;
            $updateBiaya->update();

            $jj->delete();
        }
        $Total->delete();

        $res = [
            "response" => [
                "code" => "00",
                "data" => [
                    "nomorPembayaran" => $Total->nis, // NISN
                    "idTagihan" =>  $idTagihan // ID Tagihan H2H
                ]
            ]
        ];
        return  response()->json($res);
    }

    public function DataTagihanWA(Request $req, $apikey)
    {



        $skip = 0;
        if ($req->page > 1) {
            $skip = ($req->page * 100) - 100;
        }

        // return "test";

        $key = '$2y$06$QWA';
        if ($apikey !== $key) {
            return  response()->json([
                "response" => [
                    "code" => "99"
                ]
            ]);
        }

        $siswaall = Siswa::where("status", 1)
            ->where("no_hp", '>', 0)
            ->skip($skip)
            ->take(300)
            ->get();

        $res = [];

        foreach ($siswaall as $siswa) {

            $tagihan = BiayaSiswa::where("nis", $siswa->nis)->selectRaw('sum(nominal) - sum(dibayar) as tagihan')->first();

            if ($tagihan->tagihan > 0) {
                array_push($res, $data = [
                    "nomorInduk" => $siswa->nis, // NISN
                    "nama" => $siswa->nama, // Nama SIswa
                    "no_hp" => $siswa->no_hp, // HP
                    "jenjang" => $siswa->jenjang, // JENJANG
                    "totalNominal" => number_format($tagihan->tagihan, 0, ",", "."), // Nominal
                ]);
            }
        }


        return response()->json([
            "response" => [
                "code" => "1",
                "data" => $res
            ]
        ]);
    }
}
