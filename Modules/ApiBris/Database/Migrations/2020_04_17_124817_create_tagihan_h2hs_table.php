<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagihanH2hsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tagihan_h2hs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("nis",100);
            $table->string("nama");
            $table->string("jenjang");
            $table->string("kelas");
            $table->integer("ref")->default(2);
            $table->string("biaya_id");
            $table->integer("nominal");
            $table->integer("status")->default(1); // 1 Di Tagihkan , 2 Dibatalkan
            $table->date("tgl_bayar")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tagihan_h2hs');
    }
}
