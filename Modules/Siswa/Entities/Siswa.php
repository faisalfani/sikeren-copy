<?php

namespace Modules\Siswa\Entities;

// use Jenssegers\Mongodb\Eloquent\Model;
use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $fillable = [
        'nis',
        'nisn',
        'nipd',
        'nama',
        'kelas',
        'id_kelas',
        'jk',
        'alamat_ortu','no_hp','jenjang','pin','du_dk','baru_lama','laundry'];
}
