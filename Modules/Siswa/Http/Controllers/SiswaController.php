<?php

namespace Modules\Siswa\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\Keuangan\Entities\Jurnal;
use Modules\Keuangan\Entities\BiayaSiswa;
use Modules\Keuangan\Entities\TotalBayarSiswa;

use Modules\Siswa\Entities\Siswa;
use Modules\Master\Entities\Kelas;
use Validator;
use Excel;
use App\Imports\SiswaImport;
use yajra\Datatables\Datatables;

use Illuminate\Support\Facades\Cache;

class SiswaController extends Controller
{
    public function authMiddleware(Request $request) {
        return $request->user();
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function json(){
        $siswa = Siswa::all();
         return DataTables::of($siswa)->toJson();
    }
 
    public function edit_json($id){
        return json_encode(Siswa::find($id));
    }
    public function index(Request $req)
    {
        // return "A";
        if($req->where){
            // return $req->where;
            $siswa = Siswa::
                where("nama","like","%".$req->where."%")
            ->paginate(10);

        }else{
            $siswa = Siswa::paginate(10);
        }
        if ($req->ajax()) {
            // return "ASD";
            $view = view('siswa::siswa.data',compact('siswa'))->render();
            return response()->json(['html'=>$view]); 
        }
        
        return view('siswa::siswa.index',compact("siswa"));

    }


    public function loadData(Request $req){
        $limit = $req->limit ? $req->limit : 10;
        if ($req->id > 0) {
            //info($req->id);
            info('clicked');
            $data = Siswa::
                    where('id','<',$req->id)
                    ->orderBy('id','DESC')
                    ->where(function($q) use($req) {
                        if ($req->cari) {
                            $q->where('nama','LIKE', '%'.$req->cari.'%')
                            ->orWhere('kelas','LIKE', '%'.$req->cari.'%')
                            ->orWhere('nis','LIKE', '%'.$req->cari.'%');
                        }
                    })
                    ->limit($limit)
                    ->get();
        } else {
            $data = Siswa::limit($limit)->orderBy('id', 'DESC')
                    ->where(function($q) use($req) {
                        if ($req->cari) {
                            $q->where('nama','LIKE', '%'.$req->cari.'%')
                            ->orWhere('kelas','LIKE', '%'.$req->cari.'%')
                            ->orWhere('nis','LIKE', '%'.$req->cari.'%');
                        }
                    })->get();
        }

        if ($req->cari) {
            $jumlah_siswa = Siswa::where('nama','LIKE', '%'.$req->cari.'%')
                            ->orWhere('kelas','LIKE', '%'.$req->cari.'%')
                            ->orWhere('nis','LIKE', '%'.$req->cari.'%')->count();
        } else {
            $jumlah_siswa = Siswa::count();
        }
        $load_siswa =  $data->count() > 0 ? ($req->ke * 10) + $data->count() : $jumlah_siswa;
        
        $output = '';
        $last_id = '';


        if (!$data->isEmpty()) {
            foreach ($data as $key => $row) {
                // Kebutuhan Nomor
                $no = $key + 1;
                $nomor =  $req->ke == "0" ? $no : ($req->ke * 10) + $no;


                // Kebutuhan Nama
                $jk = "";
                if($row->jk == 'L'){
                    $jk =  '<b><i class="icon-man"></i></b>';
                }else{
                    $jk = '<b><i class="icon-woman"></i></b>';
                }
                $beasiswa = ($row->beasiswa == 0) ? '<small class="label bg-secondary">Non Beasiswa</small>' : '<small class="label bg-primary">Beasiswa</small>';

                // Kebutuhan Kelas
                $tk = "";
                if($row->jenjang == 'MTS'){
                      $tk = '<span class="label bg-primary">MTS</span>';
                  }else{
                      $tk = '<span class="label bg-grey">MA</span>';
                  }

                // Kebutuhan Status
                $dudk = ($row->du_dk == 0) ? '<span class="label bg-green-800 my-1">DU</span>' : '<span class="label bg-green-400">DK</span>';
                $status = ($row->status == 1) ? '<span class="label bg-danger-800 mr-2 my-1">Aktif</span>' : '<span class="label bg-danger-400 mr-2">Tidak Aktif</span>';
                $laundry = ($row->laundry == 0) ? '<span class="label bg-blue-400 my-1">Tidak Laundry</span>' : '<span class="label bg-blue-800">Laundry</span>';


                $output .= '
                <tr>
                    <td>'.$nomor.'</td>
                    <td>
                        '. $row->nis .'<br><small>PIN: <div class="p-1 text-white bg-danger text-center"> '.$row->pin.' </div></small>
                    </td>
                    <td>
                        '.$row->nama.' ('.$jk.')<br>'.$beasiswa.'
                    </td>
                    <td>'.$row->no_hp.'</td>
                    <td>'.$row->id_kelas.'</td>
                    <td>
                        '.$tk.'<br><a href="/tagihan-kelas/'.$row->id_kelas.'">'.$row->kelas.'</a>'.'
                    </td>
                    <td>
                        '.$status.'<br>'.$laundry.'
                    </td>
                    <td>
                        <button type="button" class="btn bg-orange-600 btn-labeled btn-sm" onclick="editData('.$row->id.')" ><b><i class="icon-pencil"></i></b>Edit
                        </button>
                        <a class="btn bg-blue-800 btn-labeled btn-sm" href="/input-cash-cicil?nis='.$row->nis.'">
                            Tagihan <b><i class="icon-coin-dollar"></i></b>
                        </a>
                        <a class="btn btn-danger text-white btn-sm" onclick="hapusSiswa('.$row->id.')">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                  </tr>
                ';
                $last_id = $row->id ;
            }
            $output .= '
       <tr id="load_more">
            <td colspan="8">
                <div class="text-center mb-2">
                    Menampilkan <b>'.$load_siswa.'</b> dari <b>'. $jumlah_siswa.'</b> Data Siswa
                </div>
                <button type="button" name="load_more_button" class="btn btn-success form-control" data-id="' . $last_id . '" id="load_more_button">Load More</button>
            </td>
       </tr>
       ';
        } else {
            $output .= '
            <tr id="load_more">
                <td colspan="8">
                    <div class="text-center mb-2">
                        Menampilkan <b>'.$load_siswa.'</b> dari <b>'. $jumlah_siswa.'</b> Data Siswa
                    </div>
                    <div name="load_more_button" class="btn btn-info form-control">No Data Found</div>
                </td>
            </tr>
       ';
        }
        $response = str_replace("<head/>","",$output);
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('siswa::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $req)
    {
        // dd($req);
            $validator = Validator::make($req->all(), [
                'file-import'  => 'required|mimes:xlsx,xls,cvs'
            ]);

            if ($validator->fails()) {
                dd($validator);
                // return back()->with('errors', ['File Tidak Boleh Kosong']);
            }

             
            
            $file = $req->file('file-import'); //GET FILE
            // Excel::import(new SiswaImport, $file); //IMPORT FILE 
            Excel::import(new SiswaImport, request()->file('file-import'));
            
            // $data = Excel::import(new SiswaImport, request()->file('file-import'));
        
            return back()->with('success', 'Data Berhasil di import!');
            // dd($data);

            }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('siswa::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $req, $id)
    {
        Cache::pull('siswas');        

        $k = Kelas::find($req->idkelas);
        $kelas = $k->nama;
        
        $jenjang = $k->jenjang_id;

        if($jenjang == 1){
            $jenjang = 'MTS';
        }else{
            $jenjang = 'MA';
        }

        $siswa = Siswa::find($id);
        
        $siswa->id_kelas = $req->idkelas;
        $siswa->jenjang = $jenjang;
        $siswa->kelas = $kelas;

        if($req->no_hp !== null){
            $siswa->no_hp = $req->no_hp;
        }
        if($req->pin !== null){
            $siswa->pin = $req->pin;
        }
        if($req->status !== null){
            $siswa->status = $req->status;
        }

        
        if($req->du_dk !== null){
            $siswa->du_dk = $req->du_dk;
        }
        if($req->beasiswa !== null){
            $siswa->beasiswa = $req->beasiswa;
        }
        if($req->laundry !== null){
            $siswa->laundry = (int)$req->laundry;
        }
        if($req->baru_lama !== null){
            $siswa->baru_lama = (int)$req->baru_lama;
        }
        if($req->jk !== null){
            $siswa->jk = $req->jk;
        }
        if($req->nama !== null){
            $siswa->nama = $req->nama;
        }

        if($req->nisn !== null && $siswa->nisn !== $req->nisn){
            if($siswa->nisn !== $req->nisn){
            

                $cek = Siswa::find($req->nisn);
    
                if($cek){
                    return "NISN ".$req->nisn . " Telah digunakan Oleh ". $cek->nama;
                }else{
                   
    
    
                    $UBiayaSiswa = BiayaSiswa::
                        where("nis",$siswa->nisn)
                        ->update([
                            'nis' => $req->nisn,
                        ]);
    
                    $TotalBayarSiswa = TotalBayarSiswa::
                        where("nis",$siswa->nisn)
                        ->update([
                            'nis' => $req->nisn,
                        ]);
    
                        
                    $UJurnal = Jurnal::
                        where("nis",$siswa->nisn)
                        ->update([
                            'nis' => $req->nisn,
                        ]);
    
    
                    $siswa->nisn = $req->nisn;
                    $siswa->nis = $req->nisn;
                    $siswa->update();
    
                    return "Success";
                }
    
            }
        }

      

        $siswa->update();
        return "Success";

      

    }

    public function tambah(Request $req){
  
        // return $req;

        $k = Kelas::find($req->idkelas);
        $kelas = $k->nama;
        $jenjang = $k->jenjang_id;
        
        if($jenjang == 1){
            $jenjang = 'MTS';
        }else{
            $jenjang = 'MA';
        }


        $cek = Siswa::where("nis",$req->nis)->first();

        if(!$cek){
            $siswa = new Siswa;
            $siswa->nis = $req->nisn;
            $siswa->nipd = $req->nipd;
            $siswa->nama = $req->nama;
            $siswa->alamat_ortu = $req->alamat;
            $siswa->nisn = $req->nisn;
            $siswa->id_kelas = $req->idkelas;
            $siswa->jenjang = $jenjang;
            $siswa->kelas = $kelas;
            $siswa->no_hp = $req->no_hp;
            $siswa->pin = $req->pin;
            $siswa->du_dk = $req->du_dk;
            $siswa->jk = $req->jk;
            $siswa->laundry = (int)$req->laundry;
            $siswa->beasiswa = $req->beasiswa;
            $siswa->baru_lama = (int)$req->baru_lama;
            $siswa->save();
            return back()->with("success","Berhasil");
        }else{
            return back()->with("err","Nis Sudah digunakan");
        }
        
     
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        $siswa = Siswa::find($id);
        $siswa->save();
        return "success";
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function hapus($id)
    {
        $delete = Siswa::find($id)->delete();
        if($delete){
            return "Success";
        }else{
            return "Error";
        }
    }
}
