<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group(function() {
    Route::resource('/siswa', 'SiswaController');
    Route::get('/siswa/json/data','SiswaController@json');
    Route::get('/siswa/edit-data/{id}','SiswaController@edit_json');
    Route::post('/siswa/edit/{id}','SiswaController@edit');
    Route::post("/tambah-siswa","SiswaController@tambah");
    Route::get("/hapus-siswa/{id}","SiswaController@hapus");
    Route::post('/load-data-siswa', "SiswaController@loadData")->name('load-data-siswa');
});
