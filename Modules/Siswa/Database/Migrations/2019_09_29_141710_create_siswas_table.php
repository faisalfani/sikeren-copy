<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nis')->unique();
            $table->string('avatar')->nullable();
            $table->string('nipd')->nullable();
            $table->string('nama');
            $table->string('kelas')->nullable();
            $table->integer('id_kelas')->nullable();
            $table->string('nisn')->nullable();
            $table->char('jk',100)->nullable();
            $table->string('alamat_ortu')->nullable();
            $table->string('no_hp')->nullable();
            $table->string('jenjang')->nullable();
            $table->integer('status')->nullable();
            $table->string('pin',100)->nullable();
            $table->boolean('baru_lama')->nullable();
            $table->boolean('du_dk')->nullable();
            $table->boolean('laundry')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswas');
    }
}
