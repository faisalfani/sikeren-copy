@extends('layouts.app')

@section('title')
    Siswa
@endsection
@section('css-after')
    <!-- Select2 -->
    <link rel="stylesheet"
        href="../../plugins/select2/css/select2.min.css">
    <link rel="stylesheet"
        href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <!-- DataTables -->
    <link rel="stylesheet"
        href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-success">
                    <div class="card-body">


                        <!-- Button trigger modal -->
                        <div class="row">

                            <div class="col-md-12 mb-2">

                                <button type="button"
                                    class="btn bg-green-900 btn-labeled btn-sm"
                                    onclick="ImportSiswa()"><b><i class="icon-plus3"></i></b>Import Data
                                </button>

                                <button type="button"
                                    class="btn bg-info btn-labeled  btn-sm"
                                    onclick="tambahSiswa()"><b><i class="icon-plus3"></i></b>Tambah Siswa
                                </button>

                                <a href="/template_siswa.xlsx"
                                    class="btn bg-pink btn-labeled btn-sm float-right mr-2"><b><i class="icon-download"></i></b>Download Template
                                    (Excel)</a>
                            </div>
                        </div>

                        <div class="list-group pt-4">
                            <div class="w-100">
                                @include('comp.notif')
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    {{-- <div class="alert alert-warning p-2 text-center m-2">
                Setelah memasukan kata yang ingin dicari pada kolom Cari, tekan <strong>Enter</strong> untuk memfilter data.
              </div> --}}
                                    {{-- <form action="/siswa"> --}}
                                    <div class="form-group mx-2">
                                        <input id="formCari"
                                            type="text"
                                            class="form-control"
                                            placeholder="Masukan Nama atau NIS kemudian klik enter untuk mulai mencari!"
                                            name="cari"
                                            onEnter="alert('enter')">
                                    </div>
                                    {{-- </form> --}}
                                    <div class="table-responsive">
                                        <table id="data-table-siswa"
                                            class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    {{-- <th>Tkt</th> --}}
                                                    <th>NISN</th>
                                                    {{-- <th>PIN</th> --}}
                                                    <th>Nama</th>

                                                    <th>No Hp</th>
                                                    <th>Id Kelas</th>
                                                    <th>Kelas</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="loadData">
                                            </tbody>

                                        </table>
                                    </div>



                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <form method="POST"
        action="/siswa"
        enctype="multipart/form-data">
        @csrf
        <!-- Modal -->
        <div class="modal fade"
            id="modalForm"
            tabindex="-1"
            role="dialog"
            aria-labelledby="modalFormTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-md modal-dialog-scrollable"
                role="document">
                <div class="modal-content text-dark">
                    <div class="modal-header">
                        <h5>Import Siswa</h5>
                        <button type="button"
                            class="close"
                            data-dismiss="modal"
                            aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">


                        <div class="form-group row">
                            <label for="file"
                                class="col-md-12">{{ __('File') }}</label>

                            <div class="col-md-12">
                                <input placeholder="File Excel"
                                    type="file"
                                    class="form-control"
                                    name="file-import"
                                    required
                                    autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                            class="btn btn-secondary"
                            data-dismiss="modal"> <i class="fa fa-times"></i></button>
                        <button id="btn-create"
                            type="submit"
                            class="btn bg-primary-c"> <i class="fa fa-plus"></i> {{ __('Import Siswa') }}</button>
                        <button id="btn-update"
                            type="button"
                            class="btn bg-primary-c"><i class="fa fa-save"></i> {{ __('Simpan') }}</button>
                    </div>
                </div>
            </div>
        </div>

    </form>




    <div class="modal fade"
        id="modal-edit">
        <div class="modal-dialog"
            role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"
                        id="exampleModalLabel">Edit Siswa</h5>
                    <button type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"
                    id="edit-siswa">

                    <div class="modal-footer">
                        <button type="button"
                            class="btn btn-secondary"
                            data-dismiss="modal">Keluar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="modal fade"
        id="tambahSiswa">
        <div class="modal-dialog"
            role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"
                        id="exampleModalLabel">Tambah Siswa</h5>
                    <button type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form action="/tambah-siswa"
                        method="POST">
                        @csrf
                        <div class="col-md-12">
                            <label> NIS/NIPD </label>
                            <input required
                                name="nipd"
                                type="text"
                                class="form-control"
                                placeholder="Masukkan NIS">
                        </div>
                        <div class="col-md-12">
                            <label> NISN </label>
                            <input required
                                name="nisn"
                                type="text"
                                class="form-control"
                                placeholder="Masukkan NISN">
                        </div>
                        <div class="col-md-12">
                            <label> Nama </label>
                            <input required
                                name="nama"
                                type="text"
                                class="form-control"
                                placeholder="Masukkan Nama">
                        </div>

                        <div class="col-md-12">
                            <label> No Hp</label>
                            <input required
                                name="no_hp"
                                type="text"
                                class="form-control"
                                placeholder="Masukkan No HP">
                        </div>
                        <div class="col-md-12">
                            <label> Alamat Lengkap </label>
                            <textarea name="alamat"
                                class="form-control"
                                cols="30"
                                rows="3"></textarea>
                        </div>
                        <div class="col-md-12">
                            <label> Kelas </label>
                            <select required
                                id="t_data_k"
                                name="idkelas"
                                class="form-control">
                                <option value="">Pilih Kelas</option>
                                @foreach (Modules\Master\Entities\Kelas::get() as $item)
                                    <option value="{{ $item->id }}">
                                        {{ $item->nama }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label> PIN </label>
                            <input required
                                name="pin"
                                type="text"
                                value="111111"
                                class="form-control"
                                placeholder="Masukkan PIN (6 digit)">
                        </div>
                        <div class="col-md-12">
                            <label> Jenis Kelamin </label>
                            {{-- <select required  name="jk" class="form-control">
                  <option value="L">Laki Laki</option>
                  <option value="P">Perempuan</option>
                </select> --}}
                            <div class="form-check">
                                <input class="form-check-input"
                                    type="radio"
                                    name="jk"
                                    id="gridRadios1"
                                    value="L"
                                    checked>
                                <label class="form-check-label"
                                    for="gridRadios1">
                                    Laki-Laki
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input"
                                    type="radio"
                                    name="jk"
                                    id="gridRadios2"
                                    value="P">
                                <label class="form-check-label"
                                    for="gridRadios2">
                                    Perempuan
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <label> Beasiswa </label>
                            <div class="form-check">
                                <input class="form-check-input"
                                    type="radio"
                                    name="beasiswa"
                                    id="beasiswa_1"
                                    value="0"
                                    checked>
                                <label class="form-check-label"
                                    for="beasiswa_1">
                                    Tidak
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input"
                                    type="radio"
                                    name="beasiswa"
                                    id="beasiswa_2"
                                    value="1">
                                <label class="form-check-label"
                                    for="beasiswa_2">
                                    Iya
                                </label>
                            </div>

                        </div>
                        {{-- <div class="col-md-12">
              <label >Tipe Siswa</label>
                <div class="form-check">
                  <input class="form-check-input" type="radio"
                  name="baru_lama" id="baru_lama_1" 
                  value="1" checked>
                  <label class="form-check-label" for="baru_lama_1">
                  Siswa Baru (Intensif)
                  </label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" 
                  type="radio" 
                  name="baru_lama" 
                  id="baru_lama_2" value="0">
                  <label class="form-check-label" for="baru_lama_2">
                    Siswa Lama (Qudama)
                  </label>
                </div>
              </div> --}}
                        <div class="col-md-12">
                            <label>Laundry</label>
                            {{-- <select required name="laundry" class="form-control" >
                <option  value="1"> Laundry </option>
                <option  value="0"> Tidak Laundry </option>
              </select> --}}
                            <div class="form-check">
                                <input class="form-check-input"
                                    type="radio"
                                    name="laundry"
                                    id="laundry_1"
                                    value="1"
                                    checked>
                                <label class="form-check-label"
                                    for="laundry_1">
                                    Laundry
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input"
                                    type="radio"
                                    name="laundry"
                                    id="laundry_2"
                                    value="0">
                                <label class="form-check-label"
                                    for="laundry_2">
                                    Tidak Laundry
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12 pt-4">
                            <input type="hidden"
                                name="du_dk"
                                id="dudk_1"
                                value="0">
                            <input type="hidden"
                                name="baru_lama"
                                id="baru_lama_1"
                                value="1">
                            <button type="submit"
                                class="btn btn-block btn-primary">
                                <i class="fa fa-save"></i> Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js-after')
    <script src="../../plugins/select2/js/select2.full.min.js"></script>
    <!-- DataTables -->
    <script src="../../plugins/datatables/jquery.dataTables.js"></script>
    <script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        function loadMoreData(id = 0, ke = 0) {
            let cari = document.getElementById('formCari').value;
            axios.post('{{ route('load-data-siswa') }}', {
                    id: id,
                    _token: '{{ csrf_token() }}',
                    ke: ke,
                    cari: cari
                })
                .then(res => {
                    // console.log(res.data);
                    $('#load_more').remove();
                    var dataFromServer = res.data
                    let dataReplcae = dataFromServer.replace("<head/>", "");
                    console.log("dataReplcae", dataReplcae);
                    $('#loadData').append(dataReplcae);
                })
                .catch(err => {
                    console.log(err);
                })
        }
        let kebrp = 0;
        loadMoreData(0, kebrp);
        $(document).on('click', '#load_more_button', function() {
            var id = $(this).data('id');
            kebrp = kebrp + 1;
            console.log(kebrp);
            $('#load_more_button').html('<b>Loading...</b>');
            loadMoreData(id, kebrp);
        });

        var formCari = document.getElementById("formCari");

        // Execute a function when the user releases a key on the keyboard
        formCari.addEventListener("keyup", function(event) {
            // Number 13 is the "Enter" key on the keyboard
            if (event.keyCode === 13) {
                // Cancel the default action, if needed
                event.preventDefault();
                // Trigger the button element with a click
                // alert('cliked')
                document.getElementById("loadData").innerHTML = "";
                kebrp = 0;
                loadMoreData(0, kebrp);
            }
        });
    </script>

    <script type="text/javascript">
        //Initialize Select2 Elements
        $('#t_data_k').select2({
            theme: 'bootstrap4'
        })
        // line ini dihapus
        // $(document).ready(function() { hapus
        //     var datatables1  = $('#data-table-siswa').DataTable({
        //         searchDelay: 4000,
        //         initComplete: function () {
        //             // Apply the search
        //             // this.api().columns().every( function () {
        //                 var that = this;
        //                 $('div.dataTables_filter input').unbind();
        //                 $( 'div.dataTables_filter input' ).bind( 'keyup change clear', function (e) {
        //                     if ( datatables1.search() !== this.value ) {
        //                       if (e.keyCode === 13) {
        //                         datatables1
        //                             .search( this.value )
        //                             .draw();
        //                       }

        //                     }
        //                 } );
        //             // } );
        //         },
        //         language : {
        //           'url': '/indonesian.json'
        //         },
        //         processing: true,
        //         serverSide: true,
        //         ajax: '/siswa/json/data',
        //         columns: [
        //             { data: 'id', name: 'id' },
        //             // { data:
        //             //         function(d){
        //             //             if(d.jenjang == 'MTS'){
        //             //                 return `<span class="label bg-primary">MTS</span>`;
        //             //             }else{
        //             //                 return `<span class="label bg-grey">MA</span>`;
        //             //             }
        //             //         }
        //             //     , name: 'jenjang' },
        //             { data: function (p) {
        //                     return p.nis +'<br>'+ `<small>PIN: <div class="p-1 text-white bg-danger text-center"> ${p.pin} </div></small>`;
        //                 },
        //                     name: 'nis'
        //             },
        //             // { data: 'pin' },
        //             { data:
        //                     function (n) {
        //                         if(n.jk == 'L'){
        //                             var jk =  `<b><i class="icon-man"></i></b>`;
        //                         }else{
        //                             var jk = `<b><i class="icon-woman"></i></b>`;
        //                         }

        //                       var dudk = (n.du_dk == 0) ? 'DU' : 'DK';
        //                       var baru_lama = (n.baru_lama == 1) ? '<small class="label bg-secondary">Intensif</small>' : '<small class="label bg-primary">Qudama</small>';

        //                       return n.nama+ ' ('+jk+')'+ `<br>${baru_lama}`;
        //                     },
        //                     name: 'nama'
        //             },


        //             // { data: 'alamat_ortu' },
        //             { data: 'no_hp', name: 'no_hp' },
        //             { data: 'id_kelas', name: 'id_kelas' },
        //             { data: 
        //               function (d){
        //                   if(d.jenjang == 'MTS'){
        //                       var tk = `<span class="label bg-primary">MTS</span>`;
        //                   }else{
        //                       var tk = `<span class="label bg-grey">MA</span>`;
        //                   }
        //                 return tk+`<br><a href="/tagihan-kelas/${d.id_kelas}">${d.kelas}</a>`;
        //               }
        //             , name: 'kelas' },
        //             { data: 
        //                   function(d){


        //                   var dudk = (d.du_dk == 0) ? '<span class="label bg-green-800 my-1">DU</span>' : '<span class="label bg-green-400">DK</span>';
        //                   var status = (d.status == 1) ? '<span class="label bg-danger-800 mr-2 my-1">Aktif</span>' : '<span class="label bg-danger-400 mr-2">Tidak Aktif</span>';
        //                   var laundry = (d.laundry == 0) ? '<span class="label bg-blue-400 my-1">Tidak Laundry</span>' : '<span class="label bg-blue-800">Laundry</span>';



        //                       return `${status} ${dudk}`
        //                       + `<br>${laundry}`
        //                       ;

        //                 }
        //             , name: 'status' },
        //             { data: function(d){


        //               return `

    //                   <button type="button" class="btn bg-orange-600 btn-labeled btn-sm" onclick="editData(${d.id})" ><b><i class="icon-pencil"></i></b>Edit
    //                   </button>
    //                   <a class="btn bg-blue-800 btn-labeled btn-sm" href="/input-cash-cicil?nis=${d.nis}">
    //                     Tagihan <b><i class="icon-coin-dollar"></i></b>
    //                   </a>
    //                   <a class="btn btn-danger text-white btn-sm mt-2" onclick="hapusSiswa(${d.id})">
    //                     <i class="fa fa-trash"></i>
    //                   </a>
    //               `;
        //             }, 

        //             name: 'id' }
        //         ]
        //     });    

        // } );




        function ImportSiswa() {
            $("#file").val("");
            $("#modalForm").modal("show");
            $("#btn-create").show();
            $("#btn-update").hide();
        }

        function tambahSiswa() {
            $("#tambahSiswa").modal("show");
        }

        function hapusSiswa(id) {
            swal({
                    title: "Apakah Kamu Yakin?",
                    text: "Pastikan data yang akan dihapus sudah valid",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Hapus",
                    closeOnConfirm: false
                },
                function() {

                    $.get("/hapus-siswa/" + id)
                        .done((res) => {
                            location.reload()
                        });

                });
        }


        function editData(id) {



            $.get("/siswa/edit-data/" + id)
                .done((dsiswa) => {


                    $("#modal-edit").modal("show");

                    var datakelas = ``;

                    var ds = JSON.parse(dsiswa);


                    $.get("/data-kelas/select2/" + ds.jenjang)
                        .done((dkelas) => {

                            var dkk = JSON.parse(dkelas);
                            // console.log(dkk);
                            // console.log(ds);

                            for (let i = 0; i < dkk.length; i++) {
                                var selectedKelas = '';
                                if (ds.id_kelas == dkk[i].id) {
                                    selectedKelas = 'selected';
                                }
                                datakelas +=
                                    `<option ${selectedKelas} value="${dkk[i].id}">${dkk[i].nama}</option>`;
                            }

                            var selectA = "";
                            var selectB = "";



                            if (ds.status == 1) {
                                selectA = "selected";
                                selectB = "";
                            } else {
                                selectA = "";
                                selectB = "selected";
                            }

                            var selectBeasiswa = "";
                            var selectNonBeasiswa = "";
                            var selectBL1 = "";
                            var selectBL2 = "";
                            var selectLaundry1 = "";
                            var selectLaundry2 = "";

                            if (ds.beasiswa == 1) {
                                selectBeasiswa = "selected";
                                selectNonBeasiswa = "";
                            } else {
                                selectBeasiswa = "";
                                selectNonBeasiswa = "selected";
                            }

                            if (ds.baru_lama == 1) {
                                selectBL1 = "selected";
                                selectBL2 = "";
                            } else {
                                selectBL1 = "";
                                selectBL2 = "selected";
                            }


                            if (ds.laundry == 1) {
                                selectLaundry1 = "selected";
                                selectLaundry2 = "";
                            } else {
                                selectLaundry1 = "";
                                selectLaundry2 = "selected";
                            }

                            let jkA = '';
                            let jkB = '';

                            if (ds.jk == 'L') {
                                jkA = "selected";
                                jkB = "";
                            } else {
                                jkA = "";
                                jkB = "selected";
                            }


                            var html = `
      <div class="row">
      
        <div class="form-group col-md-6">
          <label>Nama </label>
          <input id="e_nama" value="${ds.nama}" type="text" class="form-control">
        </div>
        <div class="form-group col-md-6">
          <label>NISN </label>
          <input id="e_nisn" value="${ds.nis}" type="text" class="form-control">
        </div>
        <div class="form-group col-md-6">
          <label>No HP </label>
          <input id="no_hp" value="${ds.no_hp}" type="text" class="form-control">
        </div>

        <div class="form-group col-md-6 float-left">
          <label>Kelas</label>
          <select value="${ds.id_kelas}"  id="id_kelas" class="form-control">
            ${datakelas}
          </select>
        </div>

        <div class="form-group col-md-6 float-left">
          <label>PIN</label>
          <input id="pin" value="${ds.pin}" type="number" class="form-control">
        </div>

        <div class="form-group col-md-6 float-left">
          <label >Status </label>
          <select id="status" class="form-control">
            <option ${selectB} value="0"> Tidak Aktif </option>
            <option ${selectA} value="1"> Aktif </option>
          </select>
        </div>

        <div class="form-group col-md-6 float-left">
          <label > Gender </label>
          <select id="gender" class="form-control">
            <option ${jkA} value="L"> Laki-Laki </option>
            <option ${jkB} value="P"> Perempuan </option>
          </select>
        </div>
        
        
        <div class="form-group col-md-6 float-left">
          <label >Laundry</label>
          <select id="laundry" class="form-control">
            <option ${selectLaundry1} value="1"> Laundry </option>
            <option ${selectLaundry2} value="0"> Tidak Laundry </option>
          </select>
        </div>
        
       
        <div class="form-group col-md-6 float-left">
          <label> Beasiswa </label>
          <select id="beasiswa" class="form-control" value="${ds.beasiswa}">
            <option ${selectBeasiswa} value="1"> Beasiswa </option>
            <option ${selectNonBeasiswa} value="0"> Non Beasiswa </option>
          </select>
        </div>
        <div class="form-group col-md-12">
          <button id="btn-simpan-edit" class="btn btn-primary btn-block">
          <i class="fa fa-save"></i>
          Simpan</button>
        </div>
      </div>
    `;

                            $("#edit-siswa").html(html);

                            //Initialize Select2 Elements
                            $('#id_kelas').select2({
                                theme: 'bootstrap4'
                            })





                            $("#pin").on("keyup", function() {
                                // if(pin.toString().length !== 6){

                                var vall = $("#pin").val().substring(0, 6);
                                $("#pin").val(vall);

                            })
                            $('#btn-simpan-edit').click(() => {

                                var idkelas = $("#id_kelas").val();
                                var no_hp = $("#no_hp").val();
                                var status = $("#status").val();
                                var pin = $("#pin").val();
                                var du_dk = $("#du_dk").val();
                                var laundry = $("#laundry").val();
                                var baru_lama = $("#baru_lama").val();
                                var jk = $("#gender").val();
                                var nama = $("#e_nama").val();
                                var nisn = $("#e_nisn").val();
                                var beasiswa = $("#beasiswa").val();


                                var data = {
                                    _token: '{{ csrf_token() }}',
                                    idkelas: idkelas,
                                    no_hp: no_hp,
                                    status: status,
                                    pin: pin,
                                    du_dk: du_dk,
                                    laundry: laundry,
                                    baru_lama: baru_lama,
                                    jk: jk,
                                    nama: nama,
                                    nisn: nisn,
                                    beasiswa: beasiswa,
                                }

                                // console.log("data", data);

                                $.post("/siswa/edit/" + id,
                                        data)
                                    .done((res) => {
                                        document.getElementById("loadData").innerHTML = "";
                                        kebrp = 0;
                                        console.log("data", data);
                                        loadMoreData(0, kebrp);;
                                        $("#modal-edit").modal("hide");

                                    })

                                // }

                            })
                        });
                });
        }

        function deleteMenu(id) {
            console.log(id);
            $.ajax({
                type: "DELETE",
                url: '/menu/' + id,
                data: {
                    _method: 'delete',
                    _token: '{{ csrf_token() }}'
                },
                success: function(data) {
                    $("#menu-" + id).remove();
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        }
    </script>
@endsection
