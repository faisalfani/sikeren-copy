@extends('layouts.app')

@section('title')
    Jenjang
@endsection
@section('css-after')
    <!-- DataTables -->
    <link rel="stylesheet"
        href="../../../plugins/datatables-bs4/css/dataTables.bootstrap4.css">


    <!-- Select2 -->
    <link rel="stylesheet"
        href="../../plugins/select2/css/select2.min.css">
    <link rel="stylesheet"
        href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
@endsection

@section('content')
    <div class="container">
        <div class="row ">

            <div class="col-md-12 bg-white p-4">
                <h4 class="text-center pb-4">

                    Siswa Lulus


                    <a target="_BLANK"
                        href="/laporan-tagihan-siswa-lulus?filter=tagihan"
                        class="btn btn-primary btn-labeled float-right mr-2">
                        <b><i class="fa fa-print"></i> </b> Cetak Tagihan </a>


                </h4>
                <div class="table-responsive">
                    <table id="data-table"
                        class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NISN</th>
                                <th>Nama</th>
                                <th>Jenjang</th>
                                <th>Tagihan</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>



        <div class="modal fade"
            id="modal-edit">
            <div class="modal-dialog"
                role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"
                            id="exampleModalLabel">Lanjut ke MA</h5>
                        <button type="button"
                            class="close"
                            data-dismiss="modal"
                            aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"
                        id="edit-siswa">

                        <div class="modal-footer">
                            <button type="button"
                                class="btn btn-secondary"
                                data-dismiss="modal">Keluar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>





    </div>
    {{-- End Modal Tambah --}}
@endsection

@section('js-after')
    <script src="../../plugins/select2/js/select2.full.min.js"></script>
    <!-- DataTables -->
    <script src="../../../plugins/datatables/jquery.dataTables.js"></script>
    <script src="../../../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

    <script>
        var datatables1 = $("#data-table").DataTable({
            language: {
                'url': '/indonesian.json'
            },
            processing: true,
            serverSide: true,
            ajax: '/siswa-lulus/data',
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: function(p) {
                        return p.nis + '<br>' + `<span class="label bg-danger">PIN: ${p.pin}</span>`;
                    },
                    name: 'nis'
                },
                {
                    data: function(n) {
                        if (n.jk == 'L') {
                            var jk = `<b><i class="icon-man"></i></b>`;
                        } else {
                            var jk = `<b><i class="icon-woman"></i></b>`;
                        }


                        return n.nama + ' (' + jk + ')' +
                            `<br><span class="label bg-danger">${n.no_hp}</span>`;
                    },
                    name: 'nama'
                },
                {
                    data: function(d) {
                        if (d.jenjang == 'MTS') {
                            var tk = `<span class="label bg-success w-100 text-center">MTS</span>`;
                        } else {
                            var tk = `<span class="label bg-orange-600 w-100 text-center">MA</span>`;
                        }
                        return tk;
                    },
                    name: 'jenjang'
                },
                {
                    data: 'tagihan',
                    name: 'tagihan'
                },

                {
                    data: function(d) {

                        return `
            
                <button type="button" class="btn bg-orange-600 btn-labeled btn-sm" onclick="editData(${d.id})" ><b><i class="icon-pencil"></i></b>
                Lanjut ke MA
                </button>
                <a class="btn bg-blue-800 btn-labeled btn-sm" href="/input-cash-cicil?nis=${d.nis}">
                   Tagihan <b><i class="icon-coin-dollar"></i></b>
                </a>
            `;
                    },

                    name: 'id'
                }
            ]
        });



        function editData(id) {


            $.get("/siswa/edit-data/" + id)
                .done((dsiswa) => {


                    $("#modal-edit").modal("show");

                    var datakelas = ``;

                    var ds = JSON.parse(dsiswa);


                    $.get("/data-kelas/select2/MA")
                        .done((dkelas) => {

                            var dkk = JSON.parse(dkelas);

                            for (let i = 0; i < dkk.length; i++) {
                                datakelas += `<option value="${dkk[i].id}">${dkk[i].nama}</option>`;
                            }



                            var html = `
      <div class="row">
      
        <div class="form-group col-md-6">
          <label>Nama </label>
          <input disabled id="e_nama" value="${ds.nama}" type="text" class="form-control">
        </div>
   

        <div class="form-group col-md-6 float-left">
          <label>Kelas</label>
          <select  id="id_kelas" class="form-control">
            ${datakelas}
          </select>
        </div>


        <div class="form-group col-md-6 float-left">
          <label >Status </label>
          <select id="status" class="form-control">
            <option value="0"> Tidak Aktif </option>
            <option value="1" selected> Aktif </option>
          </select>
        </div>
        
        <div class="form-group col-md-6 float-left">
          <label >Tipe Siswa</label>
          <select id="baru_lama" class="form-control">
            <optionvalue="1"> Siswa Baru (intensif)</optionvalue=>
            <option  value="0" selected> Siswa Lama (qudama)</option>
          </select>
        </div>
        <div class="form-group col-md-12">
          <button id="btn-simpan-edit" class="btn btn-primary btn-block">
          <i class="fa fa-save"></i>
          Simpan</button>
        </div>
      </div>
    `;

                            $("#edit-siswa").html(html);


                            //Initialize Select2 Elements
                            $('#id_kelas').select2({
                                theme: 'bootstrap4'
                            })

                            $('#btn-simpan-edit').click(() => {

                                var idkelas = $("#id_kelas").val();
                                var status = $("#status").val();
                                var baru_lama = $("#baru_lama").val();



                                var data = {
                                    _token: '{{ csrf_token() }}',
                                    idkelas: idkelas,
                                    status: status,
                                    baru_lama: baru_lama,
                                }

                                $.post("/siswa/edit/" + id,
                                        data)
                                    .done((res) => {
                                        console.log("berhasil");
                                        datatables1.ajax.reload();
                                        $("#modal-edit").modal("hide");

                                    })

                            })
                        });
                });
        }
    </script>
@endsection
