@extends('layouts.app')

@section('title')
      Jenjang
@endsection
@section('css-after') 
 <!-- DataTables -->
 <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endsection

@section('content')

<div class="container">
        <div class="row " >
       
            <div class="col-md-8 offset-lg-2 bg-white p-4">
                    <form action="/jenjang" method="POST">
                        @csrf 
                        <div class="row">
                            <div class="form-group col-md-9">
                                <label> Nama</label>
                                <input id="id_data" type="hidden">   
                                <input id="nama" placeholder="Nama" type="text" name="nama" class="form-control">   
                            </div>
                            <div class="form-group col-md-3">      
                                <label class="d-none d-lg-block">-</label>                                                          
                                <a href="#" onclick="editData()" id="btn-edit" class="btn bg-primary-c">
                                    <i class="fa fa-save"></i>
                                    Simpan</a> 
                                <a id="btn-tambah-e" onclick="location.reload()" href="#" class="btn bg-primary-c"><i class="fa fa-plus"></i></a>
                                <button id="btn-tambah" type="submit" class="btn btn-block bg-primary-c">
                                      <i class="fa fa-save"></i>
                                      Tambah</button> 
                            </div>
                        </div>
                    </form>
              <div class="table-responsive">
              <table id="data-table" class="table table-bordered table-hover">
                <thead>  
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
            </div>
        </div>





    </div>
{{-- End Modal Tambah --}}
@endsection

@section('js-after')

<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script type="text/javascript">

$('#data-table').DataTable({
      language : {
            'url': '/indonesian.json'
      },
      processing: true,
      serverSide: true,
      ajax: '/jenjang/json/data',
      columns: [
          { data: 'id', name: 'id' },
          { data: 'nama', name: 'nama' },
          { data: function(d){
                var act = `
                
                <a onclick="edit('${d.id}','${d.nama}')" href="#" class="btn bg-primary-c btn-sm float-right mr-2"><i class="fa fa-edit"></i></a>
                
                `;                
                return act;
            }, 
            name: 'id'
          },
      ]
  });

$("#btn-edit").hide();
$("#btn-tambah-e").hide();
function edit(id,nm){
  // console.log(id);
  $("#nama").val(nm);
  $("#id_data").val(id);
  $("#btn-edit").show();
  $("#btn-tambah-e").show();
  $("#btn-tambah").hide();
}

function editData(){
    $.post("/jenjang/"+$("#id_data").val(),{
      _token : "{{ csrf_token() }}",
      _method: "put",
      nama : $("#nama").val()
    })
    .done((res) => {
      location.reload()
      // console.log(res)
    })
  }

  function deleteData(id){
    $.post("/jenjang/"+id,{
      _token : "{{ csrf_token() }}",
      _method: "delete"
    })
    .done((res) => {
      location.reload()
      // console.log(res)
    })
  }
</script>

@endsection