@extends('layouts.app')

@section('title')
      Jenjang
@endsection
@section('css-after') 
 <!-- DataTables -->
 <link rel="stylesheet" href="../../../plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endsection

@section('content')

<div class="container">
        <div class="row " >
       
            <div class="col-md-8 offset-lg-2 bg-white p-4">
              <h4 class="text-center pb-4">

               Tagihan Siswa (<b>{{ $kelas->nama }}</b>)

                <a target="_BLANK" href="/laporan-perkelas/{{$kelas->id}}" class="btn btn-primary btn-labeled float-right mr-2">
                  <b><i class="fa fa-print"></i> </b> Tagihan</a>

              </h4>
              <div class="table-responsive">
              <table id="data-table" class="table table-bordered table-hover">
                <thead>  
                  <tr>
                    <th>No</th>
                    <th>NIS/NIPD</th>
                    <th>Nama</th>
                    <th>Tagihan</th>
                    <th>Detail</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($res as $key => $s)
                    <tr>
                      <td> {{ $key+1 }}</td>
                      <td> {{ $s['nis'] }}</td>
                      <td> {{ $s['nama'] }} (
                        @php
                      if($s['jk'] == 'L'){
                          echo "<b><i class='icon-man'></i></b>";
                      }else{
                          echo "<b><i class='icon-woman'></i></b>";
                      }
                      @endphp
                      )</td>
                      <td> {{ rupiah($s['tagihan']) }}</td>
                      <td>

                          <a class="btn bg-blue-800 btn-labeled btn-sm" href="/input-cash-cicil?nis={{$s['nis']}}">
                              Tagihan <b><i class="icon-coin-dollar"></i></b>
                          </a>
                      </td>
                    </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            </div>
        </div>





    </div>
{{-- End Modal Tambah --}}
@endsection

@section('js-after')

<!-- DataTables -->
<script src="../../../plugins/datatables/jquery.dataTables.js"></script>
<script src="../../../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script>
$("#data-table").DataTable({
  "pageLength": 50,
  language : {
        'url': '/indonesian.json'
      }
});
</script>
@endsection