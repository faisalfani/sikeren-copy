@extends('layouts.app')

@section('title')
      Jenjang
@endsection
@section('css-after') 

<!-- Select2 -->
<link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
 
 <!-- DataTables -->
 <link rel="stylesheet" href="../../../plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="../../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
 @endsection

@section('content')

<form id="form-naik-kelas" action="/naik-kelas" method="POST" >
  @csrf
<div class="container">
        <div class="row">
            <div class="col-md-8 bg-white p-4">
              <div class="table-responsive">
              <table id="data-table" class="table table-bordered table-hover">
                <thead>  
                  <tr>
                    <th>No</th>
                    <th>NISN</th>
                    <th>Nama</th>
                    <th>Pilih</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($siswa as $key => $s)
                    <tr>
                      
                      <td> {{ $key+1 }}</td>
                      <td> {{ $s['nis'] }}</td>
                      <td> {{ $s['nama'] }} (
                        @php
                      if($s['jk'] == 'L'){
                          echo "<b><i class='icon-man'></i></b>";
                      }else{
                          echo "<b><i class='icon-woman'></i></b>";
                      }
                      @endphp
                      )</td>
                      <td>
                        <div class="icheck-success d-inline">
                        <input name="siswa[]" value="{{$s['nis']}}" type="checkbox" 
                        id="pilih-siswa-{{$key+1}}">
                          <label for="pilih-siswa-{{$key+1}}"></label>
                        </div>
                    </td>
                    </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            </div>


            <div class="col-md-4 bg-white p-4">

              <div class="stiky">
              <h4 class="text-center">
                <b >{{ $kelas->nama}}</b>
              </h4>
              
              <div class="row">
                <div class="col-md-12">
                  <button id="btn-cek-semua" type="button" class="btn btn-block btn-primary btn-labeled float-left mb-3 mr-3">
                    <b><i class="fa fa-check"></i> </b> Pilih Semua
                  </button>
    
                  <button id="btn-uncek-semua" type="button" class="btn btn-block btn-danger btn-labeled float-left mb-3">
                    <b><i class="fa fa-times"></i> </b> Batalkan Semua
                  </button>
                  <br>
                  <br>
                  <div class="form-group">
                    <label>Pilih Kelas Tujuan </label>
                    <select id="kelas_tujuan" class="form-control select2" name="kelas_tujuan" required>
                      <option value="">Pilih Kelas Tujuan</option>
                    </select>
                  </div>
                  
                  <button type="submit" class="btn btn-block btn-primary">
                      Simpan
                  </button>
                  
              </div>
            </div>

          </div>

        </div>
    </div>
    
  </form>
{{-- End Modal Tambah --}}
@endsection

@section('js-after')



<script src="../../plugins/select2/js/select2.full.min.js"></script>

<!-- DataTables -->
<script src="../../../plugins/datatables/jquery.dataTables.js"></script>
<script src="../../../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<style>
  .stiky{
    position: sticky;
    top: 100px;
  }
</style>
<script>


$('#kelas_tujuan').select2({
      theme: 'bootstrap4'
})

$.get("/data-naik-kelas/{{$kelas->id}}")
  .done(function(r){
    let res = JSON.parse(r);
    let html = '<option value="">Pilih Kelas Tujuan</option>';
    let cekTingkat = '{{$kelas->tingkat}}';
    if(cekTingkat == 9 || cekTingkat == 12){
      html = '<option selected value="0">Luluskan</option>';
    }
    for (let i = 0; i < res.length; i++) {
      html += `<option value="${res[i].id}">${res[i].nama}</option>`;
    }
    $("#kelas_tujuan").html(html);

   
  })
$("#btn-cek-semua").click(function(){

  let jumlah = "{{ $siswa->count() }}";

  for (let i = 1; i < jumlah+1; i++) {
    $("#pilih-siswa-"+i).attr('checked', true )
  }

})

$("#btn-uncek-semua").click(function(){

let jumlah = "{{ $siswa->count() }}";

for (let i = 1; i < jumlah+1; i++) {
    $("#pilih-siswa-"+i).removeAttr('checked');
}

})


$("#data-table").DataTable({
  paging: false,
  language : {
        'url': '/indonesian.json'
      }
});
</script>
@endsection