@extends('layouts.app')

@section('title')
      Jenjang
@endsection
@section('css-after') 
 <!-- DataTables -->
 <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endsection

@section('content')

<div class="container">
        <div class="row " >
          <div class="col-md-12 bg-white p-3">
            <h4 class="text-center">Managemen Kelas

              <button type="button" class=" float-right btn btn-labeled btn-primary" data-toggle="modal" data-target="#m-form">
                <b><i class="fa fa-edit"></i></b>
                Tambah
              </button>
              
            </h4>
          </div>

          <div class="modal fade" id="m-form" tabindex="-1" role="dialog" aria-labelledby="m-form" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-body">
                  <h2>Tambah Kelas</h2>
                  <form action="/kelas" method="POST">
                    @csrf 
                    <div class="row">
                      
                      <div class="form-group col-md-12">
                          <label> Nama</label>
                          <input id="id_data" type="hidden">   
                          <input required id="nama" placeholder="Nama" type="text" name="nama" class="form-control">   
                      </div>
                  
                      <div class="form-group col-md-12">
                          <label> Jenjang </label>
                          <select required name="jenjang" id="jenjang" class="form-control">
                            <option value="">Pilih Jenjang</option>
                            
                                @foreach(Modules\Master\Entities\Jenjang::all() as $data)
                                  <option 
                                  value="{{ $data->id }}">{{ $data->nama }}</option>
                                @endforeach

                          </select>
                    </div>
                    <div class="form-group col-md-12">
                      <label> Tingkat </label>
                      <select required name="tingkat" id="tingkat" class="form-control">
                        <option value="">Pilih Tingkat</option>
                        
                            @for($i = 7; $i < 13; $i++)
                              <option 
                              value="{{ $i }}">{{ $i }}</option>
                            @endfor

                      </select>
                </div>

                      
                  
                      <div class="form-group col-md-12">                                                             
                          <a href="#" onclick="editData()" id="btn-edit" class="btn bg-primary-c">
                              <i class="fa fa-save"></i>
                              Simpan</a> 
                          <a id="btn-tambah-e" onclick="location.reload()" href="#" class="btn bg-primary-c"><i class="fa fa-plus"></i></a>
                          <button id="btn-tambah" type="submit" class="btn btn-block bg-primary-c">
                                <i class="fa fa-save"></i>
                                Tambah</button> 
                      </div>
                  </div>
              </form>
          
          </div>
          </div>
        </div>
        </div>


            <div class="col-md-12 bg-white p-4">
              <div class="table-responsive">
              <table id="data-table" class="table table-bordered table-hover">
                <thead>  
                  <tr>
                    <th>Id</th>
                    <th>Nama</th>
                    <th>Jenjang</th>
                    <th>Tingkat</th>
                    <th>Jml Siswa</th>
                    <th class="text-right">Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
            </div>
        </div>





    </div>
{{-- End Modal Tambah --}}
@endsection

@section('js-after')

<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script type="text/javascript">

$('#data-table').DataTable({
      order : [[ 3, "desc" ]],
      "pageLength": 40,
      language : {
        'url': '/indonesian.json'
      },
      processing: true,
      serverSide: true,
      ajax: '/kelas/json/data',
      columns: [
          { data: 'id', name: 'id' },
          { data: 'nama', name: 'nama' },
          { data: 'jenjang', name: 'jenjang' },
          { data: 'tingkat', name: 'tingkat' },
          { data: 'jumlah_siswa', name: 'jumlah_siswa' },
          { data: function(d){
                var act = `
                <a href="#" onclick="deleteData(${d.id})" class="btn btn-danger btn-sm float-right"><i class="fa fa-trash"></i></a>
                <a onclick="edit('${d.id}','${d.nama}','${d.jenjang_id}','${d.tingkat}')" href="#" class="btn bg-primary-c btn-sm float-right mr-2"><i class="fa fa-edit"></i></a>
                <a href="/tagihan-kelas/${d.id}" class="btn bg-primary-c btn-sm float-right mr-2"><i class="fa fa-eye"></i> Lihat Tagihan </a>
                <a href="/naik-kelas/${d.id}" class="btn btn-warning btn-sm float-right mr-2"><i class="fa fa-arrow-up"></i> Naik Kelas </a>
                `;                
                return act;
            }, 
            name: 'id'
          },
      ]
  });

$("#btn-edit").hide();
$("#btn-tambah-e").hide();
function edit(id,nm,jenjang_id,tingkat){
  $("#m-form").modal("show")
  // console.log(id);
  $("#nama").val(nm);
  $("#jenjang").val(jenjang_id);
  $("#tingkat").val(tingkat);
  $("#id_data").val(id);
  $("#btn-edit").show();
  $("#btn-tambah-e").show();
  $("#btn-tambah").hide();
}

function editData(){
    $.post("/kelas/"+$("#id_data").val(),{
      _token : "{{ csrf_token() }}",
      _method: "put",
      nama : $("#nama").val(),
      tingkat:  $("#tingkat").val(),
      jenjang:  $("#jenjang").val()
    })
    .done((res) => {
      location.reload()
    })
  }

  function deleteData(id){
      swal({
        title: "Apakah Kamu Yakin?",
        text: "Pastikan data yang akan dihapus sudah valid",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Hapus",
        closeOnConfirm: false
      },
      function(){

        $.get("/hapus-kelas/"+id)
          .done((res) => {
            location.reload()
          });

      });
  }
</script>

@endsection