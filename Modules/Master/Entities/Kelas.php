<?php

namespace Modules\Master\Entities;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = 'kelas';
    protected $fillable = ['nama','jenjang_id'];
    public $timestamps = false;
}
