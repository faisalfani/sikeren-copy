<?php

namespace Modules\Master\Entities;

use Illuminate\Database\Eloquent\Model;

class Jenjang extends Model
{
    protected $table = 'jenjang';
    protected $fillable = ['nama'];
    public $timestamps = false;
}
