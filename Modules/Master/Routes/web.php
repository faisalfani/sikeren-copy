<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('auth')->group(function() {
    Route::resource('/jenjang', 'JenjangController');
    Route::get('/jenjang/json/data', 'JenjangController@json');

    // Route::resource('/jurusan', 'JurusanController');
    Route::resource('/kelas', 'KelasController');
    Route::get('/kelas/json/data', 'KelasController@json');
    Route::get('/data-kelas/select2/{tingkat}','KelasController@json_select2');
    Route::get('/data-kelas/select2','KelasController@json_select2_all');
    Route::get('/hapus-kelas/{id}','KelasController@HapusKelas');


    Route::get("/siswa-lulus/data",'KelasController@json_siswa_lulus');

    Route::get("/siswa-lulus",'KelasController@tagihanSiswaLulus');
    Route::get("/tagihan-kelas/{id}",'KelasController@tagihan');

    Route::get("/naik-kelas/{id}",'KelasController@NaikKelas');
    Route::get("/data-naik-kelas/{id}",'KelasController@DataNaikKelas');
    Route::post("/naik-kelas",'KelasController@UpdateNaikKelas');
});
