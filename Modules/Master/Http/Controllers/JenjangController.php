<?php

namespace Modules\Master\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use yajra\Datatables\Datatables;
use DB;
use Modules\Master\Entities\Jenjang;
class JenjangController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function json(){
        $data = DB::table("jenjang")
        ->get();
        return Datatables::of($data)->make(true);
    }
    
    public function index()
    {
        return view('master::jenjang');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('master::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $req)
    {
        //
        // dd($req);
        DB::table("jenjang")->insert([
            "nama" => $req->nama
            ]);

        return back()->with("success","berhasil");
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('master::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('master::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $req, $id)
    {
        $edit = Jenjang::find($id);
        $edit->nama = $req->nama;
        $edit->update();
        return $edit;
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $d = Jenjang::find($id)->delete();
        return "success";
    }
}
