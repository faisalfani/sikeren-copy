<?php

namespace Modules\Master\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use yajra\Datatables\Datatables;
use Modules\Master\Entities\Kelas;
use Modules\Master\Entities\Jenjang;
use Modules\Siswa\Entities\Siswa;
use Modules\Keuangan\Entities\BiayaSiswa;
use DB;
use PDF;

use Illuminate\Support\Facades\Cache;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
        
     public function tagihan(Request $req,$id){

        $kelas = Kelas::find($id);
        $siswa = Siswa::where("id_kelas",$id)->get();
        $res = array();
        foreach($siswa as $s){
            $tagihan = BiayaSiswa::where("nis",$s->nis)
                ->where("nominal","!=","dibayar")
                ->get();
                
            $sisa = 0;

            foreach($tagihan as $t){
                $sisa += $t->nominal - $t->dibayar;    
            }

            $data = [
                    "nis" => $s->nis, 
                    "nama" => $s->nama,
                    "jk" => $s->jk,
                    "tagihan" => $sisa,
                ];
            array_push($res,$data);
        }

        return view('master::tagihan-kelas',compact('res','kelas'));
        
     }


     public function json_siswa_lulus(){

        // $siswa = DB::table("siswas")->where("id_kelas",0)->where("status",2)
        // ->select("id","nis","nama","jenjang","jk","pin","no_hp")->get();        

        $seconds = 43200;
        $siswa = Cache::remember('siswas', $seconds, function () {   
            return DB::table("siswas as s")
                ->leftJoin('biaya_siswas as bs', 'bs.nis', 's.nis')
                ->where("id_kelas",0)
                ->where("status",2)
                ->select(
                    "s.id","s.nis","s.nama","s.jenjang","s.jk","s.pin","s.no_hp",
                    DB::raw('SUM((bs.nominal - bs.dibayar)) AS tagihan_nominal'))
                ->groupBy('s.nis')    
                ->orderBy('s.nama', 'asc')             
                ->get();                        
        });    

        $response = Datatables::of($siswa)
            ->addColumn('tagihan', function($s) {
                    
            //         $query  = DB::table("biaya_siswas")
            //             ->where("nis",$s->nis)
            //             ->where("nominal","!=","dibayar")
            //             ->selectRaw("(nominal - dibayar) as sisa")
            //             ->get();
            //         $tagihan = collect($query)->sum("sisa");
                    
                    return rupiah($s->tagihan_nominal);
            })
            ->make(true);        

        return $response;
    }
 
     public function tagihanSiswaLulus(Request $req){

        return view('master::siswa-lulus',compact('req'));
        
     }
     public function json(){
         $data = DB::table("kelas")
            ->join("jenjang","kelas.jenjang_id","jenjang.id")
            ->select("kelas.*","jenjang.nama as jenjang")
            ->get();
        return Datatables::of($data)
            ->addColumn('jumlah_siswa', function($kelas) {
            return Siswa::where("id_kelas",$kelas->id)->count();
        })
        ->toJson();
    }

    public function json_select2($tingkat){

        if($tingkat == 'MTS'){
            $idjen = 1;
        }else{
            $idjen = 2;
        }
        return json_encode(Kelas::where("jenjang_id",$idjen)->get());
    }

    public function json_select2_all(){
        return json_encode(Kelas::all());
    }
    public function index()
    {
        return view('master::kelas');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('master::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $req)
    {
        
        $new = new Kelas;
        $new->nama =  $req->nama;
        $new->tingkat =  $req->tingkat;
        $new->jenjang_id = $req->jenjang;
        $new->save();

        return back()->with("success","berhasil");
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('master::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('master::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $req, $id)
    {
        //

        $new = Kelas::find($id);
        $new->tingkat =  $req->tingkat;
        $new->nama =  $req->nama;
        $new->jenjang_id = $req->jenjang;
        $new->update();

        return "success";
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function HapusKelas($id)
    {
        $hapus = Kelas::find($id);
        $hapus->delete();
        return "success";
    }
    public function NaikKelas($id){
     
            $kelas = Kelas::find($id);
            $siswa = Siswa::where("id_kelas",$id)->get();
            return view('master::naik-kelas',compact('siswa','kelas'));   
    }

    public function DataNaikKelas($id){
        $kelas = Kelas::find($id);
        $res = Kelas::
            where("tingkat",">",$kelas->tingkat)
            ->where("tingkat","<",$kelas->tingkat+2)
            ->get();
       

        return json_encode($res);
    }

    public function UpdateNaikKelas(Request $req){

        if($req->siswa){
            
                foreach($req->siswa as $s){
                    $siswa = Siswa::where("nis",$s)->first();

                    $cekRiwayat = DB::table("riwayat_kelas")
                        ->where("nis",$siswa->nis)
                        ->where("id_kelas",$siswa->id_kelas)
                        ->first();

                    if(!$cekRiwayat){
                        $inputRiwayat = DB::table("riwayat_kelas")
                        ->insert(
                            [
                                'nis' => $siswa->nis, 
                                'id_kelas' => $siswa->id_kelas,
                                'kelas_tujuan' => $req->kelas_tujuan,
                                'tgl_lulus' => now()
                            ]
                        );
                    }


                    if($req->kelas_tujuan !== 0){
                        $kelas = Kelas::find($req->kelas_tujuan);
                    }else{
                        $kelas = '';
                    }
                    
                    if($kelas !== null){
                        $siswa->id_kelas = $kelas->id;
                        $siswa->kelas = $kelas->nama;
    
                        if($kelas->jenjang_id == 1){
                            $j = 'MTS';
                        }else{
                            $j = 'MA';
                        }
                        $siswa->jenjang = $j;
                        $siswa->update();
                    }else{
                        $siswa->id_kelas = 0;
                        $siswa->kelas = '';
                        $siswa->status = 2;
                        $siswa->update();
                    }


                }
        }
        
        return back()->with("status","success");
    }
}
