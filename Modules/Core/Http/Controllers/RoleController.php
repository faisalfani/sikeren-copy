<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $req)
    {
        //
        if($req->where){
            // return $req->where;
            $role = role::
            where("nama","like","%".$req->where."%")
            // ->orWhere("email","like","%".$req->where."%")
            ->paginate(10);

        }else{
            $role = role::paginate(10);
        }
        if ($req->ajax()) {
            $view = view('core::role.data',compact('role'))->render();
            return response()->json(['html'=>$view]); 
        }
        
        return view("core::role.index",compact("role"));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('core::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $req)
    {
        $menu = json_encode($req->menu);
        // dd($menu);
        //
        $role = new role;
       

        $role->nama = $req->nama;
        $role->menu = $menu;
        $role->save();

        return back()->with("success","Hak Akses ".$role->nama." Berhasil dibuat");

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('core::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('core::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $req, $id)
    {
        //
        $role = role::find($id);
        $menu = json_encode($req->menu);

        $role->nama = $req->nama;
        $role->menu = $menu;
        $role->update();

        return "Hak Akses ".$role->nama." Berhasil diedit";
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $role = role::find($id)->delete();
        
    }
}
