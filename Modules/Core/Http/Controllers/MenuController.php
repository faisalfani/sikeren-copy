<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\menu;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $req)
    {
        //
        if($req->where){
            // return $req->where;
            $menu = menu::
                where("nama","like","%".$req->where."%")
            ->paginate(3);

        }else{
            $menu = menu::paginate(3);
        }
        if ($req->ajax()) {
            $view = view('core::menu.data',compact('menu'))->render();
            return response()->json(['html'=>$view]); 
        }
        
        return view('core::menu.index',compact("menu"));

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('core::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $req)
    {

        $menu = new menu;
        $menu->nama = $req->nama;
        $menu->url = $req->url;
        $menu->sub = $req->sub;
        $menu->icon = $req->icon;
        $menu->save();

        return back()->with("success","Menu  ". $menu->nama ." Created successful");
        
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('core::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('core::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $req, $id)
    {
        //
        // return $req;
        
        $menu = menu::find($id);
        $menu->nama = $req->nama;
        $menu->url = $req->url;
        $menu->sub = $req->sub;
        $menu->icon = $req->icon;
        $menu->save();
        return "success";
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function deleteMenu($id)
    {
        $delete = menu::find($id)->delete();

        if($delete){
            return "Success";
        }else{
            return "Error";
        }
        //
    }
}
