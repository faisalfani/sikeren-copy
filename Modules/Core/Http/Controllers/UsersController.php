<?php

namespace Modules\Core\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        //
        if($req->where){
            // return $req->where;
            $users = User::
            where("name","like","%".$req->where."%")
            ->orWhere("email","like","%".$req->where."%")
            ->paginate(10);

        }else{
            $users = User::paginate(10);
        }
        if ($req->ajax()) {
            $view = view('core::users.data',compact('users'))->render();
            return response()->json(['html'=>$view]); 
        }
        
        return view("core::users.index",compact("users"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        //
        $cek = User::
            where("email",$req->email)
            ->first();
        if($cek){
            return back()->with("error","Email ". $req->email ." Telah digunakan");
        }
        $cek2 = User::
            where("username",$req->username)
            ->first();
        if($cek2){
            return back()->with("error","Username @". $req->username ." Telah digunakan");
        }
        // dd($req);
        $user = new User();
        $user->name = $req->name;
        $user->email = $req->email;
        $user->username = $req->username;
        $user->role_id = $req->role_id;       
        $user->password = Hash::make($req->password);
        $user->save();
        
        return back()->with("success","Menambahkan User ". $user->name ." Berhasil");


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return "tess 111111";

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
    //    return $req;?\
        $user = User::find($id);
        $user->name = $req->name;

        if($req->username !== $user->username){
            
            $cek = User::where("username",$req->username)->first();
            if(!$cek){
                $user->username = $req->username;            
            }
        }

        if(Auth::user()->role_id == 1){
            
            $user->role_id = $req->role_id;            
        }
        
        if($req->email){
            $user->email = $req->email;
        }
        
        if($req->password){
            $user->password = Hash::make($req->password);
        }
        $user->update();
        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = User::find($id)->delete();
        if($delete){
            return "Success";
        }else{
            return "Error";
        }
    }
}
