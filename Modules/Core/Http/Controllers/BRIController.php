<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use BriAPI;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Modules\ApiBris\Entities\TagihanH2h;
use Modules\Keuangan\Entities\BiayaSiswa;
use Modules\Keuangan\Entities\Jurnal;
use Modules\Siswa\Entities\Siswa;
use stdClass;

// use Validator;

class BRIController extends Controller
{


    public function buatVA(Request $req){
        // return "A";
        if (!$req->pin || !$req->nis || !$req->tagihan) {
            $pesan = "Kombinasi PIN & NIS anda salah";
            if (!$req->tagihan) $pesan = "Tagihan harus di isi";
            return response()->json([
                "status" => false,
                "pesan" => $pesan
            ], 200);
        }

        $siswa = Siswa::where("nis",$req->nis)->where("pin",$req->pin)->first();

        $tagihan_id = explode('-', $req->tagihan);
        $amount = BiayaSiswa::whereIn("id",$tagihan_id)
                    ->select(DB::raw('SUM(nominal - dibayar) AS tagihan'))
                    ->first();
        if($amount){
               
            $TagihanH2h = TagihanH2h::where("nis",$siswa->nis)->first();
            if(!$TagihanH2h){
                $tagihanH2h = new TagihanH2h;
                $tagihanH2h->nis = $siswa->nis;
                $tagihanH2h->nama = $siswa->nama;
                $tagihanH2h->jenjang = $siswa->jenjang;
                $tagihanH2h->kelas = $siswa->kelas;
                $tagihanH2h->ref = 3;
                $tagihanH2h->biaya_id = implode(",",$tagihan_id);
                $tagihanH2h->nominal = $amount->tagihan;
                // $tagihanH2h->dibayar = 0;
                $tagihanH2h->status = 1;
                $tagihanH2h->type = 2;
                $tagihanH2h->save();
            }else{
                // return response()->json([
                //     "status"=> "error",
                //     "info"=> "Tagihan sebelumnya belum dibayar"
                // ]);
            }
        
        }else{
            return response()->json([
                "status"=> "error",
                "info"=> "Tagihan tidak ditemukan"
            ]);
        }

        // return response()->json($amount, 200, );
        
        $client_id = config('bank-bri.client_id');
        $secret_id = config('bank-bri.client_secret');
        $timestamp = config('bank-bri.timestamp');
        try {
            $token = $this->BRIVAgenerateToken($client_id, $secret_id);
            //code...
        } catch (\Throwable $th) {
            return response()->json([
                "status" => false,
                "pesan" => 'Gagal mendapatkan TOKEN BRI'
            ], 200);
        }

        $institutionCode = config('bank-bri.institution_code');
        $brivaNo = config('bank-bri.briva_no');
        $expiredDate  = Carbon::now()->addMonth()->format('Y-m-d H:i:s');

        $datas = array(
            'institutionCode' => $institutionCode,
            // 'institutionCode' => "J104408A",
            'brivaNo' => $brivaNo,
            // 'brivaNo' => 0,
            // 'custCode' => 0,
            'custCode' => $req->nis,
            // 'nama' => "",
            'nama' => $siswa->nama,
            // 'amount' => "abcd!@",
            'amount' => $amount->tagihan,
            'keterangan' => "Tagihan ".$siswa->nama,
            'expiredDate' => $expiredDate
        );

        $payload = json_encode($datas, true);
        $path = config('bank-bri.briva.store');
        $verb = config('bank-bri.briva.store_verb');

        $base64sign = $this->BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret_id);

        $request_headers = array(
            "Content-Type:"."application/json",
            "Authorization:Bearer " . $token,
            "BRI-Timestamp:" . $timestamp,
            "BRI-Signature:" . $base64sign,
        );

        $urlPost = config('bank-bri.api_url_extra').$path;
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_URL, $urlPost);
        curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, "POST"); 
        curl_setopt($chPost, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);

        $resultPost = curl_exec($chPost);
        $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
        curl_close($chPost);

        // echo "Response Post : ".$resultPost;
        $res = json_decode($resultPost, true);
        if ($res['status'] == false) {
            TagihanH2h::where("nis",$siswa->nis)->delete();
        }

        return response()->json($res, 200);
    
    
    }
    public function buatTabungan(Request $req){
        // return "A";
        if (!$req->pin || !$req->nis || !$req->tabungan) {
            return response()->json([
                "status" => false,
                "pesan" => 'Field Kosong'
            ], 200);
        }

        $siswa = Siswa::where("nis",$req->nis)->where("pin",$req->pin)->first();
        $tagihanH2h = new TagihanH2h;
        $tagihanH2h->nis = $siswa->nis;
        $tagihanH2h->nama = $siswa->nama;
        $tagihanH2h->jenjang = $siswa->jenjang;
        $tagihanH2h->kelas = $siswa->kelas;
        $tagihanH2h->ref = 3;
        $tagihanH2h->biaya_id = 0;
        $tagihanH2h->nominal = $req->tabungan;
        // $tagihanH2h->dibayar = 0;
        $tagihanH2h->status = 1;
        $tagihanH2h->type = 4;
        $tagihanH2h->save();


        // return response()->json($amount, 200, );
        
        $client_id = config('bank-bri.client_id');
        $secret_id = config('bank-bri.client_secret');
        $timestamp = config('bank-bri.timestamp');
        try {
            $token = $this->BRIVAgenerateToken($client_id, $secret_id);
            //code...
        } catch (\Throwable $th) {
            return response()->json([
                "status" => false,
                "pesan" => 'Gagal mendapatkan TOKEN BRI'
            ], 200);
        }

        $institutionCode = config('bank-bri.institution_code');
        $brivaNo = config('bank-bri.briva_no');
        $expiredDate  = Carbon::now()->addMonth()->format('Y-m-d H:i:s');

        $datas = array(
            'institutionCode' => $institutionCode,
            'brivaNo' => $brivaNo,
            'custCode' => $req->nis,
            'nama' => $siswa->nama,
            'amount' => $req->tabungan,
            'keterangan' => "Tabungan ".$siswa->nama,
            'expiredDate' => $expiredDate
        );

        $payload = json_encode($datas, true);
        $path = config('bank-bri.briva.store');
        $verb = config('bank-bri.briva.store_verb');

        $base64sign = $this->BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret_id);

        $request_headers = array(
            "Content-Type:"."application/json",
            "Authorization:Bearer " . $token,
            "BRI-Timestamp:" . $timestamp,
            "BRI-Signature:" . $base64sign,
        );

        $urlPost = config('bank-bri.api_url_extra').$path;
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_URL, $urlPost);
        curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, "POST"); 
        curl_setopt($chPost, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);

        $resultPost = curl_exec($chPost);
        $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
        curl_close($chPost);

        // echo "Response Post : ".$resultPost;
        return json_decode($resultPost, true);
    
    
    }


    public function infoVA($va){

        $client_id = config('bank-bri.client_id');
        $secret_id = config('bank-bri.client_secret');
        $timestamp = config('bank-bri.timestamp');
        $secret = $secret_id;
        $token = $this->BRIVAgenerateToken($client_id, $secret_id);

        $institutionCode = config('bank-bri.institution_code');
        // $institutionCode = "j104444";
        $brivaNo = config('bank-bri.briva_no');
        // $brivaNo = 0;
        $custCode = $va;
        // $custCode = 0;

        $payload = null;
        $path = config('bank-bri.briva.show')."/".$institutionCode."/".$brivaNo."/".$custCode;
        $verb = config('bank-bri.briva.show_verb');
        $base64sign = $this->BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret);

        $request_headers = array(
            "Authorization:Bearer " . $token,
            "BRI-Timestamp:" . $timestamp,
            "BRI-Signature:" . $base64sign,
        );

        $urlPost = config('bank-bri.api_url_extra')
                    .config('bank-bri.briva.show')
                    ."/".$institutionCode."/".$brivaNo."/".$custCode;
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_URL, $urlPost);
        curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, "GET"); 
        curl_setopt($chPost, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
        $resultPost = curl_exec($chPost);
        $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
        curl_close($chPost);

        return json_decode($resultPost, true);
    }

    public function infoStatus($id){
        $tagihan = TagihanH2h::find($id);
        if (!$tagihan) {
            return response()->json(['status' => false, 'pesan' => 'Data Tagihan tidak Ditemukan'], 200);
        }
        $client_id = config('bank-bri.client_id');
        $secret_id = config('bank-bri.client_secret');
        $timestamp = config('bank-bri.timestamp');
        $secret = $secret_id;
        $token = $this->BRIVAgenerateToken($client_id, $secret_id);

        // $institutionCode = "j104408A";
        $institutionCode = config('bank-bri.institution_code');
        $brivaNo = config('bank-bri.briva_no');
        $custCode = $tagihan->nis;
        // $custCode = "22299";

        $payload = null;
        $path = config('bank-bri.briva.status')."/".$institutionCode."/".$brivaNo."/".$custCode;
        $verb = config('bank-bri.briva.status_verb');
        $base64sign = $this->BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret);

        $request_headers = array(
            "Authorization:Bearer " . $token,
            "BRI-Timestamp:" . $timestamp,
            "BRI-Signature:" . $base64sign,
        );

        $urlPost = config('bank-bri.api_url_extra')
                    .config('bank-bri.briva.status')
                    ."/".$institutionCode."/".$brivaNo."/".$custCode;
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_URL, $urlPost);
        curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, "GET"); 
        curl_setopt($chPost, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
        
        $resultPost = curl_exec($chPost);
        $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
        curl_close($chPost);

        $bri = json_decode($resultPost, true);
        // if ($bri['data']['statusBayar'] == 'Y') {
        //     $tagihan->update(['status' => 2]);
        //     foreach(explode(",",$tagihan->biaya_id) as $ta_id){
        //             $item = BiayaSiswa::find($ta_id);
        //             $item->update(['dibayar' => $item->nominal]);
                    
        //         }
        //     return response()->json([
        //          "status" => true,
        //         "pesan" => 'Data berhasil diubah'
        //     ], 200);
        // }

        return response()->json($bri, 200);
    }

    public function updateStatus(Request $req){
        if (!$req->va || !$req->status) {
            return response()->json([
                "status" => false,
                "pesan" => 'Field Kosong'
            ], 200);
        }
        $client_id = config('bank-bri.client_id');
        $secret_id = config('bank-bri.client_secret');
        $timestamp = config('bank-bri.timestamp');
        $secret = $secret_id;
        $token = $this->BRIVAgenerateToken($client_id, $secret_id);

        $institutionCode = config('bank-bri.institution_code');
        // $institutionCode = "J1044082";
        $brivaNo = config('bank-bri.briva_no');
        $custCode = $req->va;
        // $custCode = 0;
        $statusBayar = $req->status;
        // $statusBayar = "";

        $datas = array(
            'institutionCode' => $institutionCode ,
            'brivaNo' => $brivaNo,
            'custCode' => $custCode,
            'statusBayar'=> $statusBayar
        );

        $payload = json_encode($datas, true);
        $path = config('bank-bri.briva.status');
        $verb = "PUT";
        $base64sign = $this->BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret);

        $request_headers = array(
            "Content-Type:"."application/json",
            "Authorization:Bearer " . $token,
            "BRI-Timestamp:" . $timestamp,
            "BRI-Signature:" . $base64sign,
        );

        $urlPost = config('bank-bri.api_url_extra').config('bank-bri.briva.status');
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_URL, $urlPost);
        curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, "PUT"); 
        curl_setopt($chPost, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);

        $resultPost = curl_exec($chPost);
        $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
        curl_close($chPost);

        return json_decode($resultPost, true);

    }

    public function updateVA(Request $req){
        if ( !$req->va || !$req->nama || !$req->amount || !$req->keterangan) {
            return response()->json([
                "status" => false,
                "pesan" => 'Field Kosong'
            ], 200);
        }
        $client_id = config('bank-bri.client_id');
        $secret_id = config('bank-bri.client_secret');
        $timestamp = config('bank-bri.timestamp');
        $secret = $secret_id;
        $token = $this->BRIVAgenerateToken($client_id, $secret_id);

        $institutionCode = config('bank-bri.institution_code');
        // $institutionCode = "J104408A";
        $brivaNo = config('bank-bri.briva_no');
        // $brivaNo = "";

        $datas = array(
            'institutionCode' => $institutionCode ,
            'brivaNo' => $brivaNo,
            'custCode' => $req->va,
            // 'custCode' => "",
            'nama' => $req->nama,
            'amount' => $req->amount,
            'keterangan' => $req->keterangan,
            'expiredDate' =>  Carbon::now()->addMonth()->format('Y-m-d H:i:s')
        );

        $payload = json_encode($datas, true);
        $path = config('bank-bri.briva.update');
        $verb = config('bank-bri.briva.update_verb');
        $base64sign = $this->BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret);

        $request_headers = array(
            "Content-Type:"."application/json",
            "Authorization:Bearer " . $token,
            "BRI-Timestamp:" . $timestamp,
            "BRI-Signature:" . $base64sign,
        );

        $urlPost = config('bank-bri.api_url_extra').$path;
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_URL, $urlPost);
        curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, "PUT"); 
        curl_setopt($chPost, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);

        $resultPost = curl_exec($chPost);
        $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
        curl_close($chPost);

        return json_decode($resultPost, true);
    }

    public function hapusVA(Request $req){
        if ( !$req->id) {
            return response()->json([
                "status" => false,
                "pesan" => 'Field VA Kosong'
            ], 200);
        }
        $tagihan = TagihanH2h::find($req->id);
        // return response()->json($tagihan, 200);
        if(!$tagihan){
            return response()->json([
                "status" => false,
                "pesan" => 'Data Tagihan Tidak ditemukan'
            ], 200);
        }
        $siswa = Siswa::where("nis",$tagihan->nis)->first();


        $client_id = config('bank-bri.client_id');
        $secret_id = config('bank-bri.client_secret');
        $timestamp = config('bank-bri.timestamp');
        $secret = $secret_id;
        $token = $this->BRIVAgenerateToken($client_id, $secret_id);

        $institutionCode = config('bank-bri.institution_code');
        // $institutionCode = "J104408A";
        $brivaNo = config('bank-bri.briva_no');
        $custCode = $siswa->nis;


        $payload = "institutionCode=".$institutionCode."&brivaNo=".$brivaNo."&custCode=".$custCode;
        $path = config('bank-bri.briva.destroy');
        $verb = config('bank-bri.briva.destroy_verb');
        $base64sign = $this->BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret);

        $request_headers = array(
            "Authorization:Bearer " . $token,
            "BRI-Timestamp:" . $timestamp,
            "BRI-Signature:" . $base64sign,
        );

        $urlPost = config('bank-bri.api_url_extra').$path;
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_URL, $urlPost);
        curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chPost, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
        curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
        
        $resultPost = curl_exec($chPost);
        $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
        curl_close($chPost);
        $res = json_decode($resultPost, true);
        if ($res['status'] == true) {
            # code...
            $tagihan->delete();
            return response()->json([
                "status"=> "success",
                "info"=> "Tagihan BRI berhasil di batalkan"
            ]);
        }
        return response()->json($res, 200);
    }
    public function hapusin($nis){
        $client_id = config('bank-bri.client_id');
        $secret_id = config('bank-bri.client_secret');
        $timestamp = config('bank-bri.timestamp');
        $secret = $secret_id;
        $token = $this->BRIVAgenerateToken($client_id, $secret_id);

        $institutionCode = config('bank-bri.institution_code');
        $brivaNo = config('bank-bri.briva_no');
        $custCode = $nis;


        $payload = "institutionCode=".$institutionCode."&brivaNo=".$brivaNo."&custCode=".$custCode;
        $path = config('bank-bri.briva.destroy');
        $verb = config('bank-bri.briva.destroy_verb');
        $base64sign = $this->BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret);

        $request_headers = array(
            "Authorization:Bearer " . $token,
            "BRI-Timestamp:" . $timestamp,
            "BRI-Signature:" . $base64sign,
        );

        $urlPost = config('bank-bri.api_url_extra').$path;
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_URL, $urlPost);
        curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chPost, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
        curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
        
        $resultPost = curl_exec($chPost);
        $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
        curl_close($chPost);
        $tagihan  = TagihanH2h::where('nis', $nis)->delete();
        // $tagihan->delete();
        // return response()->json([
        //     "status"=> "success",
        //     "info"=> "Tagihan BRI berhasil di batalkan"
        // ]);
        return json_decode($resultPost, true);
    }


    public function sejarahVA(Request $req){
        // return response()->json($req, 200);
        if ( !$req->awal || !$req->akhir) {
            return response()->json([
                "status" => false,
                "pesan" => 'Field Kosong'
            ], 200);
        }
        $client_id = config('bank-bri.client_id');
        $secret_id = config('bank-bri.client_secret');
        $timestamp = config('bank-bri.timestamp');
        $secret = $secret_id;
        $token = $this->BRIVAgenerateToken($client_id, $secret_id);

        $institutionCode = config('bank-bri.institution_code');
        $brivaNo = config('bank-bri.briva_no');
        $custCode = $req->va;
        $startDate = $req->awal;
        $endDate = $req->akhir;

        $payload = null;
        $url = config('bank-bri.briva.report');
        $verb = config('bank-bri.briva.report_verb');
        $path = $url.'/'.$institutionCode."/".$brivaNo."/".$startDate."/".$endDate;
        $base64sign = $this->BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret);

        $request_headers = array(
            "Authorization:Bearer " . $token,
            "BRI-Timestamp:" . $timestamp,
            "BRI-Signature:" . $base64sign,
        );

        $urlPost = config('bank-bri.api_url_extra').$url."/".$institutionCode."/".$brivaNo."/".$startDate."/".$endDate;
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_URL, $urlPost);
        curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, "GET"); 
        curl_setopt($chPost, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);

        $resultPost = curl_exec($chPost);
        $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
        curl_close($chPost);

        return json_decode($resultPost, true);
    }

    public function cekStatusPembayaranByReport($idH2h, Request $req){
        $h2h = tagihanH2h::find($idH2h);
        if (!$h2h) {
            return response()->json([
                "status" => false,
                "pesan" => 'Data Tagihan tidak ditemukan'
            ], 200);
        }

        $tgl = date('Ymd', strtotime($h2h->created_at));
        $req->awal = $tgl;
        $req->akhir = $tgl;
        $getReport = $this->sejarahVA($req);

        if ($getReport['status'] === true) {
            $report = collect($getReport['data']);
            $isDibayar = $report->where('custCode', $h2h->nis)->first();
            if ($isDibayar) {
                // $h2h->update(['status' => 2,
                // 'tgl_bayar'=> now()]);
                // foreach(explode(",",$h2h->biaya_id) as $ta_id){
                //         $item = BiayaSiswa::find($ta_id);
                //         $item->update(['dibayar' => $item->nominal]);
                        
                //     }
                return response()->json([
                    "status" => true,
                    "pesan" => 'Tagihan Sudah dibayar',
                    "data" => $isDibayar
                ], 200);
            }
            return response()->json([
                    "status" => false,
                    "pesan" => 'Tagihan Belum dibayar'
                ], 200);
            // return response()->json($isDibayar, 200);
        } 
        return response()->json([
                    "status" => false,
                    "pesan" => 'Tagihan tidak ditemukan'
                ], 200);
        // return response()->json($getReport, 200);

        
    }

    public function sejarahWaktuVA(Request $req){
        if ( !$req->awal || !$req->akhir || !$req->jam_awal || !$req->jam_akhir) {
            return response()->json([
                "status" => false,
                "pesan" => 'Field Kosong'
            ], 200);
        }
        $client_id = config('bank-bri.client_id');
        $secret_id = config('bank-bri.client_secret');
        $timestamp = config('bank-bri.timestamp');
        $secret = $secret_id;
        $token = $this->BRIVAgenerateToken($client_id, $secret_id);

        $institutionCode = config('bank-bri.institution_code');
        $brivaNo = config('bank-bri.briva_no');
        $startDate = $req->awal;
        $endDate = $req->akhir;
        $startTime = $req->jam_awal;
        $endTime = $req->jam_akhir;

        $payload = null;
        $url = config('bank-bri.briva.report_time');
        $verb = config('bank-bri.briva.report_time_verb');
        $path = $url."/".$institutionCode."/".$brivaNo."/".$startDate."/".$startTime."/".$endDate."/".$endTime;
        $base64sign = $this->BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret);

        $request_headers = array(
            "Authorization:Bearer " . $token,
            "BRI-Timestamp:" . $timestamp,
            "BRI-Signature:" . $base64sign,
        );

        $urlPost = config('bank-bri.api_url_extra').$url."/".$institutionCode."/".$brivaNo."/".$startDate."/".$startTime."/".$endDate."/".$endTime;
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_URL, $urlPost);
        curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, "GET"); 
        curl_setopt($chPost, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);

        $resultPost = curl_exec($chPost);
        $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
        curl_close($chPost);

        return json_decode($resultPost, true);
    }

    function BRIVAgenerateToken($client_id, $secret_id) {
        $url = config('bank-bri.api_url_extra').config('bank-bri.get_token');
        $data = "client_id=".$client_id."&client_secret=".$secret_id;
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $data);
        
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $json = json_decode($result, true);
        $accesstoken = $json['access_token'];

        return $accesstoken;
    }

    /* Generate signature */
    function BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret) {
        $payloads = "path=$path&verb=$verb&token=Bearer $token&timestamp=$timestamp&body=$payload";
        $signPayload = hash_hmac('sha256', $payloads, $secret, true);
        return base64_encode($signPayload);
    }


    public function getTabungan($nis, $pin){
        $siswa = Siswa::where("nis",$nis)->where("pin",$pin)->first();
        if($siswa){
            $h2h = tagihanH2h::where('nis',$nis)->where('type','4')->first();
            if ($h2h) {
                return response()->json([
                    "status"=> true,
                    "data"=>$h2h
                ], 200);
            } else {
                return response()->json([
                    "status"=> false,
                    "pesan"=>"Data H2H tidak ditemukan"
                ], 200);
            }
        } else {
            return response()->json([
                    "status"=> false,
                    "pesan"=>"Data Siswa tidak ditemukan"
                ], 200);
        }
    }

    public function hapusTabungan(Request $req){
        if ( !$req->pin || !$req->nis) {
            return response()->json([
                "status" => false,
                "pesan" => 'Field Kosong'
            ], 200);
        }

        $siswa = Siswa::where("nis",$req->nis)->where("pin",$req->pin)->first();
        if($siswa){
            $h2h = tagihanH2h::where('nis',$req->nis)->where('type','4')->first();
            if ($h2h) {
                $isBris = $h2h->ref == 2 ? true : false;
                $h2h->delete();
                if($isBris) {
                    return response()->json([
                        "status"=> true,
                        "pesan"=>"Data berhasil dihapus"
                    ], 200);
                }
            } else {
                return response()->json([
                    "status"=> false,
                    "pesan"=>"Data H2H tidak ditemukan"
                ], 200);
            }
        } else {
            return response()->json([
                    "status"=> false,
                    "pesan"=>"Data Siswa tidak ditemukan"
                ], 200);
        }
        $client_id = config('bank-bri.client_id');
        $secret_id = config('bank-bri.client_secret');
        $timestamp = config('bank-bri.timestamp');
        $secret = $secret_id;
        $token = $this->BRIVAgenerateToken($client_id, $secret_id);

        $institutionCode = config('bank-bri.institution_code');
        $brivaNo = config('bank-bri.briva_no');
        $custCode = $req->nis;


        $payload = "institutionCode=".$institutionCode."&brivaNo=".$brivaNo."&custCode=".$custCode;
        $path = config('bank-bri.briva.destroy');
        $verb = config('bank-bri.briva.destroy_verb');
        $base64sign = $this->BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret);

        $request_headers = array(
            "Authorization:Bearer " . $token,
            "BRI-Timestamp:" . $timestamp,
            "BRI-Signature:" . $base64sign,
        );

        $urlPost = config('bank-bri.api_url_extra').$path;
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_URL, $urlPost);
        curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chPost, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
        curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
        
        $resultPost = curl_exec($chPost);
        $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
        curl_close($chPost);
        // $tagihan->delete();
        $briva = json_decode($resultPost, true);

        return response()->json($briva);
    }

    public function cekTabungan($va, Request $req){
        $h2h = TagihanH2h::where('nis', $va)->where('type','4')->first();
        if ($h2h->ref == 2) {
            return response()->json([
                "status" =>false,
                "pesan" => "Kamu belum melakukan pembayaran"
            ], 200);
        }

        $client_id = config('bank-bri.client_id');
        $secret_id = config('bank-bri.client_secret');
        $timestamp = config('bank-bri.timestamp');
        $secret = $secret_id;
        $token = $this->BRIVAgenerateToken($client_id, $secret_id);

        $institutionCode = config('bank-bri.institution_code');
        $brivaNo = config('bank-bri.briva_no');
        $custCode = $va;

        $payload = null;
        $path = config('bank-bri.briva.show')."/".$institutionCode."/".$brivaNo."/".$custCode;
        $verb = config('bank-bri.briva.show_verb');
        $base64sign = $this->BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret);

        $request_headers = array(
            "Authorization:Bearer " . $token,
            "BRI-Timestamp:" . $timestamp,
            "BRI-Signature:" . $base64sign,
        );

        $urlPost = config('bank-bri.api_url_extra')
                    .config('bank-bri.briva.show')
                    ."/".$institutionCode."/".$brivaNo."/".$custCode;
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_URL, $urlPost);
        curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, "GET"); 
        curl_setopt($chPost, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
        $resultPost = curl_exec($chPost);
        $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
        curl_close($chPost);

        $briva = json_decode($resultPost, true);
        // return response()->json($briva);
        if ($briva['status'] == false) {
            return response()->json($briva);
        }
        if ($briva['data']['statusBayar'] == "N") {
            return response()->json([
                "status" =>false,
                "pesan" => "Kamu belum melakukan pembayaran"
            ], 200);
        }
        if ($h2h){
            $dibayar = (int)$briva['data']['Amount'];
            $jurnal = Jurnal::where('nis', $va)->where('id_jenis_biaya','4')
                    ->orderBy('id','DESC')->first();

            $kode_trx = "TRX-". time();
            $new = new Jurnal;
            $new->nis = $va;
            $new->kode_trx =  $kode_trx;
            $new->ref = 3;
            $new->jumlah = $dibayar;
            $new->tgl_bayar = date("Y-m-d");
            $new->biaya_id = 0;
            $new->nama_biaya = "Titipan Tabungan Santri";
            $new->id_jenis_biaya = 4;
            $new->type = 1;
            $new->saldo_akhir = $jurnal ? $jurnal->saldo_akhir + $dibayar : $dibayar;
            $new->save();

            if ($new) {
                $h2h->delete();
                $req->id = $va;
                $this->hapusVA($req);
                return response()->json([
                    "status" =>true,
                    "data" => $new
                ], 200);
            }
            return response()->json([
                "status" =>false,
                "pesan" => "Gagal membuat Jurnal"
            ], 200);
            
        }
        return response()->json([
            "status" =>false,
            "pesan" => "Tidak ada data H2H"
        ], 200);
    }




    
}
