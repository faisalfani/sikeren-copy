<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Keuangan\Entities\BiayaSiswa;
use Modules\Master\Entities\Kelas;

use Illuminate\Support\Facades\Cache;

class ChartController extends Controller
{
    public function pembayaranBebas(Request $req){ 
        $tahun = $req->tahun;
        $jenis = $req->jenis;
        
        $key = "pembayaranBebas";
        if ($jenis && $tahun) {
            $key = $key . '_' . $jenis . '_' . $tahun;
        }

        if ($jenis && $tahun === null) {
            $key = $key . '_' . $jenis;
        }

        if ($jenis === null && $tahun) {
            $key = $key . '_' . $tahun;
        }        

        $seconds = 43200;
        $data = Cache::remember($key, $seconds, function () use($tahun, $jenis) {   
            $data_biaya_siswa = DB::table("biaya_siswas as bs")
            ->leftJoin("siswas as s", "s.nis", "bs.nis")
            ->where("s.status", "1")
            ->select(
                // DB::raw('COUNT(IF(nominal = dibayar, 1, NULL)) as lunas'),
                DB::raw('COUNT(DISTINCT CASE WHEN bs.nominal = dibayar THEN bs.nis END) lunas_siswa'),
                DB::raw('SUM(CASE WHEN bs.nominal = dibayar THEN bs.nominal ELSE 0 END) as lunas_uang'),
                DB::raw('COUNT(DISTINCT CASE WHEN bs.nominal != dibayar THEN bs.nis END) hutang_siswa'),
                // DB::raw('COUNT(IF(nominal != dibayar, 1, NULL)) as hutang'),
                DB::raw('SUM(CASE WHEN bs.nominal != dibayar THEN bs.nominal ELSE 0 END) as hutang_uang'),
                DB::raw('COUNT(bs.nominal) as total'),
                DB::raw('COUNT(DISTINCT bs.nis) AS siswa')
            )
            ->where('satuan','Daftar Ulang');
                        
            if ($jenis) {
                $data_biaya_siswa = $data_biaya_siswa->where('jenis_biaya_id', $jenis);
            }

            if ($tahun) {
                $data_biaya_siswa = $data_biaya_siswa->where('tahun_ajaran', $tahun);
            }

            return $data_biaya_siswa->first();
        });         

        $data->lunas_uang = rp($data->lunas_uang);
        $data->hutang_uang = rp($data->hutang_uang);
        $data->key = $key;                

        // $data= BiayaSiswa::select('nominal', 'dibayar','nis')
        // ->where('nominal', '=', DB::raw('dibayar'))->count()->groupBy('nis');
        // $data= BiayaSiswa::select(DB::raw('COUNT(DISTINCT nis) AS siswa'))->where('nominal', '=', DB::raw('dibayar'))
        //             ->where('satuan','Daftar Ulang')->first();

        // $data = $data->map(function($q) {
        //     if ($q->nominal == $q->dibayar) {
        //         return $q;
        //     }
        //     return false;
        // })->reject(function ($value) {
        //     return $value === false;
        // });

        return response()->json($data, 200);

    }

    public function pembayaranBulanan(Request $req){
        $tahun = $req->tahun;
        $jenis = $req->jenis;
        
        $key = "pembayaranBulanan";
        if ($jenis && $tahun) {
            $key = $key . '_' . $jenis . '_' . $tahun;
        }

        if ($jenis && $tahun === null) {
            $key = $key . '_' . $jenis;
        }

        if ($jenis === null && $tahun) {
            $key = $key . '_' . $tahun;
        }

        $seconds = 43200;
        $data = Cache::remember($key, $seconds, function () use($tahun, $jenis) {   
            $data_biaya_siswa = DB::table("biaya_siswas as bs")
            ->leftJoin("siswas as s", "s.nis", "bs.nis")
            ->where("s.status", "1")
            ->select(
                // DB::raw('COUNT(IF(nominal = dibayar, 1, NULL)) as lunas')
                DB::raw('COUNT(DISTINCT CASE WHEN bs.nominal = dibayar THEN bs.nis END) lunas_siswa'),
                DB::raw('SUM(CASE WHEN bs.nominal = dibayar THEN bs.nominal ELSE 0 END) as lunas_uang'),
                DB::raw('COUNT(DISTINCT CASE WHEN bs.nominal != dibayar THEN bs.nis END) hutang_siswa'),
                // DB::raw('COUNT(IF(nominal != dibayar, 1, NULL)) as hutang'),
                DB::raw('SUM(CASE WHEN bs.nominal != dibayar THEN bs.nominal ELSE 0 END) as hutang_uang'),
                DB::raw('COUNT(bs.nominal) as total'),
                DB::raw('COUNT(DISTINCT bs.nis) AS siswa')
            );

            if ($jenis) {
                $data_biaya_siswa = $data_biaya_siswa->where('jenis_biaya_id', $jenis);
            }

            if ($tahun) {
                $data_biaya_siswa = $data_biaya_siswa->where('tahun_ajaran', $tahun);
            }

            return $data_biaya_siswa->first();
        });                 

        $data->lunas_uang = rp($data->lunas_uang);
        $data->hutang_uang = rp($data->hutang_uang);
        $data->key = $key;                

        return response()->json($data, 200);

    }


    public function kepatuhanBebas() {
        // $a = BiayaSiswa::select(DB::raw('CONCAT(bulan, "/", tahun_id) as bayar'),
        //                         DB::raw('FORMAT(created_at, "MM-yyyy") as generate'))->first();
        $seconds = 43200;
        $data = Cache::remember('kepatuhanBebas', $seconds, function () {               
            $kepatuhan = BiayaSiswa::whereYear('created_at', '=', DB::raw('tahun_id'))
                  ->whereMonth('created_at', '=',DB::raw('bulan'))
                  ->where('satuan','Daftar Ulang')
                  ->count();
            $total = BiayaSiswa::where('satuan','Daftar Ulang')->count();
            $persen = $kepatuhan / $total * 100;

            return [
                'persen' => number_format((float)$persen, 2, '.', '')
            ];            
        });

        return response()->json($data, 200);
    }

    public function kepatuhanBulanan(){
        // $a = BiayaSiswa::select(DB::raw('CONCAT(bulan, "/", tahun_id) as bayar'),
        //                         DB::raw('FORMAT(created_at, "MM-yyyy") as generate'))->first();
        $seconds = 43200;
        $data = Cache::remember('kepatuhanBulanan', $seconds, function () {               
            $kepatuhan = BiayaSiswa::whereYear('created_at', '=', DB::raw('tahun_id'))
              ->whereMonth('created_at', '=',DB::raw('bulan'))
              ->where('satuan','Bulanan')
              ->count();
            $total = BiayaSiswa::where('satuan','Bulanan')->count();
            $persen = $kepatuhan / $total * 100;

            return [
                'persen' => number_format((float)$persen, 2, '.', '')
            ];
        });
        
        return response()->json($data, 200);
    }

    public function realisasiKepatuhan(Request $req){
        $tahun = $req->tahun;

        $key = "realisasiKepatuhan";        
        if ($tahun) {
            $key = $key . '_' . $tahun;
        }

        $seconds = 43200;
        $data = Cache::remember($key, $seconds, function () use($tahun) {               
            $kepatuhan = BiayaSiswa::select('bulan','tahun_id',
                                        DB::raw('SUM(nominal) as nominal'),
                                        DB::raw('SUM(dibayar) as dibayar')
                                    )
                    ->where(function($q) use($tahun) {
                            // if ($req->jenis) {
                            //     $q->where('jenis_biaya_id', $req->jenis);
                            // }
                            if ($tahun) {
                                $q->where('tahun_ajaran', $tahun);
                            }
                        })
                    // ->where('tahun_ajaran','2020')
                    ->groupBy('bulan')
                    ->orderBy('tahun_id','ASC')
                    ->orderBy('bulan','ASC')
                    ->get();
        
            $persens = $kepatuhan->map(function($q){
                $persen = $q->dibayar / $q->nominal *100;
                $q->persen = number_format((float)$persen, 2, '.', '');

                return $q->persen;
            });

            return $persens;
        });        

        return response()->json($data, 200);
    }

    public function dataKelas($tahun) {
        $seconds = 43200;
        $data_kelas = Cache::remember('dataKelas_' . $tahun, $seconds, function () use ($tahun) {               
            return DB::table('biaya_siswas as bs')
                ->leftJoin('siswas as s', 's.nis', 'bs.nis')
                ->leftJoin('kelas as k', 'k.id', 's.id_kelas')
                ->select('s.id_kelas','k.nama')
                ->groupBy('s.id_kelas')
                ->where('id_kelas','!=', '0')
                ->where('id_kelas','!=', NULL)
                ->where(function($q) use ($tahun) {
                    if ($tahun != 'Semua') {
                        $q->where('tahun_ajaran', $tahun);
                    }
                })
                ->get();            
        });            

        return response()->json($data_kelas, 200);
    }

    public function jumlahSiswa($tahun, $kelas){
        // return $kelas;        
        if ($kelas != 'Semua') {
            $kelas = Kelas::where('nama', $kelas)->first();
            if (!$kelas) {
                $kelas = 'Semua';
            }
        }        
                
        if ($kelas === 'Semua') {
            $class = 'Semua';
        } else {
            $class = $kelas->id;
        }

        $seconds = 43200;
        $data_siswa = Cache::remember('jumlahSiswa_' . $class . '_' . $tahun, $seconds, function () use($kelas, $tahun) {               
            $data_jumlah = DB::table('biaya_siswas as bs')
                      ->leftJoin('siswas as s', 's.nis', 'bs.nis')
                    //   ->leftJoin('kelas as k', 'k.id', 's.id_kelas')
                      ->select(
                          DB::raw('COUNT(DISTINCT(CASE WHEN s.jk = "L"  THEN s.nis END)) as laki'),
                          DB::raw('COUNT(DISTINCT(CASE WHEN s.jk = "P"  THEN s.nis END)) as perempuan'),
                          DB::raw('Count(DISTINCT(s.nis)) total'),
                          's.status'
                      )
                    //   ->groupBy('s.nis')
                      ->groupBy('s.status')
                      ->where('s.id_kelas','!=', '0')
                      ->where('s.id_kelas','!=', NULL)
                    //   ->where('s.status','=', '1')
                      ->where(function($q) use($kelas, $tahun) {
                          if ($kelas != 'Semua') {
                              $q->where('s.id_kelas', $kelas->id);
                          }
                          if ($tahun != 'Semua') {
                             $q->where('bs.tahun_ajaran', $tahun);
                          }
                      })
                      ->get();
            // return response()->json($data_jumlah, 200);
            $data_aktif = $data_jumlah->where('status', '1')->first();
            $data_alumni = $data_jumlah->where('status', '2')->first();
            $data_berhenti = $data_jumlah->where('status', '0')->first();

            return [
                "aktif" => $data_aktif,
                "alumni" => $data_alumni,
                "berhenti" => $data_berhenti
            ];          
        });

        
        return response()->json($data_siswa, 200);
    }
}
