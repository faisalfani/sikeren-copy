<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\CoreInfo;

class CoreSettingsController extends Controller
{
    
    public function json(){
        return CoreInfo::first();
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data = CoreInfo::first();
        // $tahun = date("Y");
        // $pattern = "/tahunSekarang/i";
        // $data->copyright_before_login = preg_replace($pattern, $tahun, $data->copyright_before_login);
        // $data->copyright_after_login = preg_replace($pattern, $tahun, $data->copyright_after_login);
        return view('core::settings.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('core::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $req)
    {
        $metode_status = 0;
        if ($req->metode_status == 'on') {
            $metode_status = 1;
        }

        // $tahun = date("Y");
        // $pattern = "/tahunSekarang/i";
        // $sebelum = preg_replace($pattern, $tahun, $req->copyright_before_login);
        // $sesudah = preg_replace($pattern, $tahun, $req->copyright_after_login);
        // return response()->json([$sebelum, $sesudah], 200);
        // dd($req);
        $info = CoreInfo::updateOrCreate(
            ['id' => '1'],
            [
                "nama" => $req->nama,
                "no_telp" => $req->no_telp,
                "alamat" => $req->alamat,
                "copyright_before_login" => $req->copyright_before_login,
                "copyright_after_login" => $req->copyright_after_login,
                "color_primary" => $req->color_primary,
                "type_dashboard" => $req->type_dashboard ? 2 : 1,
                "color_secondary" => $req->color_secondary
            ]
        );

        return back()->with("success","Data Informasi Aplikasi berhasil disimpan");
    }

    



    public function uploadGambar(Request $req){
        switch ($req->tipe) {
            case 'Logo Halaman Login':
                $imageName = "logo".'.'.$req->gambar->extension();  
                CoreInfo::updateOrCreate(
                    ['id' => '1'],
                    [
                        "logo" => $imageName
                    ]
                );
                break;
            case 'logo Setelah Login':
                $imageName = "logo2".'.'.$req->gambar->extension();  
                CoreInfo::updateOrCreate(
                    ['id' => '1'],
                    [
                        "logo_after_login" => $imageName
                    ]
                );
                break;
            case 'Favicon':
                $imageName = "icon".'.'.$req->gambar->extension();  
                CoreInfo::updateOrCreate(
                    ['id' => '1'],
                    [
                        "favicon" => $imageName
                    ]
                );
                break;
            case 'Gambar Latar Halaman Login':
                $imageName = "bgs".'.'.$req->gambar->extension();  
                CoreInfo::updateOrCreate(
                    ['id' => '1'],
                    [
                        "walpaper_image" => $imageName
                    ]
                );
                break;
            case 'Logo Laporan Kiri':
                $imageName = "l_logo".'.'.$req->gambar->extension();  
                CoreInfo::updateOrCreate(
                    ['id' => '1'],
                    [
                        "left_logo" => $imageName
                    ]
                );
                break;
            case 'Logo Laporan Kanan':
                $imageName = "r_logo".'.'.$req->gambar->extension();  
                CoreInfo::updateOrCreate(
                    ['id' => '1'],
                    [
                        "right_logo" => $imageName
                    ]
                );
                break;
            
            default:
                $imageName = "default".'.'.$req->gambar->extension();  
                break;
        }
        // $imageName = time().'.'.$req->gambar->extension();  
   
        $req->gambar->move(public_path('images/custom'), $imageName);
        
        return back()
            ->with('success',$req->tipe.' Berhasil disimpan')
            ->with('tab','gambar');

    }


    public function simpanMetode(Request $req){
        $metode_bri = $req->metode_bri ? true : false;
        $metode_bri_syariah = $req->metode_bri_syariah ? true : false;
        $metode_payment_gateway = $req->metode_payment_gateway ? true : false;


        CoreInfo::updateOrCreate(
            ['id' => '1'],
            [
                "metode_bri" => $metode_bri,
                "metode_bri_syariah" => $metode_bri_syariah,
                "metode_payment_gateway" => $metode_payment_gateway
            ]
            );


        return back()
            ->with('success','Metode Pembayaran berhasil disimpan')
            ->with('tab','metode');
        
    }


    public function ubahStatusLogoLaporan(Request $req){
        // dd($req);
        $pesan = $req->tipe == 'left_logo_status' ? 'Logo Laporan Kiri' : 'Logo Laporan Kanan';
        if ($req->tipe == 'left_logo_status') {
            CoreInfo::updateOrCreate(
                ['id' => '1'],
                [
                    "left_logo_status" => $req->status == "true" ? 1 : 0
                ]
                );
        } else {
            CoreInfo::updateOrCreate(
                ['id' => '1'],
                [
                    "right_logo_status" => $req->status == "true" ? 1 : 0
                ]
                );
        }

        return "Status ".$pesan." Berhasil disimpan";
        
    }
}
