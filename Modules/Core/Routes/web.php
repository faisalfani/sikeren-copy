<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware("auth")->group(function(){
    Route::resource('/menu', 'MenuController');
    Route::get('/menu/delete/{id}', 'MenuController@deleteMenu');
    Route::resource('/users', 'UsersController');
    Route::resource('/role', 'RoleController');


    // Created By Kosih, 1 juni 2021
    Route::resource('/settings', 'CoreSettingsController');
    Route::post('/settings/upload-gambar', 'CoreSettingsController@uploadGambar')->name('core.upload.gambar');
    Route::post('/settings/simpan-metode', 'CoreSettingsController@simpanMetode')->name('core.simpan.metode');
    Route::post('/settings/ubah-status-logo-laporan', 'CoreSettingsController@ubahStatusLogoLaporan');
    Route::get('/backup_restore', 'BackupRestoreController@index');
    Route::post('/backup_restore/generate', 'BackupRestoreController@generateBackup')->name('backup.generate');
    Route::post('/backup_restore/process', 'BackupRestoreController@processBackupFile')->name('backup.process');
    


    Route::prefix('chart')->group(function () {
        Route::get('pembayaran-bebas', 'ChartController@pembayaranBebas');
        Route::get('pembayaran-bulanan', 'ChartController@pembayaranBulanan');
        Route::get('kepatuhan-bebas', 'ChartController@kepatuhanBebas');
        Route::get('kepatuhan-bulanan', 'ChartController@kepatuhanBulanan');
        Route::get('realisasi-kepatuhan', 'ChartController@realisasiKepatuhan');
        Route::get('data-kelas/{tahun}', 'ChartController@dataKelas');
        Route::get('jumlah-siswa/{tahun}/{kelas}', 'ChartController@jumlahSiswa');
    });

});

Route::get('/app-json', 'BackupRestoreController@appJson');

Route::get('/setting-json', 'CoreSettingsController@json');
Route::get('bri', 'BRIController@cobain');

Route::prefix('briva')->group(function () {
    Route::post('buat-va', 'BRIController@buatVA');
    // info-va
    Route::get('info-va/{va}', 'BRIController@infoVA');
    // info-status
    Route::get('info-status/{id}', 'BRIController@infoStatus');
    // update-status
    Route::post('update-status', 'BRIController@updateStatus');
    // update-va
    Route::post('update-va', 'BRIController@updateVA');
    // hapus-va
    Route::post('hapus-va', 'BRIController@hapusVA');
    Route::get('hapus-va/{nis}', 'BRIController@hapusin');
    // sejarah-va
    Route::post('sejarah-va', 'BRIController@sejarahVA');
    // sejarah-waktu-va
    Route::post('sejarah-waktu-va', 'BRIController@sejarahWaktuVA');
    // Status VA dari get report
    // Cek Status Pembayaran
    Route::get('cek-status-pembayaran/{id}', 'BRIController@cekStatusPembayaranByReport');
    
    // tabungan
    Route::get('tabungan/{nis}/{pin}', 'BRIController@getTabungan');
    Route::post('buat-tabungan', 'BRIController@buatTabungan');
    Route::get('cek-tabungan/{nis}', 'BRIController@cekTabungan');
    Route::post('hapus-tabungan', 'BRIController@hapusTabungan');
});