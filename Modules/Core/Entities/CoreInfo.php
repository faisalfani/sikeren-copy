<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class CoreInfo extends Model
{
    protected $guarded = [];
}
