@extends('layouts.app')

@section('title')
      Menu
@endsection
@section('css-after') 
 <!-- Select2 -->
 <link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">
 <link rel="stylesheet" href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

<style>
.menu{
  padding: 10px 12px;
  border-radius: 30px;
}
.menu:hover{
  background: #fff
}
</style>
@endsection

@section('content')
<div class="card container card-success">
  <div class="card-body">
    
<div class="row ">
  <div class="col-md-12">
     

    <!-- Button trigger modal -->
    <div class="row">
      
          <div class="col-md-6 mb-2">
              <button type="button" class="btn text-white bg-primary-c" onclick="createUser()">
                  <i class="fa fa-plus"></i>                   
                Tambah Menu
              </button>
              {{-- <button type="button" class="btn text-white bg-primary-c" data-toggle="modal" data-target="#modalForm">
                  Filter
              </button> --}}
          </div>
          <div class="col-md-6 mb-2 input-group float-right">
            <input id="search" placeholder="Search" type="text" class="form-control">
            <div class="input-group-append">
              <button onclick="location.reload()" class="btn text-white bg-primary-c" type="button">
                <i class="fa fa-sync"></i>
              </button>
            </div>
          </div>
      </div>

    <div class="list-group pt-4">
        <div class="w-100">
            @include('comp.notif')
        </div>
        {{-- <div class="list-group-item bg-primary-c">
          <div class="row">
            <div class="col">
               No
            </div>
            <div class="col-md-3">
                Nama
             </div>
            <div class="col-md-3">
                URL
            </div>
            <div class="col-md-2">
                Sub
            </div>
            <div class="col-md-3 text-right">
                Action
            </div>
          </div>
        </div>
        
        <div id="list-post-data">
          
          <div id="post-data">
            @include('core::menu.data')
          </div>

        </div>
       
      </div> --}}



      {{-- Menu --}}


      <div >
        <div class="row" >
            <div class="col-md-12">
                <div id="list-post-data">
                  <ul id="post-data">
                      @include('core::menu.data')
                  </ul>
                </div>
            </div>
        </div>
    </div>
    

      
      
      {{-- End Menu --}}
      
      
      <div class="row">
        <div class="col-md-12 text-center pt-4">
          <div onclick="loadMore()" id="btn-load-more" class="btn btn-flat text-primary">
            <i class="fa fa-arrow-down"></i>
          </div>
        </div>
      </div>

      
</div>
</div>
</div>


</div>
</div>
<form  method="POST" action="/menu">
  @csrf      
      <!-- Modal -->
      <div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="modalFormTitle" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-scrollable" role="document">
          <div class="modal-content text-dark">
            <div class="modal-header">
              <h5>Menu</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">


                    <div class="form-group row">
                        <label for="nama" class="col-md-12">{{ __('Nama') }}</label>

                        <div class="col-md-12">
                            <input placeholder="Nama Menu" id="nama" type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama') }}" required autocomplete="nama" autofocus>

                            @error('nama')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="url" class="col-md-12">{{ __('URL') }}</label>
                        <div class="col-md-12">
                            <input placeholder="example ('/menu') " id="url" type="text" class="form-control" name="url" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="url" class="col-md-12">{{ __('Icon (Font Awesome Free)') }}</label>
                        <div class="col-md-12">
                            <input placeholder="example ('fa fa-user') " id="icon" type="text" class="form-control" name="icon" required>
                        </div>
                    </div>

                    <div class="form-group row">
                      <label for="sub" class="col-md-12">{{ __('Sub Menu') }}</label>
                      <div class="col-md-12">
                          <select class="form-control select2" name="sub" id="sub">
                            <option value="">Pilih Menu</option>

                            @foreach (App\menu::all() as $item)
                                <option value="{{ $item->id }}"> {{ $item->nama }}</option>                            
                            @endforeach

                          </select>
                      </div>
                  </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal"> <i class="fa fa-times"></i></button>
              <button id="btn-create" type="submit" class="btn bg-primary-c"> <i class="fa fa-plus"></i> {{ __('Tambah Menu') }}</button>
              <button id="btn-update" type="button" class="btn bg-primary-c"><i class="fa fa-save"></i> {{ __('Simpan') }}</button>
            </div>
          </div>
        </div>
      </div>

    </form>
@endsection


@section('js-after')
<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>

<script type="text/javascript">


$('.select2').select2({
      theme: 'bootstrap4'
})


  var page = 1;
  var where = "";  


  function loadMore(){
    page++;
    loadMoreData(page);
  }

  $("#btn-load-more").hover(() => {
    loadMore();
  });

  $("#search").keyup(function() {
    where = $(this).val();
    page = 1;
    if(where !== ''){
      loadMoreData(page);
    }
  });

	function loadMoreData(page){
	  $.ajax(
	        {
	            url: '?where='+where+'&page=' + page,
	            type: "get",
	            beforeSend: function()
	            {
	                $('.ajax-load').show();
	            }
	        })
	        .done(function(data)
	        {
	            if(data.html == " "){
	                $('.ajax-load').html("No more records found");
	                return;
	            }
	            $('.ajax-load').hide();

              if(where !== '' && page == 1){
                $("#post-data").html(data.html);
              }else{
                $("#post-data").append(data.html);
              }
	            
	        })
	        .fail(function(jqXHR, ajaxOptions, thrownError)
	        {
	              alert('server not responding...');
	        });
	}


  function createUser(){
    $("#nama").val("");
    $("#url").val("");
    $("#icon").val("");
    $("#sub").val("");
    $("#modalForm").modal("show");
    $("#btn-create").show();
    $("#btn-update").hide();
  }

  function edit(data){
    var d = JSON.parse(data);
    console.log(d);
   
    $("#nama").val(d.nama);
    $("#url").val(d.url);
    $("#icon").val(d.icon);
    $("#sub").val(d.sub);
    $("#modalForm").modal("show");
    $("#btn-create").hide();
    $("#btn-update").show();


    $("#btn-update").click(function(){

      var _data = {
        nama : $("#nama").val(),
        url : $("#url").val(),
        icon: $("#icon").val(),
        sub: $("#sub").val(),
        _method: 'PUT',
        _token : '{{ csrf_token() }}'
      }
          $.ajax({
              type: "PUT",
              url: '/menu/' + d.id,
              data: _data,
              success: function (data) {
                // console.log(data)
                location.reload()

              },
              error: function (data) {
                  console.log('Error:', data);
              }
          });
    })

  }

  function deleteMenu(id){

    
    swal({
      title: "Apakah Kamu Yakin?",
      text: "Pastikan data yang akan dihapus sudah valid",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Hapus",
      closeOnConfirm: false
    },
    function(){
        // console.log(id);
        $.get("/menu/delete/"+id)
          .done((res) => {
            location.reload()
          })
    });
  }


</script>

@endsection