
<?php
    
function createMenu($m){
  ?>
  <div class="card px-2">
      <div class="row p-2">
          <div class="col-md-3">
            <i class="@if($m->icon) {{ $m->icon }} @else fa fa-stream @endif mr-2"></i>  
            {{-- {{ $m->id }} /  --}}
            {{ $m->nama }}
          </div>
          <a target="_BLANK" href="{{ $m->url }}" class="col-md-3">
              {{ $m->url }}
          </a>
          <div class="col-md-6 text-right ">
                <a onclick="edit('{{ $m }}')" href="#" class="btn btn-sm bg-primary-c text-light">
                    <i class="fa fa-edit"></i>
                </a>
                @if ($m->sub !== null)
                    
                <a onclick="deleteMenu('{{ $m->id }}')" href="#" class="btn btn-sm btn-outline-danger">
                    <i class="fa fa-trash"></i>
                </a>

                @endif
          </div>
      </div>
    </div>
  <?php
}
?>
    
@foreach ($menu->where("sub",null) as $key => $m)            
            <li data-icon-cls="fa fa-calendar" id="menu-{{ $m->id }}">
                
                @php
                    createMenu($m);
                    $submenu = App\menu::where("sub",$m->id)->get();
                @endphp
                     
                   <ul>
                        <?php
                        foreach ($submenu as $key => $sub) {
                        $submenu1 = App\menu::where("sub",$sub->id)->get();

                        ?>
                       <li>
                         
                          <?php createMenu($sub);?>
                                <ul>
                                  <?php
                                  foreach ($submenu1 as $key => $sub1) {
                                  ?>
                                <li>
                                  
                                  <?php createMenu($sub1);?>
                                    
                                </li>
                              <?php } ?>
                            </ul> 
                          
                       </li>
                    <?php } ?>
                   </ul> 
            </li>
@endforeach