@extends('layouts.app')

@section('title')
      Settings
@endsection

@section('css-after')
<style>
  .tengahin{
  display: flex;
  justify-content: center;
  align-items: center;
}
</style>
@endsection
@section('content')
<div class="container" style="min-height: 600px;">
<div class="row justify-content-md-center">
  <div class="col-md-12">
     <div class="card card-outline card-primary">
         <div class="card-body">

            {{-- Tab Menu --}}
            <ul class="nav nav-tabs mb-3" role="tablist">
                <li class="nav-item">
                    <a id="tabInfo" class="nav-link " href="#panelInfo" role="tab" data-toggle="tab">Informasi WEB</a>
                </li>
                <li class="nav-item">
                    <a id="tabGambar" class="nav-link" href="#panelGambar" role="tab" data-toggle="tab">Gambar / Foto</a>
                </li>
                <li class="nav-item">
                    <a id="tabMetode" class="nav-link" href="#panelMetode" role="tab" data-toggle="tab">Metode Bayar</a>
                </li>
                <li class="nav-item">
                    <a id="tabInfo" class="nav-link " href="#panelApp" role="tab" data-toggle="tab">Informasi Aplikasi Santri</a>
                </li>
            </ul>

            @include('comp.notif')
            {{-- Konten Tab --}}
            <div class="tab-content">
            
                <div role="tabpanel" class="tab-pane fade  py-3" id="panelInfo">

                    {{-- Isi tab Informasi --}}
                    
                    <form  method="POST" action="/settings">
                      @csrf      
                        
                        <div class="form-group">
                          <label for="">Nama Pesantren</label>
                          <input type="text" class="form-control" name="nama" value="{{$data->nama ? $data->nama : 'SANTREN.ID'}}">
                        </div>
                        <div class="form-group">
                          <label for="">Nomor Telepon Pesantren</label>
                          <input type="text" class="form-control" name="no_telp" value="{{$data->no_telp ? $data->no_telp : '0812'}}">
                        </div>
                        <div class="form-group">
                          <label for="">Alamat Pesantren</label>
                          <textarea rows="5" class="form-control" name="alamat">{{$data->alamat ? $data->alamat : 'Lorem ipsum dolor sit amet.'}}</textarea>
                        </div>
                   
                        <div class="callout callout-warning lead">
                        <h4>Perhatian!</h4>
                        <p>
                        Gunakan tulisan <b>tahunSekarang</b> untuk mendapatkan nilai tahun saat ini secara otomatis
                        </p>
                        </div>

                        <div class="row">
                          <div class="col">
                            
                            <div class="form-group">
                              <label for="">Copyright Sebelum Login</label>
                              <textarea rows="5" class="form-control" name="copyright_before_login">{{$data->copyright_before_login ? $data->copyright_before_login : 'Copyright © 2019 - 2021. Before Login'}}
                              </textarea>
                            </div>
                          </div>
                          <div class="col">
                            
                            <div class="form-group">
                              <label for="">Copyright Setelah Login</label>
                              <textarea rows="5" class="form-control" name="copyright_after_login">{{$data->copyright_after_login ? $data->copyright_after_login : 'Copyright © 2019 - 2021. after Login'}}
                              </textarea>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <label for="">Warna Utama</label>
                              <input type="color" class="form-control" name="color_primary" value="{{$data->color_primary}}">
                            </div>
                          </div>
                          <div class="col">
                            <div class="form-group">
                              <label for="">Warna Kedua</label>
                              <input type="color" class="form-control" name="color_secondary" value="{{$data->color_secondary}}">
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="custom-control custom-switch">
                            <input onchange="ubahin()" name="type_dashboard" type="checkbox" class="custom-control-input" id="type_dashboard" {{$data->type_dashboard == '2'? 'checked' : ''}}>
                            <label class="custom-control-label" for="type_dashboard" id="label_type_dashboard">Basic Dashboard</label>
                          </div>
                        </div>

                        <button type="submit" class="btn btn-block bg-danger btn-labeled">
                          <b><i class="icon-floppy-disk"></i></b>
                          SIMPAN
                        </button>
                    </form>




                </div>

                {{-- Isi tab Gambar --}}
                <div role="tabpanel" class="tab-pane fade py-3" id="panelGambar">
                        <div id="settingGambar">
                          <div class="row">
                            <div class="col-md-6">
                              <div class="h5">Logo Halaman Login</div>
                              @if ($data->logo)  
                                <img src="{{ $data->logo ? '/images/custom/'.$data->logo : '/img/logo.png'}}" alt="Logo" class="img-fluid bg-secondary p-2 my-2">
                              @else
                                  <div class="bg-secondary mb-2 tengahin" style="width:100%;min-height:500px;">
                                    Belum ada Logo/Gambar
                                  </div>
                              @endif

                              <form action="{{ route('core.upload.gambar') }}" method="POST" enctype="multipart/form-data">
                              @csrf
                                <div class="input-group">
                                  <input type="hidden" name="tipe" value="Logo Halaman Login">
                                  <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="logo"  name="gambar" onchange="setText('logo','textlogo')">
                                    <label class="custom-file-label" for="logo" id="textlogo">Pilih File</label>
                                  </div>
                                  <div class="input-group-append">
                                    <button class="input-group-text bg-primary-c" >Simpan</button>
                                  </div>
                                </div>
                              </form>

                            </div>

                            <div class="col-md-6">
                              <div class="">
                                <div class="h5">Logo Setelah Login</div>
                                @if ($data->logo_after_login)  
                                  <img src="{{ $data->logo_after_login ? '/images/custom/'.$data->logo_after_login : '/logo.png'}}" alt="Logo Setelah Login" class="img-fluid bg-secondary p-2 my-2">
                                @else
                                    <div class="bg-secondary mb-2 tengahin" style="width:100%;min-height:70px;">
                                      Belum ada Logo/Gambar
                                    </div>
                                @endif
                                <form action="{{ route('core.upload.gambar') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                  <div class="input-group">
                                    <input type="hidden" name="tipe" value="logo Setelah Login">
                                    <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="logoHeader"  name="gambar" onchange="setText('logoHeader','textLogoHeader')">
                                      <label class="custom-file-label" for="logoHeader" id="textLogoHeader">Pilih File</label>
                                    </div>
                                    <div class="input-group-append">
                                      <button class="input-group-text bg-primary-c" >Simpan</button>
                                    </div>
                                  </div>
                                </form>
                              </div>
                              <div class="mt-3">
                                <div class="h5">Favicon</div>

                                @if ($data->favicon)  
                                    <img src="{{ $data->favicon ? '/images/custom/'.$data->favicon : '/img/icon.png'}}" alt="Logo Setelah Login" class="img-fluid bg-secondary p-2 my-2">
                                @else
                                    <div class="bg-secondary mb-2 tengahin" style="width:100px;height:100px;text-align:center;">
                                      Belum ada Logo / Gambar
                                    </div>
                                @endif

                                <form action="{{ route('core.upload.gambar') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                  <div class="input-group">
                                    <input type="hidden" name="tipe" value="Favicon">
                                    <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="faviconG"  name="gambar" onchange="setText('faviconG','textfaviconG')">
                                      <label class="custom-file-label" for="faviconG" id="textfaviconG">Pilih File</label>
                                    </div>
                                    <div class="input-group-append">
                                      <button class="input-group-text bg-primary-c" >Simpan</button>
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>

                          <div class="row mt-3">
                            <div class="col-md-6">
                              <div class="row">
                                <div class="col">
                                  <div class="h5">Logo Laporan Kiri</div>
                                </div>
                                <div class="col text-right">
                                  @if ($data->left_logo)
                                      <div class="form-group m-0">
                                        <div class="custom-control custom-switch">
                                          <input name="left_logo_status" type="checkbox" class="custom-control-input" id="left_logo_status" onchange="setStatusLogo('left_logo_status', 'alertKiri')" {{$data->left_logo_status == '1' ? 'checked' : ''}}>
                                          <label class="custom-control-label" for="left_logo_status"></label>
                                        </div>
                                      </div>
                                  @endif
                                </div>
                              </div>
                              @if ($data->left_logo)  
                                <div class="alert alert-success p-2 d-none" id="alertKiri">
                                </div>
                                <img src="{{ $data->left_logo ? '/images/custom/'.$data->left_logo : '/img/logo.png'}}" alt="Logo Kiri" class="img-fluid bg-secondary p-2 my-2">
                              @else
                                  <div class="bg-secondary mb-2 tengahin" style="width:100%;min-height:500px;">
                                    Belum ada Logo/Gambar
                                  </div>
                              @endif
                              
                              <form action="{{ route('core.upload.gambar') }}" method="POST" enctype="multipart/form-data">
                              @csrf
                                <div class="input-group">
                                  <input type="hidden" name="tipe" value="Logo Laporan Kiri">
                                  <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="logoKiri"  name="gambar" onchange="setText('logoKiri','textlogoKiri')">
                                    <label class="custom-file-label" for="logoKiri" id="textlogoKiri">Pilih File</label>
                                  </div>
                                  <div class="input-group-append">
                                    <button class="input-group-text bg-primary-c" >Simpan</button>
                                  </div>
                                </div>
                              </form>
                            </div>

                            <div class="col-md-6">
                              <div class="row">
                                <div class="col">
                                  <div class="h5">Logo Laporan Kanan</div>
                                </div>
                                <div class="col-3 text-right">
                                  @if ($data->right_logo)
                                  <div class="form-group m-0">
                                    <div class="custom-control custom-switch">
                                      <input name="right_logo_status" type="checkbox" class="custom-control-input" id="right_logo_status" onchange="setStatusLogo('right_logo_status','alertKanan')" {{$data->right_logo_status == '1' ? 'checked' : ''}}>
                                      <label class="custom-control-label" for="right_logo_status"></label>
                                    </div>
                                  </div>
                                  @endif
                                </div>
                              </div>
                              @if ($data->right_logo)  
                                <div class="alert alert-success p-2 d-none" id="alertKanan">
                                </div>
                                <img src="{{ $data->right_logo ? '/images/custom/'.$data->right_logo : '/img/logo.png'}}" alt="Logo Kiri" class="img-fluid bg-secondary p-2 my-2">
                              @else
                                  <div class="bg-secondary mb-2 tengahin" style="width:100%;min-height:500px;">
                                    Belum ada Logo/Gambar
                                  </div>
                              @endif
                              
                              <form action="{{ route('core.upload.gambar') }}" method="POST" enctype="multipart/form-data">
                              @csrf
                                <div class="input-group">
                                  <input type="hidden" name="tipe" value="Logo Laporan Kanan">
                                  <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="logoKanan"  name="gambar" onchange="setText('logoKanan','textlogoKanan')">
                                    <label class="custom-file-label" for="logoKanan" id="textlogoKanan">Pilih File</label>
                                  </div>
                                  <div class="input-group-append">
                                    <button class="input-group-text bg-primary-c" >Simpan</button>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </div>

                          <div class="mt-3">
                            <div class="h5">Gambar Latar Halaman Login</div>
                            @if ($data->walpaper_image)  
                                <img src="{{ $data->walpaper_image ? '/images/custom/'.$data->walpaper_image : '/img/bgs.jpg'}}" alt="Logo Setelah Login" class="img-fluid bg-secondary p-2 my-2">
                              @else
                                  <div class="bg-secondary mb-2 tengahin" style="width:100%;min-height:500px;">
                                    Belum ada Logo/Gambar
                                  </div>
                              @endif
                            
                            <form action="{{ route('core.upload.gambar') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                              <div class="input-group">
                                <input type="hidden" name="tipe" value="Gambar Latar Halaman Login">
                                <div class="custom-file">
                                  <input type="file" class="custom-file-input" id="gambarLatar"  name="gambar" onchange="setText('gambarLatar','textgambarLatar')">
                                  <label class="custom-file-label" for="gambarLatar" id="textgambarLatar">Pilih File</label>
                                </div>
                                <div class="input-group-append">
                                  <button class="input-group-text bg-primary-c" >Simpan</button>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>

                </div>


                {{-- Isi Tab Metode Bayar --}}
                <div role="tabpanel" class="tab-pane fade py-3" id="panelMetode">
                  <form action="{{ route('core.simpan.metode') }}" method="POST">
                    @csrf
                    <div class="form-group">
                      <div class="custom-control custom-switch">
                        <input name="metode_bri" type="checkbox" class="custom-control-input" id="metodeBRI" {{$data->metode_bri ? 'checked' : ''}}>
                        <label class="custom-control-label" for="metodeBRI">BRI</label>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-switch">
                        <input name="metode_bri_syariah" type="checkbox" class="custom-control-input" id="metodeBRISyariah" {{$data->metode_bri_syariah ? 'checked' : ''}}>
                        <label class="custom-control-label" for="metodeBRISyariah">BSI</label>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-switch">
                        <input name="metode_payment_gateway" type="checkbox" class="custom-control-input" id="metodeGateway" {{$data->metode_payment_gateway ? 'checked' : ''}}>
                        <label class="custom-control-label" for="metodeGateway">Payment Gateway</label>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-block btn-primary">
                      <i class="fa fa-save"></i> Simpan
                    </button>
                  </form>

                </div>

                {{-- Tab Applikasi Santri--}}
                <div role="tabpanel" class="tab-pane fade  py-3" id="panelApp">

                    {{-- Isi tab Informasi --}}
                    <div class="row mb-3">
                      <div class="col-md-6">
                        <div class="h5">Logo Aplikasi Santri</div>
                        @if ($data->app_santri_logo)  
                          <div class="alert alert-success p-2 d-none" id="alertKiri">
                          </div>
                          <img src="{{ $data->left_logo ? '/images/custom/'.$data->left_logo : '/img/logo.png'}}" alt="Logo Kiri" class="img-fluid bg-secondary p-2 my-2">
                        @else
                            <div class="bg-secondary mb-2 tengahin" style="width:100%;min-height:500px;">
                              Belum ada Logo/Gambar
                            </div>
                        @endif
                        
                        <form action="{{ route('core.upload.gambar') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                          <div class="input-group">
                            <input type="hidden" name="tipe" value="Logo Aplikasi Santri">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" id="logoAplikasiSantri"  name="gambar" onchange="setText('logoAplikasiSantri','textlogoAplikasiSantri')">
                              <label class="custom-file-label" for="logoAplikasiSantri" id="textlogoAplikasiSantri">Pilih File</label>
                            </div>
                            <div class="input-group-append">
                              <button class="input-group-text bg-primary-c" >Simpan</button>
                            </div>
                          </div>
                        </form>
                      </div>

                      <div class="col-md-6">
                        <div class="h5">Splashscreen Aplikasi Santri</div>

                        @if ($data->app_santri_splashscreen)  
                          <div class="alert alert-success p-2 d-none" id="alertKanan">
                          </div>
                          <img src="{{ $data->app_santri_splashscreen ? '/images/custom/'.$data->right_logo : '/img/logo.png'}}" alt="Logo Kiri" class="img-fluid bg-secondary p-2 my-2">
                        @else
                            <div class="bg-secondary mb-2 tengahin" style="width:100%;min-height:500px;">
                              Belum ada Logo/Gambar
                            </div>
                        @endif
                        
                        <form action="{{ route('core.upload.gambar') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                          <div class="input-group">
                            <input type="hidden" name="tipe" value="Splashscreen Aplikasi">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" id="splashScreen"  name="gambar" onchange="setText('splashScreen','textsplashScreen')">
                              <label class="custom-file-label" for="splashScreen" id="textsplashScreen">Pilih File</label>
                            </div>
                            <div class="input-group-append">
                              <button class="input-group-text bg-primary-c" >Simpan</button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                    <form  method="POST" action="/settings">
                      @csrf     
                         
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <label for="">Nama Aplikasi Santri</label>
                              <input type="text" class="form-control" name="nama" value="{{$data->nama ? $data->nama : 'SANTREN.ID'}}">
                            </div>
                          </div>
                          <div class="col">
                            <div class="form-group">
                              <label for="">Nama Singkatan Aplikasi Santri</label>
                              <input type="text" class="form-control" name="nama" value="{{$data->nama ? $data->nama : 'SANTREN.ID'}}">
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="">Detail Aplikasi Santri</label>
                          <textarea rows="5" class="form-control" name="alamat">{{$data->alamat ? $data->alamat : 'Lorem ipsum dolor sit amet.'}}</textarea>
                        </div>
                        

                        <button type="submit" class="btn btn-block btn-primary mt-3">
                          <i class="fa fa-save"></i> Simpan
                        </button>
                    </form>




                </div>
            </div>

         </div>
     </div>
    
  </div>
</div>
</div>


@endsection


@section('js-after')

<script>


var tab = "{{session('tab') ? session('tab') : 'info'}}";
//  let tab

switch (tab) {
  case 'gambar':
    document.getElementById('tabGambar').classList.add('active');
    document.getElementById('panelGambar').classList.add('active','show');
    break;
  case 'metode':
    document.getElementById('tabMetode').classList.add('active');
    document.getElementById('panelMetode').classList.add('active','show');
    break;

  default:
    document.getElementById('tabInfo').classList.add('active')
    document.getElementById('panelInfo').classList.add('active','show');
    break;
}


function setStatusLogo(id, al){
  let stat = document.getElementById(id).checked
  let alertin = document.getElementById(al)
  let _token   = document.querySelector('input[name=_token]').value
  console.log(stat);
  $.ajax({
        url: "/settings/ubah-status-logo-laporan",
        type:"POST",
        data:{
          status:stat,
          tipe:id,
          _token: _token
        },
        success:function(response){
          alertin.innerHTML = response
          alertin.classList.add("d-block");

          setTimeout(() => {
            alertin.classList.remove("d-block");
          }, 3000);
          
        },
       });
}


function setText(id,idText){
  var name = document.getElementById(id).files.item(0).name; 
  document.getElementById(idText).innerHTML = name
}

function ubahin(){
  let switchin = document.getElementById('type_dashboard').checked;
  console.log(switchin);
  document.getElementById('label_type_dashboard').innerHTML = 'Basic Dashoboard'
  if (switchin) {
    document.getElementById('label_type_dashboard').innerHTML = 'Advenced Dashoboard'
  }
}

ubahin()


  
</script>


@endsection