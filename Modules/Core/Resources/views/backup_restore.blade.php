@extends('layouts.app')

@section('title')
      Settings
@endsection

@section('css-after')
<style>
  .tengahin{
  display: flex;
  justify-content: center;
  align-items: center;
}
.tableFixHead          { overflow: auto; height: 100px; }
.tableFixHead thead th { position: sticky; top: 0; z-index: 1; }

/* Just common table stuff. Really. */
table  { border-collapse: collapse; width: 100%; }
th, td { padding: 8px 16px; }
th     { background:#eee; }
</style>
<link rel="stylesheet" href="http://127.0.0.1:8001/admin/LTE/plugins/iCheck/square/blue.css">
  <link rel="stylesheet" href="http://127.0.0.1:8001/admin/LTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
@endsection
@section('content')
<div class="container" style="min-height: 600px;">
<div class="row">
    <div class="col-12">
        <div class="card">
        <div class="card-header">
            <h3 class="card-title">Backup & Restore Database</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body"  id="pjax-container">
            <div class="btn-group mb-2">
                <button type="button" class="btn btn-default btn-flat" style="cursor: default;"><i class="fa fa-save mr-2"></i> Backup</button>
                <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" data-loading-text="Tunggu..">
                  {{-- <span class="sr-only">Toggle Dropdown</span> --}}
                </button>
                <ul class="dropdown-menu keep-open" role="menu" style="min-width:450px;">
                  <li style="height: 200px; overflow:auto; padding:10px;" class="table-responsive">
                    <table class="table table-bordered table-hover">
                      <tr><th></th><th>Table</th><th>Rows</th><th>Size</th></tr>
                      @foreach ($tableInfo as $table)
                      <tr class="p-1">
                        <td class="checkbox icheck p-1">
                            <input type="checkbox"  class="column-select-item table-process" data-id="{{ $table->table_name }}" checked/>
                        </td>
                        <td class="p-1">
                          {{ $table->table_name }}
                        </td>
                        <td class="p-1">
                          {{ $table->table_rows }}
                        </td>
                        <td class="p-1">
                          {{ number_format($table->data_length/1048576, 4) }}MB
                        </td>
                      </tr>
                    @endforeach
                    </table>
                  </li>
                  <li class="divider" style="border-top: 1px solid #eaeae1; padding-bottom: 5px;">
                  </li>
                  <li>
                        <div class="input-group mb-1 p-2">
                            <input type="text" class="form-control" id="file-name" value="" placeholder="Nama file">
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon2">.sql</span>
                            </div>
                        </div>
                        {{-- <div class="float-right">
                        <button class="btn btn-sm btn-default column-un-select-all">X</button>&nbsp;
                        <button class="btn btn-sm btn-default column-select-all">All</button>&nbsp;
                        <button class="btn btn-sm btn-success column-select-submit" onClick="generateBackup($(this));" id="generate">Submit</button>
                        </div> --}}
                        <div class="btn-toolbar w-100 p-2" role="toolbar" aria-label="Toolbar with button groups">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <button class="btn btn-secondary column-un-select-all">Bersihkan</button>
                                <button class="btn btn-warning column-select-all">Pilih Semua</button>
                            </div>
                            <div class="btn-group flex-grow-1 ml-3" role="group" aria-label="Basic example">
                                <button class="btn btn-success column-select-submit " onClick="generateBackup($(this));" id="generate">Backup</button>
                            </div>
                        </div>

                    </li>
              </ul>
              </div>
            

            <div class="table-responsive">
            <table id="main-table" class="table table-hover box-body text-wrap table-bordered">
            <thead>
            <tr>
                <th class="text-center" style="width:1px;">No</th>
                <th>Tanggal</th>
                <th>Nama File</th>
                <th style="width:1px;    white-space: nowrap;">Ukuran File</th>
                <th class="text-center" colspan="3">Aksi</th>
            </tr>
            </thead>
            <tbody>
                @php
                    $sort = count($arrFiles)
                @endphp
                @foreach ($arrFiles as $file)
                <tr>
                    <td class="text-center">{{($sort--) }}</td>
                    <td>{{ tgl_indo_dt($file['time'])}}</td>
                    <td>{{ $file['name']}}</td>
                    <td class="text-center">{{ $file['size']}}</td>
                    <td class="text-center px-1">{!! '<a href="?download='.$file['name'].'"><button title="Unduh" data-loading-text="Tunggu..." class="btn btn-sm btn-primary"><i class="fas fa-download"></i> Unduh</button ></a>' !!}</td>
                    <td class="text-center px-1">{!! '<button  onClick="processBackup($(this),\''.$file['name'].'\',\'remove\');" title="Hapus" data-loading-text="Tunggu..." class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-trash"></span> Hapus</button >' !!}</td>
                    <td class="text-center px-1">{!! '<button  onClick="processBackup($(this),\''.$file['name'].'\',\'restore\');" title="Pulihkan" data-loading-text="Tunggu..." class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-retweet"></span> Pulihkan</button >' !!}</td>
                </tr>
                @endforeach
            </tbody>
            </table>
            </div>
        </div>
        <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
</div>


@endsection


@section('js-after')
<script src="{{url('/js/icheck.min.js')}}"></script>
<script src="{{url('/').'/js/sweetalert2.all.min.js'}}"></script>
<script type="text/javascript">
    function alertJs(type = 'error', msg = '') {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });
      Toast.fire({
        type: type,
        title: msg
      })
    }
  function processBackup(obj,file,action) {
    //   console.log("action", action);
    //   return false;
      let a = {
          title: 'Anda yakin untuk menghapus data Backup ini?',
          text: "Anda tidak akan bisa mengembalikan file backup yang telah dihapus",
          confirmButtonText: 'Ya, Hapus',          
      }
      let b = {
          title: 'Anda yakin untuk Memulihkan data Backup ini?',
          text: "Jika ada data pada table, maka data tersebut akan diganti sesuai data pada file Backup",
          confirmButtonText: 'Ya, Pulihkan',          
      }

      let info = (action == 'remove') ? a : b

      Swal.fire({
        title: info.title,
        text: info.text,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: info.confirmButtonText,
        cancelButtonText: 'Batalkan'
      }).then((result) => {
        if (result.value) {
          $('#loading').show()
          obj.button('loading');
          $.ajax({
            type: 'POST',
            dataType:'json',
            url: '{{ route('backup.process') }}',
            data: {
              "_token": "{{ csrf_token() }}",
              "file":file,
              "action":action,
            },
            success: function (response) {
              // console.log(response);
              if(parseInt(response.error) ==0){
                  alertJs('success', response.msg);
                  location.reload();
              }else{
                alertJs('error', response.msg);
              }
              $('#loading').hide();
              obj.button('reset');
            }
          });
        }
      })
  }

  function generateBackup(obj) {
      $('#loading').show()
      obj.button('loading');
      var selected = [];
      $('.table-process:checked').each(function(){
          selected.push($(this).data('id'));
      });
      var includeTables = selected.join();

      $.ajax({
        type: 'POST',
        dataType:'json',
        url: '{{ route('backup.generate') }}',
        data: {
          "_token": "{{ csrf_token() }}",
          "includeTables": includeTables,
          "fileName": $('#file-name').val(),
        },
        success: function (response) {
          if(parseInt(response.error) ==0){
                Swal.fire(
                'Success!',
                '',
                'success'
                );
                location.reload();
          }else{
            Swal.fire(
              response.msg,
              'You clicked the button!',
              'error'
              )
          }
          $('#loading').hide();
          obj.button('reset');
        }
      });

  }

    $('.column-select-all').on('click', function () {
      $('.column-select-item').iCheck('check');
      return false;
    });

    $('.column-un-select-all').on('click', function () {
      $('.column-select-item').iCheck('uncheck');
      return false;
    });

</script>

@endsection