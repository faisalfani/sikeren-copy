@foreach ($role as $key => $u)            
    <div id="role-{{ $u->id }}" class="list-group-item list-group-item-action">
        <div class="row">
        <div class="col-md-1">
                {{ $role->firstItem() + $key }}
        </div>
        <div class="col-md-5">
            {{ $u->nama }}
        </div>
        
        <div class="col-md-6 text-right">
        <a onclick="editRole('{{ $u->menu }}','{{ $u->id}}','{{$u->nama}}')" href="#" class="btn btn-sm bg-primary-c text-light">
                <i class="fa fa-object-ungroup"></i>
            </a>
            <a onclick="deleteRole('{{ $u->id }}')" href="#" class="btn btn-sm btn-outline-danger">
                <i class="fa fa-trash"></i>
            </a>
        </div>
        </div>
    </div>
@endforeach