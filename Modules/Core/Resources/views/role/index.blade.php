@extends('layouts.app')

@section('title')
      Users
@endsection


@section('content')
<div class="card container card-success">
  <div class="card-body">


<div class="row">
  <div class="col-md-12">
     

    <!-- Button trigger modal -->
    <div class="row">
      
          <div class="col-md-6 mb-2">
              <button type="button" class="btn text-white bg-primary-c" onclick="createRole()">
                  <i class="fa fa-plus"></i> 
                Tambah 
              </button>
              {{-- <button type="button" class="btn text-white bg-primary-c" data-toggle="modal" data-target="#modalForm">
                  Filter
              </button> --}}
          </div>
          <div class="col-md-6 mb-2 input-group float-right">
            <input id="search" placeholder="Cari" type="text" class="form-control">
            <div class="input-group-append">
              <button onclick="location.reload()" class="btn text-white bg-primary-c" type="button">
                <i class="fa fa-sync"></i>
              </button>
            </div>
          </div>
      </div>

    <div class="list-group pt-4">
        <div class="w-100">
            @include('comp.notif')
        </div>
        <div class="list-group-item bg-primary-c">
          <div class="row">
            <div class="col-md-1">
               No
            </div>
            <div class="col-md-5">
                Nama
             </div>
            <div class="col-md-6 text-right">
                Aksi
            </div>
          </div>
        </div>
        
        <div id="list-post-data">
          
          <div id="post-data">
            @include('core::role.data')
          </div>

        </div>
       
      </div>

      <div class="row">
        <div class="col-md-12 text-center pt-4">
          <div onclick="loadMore()" id="btn-load-more" class="btn btn-flat text-primary">
            <i class="fa fa-arrow-down"></i>
          </div>
        </div>
      </div>

      
</div>
</div>
</div>

</div>
</div>


<form  method="POST" action="/role">
  @csrf      
      <!-- Modal -->
      <div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="modalFormTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
          <div class="modal-content text-dark">
            <div class="modal-header">
              <h5>Hak Akses (Roles) </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">


                    <div class="form-group row">
                        <label for="nama" class="col-md-12">{{ __('Nama') }}</label>

                        <div class="col-md-12">
                            <input placeholder="Nama Hak Akses" id="nama" type="text" class="form-control" name="nama" required autofocus>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="nama" class="col-md-12">{{ __('Pilih Menu') }}</label>                      
                      <div class="col-md-12">
                        







<?php
    
function createMenu($m,$key){
  ?>
  <div class="card px-2">
      <div class="row p-2">
          <div class="col-md-3">

            <i class="@if($m->icon) {{ $m->icon }} @else fa fa-stream @endif mr-2"></i>  
            {{-- {{ $m->id }} /  --}}
            {{ $m->nama }}
          </div>
          <a target="_BLANK" href="{{ $m->url }}" class="col-md-3">
              {{ $m->url }}
          </a>
          <div class="col-md-6 text-right ">
              <div class="custom-control custom-switch custom-switch-off-secondary custom-switch-on-success">
                  <input name="menu[]" value="{{ $m->id }}" type="checkbox" class="custom-control-input menu-input" id="menu-{{ $m->id }}">
                  <label class="custom-control-label" for="menu-{{ $m->id }}"></label>
              </div>
          </div>
      </div>
    </div>
  <?php
}
?>
@php
    $menu = App\menu::all();
@endphp
<ul>
@foreach ($menu->where("sub",null) as $key => $m)            
            <li>
                
                @php
                    createMenu($m,$key);
                    $submenu = $menu->where("sub",$m->id);
                @endphp
                     
                @if ($submenu->count() > 0)
                   <ul>
                       <li>
                           @foreach($submenu as $key => $sub)
                              @php
                                    createMenu($sub,$key);
                                    $submenu1 = $menu->where("sub",$sub->id);
                              @endphp
                                  @if ($submenu1->count() > 0)
                                  <ul>
                                      <li>
                                          @php
                                          foreach ($submenu1 as $key => $sub1) {
                                              createMenu($sub1,$key);
                                          }
                                          @endphp
                                      </li>
                                  </ul> 
                                  @endif     
                           @endforeach
                       </li>
                   </ul> 
                @endif     
            </li>
@endforeach

</ul>



                      </div>
                  </div>

                
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">
                  <i class="fa fa-times"></i>
              </button>
              <button id="btn-create" type="submit" class="btn bg-primary-c">
                  <i class="fa fa-plus"></i>                   
                  {{ __('Tambah Role') }}</button>
                  <button id="btn-update" type="button" class="btn bg-primary-c">
                      <i class="fa fa-save"></i>                   
                      {{ __('Simpan') }}
                  </button>
            </div>
          </div>
        </div>
      </div>

    </form>
@endsection


@section('js-after')

<script type="text/javascript">



  var page = 1;
  var where = "";  


  function loadMore(){
    page++;
    loadMoreData(page);
  }

  $("#btn-load-more").hover(() => {
    loadMore();
  });

  $("#search").keyup(function() {
    where = $(this).val();
    page = 1;
    if(where !== ''){
      loadMoreData(page);
    }
  });

	function loadMoreData(page){
	  $.ajax(
	        {
	            url: '?where='+where+'&page=' + page,
	            type: "get",
	            beforeSend: function()
	            {
	                $('.ajax-load').show();
	            }
	        })
	        .done(function(data)
	        {
	            if(data.html == " "){
	                $('.ajax-load').html("No more records found");
	                return;
	            }
	            $('.ajax-load').hide();

              if(where !== '' && page == 1){
                $("#post-data").html(data.html);
              }else{
                $("#post-data").append(data.html);
              }
	            
	        })
	        .fail(function(jqXHR, ajaxOptions, thrownError)
	        {
	              alert('server not responding...');
	        });
	}


  function createRole(){
    $("#name").val('');
    $("#username").val('');
    $("#email").val('');
    $("#modalForm").modal("show");
    $("#btn-create").show();
    $("#btn-update").hide();
  }

  function editRole(data,id,nama){
    
    $("#nama").val(nama);

    var menuu = JSON.parse(data);

    $(".menu-input").removeAttr("checked","");

    for(var i = 0; i < menuu.length; i++){
      $("#menu-"+menuu[i]).attr("checked","checked");
    }

    $("#modalForm").modal("show");
    $("#btn-create").hide();
    $("#btn-update").show();


    $("#btn-update").click(function(){


      var val = [];
      $(':checkbox:checked').each(function(i){
        val[i] = $(this).val();
      });

      // console.log(val)

      var _data = {
        nama : $("#nama").val(),
        menu : val,
        _method: 'PUT',
        _token : '{{ csrf_token() }}'
      }
          $.ajax({
            type: "PUT",
            url: '/role/' + id,
            data: _data,
            success: function (data) {
              
              location.reload()

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    })

  }

  function deleteRole(id){
    // console.log(id);

    
    swal({
      title: "Apakah Kamu Yakin?",
      text: "Pastikan data yang akan dihapus sudah valid",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Hapus",
      closeOnConfirm: false
    },
    function(){
        $.ajax({
            type: "DELETE",
            url: '/role/' + id,
            data: {_method: 'delete', _token : '{{ csrf_token() }}'},
            success: function (data) {
              location.reload();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
  }
</script>


@endsection