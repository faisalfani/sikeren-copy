@foreach ($users as $key => $u)            
    <div id="user-{{ $u->id }}" class="list-group-item list-group-item-action">
        <div class="row">
        <div class="col-md-1">
                {{ $users->firstItem() + $key }}
        </div>
        <div class="col-md-3">
            {{ $u->name }}
        </div>
        <div class="col-md-3">
            {{ '@'.$u->username }} <br> {{ $u->email }}
        </div>
        <div class="col-md-2">
            {{ $u->role_id }}
        </div>
        <div class="col-md-3 text-right">
            <a onclick="editUser('{{ $u }}')" href="#" class="btn btn-sm bg-primary-c text-light">
                <i class="fa fa-edit"></i>
            </a>
            <a onclick="deleteUser('{{ $u->id }}')" href="#" class="btn btn-sm btn-outline-danger">
                <i class="fa fa-trash"></i>
            </a>
        </div>
        </div>
    </div>
@endforeach