@extends('layouts.app')

@section('title')
      Users
@endsection


@section('content')
<div class="container">
<div class="row ">
  <div class="col-md-12">
     

    <!-- Button trigger modal -->
    <div class="row">
      
          <div class="col-md-6 mb-2">
              <button type="button" class="btn text-white bg-primary-c" onclick="createUser()">
                  <i class="fa fa-plus"></i> 
                Tambah User
              </button>
              {{-- <button type="button" class="btn text-white bg-primary-c" data-toggle="modal" data-target="#modalForm">
                  Filter
              </button> --}}
          </div>
          <div class="col-md-6 mb-2 input-group float-right">
            <input id="search" placeholder="Cari User" type="text" class="form-control">
            <div class="input-group-append">
              <button onclick="location.reload()" class="btn text-white bg-primary-c" type="button">
                <i class="fa fa-sync"></i>
              </button>
            </div>
          </div>
      </div>

    <div class="list-group pt-4">
        <div class="w-100">
            @include('comp.notif')
        </div>
        <div class="list-group-item bg-primary-c">
          <div class="row">
            <div class="col-md-1">
               No
            </div>
            <div class="col-md-3">
                Nama
             </div>
            <div class="col-md-3">
                Username <br> Email
            </div>
            <div class="col-md-2">
                Roles
            </div>
            <div class="col-md-3 text-right">
                Aksi
            </div>
          </div>
        </div>
        
        <div id="list-post-data">
          
          <div id="post-data">
            @include('core::users.data')
          </div>

        </div>
       
      </div>

      <div class="row">
        <div class="col-md-12 text-center pt-4">
          <div onclick="loadMore()" id="btn-load-more" class="btn btn-flat text-primary">
            <i class="fa fa-arrow-down"></i>
          </div>
        </div>
      </div>

      
</div>
</div>
</div>

</div>
<form  method="POST" action="/users">
  @csrf      
      <!-- Modal -->
      <div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="modalFormTitle" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-scrollable" role="document">
          <div class="modal-content text-dark">
            <div class="modal-header">
              <h5>Users</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

                 
                  <div class="form-group row">
                      <label for="username" class="col-md-12">{{ __('Username') }}</label>

                      <div class="col-md-12">
                          <input placeholder="Username" id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>

                          @error('username')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                      </div>
                  </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-12">{{ __('Name') }}</label>

                        <div class="col-md-12">
                            <input placeholder="Enter Name" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-12">{{ __('E-Mail Address') }}</label>

                        <div class="col-md-12">
                            <input placeholder="Enter Email" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-12">{{ __('Password') }}</label>

                        <div class="col-md-12">
                            <input placeholder="Enter Password" id="password" type="text" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    @if (Auth::user()->role_id == 1)
                        
                    <div class="form-group">
                      <label class="col-md-12">
                        Hak Akses
                      </label>
                      <select name="role_id" class="form-control" id="role_id">
                        <option value="">Pilih Role</option>
                        @foreach (App\role::all() as $item)
                            <option value="{{ $item->id }}">{{ $item->nama }}</option>                            
                        @endforeach
                      </select>
                    </div>
                
                    @endif
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">
                  <i class="fa fa-times"></i>
              </button>
              <button id="btn-create" type="submit" class="btn bg-primary-c">
                  <i class="fa fa-plus"></i>                   
                  {{ __('Tambah User') }}</button>
                  <button id="btn-update" type="button" class="btn bg-primary-c">
                      <i class="fa fa-save"></i>                   
                      {{ __('Simpan') }}
                  </button>
            </div>
          </div>
        </div>
      </div>

    </form>
@endsection


@section('js-after')

<script type="text/javascript">



  var page = 1;
  var where = "";  


  function loadMore(){
    page++;
    loadMoreData(page);
  }

  $("#btn-load-more").hover(() => {
    loadMore();
  });

  $("#search").keyup(function() {
    where = $(this).val();
    page = 1;
    if(where !== ''){
      loadMoreData(page);
    }
  });

	function loadMoreData(page){
	  $.ajax(
	        {
	            url: '?where='+where+'&page=' + page,
	            type: "get",
	            beforeSend: function()
	            {
	                $('.ajax-load').show();
	            }
	        })
	        .done(function(data)
	        {
	            if(data.html == " "){
	                $('.ajax-load').html("No more records found");
	                return;
	            }
	            $('.ajax-load').hide();

              if(where !== '' && page == 1){
                $("#post-data").html(data.html);
              }else{
                $("#post-data").append(data.html);
              }
	            
	        })
	        .fail(function(jqXHR, ajaxOptions, thrownError)
	        {
	              alert('server not responding...');
	        });
	}


  function createUser(){
    $("#name").val('');
    $("#username").val('');
    $("#email").val('');
    $("#modalForm").modal("show");
    $("#btn-create").show();
    $("#btn-update").hide();
  }

  function editUser(data){
    var d = JSON.parse(data);
    console.log(d);
    $("#name").val(d.name);
    $("#username").val(d.username);
    $("#email").val(d.email);
    $("#role_id").val(d.role_id);
    $("#modalForm").modal("show");
    $("#btn-create").hide();
    $("#btn-update").show();


    $("#btn-update").click(function(){

      var _data = {
        name : $("#name").val(),
        username : $("#username").val(),
        email: $("#email").val(),
        password: $("#password").val(),
        role_id: $("#role_id").val(),
        _method: 'PUT',
        _token : '{{ csrf_token() }}'
      }
          $.ajax({
            type: "PUT",
            url: '/users/' + d.id,
            data: _data,
            success: function (data) {
              
              location.reload()

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    })

  }

  function deleteUser(id){

    swal({
      title: "Apakah Kamu Yakin?",
      text: "Pastikan data yang akan dihapus sudah valid",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Hapus",
      closeOnConfirm: false
    },
    function(){
        // console.log(id);
        $.ajax({
            type: "DELETE",
            url: '/users/' + id,
            data: {_method: 'delete', _token : '{{ csrf_token() }}'},
            success: function (data) {
                location.reload();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

    });
  }

  
</script>


@endsection