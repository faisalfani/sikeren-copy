<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Entities\CoreInfo;

class CoreSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tahun = date("Y");
        CoreInfo::create([
            'id' => '1',
            'alamat' => 'Tasikmalaya, Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quaerat, quisquam.',
            'copyright_before_login' => 'Copyright © 2019 - '.$tahun.'. Before Login',
            'copyright_after_login' => 'Copyright © 2019 - '.$tahun.'. After Login',

        ]);

        // $this->call("OthersTableSeeder");
    }
}
