<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama', 100)->default('CV Alphabet');
            // $table->text('deskripsi')->default('Sistem Keuangan Pesantren');
            $table->text('alamat');
            $table->string('no_telp', 20)->default('0853 2013 2014');
            $table->text('copyright_before_login');
            $table->text('copyright_after_login');
            $table->string('color_primary', 10)->default('#00695C');
            $table->string('color_secondary', 10)->default('#4CAF50');
            $table->boolean('metode_bri')->default(false);
            $table->boolean('metode_bri_syariah')->default(false);
            $table->boolean('metode_payment_gateway')->default(false);

            $table->string('logo', 10)->nullable();
            $table->string('logo_after_login', 10)->nullable();
            $table->string('favicon', 10)->nullable();
            $table->string('left_logo', 10)->nullable();
            $table->boolean('left_logo_status')->default(false);
            $table->string('right_logo', 10)->nullable();
            $table->boolean('right_logo_status')->default(false);
            $table->string('walpaper_image', 10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_infos');
    }
}
