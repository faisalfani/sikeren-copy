<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/searchKelas', 'GenerateBiayaController@searchKelas');
Route::get('/rincianBiaya/select2', 'RincianBiayaController@rincianBiayaSelect2');

Route::middleware("auth")->prefix('keuangan')->group(function () {
    Route::get('/', 'KeuanganController@index');

    Route::resource('/jenis-biaya', 'JenisBiayaController');
    Route::get('/jenis-biaya/json/data', 'JenisBiayaController@json');
    Route::get('/jenis-biaya/json/data/select2', 'JenisBiayaController@json_select2');

    Route::resource('/master-biaya', 'MasterBiayaController');
    Route::get('/master-biaya/json/data', 'MasterBiayaController@json');

    Route::resource('/rincian-biaya', 'RincianBiayaController');
    Route::get('/rincian-biaya/json/data/{id}', 'RincianBiayaController@json');
    Route::get('/rincian-biaya/edit/{id}', 'RincianBiayaController@editData');
    Route::post('/rincian-biaya/edit', 'RincianBiayaController@edit');


    Route::resource('/generate-biaya', 'GenerateBiayaController');

    Route::resource('/biaya-siswa', 'BiayaSiswaController');
    Route::get('/biaya-siswa/json/data', 'BiayaSiswaController@json');



    Route::get("/idkelas-to-text/{id}", 'RincianBiayaController@idkelasTotext');
});
