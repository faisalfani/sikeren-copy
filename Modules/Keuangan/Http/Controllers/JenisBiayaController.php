<?php

namespace Modules\Keuangan\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use yajra\Datatables\Datatables;
use DB;
use Modules\Keuangan\Entities\JenisBiaya;

class JenisBiayaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('keuangan::jenisBiaya');
    }
    public function json_select2(){

        return json_encode(JenisBiaya::all());
    }
    public function json(){
        $data = JenisBiaya::all();
        return Datatables::of($data)->make(true);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('keuangan::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $req)
    {
        //
        $data = new JenisBiaya;
        $data->nama = $req->nama;
        $data->save();

        return back()->with("success","Data Berhasil disimpan");
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('keuangan::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('keuangan::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $req, $id)
    {
        //
        $data = JenisBiaya::find($id);
        $data->nama = $req->nama;
        $data->save();
        return "success";
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $data = JenisBiaya::find($id)->delete();
    }
}
