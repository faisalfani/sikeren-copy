<?php

namespace Modules\Keuangan\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use yajra\Datatables\Datatables;
use DB;
use Illuminate\Support\Facades\DB as FacadesDB;
use Modules\Keuangan\Entities\JenisBiaya;
use Modules\Keuangan\Entities\MasterBiaya;
use Modules\Keuangan\Entities\RincianBiaya;

class RincianBiayaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function json($id)
    {
        $data = DB::table("rincian_biayas as r")
            ->join("jenis_biayas as j", "r.jenis_biaya_id", "j.id")
            ->select("r.*", 'j.nama as nama')
            ->where("r.master_biaya_id", $id)
            ->get();
        return Datatables::of($data)
            ->addColumn('kelas_text', function ($d) {

                $kelas =  explode(',', $d->kelas);

                $res = array();
                foreach ($kelas as $kk) {
                    $kkks =  DB::table("kelas")->where("id", $kk)->first();
                    if ($kkks) {
                        array_push($res, $kkks->nama);
                    }
                }
                return  $res;
            })
            ->rawColumns(['kelas_text'])
            ->toJson();
    }

    public function editData($id)
    {
        $data = RincianBiaya::find($id);
        $masterData  = MasterBiaya::find($data->master_biaya_id);

        $res = array($data, $masterData);
        return json_encode($res);
    }


    public function index()
    {
        return view('keuangan::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('keuangan::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $req)
    {
        //
        // dd($req);
        $data = new RincianBiaya;
        $data->master_biaya_id = $req->r_id;
        $data->jenis_biaya_id = $req->r_jenis_biaya;
        $data->kelas = implode(",", $req->r_kelas);;
        $data->jenis_siswa = $req->baru_lama ? $req->baru_lama : '2';
        $data->satuan = $req->r_satuan;
        $data->prioritas = $req->r_prioritas;
        $data->jk = $req->r_jk;
        $data->nominal = str_replace('.', '', $req->r_nominal);
        $data->status = $req->r_status;
        $data->beasiswa = $req->r_beasiswa;
        $data->save();
        return "success";
    }

    public function edit(Request $req)
    {
        //
        // dd($req);
        $data = RincianBiaya::find($req->r_id_e);
        $data->jenis_biaya_id = $req->r_jenis_biaya_e;
        $data->kelas = implode(",", $req->r_kelas_e);
        $data->satuan = $req->r_satuan_e;
        $data->jenis_siswa = $req->baru_lama;
        $data->prioritas = $req->r_prioritas_e;
        $data->jk = $req->r_jk_e;
        $data->nominal = str_replace('.', '', $req->r_nominal_e);
        $data->update();
        return "success";
    }

    public function rincianBiayaSelect2(Request $req)
    {
        $masterBiaya = MasterBiaya::where('tingkat', $req->tingkat)->first();

        $data = FacadesDB::table("rincian_biayas as rb")
            ->leftJoin('jenis_biayas as jb', 'rb.jenis_biaya_id', 'jb.id')
            ->where('rb.master_biaya_id', $masterBiaya->id)
            ->where('rb.satuan', 'Bebas')
            // ->whereRaw('jb.nama LIKE "%' . $req->q . '%"')
            ->select('jb.*', 'rb.id as rincian_biaya_id')

            ->get();


        return $data;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('keuangan::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */


    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $data = RincianBiaya::find($id)->delete();
        return "success";
    }

    function idkelasTotext($id)
    {
        return $id;
    }
}
