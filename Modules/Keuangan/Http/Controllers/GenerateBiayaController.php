<?php

namespace Modules\Keuangan\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use DB;
use Illuminate\Support\Facades\DB as FacadesDB;
use Modules\Keuangan\Entities\JenisBiaya;
use Modules\Siswa\Entities\Siswa;
use Modules\Master\Entities\Kelas;
use Modules\Keuangan\Entities\MasterBiaya;
use Modules\Keuangan\Entities\RincianBiaya;
use Modules\Keuangan\Entities\BiayaSiswa;

class GenerateBiayaController extends Controller
{

    public function searchKelas(Request $req)
    {
        if ($req->has('q')) {
            $cari = $req->q;
            $data = Kelas::select('id', 'nama')
                ->whereRaw('nama LIKE "%' . $cari . '%"')
                ->get();
            return $data;
        } else {
            return "gagal";
        }
    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('keuangan::generateBiaya');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('keuangan::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $req)
    {
        $report = [];

        foreach ($req->bulan as $key => $value) {
            $bulan = $value;
            $namaBulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

            try {

                // return $bulan;
                if ($bulan == 7) {
                    $biaya = DB::table("rincian_biayas as r")
                        ->join("jenis_biayas as j", "r.jenis_biaya_id", "j.id")
                        ->select("r.*", 'j.nama as nama')
                        ->where("r.satuan", 'Daftar Ulang')
                        ->get();
                } else {
                    $biaya = DB::table("rincian_biayas as r")
                        ->join("jenis_biayas as j", "r.jenis_biaya_id", "j.id")
                        ->select("r.*", 'j.nama as nama')
                        ->where("r.satuan", 'Bulanan')
                        ->get();
                }


                if ($req->nis) {
                    $siswa = Siswa::where("nis", $req->nis)->where("status", 1)->get();
                } else {

                    if ($req->jenjang) {


                        if ($req->jenjang == 'MA') {
                            $kel = 10;
                        } else {
                            $kel = 7;
                        }



                        if ($req->kelas_khusus != "") {

                            $kelas = kelas::find($req->kelas_khusus);

                            $siswa = Siswa::select("nis")
                                ->where("jenjang", $req->jenjang)
                                ->where("status", 1)
                                ->where("id_kelas", $kelas->id)->get();

                            $result = [
                                "persiswa" => true,
                                "data" => $siswa,
                                "bulan" => $namaBulan[$bulan - 1],
                                "jenis_biaya_bebas" => $req->jenis_biaya_bebas,
                                "z" => 0
                            ];
                            array_push($report, $result);
                            continue;
                        } else if ($req->kelas == 100) {


                            $siswa = Siswa::select("nis")->where("status", 1)->get();

                            $result =  [
                                "persiswa" => true,
                                "data" => $siswa,
                                "bulan" => $namaBulan[$bulan - 1],
                                "jenis_biaya_bebas" => $req->jenis_biaya_bebas,
                                "z" => 1
                            ];
                            array_push($report, $result);
                            continue;
                        } else {


                            $kelas = kelas::where("tingkat", $req->kelas + $kel)
                                ->pluck('id')
                                ->all();
                            $siswa = Siswa::select("nis")
                                ->where("jenjang", $req->jenjang)
                                ->where("status", 1)
                                ->whereIn("id_kelas", $kelas)->take(10)->get();

                            $result = [
                                "persiswa" => true,
                                "data" => $siswa,
                                "bulan" => $namaBulan[$bulan - 1],
                                "jenis_biaya_bebas" => $req->jenis_biaya_bebas,
                                "z" => 2,

                            ];
                            // return $result;
                            array_push($report, $result);
                            continue;
                        }
                    } else {
                        $siswa = Siswa::select("nis")->where("status", 1)->get();

                        $result = [
                            "persiswa" => true,
                            "data" => $siswa,
                            "bulan" => $namaBulan[$bulan - 1],
                            "jenis_biaya_bebas" => $req->jenis_biaya_bebas,
                            "z" => 3
                        ];
                        array_push($report, $result);
                        continue;
                    }
                }

                $res = [];
                $s = $siswa[0];
                if ($s) {

                    foreach ($biaya as $b) {

                        $kelas =  explode(",", $b->kelas);

                        $lanjutkan = false;

                        foreach ($kelas as $k) {
                            if ($s->id_kelas == $k) {
                                $lanjutkan = true;
                            }
                        }


                        if ($lanjutkan == true) {

                            if ($s->beasiswa == $b->beasiswa) {
                                $bulan = $bulan;

                                if ($bulan < 7) {
                                    $tahun = $req->tahun + 1;
                                } else {
                                    $tahun = $req->tahun;
                                }



                                $daftarUlang = false;
                                $lolosCek = false;

                                if ($b->satuan == 'Daftar Ulang') {
                                    $cek = BiayaSiswa::where("nis", $s->nis)
                                        ->where("tahun_id", $tahun)
                                        ->where("rincian_biaya_id", $b->id)
                                        ->first();
                                    if (!$cek) {
                                        $lolosCek = true;
                                    }
                                    $daftarUlang = true;
                                } else if ($b->satuan == 'Bulanan') {
                                    $cek = BiayaSiswa::where("nis", $s->nis)
                                        ->where("tahun_id", $tahun)
                                        ->where("bulan", $bulan)
                                        ->where("rincian_biaya_id", $b->id)
                                        ->first();

                                    if (!$cek) {

                                        if ($bulan != 7) {
                                            $lolosCek = true;
                                        }
                                    }
                                }

                                if ($lolosCek == true) {

                                    $cekJk = false;

                                    if ($s->jk == 'L') {
                                        $jkk = 1;
                                    } else {
                                        $jkk = 2;
                                    }

                                    if ($b->jk == 0) {
                                        $cekJk = true;
                                    } else if ($b->jk == $jkk) {
                                        $cekJk = true;
                                    }

                                    if ($cekJk) {

                                        // Jenis biaya 1 = DU /  2 = DK
                                        // kode 0 DU /  kode 1 DK

                                        if ($b->jenis_biaya_id == 1 and $s->du_dk == 1) {
                                            $lanj = false;
                                        } else if ($b->jenis_biaya_id == 2 and $s->du_dk == 0) {
                                            $lanj = false;
                                        } else {
                                            $lanj = true;
                                        }

                                        if ($lanj == true) {

                                            if ($b->jenis_biaya_id == 5) {
                                                if ($s->laundry == 1) {
                                                    $NextLaundry = true;
                                                } else {
                                                    $NextLaundry = false;
                                                }
                                            } else {
                                                $NextLaundry = true;
                                            }

                                            if ($NextLaundry) {


                                                // Cek Siswa Baru
                                                // 2 Semua Siswa
                                                // 1 Baru
                                                // 0 Lama

                                                if ($b->jenis_siswa == 2) {
                                                    $NextSiswaBaru = true;
                                                } else if ($b->jenis_siswa == 1) {

                                                    if ($s->baru_lama == 1) {
                                                        $NextSiswaBaru = true;
                                                    } else {
                                                        // dd($s->baru_lama.$b->jenis_siswa);
                                                        $NextSiswaBaru = false;
                                                    }
                                                } else {
                                                    $NextSiswaBaru = true;
                                                }

                                                if ($NextSiswaBaru) {

                                                    if ($bulan < 7) {
                                                        $tahunn = $req->tahun + 1;
                                                    } else {
                                                        $tahunn = $req->tahun;
                                                    }

                                                    $new = new BiayaSiswa;
                                                    $new->nis = $s->nis;
                                                    $new->rincian_biaya_id = $b->id;
                                                    $new->jenis_biaya_id = $b->jenis_biaya_id;
                                                    $new->satuan = $b->satuan;
                                                    $new->prioritas = $b->prioritas;
                                                    $new->nominal = $b->nominal;
                                                    $new->nama = $b->nama;

                                                    if ($daftarUlang) {

                                                        $new->bulan = 7;
                                                    } else {
                                                        $new->bulan = $bulan;
                                                    }

                                                    $new->tahun_id = $tahunn;

                                                    if ($bulan < 7) {
                                                        $new->tahun_ajaran = $tahunn - 1;
                                                    } else {
                                                        $new->tahun_ajaran = $tahunn;
                                                    }

                                                    $new->save();





                                                    array_push($res, $new->idate);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if ($req->jenis_biaya_bebas) {
                        $biayaBebas = FacadesDB::table("rincian_biayas as rb")
                            ->leftJoin('jenis_biayas as jb', 'rb.jenis_biaya_id', 'jb.id')
                            ->where('rb.id', $req->jenis_biaya_bebas)
                            ->where('rb.satuan', 'Bebas')
                            ->select('rb.*', 'jb.nama as nama')
                            ->first();

                        $kelas =  explode(",", $biayaBebas->kelas);

                        $lanjutkan = false;

                        foreach ($kelas as $k) {
                            if ($s->id_kelas == $k) {
                                $lanjutkan = true;
                            }
                        }


                        if (
                            $lanjutkan == true
                        ) {

                            if (
                                $s->beasiswa == $biayaBebas->beasiswa
                            ) {

                                if ($bulan < 7) {
                                    $tahun = $req->tahun + 1;
                                } else {
                                    $tahun = $req->tahun;
                                }



                                $lolosCek = true;


                                if ($lolosCek == true) {

                                    $cekJk = false;

                                    if ($s->jk == 'L') {
                                        $jkk = 1;
                                    } else {
                                        $jkk = 2;
                                    }

                                    if ($biayaBebas->jk == 0) {
                                        $cekJk = true;
                                    } else if ($biayaBebas->jk == $jkk) {
                                        $cekJk = true;
                                    }

                                    if ($cekJk) {

                                        // Jenis biaya 1 = DU /  2 = DK
                                        // kode 0 DU /  kode 1 DK

                                        if ($biayaBebas->jenis_biaya_id == 1 and $s->du_dk == 1) {
                                            $lanj = false;
                                        } else if ($biayaBebas->jenis_biaya_id == 2 and $s->du_dk == 0) {
                                            $lanj = false;
                                        } else {
                                            $lanj = true;
                                        }

                                        if ($lanj == true) {

                                            if ($biayaBebas->jenis_biaya_id == 5) {
                                                if ($s->laundry == 1) {
                                                    $NextLaundry = true;
                                                } else {
                                                    $NextLaundry = false;
                                                }
                                            } else {
                                                $NextLaundry = true;
                                            }

                                            if ($NextLaundry) {


                                                // Cek Siswa Baru
                                                // 2 Semua Siswa
                                                // 1 Baru
                                                // 0 Lama

                                                if ($biayaBebas->jenis_siswa == 2) {
                                                    $NextSiswaBaru = true;
                                                } else if ($biayaBebas->jenis_siswa == 1) {

                                                    if ($s->baru_lama == 1) {
                                                        $NextSiswaBaru = true;
                                                    } else {
                                                        // dd($s->baru_lama.$biayaBebas->jenis_siswa);
                                                        $NextSiswaBaru = false;
                                                    }
                                                } else {
                                                    $NextSiswaBaru = true;
                                                }

                                                if ($NextSiswaBaru) {

                                                    if ($bulan < 7) {
                                                        $tahunn = $req->tahun + 1;
                                                    } else {
                                                        $tahunn = $req->tahun;
                                                    }


                                                    if ($bulan < 7) {
                                                        $tahunNews = $tahunn - 1;
                                                    } else {
                                                        $tahunNews = $tahunn;
                                                    }

                                                    $newBiayaBebas = BiayaSiswa::firstOrCreate([
                                                        "nis" => $s->nis,
                                                        "rincian_biaya_id" => $biayaBebas->id,
                                                        "jenis_biaya_id" => $biayaBebas->jenis_biaya_id,
                                                        "satuan" => $biayaBebas->satuan,
                                                        "prioritas" => $biayaBebas->prioritas,
                                                        "nominal" => $biayaBebas->nominal,
                                                        "nama" => $biayaBebas->nama,
                                                        "bulan" => $bulan,
                                                        "tahun_id" => $tahunn,
                                                        "tahun_ajaran" => $tahunNews

                                                    ]);

                                                    array_push($res, $newBiayaBebas->idate);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }



                    if (count($res) > 0) {
                        $result = [
                            "status" => "success",
                            "nama" => $s->nama, // Nama
                            "bulan" => $namaBulan[$bulan - 1]
                        ];
                    } else {
                        $result = [
                            "status" => "complite",
                            "nama" => $s->nama,
                            "bulan" => $namaBulan[$bulan - 1]
                        ];
                    }
                    array_push($report, $result);
                    continue;
                }

                //code...
            } catch (\Throwable $e) {

                $result = [
                    "status" => "error",
                    "nis" => $req->nis,
                    "e" => $e,
                    "bulan" => $namaBulan[$bulan - 1]
                ];

                array_push($report, $result);

                // return json_encode($result);
            }
        }

        return json_encode($report);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('keuangan::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('keuangan::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
