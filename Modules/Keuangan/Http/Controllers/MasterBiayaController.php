<?php

namespace Modules\Keuangan\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use yajra\Datatables\Datatables;
use DB;
use Modules\Keuangan\Entities\MasterBiaya;

class MasterBiayaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    
     public function json(){
        $data = MasterBiaya::all();
        return Datatables::of($data)->make(true);
    }

    public function index()
    {
        return view('keuangan::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('keuangan::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $req)
    {
        // 'nama','tingkat','angkatan','status'
        //
        $data = new MasterBiaya;
        $data->nama = $req->nama;
        $data->tingkat = $req->tingkat;
        $data->status = $req->status;
        $data->save();

        return back()->with("success","data berhasil ditambahkan");
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('keuangan::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('keuangan::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $req, $id)
    {
        
        //
        $data = MasterBiaya::find($id);
        $data->nama = $req->nama;
        $data->tingkat = $req->tingkat;
        $data->status = $req->status;
        $data->update();

        return "success";
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $data = MasterBiaya::find($id)->delete();
        return "success";
    }
}
