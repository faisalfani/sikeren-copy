<?php

namespace Modules\Keuangan\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use yajra\Datatables\Datatables;
use DB;
use Modules\Keuangan\Entities\JenisBiaya;
use Modules\Keuangan\Entities\BiayaSiswa;
use Modules\Keuangan\Entities\MasterBiaya;
use Modules\Keuangan\Entities\RincianBiaya;
use Modules\Siswa\Entities\Siswa;

class BiayaSiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('keuangan::biayaSiswa');
    }

    public function json(Request $req){
        // $search = $req->search['value'];
        // $search = "Aa Darmawan";
        // return $a;
        $data = BiayaSiswa::
                with('siswa');
    
        return Datatables::of($data)
        ->addColumn("nama_siswa",function($d){

            // $siswa = Siswa::where("nis",$d->nis)->first();
            $siswa = $d->siswa ?  $d->siswa->nama : $d->nis;
            return $siswa;
            if($siswa){
                return $siswa->nama;
            }else{
                return $d->nis;
            }
        })
        
        ->addColumn("sisa",function($d){
            return $d->nominal - $d->dibayar;
        })
        ->editColumn("bulan",function($d){
            return bulan_text($d->bulan)." ".$d->tahun_id;
        })
        ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('keuangan::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('keuangan::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('keuangan::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
