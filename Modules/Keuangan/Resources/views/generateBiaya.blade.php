@extends('layouts.app')

@section('title')
    Generate Biaya
@endsection
@section('css-after')
    <!-- daterange picker -->
    <link rel="stylesheet"
        href="../../plugins/daterangepicker/daterangepicker.css">
    <!-- Select2 -->
    <link rel="stylesheet"
        href="../../plugins/select2/css/select2.min.css">
    <link rel="stylesheet"
        href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <style>
        /* .slide-transition {
                                                                                                                                                                                                                                                                                                          text-align: center;
                                                                                                                                                                                                                                                                                                          animation: slide 1s ease-in-out;
                                                                                                                                                                                                                                                                                                      }
                                                                                                                                                                                                                                                                                                      @keyframes slide {
                                                                                                                                                                                                                                                                                                          25% { text-align: left; }
                                                                                                                                                                                                                                                                                                          50% { text-align: center; }
                                                                                                                                                                                                                                                                                                          75% { text-align: right; }
                                                                                                                                                                                                                                                                                                          100% { text-align: center; }
                                                                                                                                                                                                                                                                                                      } */
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            color: black !important;
        }

        .select2-container .select2-selection--single {
            height: 100% !important;
        }

    </style>
@endsection

@section('content')
    <div class="container bg-white p-4">

        <div class="row">
            <div class="col-md-4 offset-lg-4">


                <h5 class="w-100 mb-4 text-center">
                    Generate Biaya Siswa
                    <br>

                    ({{ tgl_indo(Date('Y-m-d')) }})
                </h5>


                <div class="col-md-12">

                    <div class="form-group">
                        <label>Tahun Ajaran </label>
                        @php
                            $y = Date('Y') - 2;
                            
                            if (Date('m') < 7) {
                                $tahunnn = Date('Y') - 2;
                            } else {
                                $tahunnn = Date('Y');
                        } @endphp <select id="tahun_ajaran"
                            class="form-control select2"
                            style="width: 100%;">
                            @for ($i = 0; $i < 4; $i++)
                                <option @if ($y + $i == $tahunnn) selected @endif
                                    value="{{ $y + $i }}">Tahun Ajaran {{ $y + $i }} / {{ $y + $i + 1 }}
                                </option>
                            @endfor
                        </select>
                    </div>
                </div>


                <div class="col-md-12">

                    <div class="form-group">
                        <label>Bulan</label>
                        <select id="bulan"
                            class="form-control js-example-basic-multiple"
                            name="states[]"
                            multiple="multiple"
                            style="width: 100%;">

                            @php
                                $bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                            @endphp

                            @foreach ($bulan as $key => $item)
                                <option @if (Date('m') == $key + 1) selected @endif
                                    value="{{ $key + 1 }}">{{ $item }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>



                <div class="col-md-12">
                    <div class="form-group">
                        <label>Tingkat</label>
                        <select required
                            id="jenjang"
                            class="form-control select2"
                            style="width: 100%;">
                            @foreach (Modules\Keuangan\Entities\MasterBiaya::all() as $item)
                                <option value="{{ $item->tingkat }}"
                                    @if ($item->tingkat == 'MTS') selected @endif>
                                    {{ $item->tingkat }}
                                </option>
                            @endforeach

                        </select>
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="form-group"
                        id="list-kelas">
                        <label>Kelas</label>
                        <select required
                            id="kelas"
                            class="form-control select2"
                            style="width: 100%;">
                            <option value="0"
                                selected="selected">Kelas 7</option>
                            <option value="1">Kelas 8</option>
                            <option value="2">Kelas 9</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>Per Kelas Khusus (Opsional) </label>
                        <select class="form-control select2"
                            id="per_kelas_khusus"></select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>Jenis Satuan</label>
                        <h6 id="jenis_satuan">Bulanan</h6>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>Jenis Biaya Bebas </label>
                        <select class="form-control"
                            id="jenis_biaya_bebas"
                            class="jenis_biaya_bebas">
                        </select>
                    </div>
                </div>


                <div class="col-md-12">

                    <div class="form-group">
                        <label>NIS/NIPD</label>
                        <input type="text"
                            class="form-control"
                            id="nis"
                            placeholder="NIS/NIPD">
                    </div>

                </div>

                <div class="col-md-12">
                    <label></label>
                    <div class="form-group mt-2">
                        <button onclick="generateBiaya()"
                            class="btn bg-primary-c btn-block ">
                            <i class="fa fa-sync mr-2"></i>
                            Generate</button>
                    </div>
                </div>

            </div>
        </div>

        <div id="notif-status-err"></div>
        <br>
        <div id="notif-status-s"></div>
        <br>
        <div id="notif-status"></div>
        <br>
        <div id="notif-status-jml"></div>
    </div>

    <br>
    <br>
    <br>
    <br>
@endsection

@section('js-after')
    <script src="../../plugins/moment/moment.min.js"></script>
    <!-- date-range-picker -->
    <script src="../../plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/js/select2.full.min.js"></script>

    <script>
        var jenjangID = 'MTS'

        const generateDataBebas = () => {
            $('#jenis_biaya_bebas').html('')
            $('#jenis_biaya_bebas').append(`<option value="">
                                       Pilih Jenis Biaya Bebas
                                  </option>`);
            $.get(`/rincianBiaya/select2?tingkat=${getJenjangId()}`, function(data) {
                data.forEach(opt => {
                    $('#jenis_biaya_bebas').append(`<option value="${opt.rincian_biaya_id}">
                                       ${opt.nama}
                                  </option>`);
                });
            });
        }
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();

            generateDataBebas()
        });


        $('#per_kelas_khusus').select2({
            theme: 'bootstrap4',
            placeholder: 'Cari...',
            ajax: {
                url: '/searchKelas',
                dataType: 'json',
                delay: 250,
                processResults: function(data) {
                    console.log(data);
                    return {
                        results: $.map(data, function(item) {
                            return {
                                text: item.nama,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        const getJenjangId = () => {
            let a = document.getElementById('jenjang')
            return a.value;
        };

        $('#jenis_biaya_bebas').select2();


        $("#bulan").change(function() {
            if ($(this).val() == 7) {
                $("#jenis_satuan").html(" Daftar Ulang ");
            } else {
                $("#jenis_satuan").html(" Bulanan ");
            }
        });

        $("#jenjang").change(function() {
            generateDataBebas()
            var list = ``;

            if ($(this).val() == 'MTS') {
                list = ` <label>Kelas</label>
            <select id="kelas" class="form-control select2" style="width: 100%;">
          
              <option value="0" selected="selected">Kelas 7</option>
              <option value="1">Kelas 8</option>
              <option value="2">Kelas 9</option>
            </select>`;
            } else {
                list = ` <label>Kelas</label>
            <select id="kelas" class="form-control select2" style="width: 100%;">              
              <option value="0"  selected="selected">Kelas 10</option>
              <option value="1">Kelas 11</option>
              <option value="2">Kelas 12</option>
            </select>`;
            }
            $("#list-kelas").html(list);

            $('#kelas').select2({
                theme: 'bootstrap4'
            })

        })

        function generateBiaya() {
            $("#notif-status-err").html("");
            $("#notif-status-s").html("");
            $("#notif-status").html("");
            $("#notif-status-jml").html("");

            var thn = $("#tahun_ajaran").val();
            var bulan = $("#bulan").val();
            // console.log("bulan", bulan);
            // return false;
            var nis = $("#nis").val();
            var jenjang = $("#jenjang").val();
            var kelas = $("#kelas").val();
            var kelas_khusus = $("#per_kelas_khusus").val();
            var jenis_biaya_bebas = $("#jenis_biaya_bebas").val();

            var data = {
                tahun: thn,
                bulan: bulan,
                nis: nis,
                jenjang: jenjang,
                kelas: kelas,
                kelas_khusus: kelas_khusus,
                jenis_biaya_bebas: jenis_biaya_bebas,
            }

            function generateTagihan(nis, start, persen, jumlah) {


                data.nis = nis
                $.post("/keuangan/generate-biaya", data)
                    .done((res1) => {
                        $("#err-" + nis).hide();
                        persen = start / jumlah * 100;




                        var jsonKedua = JSON.parse(res1);
                        console.log("p", jsonKedua[0].nama, start, persen, jumlah);
                        // console.log("x", x);

                        jsonKedua.forEach(x => {

                            $("#notif-status").html(`
                            <span class="w-100 text-center text-success"> Siswa ke - ${start}</span>
                            <div class="progress">
                                <div id="pro" class="progress-bar progress-bar-striped" 
                                style="width:${persen}%">
                                    ${persen.toFixed(2)} %
                                </div>
                            </div>
                            `);

                            if (x.status == 'success') {
                                $("#notif-status-s").append(`
                                    <div class="btn btn-primary btn-block btn-sm mt-2">
                                        Genarate  Tagihan <b>${x.nama}</b> Bulan <b>${x.bulan}</b> Berhasil
                                    </div>
                                `);
                            } else if (x.status == 'error') {
                                $('#notif-status-err').append(`
                                <div id="err-${nis}" 
                                    class="btn-gen-u py-3 btn btn-danger btn-block mt-2">
                                    Cek Tagihan <b>${nis}</b> Bulan <b>${x.bulan}</b>  Gagal
                                    
                                </div>
                                `);

                            } else {
                                $("#notif-status-s").append(`
                                    <div  class="btn btn-secondary btn-block btn-sm mt-2">
                                        Tagihan <b>${x.nama}</b> Bulan <b>${x.bulan}</b>  Telah di Generate 
                                    </div>
                                `);
                            }

                        });
                    })
                    .fail(res2 => {

                        $('#notif-status-err').append(`
                        <div id="err-${nis}" 
                            class="btn-gen-u py-3 btn btn-danger btn-block mt-2">
                            Cek Tagihan <b>${nis}</b> Bulan <b>${x.bulan}</b>  Gagal
                            
                        </div>
                        `);


                    });

            }
            $.post("/keuangan/generate-biaya", data)
                .done((res) => {
                    var jsonAwal = JSON.parse(res);
                    jsonAwal.forEach(r => {
                        if (r.persiswa) {
                            var dat = r.data;
                            console.log(dat);

                            var start = 1;
                            var persen = 0;
                            var jumlah = dat.length;

                            $("#notif-status-jml").append(`
                                <div class="btn btn-info btn-block btn-sm">
                                Total <b>${jumlah}</b> Siswa, Bulan <b>${r.bulan}</b>
                                </div>
                            `);

                            var that = this




                            for (d in dat) {
                                // setTimeout(function(){
                                generateTagihan(dat[d].nis, start, persen, jumlah)
                                // }, 500);
                                start = start + 1
                            }



                        } else {
                            if (r.status == 'success') {
                                $("#notif-status-s").append(`
                                    <div class="btn btn-primary btn-block btn-sm mt-2">
                                        Genarate  Tagihan <b>${r.nama}</b> Bulan <b>${r.bulan}</b> Berhasil
                                    </div>
                                `);
                            } else {
                                $("#notif-status-s").append(`
                                    <div  class="btn btn-secondary btn-block btn-sm mt-2">
                                        Tagihan <b>${r.nama}</b> Bulan <b>${r.bulan}</b>  Telah di Generate 
                                    </div>
                                `);
                            }
                        }
                    });


                })


        }
    </script>
@endsection
