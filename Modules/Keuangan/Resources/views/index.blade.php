@extends('layouts.app')

@section('title')
    Jenis Biaya
@endsection
@section('css-after')
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet"
        href="../../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- DataTables -->
    <link rel="stylesheet"
        href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <!-- Select2 -->
    <link rel="stylesheet"
        href="../../plugins/select2/css/select2.min.css">
    <link rel="stylesheet"
        href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <!-- Bootstrap4 Duallistbox -->
    <link rel="stylesheet"
        href="../../plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
    <style>
        .modal-hover {
            display: none;
        }

        .btn-modal-hover:hover .modal-hover {
            display: block;
        }

    </style>
@endsection

@section('content')
    <div id="data-main">
        <div class="btn-group thesmallprint">
            <!-- Button trigger modal -->
            <a href="/keuangan/jenis-biaya"
                class="btn btn-sm bg-success btn-labeled  btn-outline-success mt-2">
                <b><i class="icon-plus3"></i></b>
                Jenis Biaya
            </a>

            <button onclick="tambah()"
                type="button"
                class="btn btn-sm bg-info btn-labeled btn-outline-success mt-2">
                <b><i class="icon-plus-circle2"></i></b>
                Master Biaya
            </button>

            <a href="/keuangan/generate-biaya"
                class="btn btn-sm bg-purple btn-labeled btn-outline-success mt-2">
                <b><i class="icon-sync"></i></b>
                Generate Biaya
            </a>

            <a href="/keuangan/biaya-siswa"
                class="btn btn-sm bg-pink btn-labeled  btn-outline-success mt-2">
                <b><i class="icon-coin-dollar"></i></b>
                Biaya Siswa
            </a>


        </div>
        <div id="dataload"></div>
        <div class="row ">

            <div class="col-md-4 bg-white p-2">

                <!-- Modal -->
                <div class="modal fade"
                    id="modal-form0"
                    tabindex="-1"
                    role="dialog"
                    aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog"
                        role="document">
                        <div class="modal-content">
                            <div class="modal-header bg-primary-c">
                                <h5 class="modal-title">Master Biaya</h5>
                                <button type="button"
                                    class="close"
                                    data-dismiss="modal"
                                    aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                {{-- form --}}
                                <div class="card card-success">
                                    <!-- /.card-header -->
                                    <div class="card-body">


                                        <form action="/keuangan/master-biaya"
                                            method="POST">
                                            @csrf
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label> Nama</label>
                                                    <input id="id_data"
                                                        type="hidden">
                                                    <input autofocus
                                                        id="nama"
                                                        required
                                                        placeholder="Nama"
                                                        type="text"
                                                        name="nama"
                                                        class="form-control">
                                                </div>

                                                <div class="form-group col-md-12">
                                                    <label> Tingkat</label>
                                                    <select name="tingkat"
                                                        required
                                                        id="tingkat"
                                                        class="form-control">
                                                        <option value="">Tingkat</option>
                                                        <option value="MTS">MTS</option>
                                                        <option value="MA">MA</option>
                                                    </select>
                                                </div>


                                                <div class="form-group col-md-12">
                                                    <label> Status </label>
                                                    <select name="status"
                                                        required
                                                        id="status"
                                                        class="form-control">
                                                        <option value="">Status</option>
                                                        <option value="1">Aktif</option>
                                                        <option value="0">Non Aktif</option>
                                                    </select>
                                                </div>


                                            </div>



                                            <div class="modal-footer">
                                                <button type="button"
                                                    class="btn btn-secondary"
                                                    data-dismiss="modal">
                                                    Batal
                                                </button>


                                                <a href="#"
                                                    onclick="editData()"
                                                    id="btn-edit"
                                                    class="btn bg-primary-c">
                                                    <i class="fa fa-save"></i>
                                                    Simpan</a>
                                                <button id="btn-tambah"
                                                    type="submit"
                                                    class="btn btn-block bg-primary-c">
                                                    <i class="fa fa-save"></i>
                                                    Tambah</button>

                                            </div>

                                        </form>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                {{-- End --}}
                            </div>
                        </div>
                    </div>
                </div>


                <div class="table-responsive mt-4">
                    <table id="data-table"
                        class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nama</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>



            </div>




            <div class="col-md-8">

                <div id="buka-data">

                </div>
            </div>


        </div>
    </div>


    <div id="edit-data">

    </div>
@endsection

@section('js-after')
    <!-- Select2 -->
    <script src="../../plugins/select2/js/select2.full.min.js"></script>

    <!-- DataTables -->
    <script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <!-- Bootstrap4 Duallistbox -->
    <script src="../../plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>

    <script type="text/javascript">
        var table = $('#data-table').DataTable({
            processing: true,
            lengthChange: false,
            serverSide: true,
            searching: false,
            ajax: '/keuangan/master-biaya/json/data',
            columns: [{
                data: function(d) {
                    var act = `
                <h6>
                <b>${d.nama}</b> <br>
                Tingkat : ${d.tingkat} <br>
                </h6>
                <a id="bukaData-${d.id}" onclick="bukaData('${d.id}','${d.nama}','${d.tingkat}')" href="#" class="btn bg-blue-800 btn-labeled btn-sm float-right mr-2"><b><i class="icon-point-right"></i></b> Buka</a>

                <a onclick="deleteData('${d.id}')" href="#" class="btn bg-danger-600 btn-sm btn-labeled mr-2"><b><i class="icon-bin"></i></b> Hapus</a>

                <a onclick="edit('${d.id}','${d.nama}','${d.tingkat}','${d.angkatan}','${d.status}')" href="#" class="btn bg-orange-600 btn-labeled btn-sm mr-2"><b><i class="icon-pencil"></i></b> Edit</a>
                
                `;
                    return act;
                },
                name: 'id'
            }, ]
        });

        $("#btn-edit").hide();


        function tambah() {
            $("#nama").val("");
            $("#tingkat").val("");
            $("#status").val("");
            $("#id_data").val("");
            $("#btn-edit").hide();
            $("#btn-tambah").show();
            $("#modal-form0").modal("show");

        };

        function edit(id, nm, tingkat, angkatan, status) {
            // console.log(id);
            $("#nama").val(nm);
            $("#tingkat").val(tingkat);
            $("#status").val(status);
            $("#id_data").val(id);
            $("#btn-edit").show();
            $("#btn-tambah").hide();
            $("#modal-form0").modal("show");
        }

        function editData() {
            $.post("/keuangan/master-biaya/" + $("#id_data").val(), {
                    _token: "{{ csrf_token() }}",
                    _method: "put",
                    nama: $("#nama").val(),
                    tingkat: $("#tingkat").val(),
                    status: $("#status").val()
                })
                .done((res) => {
                    // location.reload()
                    table.ajax.reload();
                    $("#modal-form0").modal("hide");
                    // console.log(res)
                })
        }

        function deleteData(id) {

            swal({
                    title: "Apakah Kamu Yakin?",
                    text: "Pastikan data yang akan dihapus sudah valid",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Hapus",
                    closeOnConfirm: false
                },
                function() {
                    $.post("/keuangan/master-biaya/" + id, {
                            _token: "{{ csrf_token() }}",
                            _method: "delete"
                        })
                        .done((res) => {
                            location.reload()
                        })

                });

        }





        function tambah_r() {
            $("#nama").val("");
            $("#tingkat").val("");
            $("#angkatan").val("");
            $("#status").val("");
            $("#id_data").val("");
            $("#btn-edit").hide();
            $("#btn-tambah").show();
            $("#modal-form0-r").modal("show");
        }

        // Jenis Biaya


        function bukaData(id, nm, tingkat) {

            $.get("/data-kelas/select2/" + tingkat)
                .done((dkelas) => {


                    $.get("/keuangan/jenis-biaya/json/data/select2")
                        .done((res) => {
                            // console.log(res);
                            var dataop = ``;
                            var resdata = JSON.parse(res);
                            for (let index = 0; index < resdata.length; index++) {
                                dataop += `<option value="${resdata[index].id}">${resdata[index].nama}</option>`;
                            }

                            var datakelas = ``;
                            var dkk = JSON.parse(dkelas);

                            // console.log(dkk);

                            for (let i = 0; i < dkk.length; i++) {
                                datakelas += `<option value="${dkk[i].id}">${dkk[i].nama}</option>`;
                            }





                            var tambahData = `
  
<button onclick="tambah_r()" type="button" class="btn btn-sm bg-pink-300 btn-labeled" >
    <b><i class="icon-plus3"></i></b>
    Rincian Biaya
    </button>


<!-- Modal -->
<div class="modal fade" id="modal-form0-r" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    <div class="modal-header bg-primary-c">
        <h5 class="modal-title">Rincian Biaya</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">

            

            <form action="/keuangan/rincian-biaya" method="POST" id="form-tambah-rincian-biaya">
                @csrf 
                <div class="row text-dark">
                    <div class="form-group col-md-6">
                        <label> Jenis Biaya</label>
                        <input name="r_id" value="${id}" id="r_id_data" type="hidden">   
                        <select  name="r_jenis_biaya" required id="r_jenis_biaya" class="form-control">
                          ${dataop}
                        </select>
                    </div>
                    
                    <div class="form-group col-md-6">
                        <label> Nominal</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Rp</span>
                          </div>
                            <input id="r_nominal" required placeholder="Nominal"
                            type="text" name="r_nominal" class="form-control">   
                        </div>
                    </div>
                    
                    <div class="col-12">
                      <div class="form-group">
                        <label>Kelas</label>
                          <select class="duallistbox" multiple name="r_kelas[]" required id="r_kelas" style="display: none;">
                            ${datakelas}
                          </select>
                      </div>
                    </div>
                    
                    <div class="form-group col-md-6">
                        <label> Jenis Tagihan</label>  
                        <select name="r_satuan" required id="r_satuan" class="form-control">
                          <option value="Bulanan">Bulanan</option>
                          <option value="Daftar Ulang">Daftar Ulang</option>
                          <option value="Bebas">Bebas</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label> Prioritas</label>
                        <input id="r_prioritas" required placeholder="Prioritas"
                        type="number" value="1" name="r_prioritas" class="form-control">   
                    </div>

                    <!-- radio -->
                <div class="form-group col-md-6">
                  <label class="col-md-12" > Untuk : </label>
                  <div class="icheck-success d-inline">
                    <input value="0" type="radio" name="r_jk" checked id="radioSuccess1">
                    <label for="radioSuccess1"> Semua
                    </label>
                  </div>
                  <div class="icheck-success d-inline">
                    <input value="1" type="radio" name="r_jk" id="radioSuccess2">
                    <label for="radioSuccess2"> Putra
                    </label>
                  </div>
                  <div class="icheck-success d-inline">
                    <input value="2" type="radio" name="r_jk" id="radioSuccess3">
                    <label for="radioSuccess3"> Putri
                    </label>
                  </div>

                  
                </div>
                <div class="form-group col-md-6">
                    <label class="col-md-12 mt-2" > Status Beasiswa : </label>
                    <div class="icheck-success d-inline">
                        <input value="1" type="radio" name="r_beasiswa"  id="radioBeasiswa1">
                        <label for="radioBeasiswa1"> Beasiswa
                        </label>
                    </div>
                    <div class="icheck-success d-inline">
                        <input value="0" type="radio" name="r_beasiswa" checked id="radioBeasiswa2">
                        <label for="radioBeasiswa2"> Non Beasiswa
                        </label>
                    </div>
                </div>
                    

                    <div class="form-group col-md-12 d-none">
                        <label class="col-md-12"> Status </label>
                        <div class="icheck-success d-inline">
                          <input checked value="1" type="radio" name="r_status" id="radioSuccess4">
                          <label for="radioSuccess4"> Aktif
                          </label>
                        </div>
                        <div class="icheck-success d-inline">
                          <input value="0" type="radio" name="r_status" id="radioSuccess5">
                          <label for="radioSuccess5"> Tidak Aktif
                          </label>
                        </div>
                    </div>

                
                </div>



            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Batal
                </button>
                

                <button data-dismiss="modal" id="btn-tambah-rincian-biaya" type="button" class="btn btn-block bg-primary-c">
                    <i class="fa fa-save"></i>
                    Tambah</button> 

            </div>

            </form>
    </div>
    </div>
</div>
</div>
      
  `;
                            var html = `<div class="card card-success">
                <div class="card-header">

                  <h3 class="card-title">Rincian Biaya <span class="label bg-danger-800"><b>${nm}</b></span></h3>
  
                  <div class="card-tools">

                    ${tambahData}

                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                      <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                      <i class="fas fa-times"></i></button>
                  </div>
                </div>
                <div class="card-body">
                  <div id="buka-data-rincian"></div>
                </div>
              </div>
`;

                            $("#buka-data").html(html);


                            //Bootstrap Duallistbox
                            $('.duallistbox').bootstrapDualListbox()

                            //Initialize Select2 Elements
                            $('#r_jenis_biaya').select2({
                                theme: 'bootstrap4'
                            })

                            //Initialize Select2 Elements
                            $('#r_kelas').select2({
                                theme: 'bootstrap4'
                            })


                            rupiah('#r_nominal');


                            var dataRincian = `
              <div class="table-responsive">
                <table id="data-table-rincian" class="table table-bordered table-hover">
                  <thead>  
                    <tr>
                      <th>Nama</th>
                      <th>Prioritas</th>
                      <th>Jenis Tagihan</th>
                      <th>Putra/Putri</th>
                      <th>Kelas</th>
                      <th>Nominal</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>`;

                            $("#buka-data-rincian").html(dataRincian);


                            var tableRincian = $('#data-table-rincian').DataTable({
                                processing: true,
                                lengthChange: true,
                                serverSide: true,
                                searching: true,
                                ajax: '/keuangan/rincian-biaya/json/data/' + id,
                                columns: [{
                                        data: function(d) {
                                            return `${d.nama} `;
                                        },
                                        name: 'nama'
                                    },
                                    {
                                        data: function(d) {
                                            return `${d.prioritas} `;
                                        },
                                        name: 'prioritas'
                                    },
                                    {
                                        data: function(s) {



                                            if (s.satuan == 'Daftar Ulang') {
                                                return "<span class='label bg-green-600'>Daftar Ulang</span>"
                                            } else if (s.satuan == 'Bulanan') {
                                                return "<span class='label bg-teal-600'>Bulanan</span>"

                                            } else {
                                                return "<span class='label bg-yellow'>Bebas</span>"
                                            }
                                        },
                                        name: 'satuan'

                                    },
                                    {
                                        data: function(d) {
                                            if (d.jk == 0) {
                                                return "<span class='label bg-info'>Semua</span>";
                                            } else if (d.jk == 1) {
                                                return "<span class='label bg-success-800'>Putra</span>";
                                            } else {
                                                return "<span class='label bg-pink'>Putri</span>";
                                            }
                                        },
                                        name: 'jk'
                                    },
                                    {
                                        data: function(d) {


                                            console.log(d.kelas_text);
                                            let res = d.kelas_text;
                                            let vi = '';
                                            for (let a = 0; a < res.length; a++) {

                                                vi += "(" + res[a] + ") ";

                                            }
                                            return `
                  <span class='btn btn-sm btn-primary btn-modal-hover'><i class='fa fa-eye'></i>
                  Lihat Kelas

                  <div class="card modal-hover" style="position:absolute;z-index:1000;width:200px;left:-140px;top:-30px">
                    <div class="card-body text-muted">
                      ${vi}
                    </div>
                  </div>
                  
                </span>
                  
                  `;


                                        },
                                        name: 'kelas'
                                    },
                                    {
                                        data: function(d) {
                                            return Rupiah(d.nominal);
                                        },
                                        name: 'nominal'
                                    },
                                    {
                                        data: function(d) {

                                            var act = `
                   
                    <a href="#" onclick="deleteData_r(${d.id})" class="btn btn-danger btn-sm float-right"><i class="fa fa-trash"></i></a>

                    <a href="#" onclick="editData_r('${d.id}')" class="btn btn-success btn-sm float-right"><i class="fa fa-edit"></i></a>
  
                    `;
                                            return act;
                                        },
                                        name: 'id'
                                    },
                                ]
                            });







                            var frm = $('#form-tambah-rincian-biaya');
                            var btnForm = $('#btn-tambah-rincian-biaya');
                            btnForm.click(function(ev) {
                                ev.preventDefault();
                                $.ajax({
                                    type: frm.attr('method'),
                                    url: frm.attr('action'),
                                    data: frm.serialize(),
                                    success: function(data) {
                                        $("#modal-form0-r").modal("hide");
                                        $("#bukaData-" + id).click();
                                    }
                                });
                            });








                        })
                })





        }



        function deleteData_r(id) {
            swal({
                    title: "Apakah Kamu Yakin?",
                    text: "Pastikan data yang akan dihapus sudah valid",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Hapus",
                    closeOnConfirm: false
                },
                function() {
                    $.post("/keuangan/rincian-biaya/" + id, {
                            _token: "{{ csrf_token() }}",
                            _method: "delete"
                        })
                        .done((res) => {
                            location.reload()
                        })
                });
        }

        function editData_r(id) {

            $.get("/keuangan/rincian-biaya/edit/" + id)
                .done((d_e) => {

                    var daaa = JSON.parse(d_e);

                    var de = daaa[0];
                    var dmaster = daaa[1];

                    $.get("/data-kelas/select2/" + dmaster['tingkat'])
                        .done((dkelas) => {


                            $.get("/keuangan/jenis-biaya/json/data/select2")
                                .done((res) => {
                                    // console.log(res);
                                    var dataop = ``;
                                    var resdata = JSON.parse(res);
                                    for (let index = 0; index < resdata.length; index++) {
                                        var selectedJns = '';
                                        if (resdata[index].id == de['jenis_biaya_id']) {
                                            selectedJns = 'selected';
                                        }
                                        dataop +=
                                            `<option ${selectedJns} value="${resdata[index].id}">${resdata[index].nama}</option>`;
                                    }

                                    var datakelas = ``;
                                    var dkk = JSON.parse(dkelas);

                                    // console.log(dkk);

                                    for (let i = 0; i < dkk.length; i++) {
                                        var selectedKelas = '';
                                        var strArray = de['kelas'].split(",");

                                        // Display array values on page
                                        for (var a = 0; a < strArray.length; a++) {
                                            console.log(strArray[a] + " == " + dkk[i].nama);
                                            if (dkk[i].id == strArray[a]) {
                                                selectedKelas = 'selected';
                                            }
                                        }
                                        datakelas +=
                                            `<option ${selectedKelas} value="${dkk[i].id}">${dkk[i].nama}</option>`;
                                    }

                                    function convertToRupiah(angka) {
                                        var rupiah = '';
                                        var angkarev = angka.toString().split('').reverse().join('');
                                        for (var i = 0; i < angkarev.length; i++)
                                            if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.';
                                        return rupiah.split('', rupiah.length - 1).reverse().join('');
                                    }
                                    var dataNominal = convertToRupiah(de['nominal']);


                                    var checkSemua = '';
                                    var checkPutra = '';
                                    var checkPutri = '';

                                    if (de['jk'] == '0') {
                                        checkSemua = 'checked';
                                    } else if (de['jk'] == '1') {
                                        checkPutra = 'checked';
                                    } else if (de['jk'] == '2') {
                                        checkPutri = 'checked';
                                    }

                                    var bulananTrue = '';
                                    var daftarUlangTrue = '';
                                    var bebasTrue = '';

                                    if (de['satuan'] == 'Bulanan') {
                                        bulananTrue = 'selected';
                                    } else if (de['satuan'] == 'Daftar Ulang') {
                                        daftarUlangTrue = 'selected';
                                    } else {
                                        bebasTrue = 'selected';
                                    }



                                    var jenisTagihan = `<option ${bulananTrue} value="Bulanan">Bulanan</option>
                          <option ${daftarUlangTrue} value="Daftar Ulang">Daftar Ulang</option>
                          <option ${bebasTrue} value="Bebas">Bebas</option>`;

                                    var jenisKelamin = ` <div class="form-group col-md-6">
                                    <label class="col-md-12" > Untuk : </label>
                                    <div class="icheck-success d-inline">
                                        <input value="0" type="radio" ${checkSemua} name="r_jk_e" id="radioSuccessEdit1">
                                        <label for="radioSuccessEdit1"> Semua
                                        </label>
                                    </div>
                                    <div class="icheck-success d-inline">
                                        <input value="1" type="radio" ${checkPutra} name="r_jk_e" id="radioSuccessEdit2">
                                        <label for="radioSuccessEdit2"> Putra
                                        </label>
                                    </div>
                                    <div class="icheck-success d-inline">
                                        <input value="2" type="radio" ${checkPutri} name="r_jk_e" id="radioSuccessEdit3">
                                        <label for="radioSuccessEdit3"> Putri
                                        </label>
                                    </div>
                                    </div>`;



                                    var editData = `

<!-- Modal -->
<div class="modal fade active" id="modal-1form-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div style="display:block" class="modal-dialog modal-lg active" role="document">
    <div class="modal-content">
    <div class="modal-header bg-primary-c">
        <h5 class="modal-title">Rincian Biaya</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">

            

            <form action="/keuangan/rincian-biaya/edit" method="POST" id="form-edit-rincian-biaya">
                @csrf 
                <div class="row text-dark">
                    <div class="form-group col-md-6">
                        <label> Jenis Biaya</label>
                        <input name="r_id_e" value="${id}" id="r_id_data" type="hidden">   
                        <select value="${de['jenis_biaya']}" name="r_jenis_biaya_e" required id="r_jenis_biaya_e" class="form-control">
                          ${dataop}
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label> Nominal</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Rp</span>
                          </div>
                            <input value="${dataNominal}" id="r_nominal" required placeholder="Nominal"
                            type="text" name="r_nominal_e" class="form-control">   
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Kelas</label> 
                        <select  class="duallistbox" multiple name="r_kelas_e[]" required id="r_kelas_e" class="form-control">
                          ${datakelas}
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label> Jenis Tagihan</label>  
                        <select name="r_satuan_e" required id="r_satuan" class="form-control">
                          ${jenisTagihan}
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label> Prioritas</label>
                        <input id="r_prioritas_e" required placeholder="Prioritas"
                        type="number" value="${de['prioritas']}" name="r_prioritas_e" class="form-control">   
                    </div>

                    <!-- radio -->
                    ${jenisKelamin}
                    <div class="form-group col-md-6">
                        <label class="col-md-12 mt-2" > Status Beasiswa : </label>
                        <div class="icheck-success d-inline">
                            <input value="1" type="radio" name="r_beasiswa" ${de['beasiswa'] == '1' ? 'checked' : ''}  id="radioBeasiswaEdit1">
                            <label for="radioBeasiswaEdit1"> Beasiswa
                            </label>
                        </div>
                        <div class="icheck-success d-inline">
                            <input value="0" type="radio" name="r_beasiswa" ${de['beasiswa'] == '0' ? 'checked' : ''} id="radioBeasiswaEdit2">
                            <label for="radioBeasiswaEdit2"> Non Beasiswa
                            </label>
                        </div>
                    </div>

                    

                
                </div>



            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Batal
                </button>
                

                <button id="btn-simpan-edit" type="submit" class="btn btn-block bg-primary-c">
                    <i class="fa fa-save"></i>
                    Simpan</button> 

            </div>

            </form>
    </div>
    </div>
</div>
</div>
      
  `;

                                    $("#edit-data").html(editData);


                                    //Bootstrap Duallistbox
                                    $('.duallistbox').bootstrapDualListbox()

                                    $("#modal-1form-edit").modal("show");

                                    //Initialize Select2 Elements
                                    $('#r_jenis_biaya_e').select2({
                                        theme: 'bootstrap4'
                                    })

                                    //Initialize Select2 Elements
                                    $('#r_kelas_e').select2({
                                        theme: 'bootstrap4'
                                    })


                                    var frm = $('#form-edit-rincian-biaya');
                                    var btnForm = $('#btn-simpan-edit');
                                    btnForm.click(function(ev) {
                                        ev.preventDefault();
                                        $.ajax({
                                            type: frm.attr('method'),
                                            url: frm.attr('action'),
                                            data: frm.serialize(),
                                            success: function(data) {
                                                // alert('ok');
                                                $("#modal-1form-edit").modal("hide");
                                                $("#bukaData-" + dmaster['id']).click();
                                            }
                                        });
                                    });



                                })
                        })

                })

        }
    </script>
@endsection
