@extends('layouts.app')

@section('title')
      Jenis Biaya
@endsection
@section('css-after') 
 <!-- DataTables -->
 <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endsection

@section('content')

<div class="container">
        <div class="row " >
      
            <div class="col-md-10 offset-lg-1 bg-white p-4">

              <div class="alert alert-warning p-2 text-center m-2">
                Setelah memasukan kata yang ingin dicari pada kolom Cari, tekan <strong>Enter</strong> untuk memfilter data.
              </div>
              <div class="table-responsive">
                
              <table id="data-table" class="table table-bordered table-hover">
                <thead>  
                  <tr>
                    <th>No</th>
                    <th>NIS</th>
                    <th>Nama Siswa</th>
                    <th>jenis Biaya</th>
                    <th>Sisa</th>
                    <th>Bulan</th>
                    <th style="width:130px">Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
            </div>
        </div>




        

<div id="modal-edit-tagihan" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Tagihan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <form  action="/edit-nominal-tagihan" method="POST">
            @csrf
            <input type="hidden" id="id-tagihan-edit" name="id">

            <div class="form-group clearfix" id="edit-du-dk">
             
            </div>

            <div class="form-group">
              <label> NIS </label>
              <div class="input-group">
                <input name="nis" placeholder="..." type="text" class="form-control" id="nis-tagihan-edit">  
              </div>
            </div>

            <div class="form-group">
              <label> Nama tagihan </label>
              <div class="input-group">
                <input name="nama" placeholder="..." type="text" class="form-control" id="nama-tagihan-edit">  
              </div>
            </div>

            <div class="form-group">
              <label> Nominal </label>
              <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Rp</span>
                  </div>
                <input name="nominal" placeholder="..." type="text" class="form-control" id="nominal-tagihan-edit">  
              </div>
            </div>
            <button class="btn btn-primary">
              <i class="fa fa-save"></i>
              Simpan
            </button>
        </form>
      </div>
    </div>
  </div>
</div>




    </div>
{{-- End Modal Tambah --}}
@endsection

@section('js-after')

<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script type="text/javascript">

  $(document).ready(function() {
    var datatables1  = $('#data-table').DataTable({
          initComplete: function () {
              // Apply the search
              // this.api().columns().every( function () {
                  var that = this;
                  $('div.dataTables_filter input').unbind();
                  $( 'div.dataTables_filter input' ).bind( 'keyup change clear', function (e) {
                      if ( datatables1.search() !== this.value ) {
                        if (e.keyCode === 13) {
                          datatables1
                              .search( this.value )
                              .draw();
                        }
                          
                      }
                  } );
              // } );
          },
        language : {
              'url': '/indonesian.json'
        },
        processing: true,
        serverSide: true,
        ajax: '/keuangan/biaya-siswa/json/data',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'nis', name: 'nis' },
            { data: 'nama_siswa', name: 'siswa.nama' },
            { data: 'nama', name: 'nama' },
            { data: 'sisa', name: 'sisa' },
            { data: 'bulan', name: 'bulan' },
            { data: function(d){
                  var act = `
                
                  <button onclick="hapusTagihan('${d.id}')" 
                    class="btn btn-danger btn-sm float-right">
                    <i class="fa fa-trash"></i>
                  </button>    
                  <a href="/input-cash-cicil?nis=${d.nis}" class="btn float-right btn-primary btn-sm">
                    <i class="fa fa-eye"></i>
                  </a>  
                  <a  href="#" onclick="editTagihan('${d.id}','${d.nominal}','${d.jenis_biaya_id}','${d.nis}','${d.nama}',)" class="btn float-right btn-success btn-sm"> 
                  <i class="fa fa-edit"></i>
                  </a>

                  `;                
                  return act;
              }, 
              name: 'id'
            }
        ]
    });
  });


  function editTagihan(id,nominal,jb,nis,nama){
   $("#id-tagihan-edit").val(id);

  if(jb == 1 || jb == 2){

    var ck1 = '';
    var ck2 = '';

    if(jb == 1){
      ck1 = 'checked';
    }else if(jb == 2){
      ck2 = 'checked';
    }
    $("#edit-du-dk").html(`
      <label class="w-100"> Uang Makan</label>
          <div class="icheck-success d-inline">
            <input onclick="setval(400000)" ${ck1} value="0" type="radio" name="du_dk"  id="radioSuccess1">
            <label for="radioSuccess1">
              Dapur Umum (DU)
            </label>
          </div>
          <div class="icheck-success d-inline">
            <input onclick="setval(430000)" ${ck2} value="1" type="radio" name="du_dk" id="radioSuccess2">
            <label for="radioSuccess2">
                Dapur Keluarga (DK)
            </label>
          </div>
      `);
  }else{
    $("#edit-du-dk").html('');
  }


   $("#nominal-tagihan-edit").val(nominal);
   $("#nis-tagihan-edit").val(nis);
   $("#nama-tagihan-edit").val(nama);
   $("#modal-edit-tagihan").modal("show");
 }



 function hapusTagihan(id){

  swal({
      title: "Apakah Kamu Yakin?",
      text: "Pastikan data yang akan dihapus sudah valid",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Hapus",
      closeOnConfirm: false
    },
    function(){

      $.get("/hapus-tagihan/"+id)
        .done((res) => {
        location.reload()
        })
    });
  }

</script>

@endsection