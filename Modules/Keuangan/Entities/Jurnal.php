<?php

namespace Modules\Keuangan\Entities;

use Illuminate\Database\Eloquent\Model;

class Jurnal extends Model
{
    protected $fillable = ['kode_trx','nis','ref','biaya_id','nama_biaya','id_jenis_biaya',
    'type','jumlah','tgl_bayar','saldo_akhir','ket'];


    public function tagihan()
    {
        return $this->hasOne('Modules\Keuangan\Entities\BiayaSiswa', 'id', 'biaya_id');
    }

}
