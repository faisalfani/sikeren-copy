<?php

namespace Modules\Keuangan\Entities;

use Illuminate\Database\Eloquent\Model;

class TotalBayarSiswa extends Model
{
    protected $fillable = ['kode_trx',"nis","ref","biaya_id","jumlah","tgl_bayar"];
}
