<?php

namespace Modules\Keuangan\Entities;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\Relations\HasOne;
use Modules\Siswa\Entities\Siswa;

class BiayaSiswa extends Model
{
    protected $fillable = ['nis','nama','jenis_biaya_id','rincian_biaya_id','satuan','prioritas','nominal','dibayar','bulan','tahun_id','tahun_ajaran'];

    public function siswa(){
        return $this->hasOne(Siswa::class, 'nis', 'nis');
    }
}
