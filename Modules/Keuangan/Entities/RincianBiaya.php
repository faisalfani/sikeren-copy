<?php

namespace Modules\Keuangan\Entities;

use Illuminate\Database\Eloquent\Model;

class RincianBiaya extends Model
{
    protected $fillable = ['master_biaya_id','keterangan','jenis_biaya_id','kelas','satuan','prioritas','jk','nominal','status','jenis_siswa'];
}
