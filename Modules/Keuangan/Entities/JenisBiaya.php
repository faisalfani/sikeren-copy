<?php

namespace Modules\Keuangan\Entities;

use Illuminate\Database\Eloquent\Model;

class JenisBiaya extends Model
{
    protected $fillable = ['nama'];
}
