<?php

namespace Modules\Keuangan\Entities;

use Illuminate\Database\Eloquent\Model;

class MasterBiaya extends Model
{
    protected $fillable = ['nama','tingkat','status'];
}
