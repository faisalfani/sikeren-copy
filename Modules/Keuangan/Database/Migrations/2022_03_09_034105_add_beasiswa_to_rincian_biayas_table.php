<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBeasiswaToRincianBiayasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rincian_biayas', function (Blueprint $table) {
            $table->boolean('beasiswa')->default(false)->after('jenis_siswa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rincian_biayas', function (Blueprint $table) {
            $table->dropColumn('beasiswa');
        });
    }
}
