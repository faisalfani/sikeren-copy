<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBiayaSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biaya_siswas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("nis");
            $table->integer("rincian_biaya_id");
            $table->integer("jenis_biaya_id");
            $table->string("nama");
            $table->string("satuan");
            $table->integer("prioritas");
            $table->integer("nominal");
            $table->integer("dibayar");
            $table->integer("bulan");
            $table->integer("tahun_id");
            $table->integer("tahun_ajaran");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biaya_siswas');
    }
}
