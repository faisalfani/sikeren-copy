<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJurnalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jurnals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("kode_trx");
            $table->string("nis");
            $table->integer("ref")->nullable();
            $table->integer("biaya_id");
            $table->string("nama_biaya");
            $table->integer("id_jenis_biaya");
            $table->integer("type");
            $table->integer("jumlah");
            $table->date("tgl_bayar");
            $table->integer("saldo_akhir")->nullable();
            $table->string("ket")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jurnals');
    }
}
