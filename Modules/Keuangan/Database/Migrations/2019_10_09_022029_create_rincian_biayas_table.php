<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRincianBiayasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rincian_biayas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("master_biaya_id");
            $table->integer("jenis_biaya_id");
            $table->text("kelas");
            $table->string("satuan");
            $table->integer("prioritas");
            $table->integer("jk");
            $table->integer("nominal");
            $table->integer("status");
            $table->integer("jenis_siswa")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rincian_biayas');
    }
}
