<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTotalBayarSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('total_bayar_siswas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("kode_trx")->nullable();
            $table->integer("nis");
            $table->integer("ref")->nullable();
            $table->text("biaya_id");
            $table->integer("jumlah");
            $table->date("tgl_bayar");
            $table->integer("id_tagihan_h2h")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('total_bayar_siswas');
    }
}
