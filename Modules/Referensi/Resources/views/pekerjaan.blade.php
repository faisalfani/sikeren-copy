@extends('layouts.app')

@section('title')
      Alamat
@endsection
@section('css-after') 
 <!-- DataTables -->
 <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endsection

@section('content')

<div class="container">
    <div class="card card-outline card-success">
      <div class="card-body">



        <div class="row" >
            <div class="col-md-12">
                <form action="/pekerjaan" method="POST">
                  @csrf 
                  <div class="input-group col-md-12 mb-4">
                      <input placeholder="Nama" type="text" name="nama" class="form-control">   
                        <div class="input-group-append">
                            <button type="submit" class="btn  bg-primary-c">
                                <i class="fa fa-save"></i>
                                Simpan</button>   
                        </div>
                    </div>
              </form>
            </div>

            <div class="col-md-12">
              <div class="table-responsive">
              <table id="data-table" class="table table-bordered table-hover">
                <thead>  
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    {{-- <th>Action</th> --}}
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
            </div>
        </div>




      </div>
    </div>
  </div>

{{-- End Modal Tambah --}}
@endsection

@section('js-after')

<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script type="text/javascript">

$('#data-table').DataTable({
      processing: true,
      serverSide: true,
      ajax: '/pekerjaan/json',
      columns: [
          { data: 'id', name: 'id' },
          { data: 'nama', name: 'nama' },

      ]
  });

</script>

@endsection