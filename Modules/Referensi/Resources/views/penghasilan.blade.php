@extends('layouts.app')

@section('title')
      Alamat
@endsection
@section('css-after') 
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@endsection

@section('content')



<div class="container">
    <div class="card card-outline card-success">
      <div class="card-body">

        <div class="row" >
{{-- aa --}}
            <div class="col-md-4">
                <form action="/penghasilan" method="POST">
                  @csrf 
                  <div class="form-group col-md-12">
                      <label> Nama</label>
                      <input placeholder="Nama" type="text" name="nama" class="form-control">   
                    </div>
                    <div class="form-group col-md-12">
                        <button type="submit" class="btn  bg-primary-c">
                          <i class="fa fa-save"></i>
                          Simpan</button> 
                    </div>
                  </form>
            </div>

            <div class="col-md-12">
              <div class="table-responsive">
              <table id="data-table" class="table table-bordered table-hover">
                <thead>  
                  <tr>
                    <th>Kategori</th>
                    <th>Min</th>
                    <th>Max</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
            </div>
        </div>
  
      </div>
    </div>
  </div>

@endsection

@section('js-after')

<!-- DataTables -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">

$('#data-table').DataTable({
      processing: true,
      serverSide: true,
      ajax: '/penghasilan-ortu/json',
      columns: [
          { data: 'kategori', name: 'kategori' },
          { data: 'min', name: 'min' },
          { data: 'max', name: 'max' },
      ]
  });

</script>

@endsection