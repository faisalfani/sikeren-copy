@extends('layouts.app')

@section('title')
      Alamat
@endsection
@section('css-after') 

 <!-- DataTables -->
 <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endsection

@section('content')
<div class="container">
  <div class="card card-outline card-success">
    <div class="card-body">
      
        <div class="row" >
            <div class="col-md-12">
              <div class="table-responsive">
              <table id="data-table" class="table table-bordered table-hover">
                <thead>  
                  <tr>
                    <th>No</th>
                    <th>Desa</th>
                    <th>Kecamatan</th>
                    <th>Kota/Kab</th>
                    <th>Provinsi</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
            </div>
        </div>
  

      </div>
    </div>
  </div>

@endsection

@section('js-after')




<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script type="text/javascript">

$('#data-table').DataTable({
      processing: true,
      serverSide: true,
      ajax: '/alamat/json',
      columns: [
          { data: 'id', name: 'id' },
          { data: 'desa', name: 'desa' },
          { data: 'kec', name: 'kec' },
          { data: 'kota', name: 'kota' },
          { data: 'provinsi', name: 'provinsi' }
      ]
  });

</script>

@endsection