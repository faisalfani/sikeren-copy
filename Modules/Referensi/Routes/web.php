<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group(function() {
    

    Route::get('/alamat/json', 'ReferensiController@alamat_json');
    Route::get('/alamat', 'ReferensiController@alamat');

    Route::resource('/sekolah', 'SekolahController');
    Route::get('/sekolah/json/data', 'SekolahController@json');

    Route::get('/pekerjaan/json', 'ReferensiController@pekerjaan_json');
    Route::get('/pekerjaan', 'ReferensiController@pekerjaan');
    Route::post('/pekerjaan', 'ReferensiController@tambahPekerjaan');

    Route::get('/penghasilan-ortu/json', 'ReferensiController@penghasilan_json');
    Route::get('/penghasilan-ortu', 'ReferensiController@penghasilan');

});
