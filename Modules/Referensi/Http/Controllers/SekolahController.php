<?php

namespace Modules\Referensi\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use yajra\Datatables\Datatables;
use DB;

class SekolahController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function json(){
        $data = DB::table("sekolah as sekolah")
                  ->join("regencies as kota","sekolah.kabupaten_id","kota.id")
                  ->join("provinces as provinsi","kota.province_id","provinsi.id")
                  ->select("sekolah.*","kota.name as kota","provinsi.name as provinsi")
                  ->get();
        return Datatables::of($data)->make(true);
    }
    public function index()
    {
        return view('referensi::sekolah');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('referensi::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('referensi::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('referensi::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
