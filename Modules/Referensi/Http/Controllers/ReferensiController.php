<?php

namespace Modules\Referensi\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use yajra\Datatables\Datatables;
use DB;
use Modules\Referensi\Entities\Pekerjaan;

class ReferensiController extends Controller
{
    public function authMiddleware(Request $request) {
        return $request->user();
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function alamat_json(){
        $alamat = DB::table("villages as desa")
                  ->join("districts as kec","desa.district_id","kec.id")
                  ->join("regencies as kota","kec.regency_id","kota.id")
                  ->join("provinces as provinsi","kota.province_id","provinsi.id")
                  ->where("provinsi.id","32")
                  ->select("desa.id","desa.name as desa","kec.name as kec","kota.name as kota","provinsi.name as provinsi")
                  ->get();
        return Datatables::of($alamat)->make(true);
    }

    
    public function alamat()
    {
        return view('referensi::alamat');
    }

    public function pekerjaan_json(){
        $data = DB::table("pekerjaan")
        ->get();
        return Datatables::of($data)->make(true);
    }

    public function pekerjaan()
    {
        return view('referensi::pekerjaan');
    }
    public function tambahPekerjaan(Request $req){
        // dd($req);
        $new = new Pekerjaan;
        $new->nama = $req->nama;
        $new->save();

        return back()->with("success","Data berhasil ditambahkan");

    }


    public function penghasilan_json(){
        $data = DB::table("penghasilan_ortus")->get();
        return Datatables::of($data)->make(true);
    }

    public function penghasilan()
    {
        return view('referensi::penghasilan');
    }

    public function index()
    {
        return view('referensi::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('referensi::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('referensi::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('referensi::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
