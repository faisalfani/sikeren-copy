<?php

namespace Modules\Referensi\Entities;

use Illuminate\Database\Eloquent\Model;

class penghasilanOrtu extends Model
{
    protected $fillable = ['kategori','min','max'];
}
