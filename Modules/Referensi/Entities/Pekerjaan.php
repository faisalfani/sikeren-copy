<?php

namespace Modules\Referensi\Entities;

use Illuminate\Database\Eloquent\Model;

class pekerjaan extends Model
{
    protected $table = 'pekerjaan';
    protected $fillable = ['nama'];
    public $timestamps = false;
}
