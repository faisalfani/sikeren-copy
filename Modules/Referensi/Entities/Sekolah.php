<?php

namespace Modules\Referensi\Entities;

use Illuminate\Database\Eloquent\Model;

class sekolah extends Model
{
    protected $table = 'sekolah';
    protected $fillable = ['nama','id_kabupaten'];
    public $timestamps = false;
}
