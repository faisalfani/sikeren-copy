<?php

namespace Modules\Pegawai\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use \Modules\Pegawai\Entities\TipePegawai;



class TipePegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data = TipePegawai::all();
        return view('pegawai::tipe_pegawai', compact("data"));
    }

    public function store(Request $req)
    {

      
        if($req->edit){
            $data = TipePegawai::find($req->edit);
            $data->kode = $req->kode;
            $data->nama = $req->nama;
            $data->update();
            
        }else{
            // dd($req);
            $data = new TipePegawai;
            $data->kode = $req->kode;
            $data->nama = $req->nama;
            $data->save();

        }

        return back()->withSuccess("Data Berhasil di tambahkan");
    }

    public function destroy($id)
    {
        TipePegawai::find($id)->delete();
        return back()->withSuccess("Data Berhasil di hapus");

    }
}
