<?php

namespace Modules\Pegawai\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use \Modules\Pegawai\Entities\JenisPegawai;
use \Modules\Pegawai\Entities\TipePegawai;
use \Modules\Pegawai\Entities\Pegawai;
use \Modules\Pegawai\Entities\MasterPenggajian;
use \Modules\Pegawai\Entities\IhsanDanPotongan;
use stdClass;
use yajra\Datatables\Datatables;


class MasterPenggajianController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
   

      public function cariPegawai(Request $req){
       
            if ($req->has('q')) {
                $cari = $req->q;

                $data = Pegawai::select('id', 'nama', 'tipe_pegawai_id', 'jenis_pegawai_id')
                ->whereRaw('nama LIKE "%'.$cari.'%" OR nik LIKE "%'.$cari.'%" OR id LIKE "%'.$cari.'%" ')
                ->get();
                $data = $data->map(function($q) {
                    $tipe = TipePegawai::find($q->tipe_pegawai_id);
                    $jenis = JenisPegawai::find($q->jenis_pegawai_id);
                    $q->nama = $q->nama ." (".$tipe->kode." - ".$jenis->nama.")";
                    return $q;
                });
               if(count($data) > 0){
                return $data;
               }else{
                   return array([
                        "id"=> 0,
                        "nama"=> "Semua",
                    ]);
               }
              

               
            }
      }
     
    public function json(){
        return Datatables::of(MasterPenggajian::all())
        ->addColumn('nama', function($d) {
            $r = IhsanDanPotongan::find($d->itp_id);
            return  $r->nama;
        })
        ->addColumn('tipe_pegawai', function($p) {
            $tipe = TipePegawai::find($p->tipe_pegawai_id);
            if($tipe){
                return  $tipe->nama . "(".$tipe->kode. ")";
            }else{
                return "Semua";
            }
           
        })
        ->addColumn('jenis_pegawai', function($p) {
            $jenis = JenisPegawai::find($p->jenis_pegawai_id);
            if($jenis){
                return  $jenis->nama;
            }else{
                return "Semua";
            }
        })
        ->addColumn('pegawai', function($d) {
            if($d->pegawai_id){
                $pg = explode(",", $d->pegawai_id);
                if (count($pg) > 1) {
                    return count($pg)." Pegawai";
                } else {
                    $r = Pegawai::find($pg[0]);
                    return $r->nama;
                }
            }else{
                return "Semua";
            }
        })
        ->addColumn('list_pegawai', function($d) {
            if($d->pegawai_id){
                $pg = explode(",", $d->pegawai_id);
                if (count($pg) > 1) {
                    $pg = Pegawai::whereIn("id", $pg)->select("id", "nama")->orderBy('nama','ASC')->get();
                    return json_decode($pg, true);
                } else {
                    $r = Pegawai::where("id",$pg[0])->select("id", "nama")->orderBy('nama','ASC')->get();
                    return json_decode($r, true);
                }
            }else{
                return "All";
            }
        })

        
        ->make(true);
    }
    public function index()
    {

        $jenis = JenisPegawai::all();
        $tipe =  TipePegawai::all();

        return view('pegawai::master', compact("jenis","tipe"));
    }
    public function show($id){
        $data = MasterPenggajian::where("id",$id)->first();
        $pg = "";
        if ($data->pegawai_id) {
            $pegawai = explode(",", $data->pegawai_id);
            $pg = Pegawai::whereIn("id", $pegawai)->get();
        }
        $data->pegawai = $pg;
        return response()->json($data, 200);
    }

    public function store(Request $req)
    {
        // return $req;
        $pegawai = $req->pegawai_id;
        if ($pegawai) {
            if (count($pegawai) > 1) {
                if (($key = array_search("All", $pegawai)) !== false) {
                    array_splice($pegawai, $key, 1);
                }
            } else {
                if (($key = array_search("All", $pegawai)) !== false) {
                    $pegawai = '';
                }
            }
        }
        

        if ($pegawai != '') {
            $pegawai = implode(",",$pegawai);
        } else {
            $pegawai = NULL;
        }
        // return response()->json($pegawai, 200);
        $jumihsn = str_replace(".", "",$req->jumlah_ihsan);

        if($req->edit){
            $data = MasterPenggajian::find($req->edit);
            $data->tipe =  $req->tipe;
            $data->itp_id =  $req->itp;
            $data->tipe_pegawai_id =  $req->tipe_pegawai;
            $data->jenis_pegawai_id =  $req->jenis_pegawai;
            $data->is_persen =  $req->is_persen ? true : false;
            $data->jumlah_ihsan = $req->is_persen ? $req->jumlah_ihsan : $jumihsn;
            $data->pegawai_id =  $pegawai;
            $data->berlaku_sampai =  $req->berlaku_sampai;
            $data->dikalikan_masa_kerja =  $req->dikalikanmasakerja;
            $data->dikalikan_jam_kerja =  $req->dikalikanjamkerja;
            $data->status =  $req->status;
            $data->save();
        }else{
            $data = new MasterPenggajian;
  
            $data->tipe =  $req->tipe;
            $data->itp_id =  $req->itp;
            $data->tipe_pegawai_id =  $req->tipe_pegawai;
            $data->jenis_pegawai_id =  $req->jenis_pegawai;
            $data->jumlah_ihsan = $req->is_persen ? $req->jumlah_ihsan : $jumihsn;
            $data->pegawai_id =  $pegawai;
            $data->is_persen =  $req->is_persen ? true : false;
            $data->berlaku_sampai =  $req->berlaku_sampai;
            $data->dikalikan_masa_kerja =  $req->dikalikanmasakerja;
            $data->dikalikan_jam_kerja =  $req->dikalikanjamkerja;
            $data->status =  $req->status;
            $data->save();
        }
      

        return back()->withSuccess("Data Berhasil di tambahkan");

    }

    public function destroy($id)
    {
        //
        $data = MasterPenggajian::find($id);
        $data->delete();

        return back()->withSuccess("Data Berhasil di hapus");
    }
}
