<?php

namespace Modules\Pegawai\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use \Modules\Pegawai\Entities\JenisPegawai;
use \Modules\Pegawai\Entities\TipePegawai;
use \Modules\Pegawai\Entities\Pegawai;
use yajra\Datatables\Datatables;


class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
   

     public function cekNik($nik){
        $data = Pegawai::where('nik', $nik)->first();
        if ($data) {
            return 1;
        } else {
            return 0;
        }
        // return response()->json($nik, 200);
     }

     public function json(){
        return Datatables::of(Pegawai::all())
        ->addColumn('tipe_pegawai', function($p) {
            $tipe = TipePegawai::find($p->tipe_pegawai_id);
            return  $tipe->nama . "(".$tipe->kode. ")";
        })
        ->addColumn('jenis_pegawai', function($p) {
            $jenis = JenisPegawai::find($p->jenis_pegawai_id);
            return  $jenis->nama;
        })
        ->make(true);
    }
    
    public function index()
    {

        $jenis = JenisPegawai::all();
        $tipe = TipePegawai::all();

        return view('pegawai::index', compact("jenis","tipe"));
    }
    public function show($id){
        return Pegawai::find($id);
    }

    public function store(Request $req)
    {
        //
        // return $req;
        if ($req->sertifikasi == "on") {
            $serti = 0;
        } else {
            $serti = 1;
        }
        if($req->edit){

            $data = Pegawai::find($req->edit);
            $data->nik =  $req->nik;
            $data->nama =  $req->nama;
            $data->alamat =  $req->alamat;
            $data->sertifikasi =  $serti;
            $data->bank =  $req->nama_bank;
            $data->no_rek =  $req->no_rek;
            $data->no_hp =  $req->no_hp;
            $data->pin =  $req->pin;
            if ($req->pin_is_password) {
                # code...
                $data->password =  bcrypt($req->pin);
            }
            $data->tgl_masuk =  $req->tgl_masuk;
            $data->tipe_pegawai_id =  $req->tipe_pegawai;
            $data->jenis_pegawai_id =  $req->jenis_pegawai;
            $data->save();
        }else{

            $data = new Pegawai;
            $data->nik =  $req->nik;
            $data->nama =  $req->nama;
            $data->alamat =  $req->alamat;
            $data->sertifikasi =  $serti;
            $data->bank =  $req->nama_bank;
            $data->no_rek =  $req->no_rek;
            $data->no_hp =  $req->no_hp;
            $data->pin =  $req->pin;
            $data->password =  bcrypt($req->pin);
            $data->tgl_masuk =  $req->tgl_masuk;
            $data->tipe_pegawai_id =  $req->tipe_pegawai;
            $data->jenis_pegawai_id =  $req->jenis_pegawai;
            $data->save();
        }
      

        return back()->withSuccess("Data Berhasil di tambahkan");

    }

    public function destroy($id)
    {
        //
        $data = Pegawai::find($id);
        $data->delete();

        return back()->withSuccess("Data Berhasil di hapus");
    }
}
