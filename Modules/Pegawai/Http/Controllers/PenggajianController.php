<?php

namespace Modules\Pegawai\Http\Controllers;

use Date;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use yajra\Datatables\Datatables;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use \Modules\Pegawai\Entities\Pegawai;
use \Modules\Pegawai\Entities\Penggajian;
use \Modules\Pegawai\Entities\TipePegawai;

use \Modules\Pegawai\Entities\JenisPegawai;
use \Modules\Pegawai\Entities\IhsanDanPotongan;
use \Modules\Pegawai\Entities\MasterPenggajian;


class PenggajianController extends Controller
{
  
    public function json(){
        return Datatables::of(Penggajian::all())
        ->addColumn('nama', function($d) {
            $r = Pegawai::find($d->pegawai_id);
            return  $r->nama;
        })
      
        ->addColumn('list_tunjangan', function($d) {
            $dd = explode(",",$d->tunjangan_id);
            $r = MasterPenggajian::whereIn("id",$dd)->with("itp")->get();
            
            $res = "";
            foreach($r as $rr){
                if($rr->dikalikan_jam_kerja){
                    $total = $rr->jumlah_ihsan * $d->jam_kerja;
                }else if($rr->dikaikan_masa_kerja){
                    $total = $rr->jumlah_ihsan * $d->masa_kerja;
                }else{
                    $total =  $rr->jumlah_ihsan;
                }
                $res =  $res . $rr->itp->nama . " (".rupiah($total)."), ";
            }
            return $res;
        })

        ->addColumn('list_potongan', function($d) {
            $dd = explode(",",$d->potongan_id);
            $r = MasterPenggajian::whereIn("id",$dd)->with("itp")->get();
            $res = "";
            foreach($r as $rr){
                $res = $res . $rr->itp->nama . " (".rupiah($rr->jumlah_ihsan)."), ";
            }
            return $res;
        })
        
        ->make(true);
    }
    public function json2(Request $req){
        // return "asdd";
        // return response()->json($req, 200);
        $b = [
        'Lol',
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'];
        if ($buln = array_search($req->bulan, $b)) {
            // $buln = $buln+1;
        } else {
            $buln = $req->bulan;
        }
        // return $buln;
        $terakhir = Carbon::create($req->tahun, $buln)->lastOfMonth()->format('Y-m-d'); 
        $data = Pegawai::where(function($q) use($req){
            if ($req->tipe) {
                $q->where('tipe_pegawai_id', $req->tipe);
            }
            if ($req->jenis) {
                $q->where('jenis_pegawai_id', $req->jenis);
            }
        })->get();

        $data = $data->map(function($q) use($terakhir, $req){
            $ip_jumlahData = MasterPenggajian::where('itp_id', 1)
                ->where(function($dt) use($q) {
                    $dt->where('tipe_pegawai_id', $q->tipe_pegawai_id)
                    ->orWhereNull('tipe_pegawai_id');
                })
                ->where(function($dt) use($q) {
                    $dt->where('jenis_pegawai_id', $q->jenis_pegawai_id)
                    ->orWhereNull('jenis_pegawai_id');
                })
                ->where(function($dt) use($q) {
                    $dt->where(function($z) use($q) {
                        $z->where('pegawai_id', 'LIKE', "%,".$q->id.",%")
                        ->orWhere('pegawai_id', 'LIKE', $q->id)
                        ->orWhere('pegawai_id', 'LIKE', $q->id.",%")
                        ->orWhere('pegawai_id', 'LIKE', "%,".$q->id);
                    })
                    ->orWhereNull('pegawai_id');
                })
                ->where("status", 1)
                ->where(function($q) use($terakhir) {
                    $q->whereDate('berlaku_sampai','>=', $terakhir)->orWhereNull('berlaku_sampai');
                })
                ->orderBy('pegawai_id', 'DESC')
                ->first();
            if ($ip_jumlahData) {
                $ip_jumlah = $ip_jumlahData->jumlah_ihsan;
            } else {
                $ip_jumlah = 0;
            }
            $ijp_jumlahData = MasterPenggajian::where('itp_id', 2)
                ->where(function($dt) use($q) {
                    $dt->where('tipe_pegawai_id', $q->tipe_pegawai_id)
                    ->orWhereNull('tipe_pegawai_id');
                })
                ->where(function($dt) use($q) {
                    $dt->where('jenis_pegawai_id', $q->jenis_pegawai_id)
                    ->orWhereNull('jenis_pegawai_id');
                })
                ->where(function($dt) use($q) {
                    $dt->where(function($z) use($q) {
                        $z->where('pegawai_id', 'LIKE', "%,".$q->id.",%")
                        ->orWhere('pegawai_id', 'LIKE', $q->id)
                        ->orWhere('pegawai_id', 'LIKE', $q->id.",%")
                        ->orWhere('pegawai_id', 'LIKE', "%,".$q->id);
                    })
                    ->orWhereNull('pegawai_id');
                })
                ->where("status", 1)
                ->where(function($q) use($terakhir) {
                    $q->whereDate('berlaku_sampai','>=', $terakhir)->orWhereNull('berlaku_sampai');
                })
                ->orderBy('pegawai_id', 'DESC')
                ->first();
            if ($ijp_jumlahData) {
                $ijp_jumlah = $ijp_jumlahData->jumlah_ihsan;
            } else {
                $ijp_jumlah = 0;
            }

            $dataTunjangan = DB::table('master_penggajians as mg')
                ->leftJoin('ihsan_dan_potongans as ip', 'mg.itp_id', 'ip.id')
                ->where('mg.tipe', 1)
                ->where(function($dt) use($q) {
                    $dt->where('mg.tipe_pegawai_id', $q->tipe_pegawai_id)
                    ->orWhereNull('mg.tipe_pegawai_id');
                })
                ->where(function($dt) use($q) {
                    $dt->where('mg.jenis_pegawai_id', $q->jenis_pegawai_id)
                    ->orWhereNull('mg.jenis_pegawai_id');
                })
                ->where(function($dt) use($q) {
                    $dt->where(function($z) use($q) {
                        $z->where('mg.pegawai_id', 'LIKE', "%,".$q->id.",%")
                        ->orWhere('mg.pegawai_id', 'LIKE', $q->id)
                        ->orWhere('mg.pegawai_id', 'LIKE', $q->id.",%")
                        ->orWhere('mg.pegawai_id', 'LIKE', "%,".$q->id);
                    })
                    ->orWhereNull('mg.pegawai_id');
                })
                ->where("mg.status", 1)
                ->where(function($q) use($terakhir) {
                    $q->whereDate('mg.berlaku_sampai','>=', $terakhir)->orWhereNull('mg.berlaku_sampai');
                })
                ->select('mg.id','mg.jumlah_ihsan', 'ip.nama')->get();
            $idTunjangan = $dataTunjangan->pluck('id');
            $dataPotongan = DB::table('master_penggajians as mg')
                            ->leftJoin('ihsan_dan_potongans as ip', 'mg.itp_id', 'ip.id')
                            ->where('mg.tipe', 0)
                            ->where(function($dt) use($q) {
                                $dt->where('mg.tipe_pegawai_id', $q->tipe_pegawai_id)
                                ->orWhereNull('mg.tipe_pegawai_id');
                            })
                            ->where(function($dt) use($q) {
                                $dt->where('mg.jenis_pegawai_id', $q->jenis_pegawai_id)
                                ->orWhereNull('mg.jenis_pegawai_id');
                            })
                            ->where(function($dt) use($q) {
                                $dt->where(function($z) use($q) {
                                    $z->where('mg.pegawai_id', 'LIKE', "%,".$q->id.",%")
                                    ->orWhere('mg.pegawai_id', 'LIKE', $q->id)
                                    ->orWhere('mg.pegawai_id', 'LIKE', $q->id.",%")
                                    ->orWhere('mg.pegawai_id', 'LIKE', "%,".$q->id);
                                })
                                ->orWhere('mg.pegawai_id', '')
                                ->orWhereNull('mg.pegawai_id');
                            })
                            ->where(function($q) use($terakhir) {
                                $q->whereDate('mg.berlaku_sampai','>=', $terakhir)->orWhereNull('mg.berlaku_sampai');
                            })
                            ->where("mg.status", 1)
                            ->select('mg.id','mg.jumlah_ihsan', 'ip.nama', 'mg.is_persen')->get();
            
            
            $idPotongan = $dataPotongan->pluck('id');
            // MasterPenggajian::where('tipe', 1)->select('jumlah_ihsan');
            
            $jp = Penggajian::where('bulan', $req->bulan)->where('tahun', $req->tahun)->where('pegawai_id', $q->id)->first();
            if ($jp) {
                $jpin = $jp->ihsan_jk_jp;
            } else {
                $jpin = 0;
            }
            $tahun = Carbon::createFromFormat('Y-m-d', $q->tgl_masuk);
            $now = Carbon::now();
            // $mk = $now - $tahun;
            $mk = $tahun->diffInYears($now);
            $dataPotongan = $dataPotongan->map(function($q) use($ip_jumlah, $mk) {
                if ($q->is_persen) {
                    $q->jumlah_ihsan = $mk * $ip_jumlah * $q->jumlah_ihsan/100;
                }
               
                return $q;
            });
            $total = ($mk * $ip_jumlah) + ($ijp_jumlah  * $jpin)+ $dataTunjangan->sum('jumlah_ihsan') - $dataPotongan->sum('jumlah_ihsan');
            $z = [
                "id" => $q->id,
                "nama" => $q->nama,
                "tgl_masuk" => $q->tgl_masuk,
                "mk" => $mk,
                "ip_id" => $ip_jumlah == 0 ? 0 : $ip_jumlahData->id,
                "ip_jumlah" => $mk * $ip_jumlah,
                "jp" => $jpin,
                "ijp_id" => $ijp_jumlah == 0 ? 0 : $ijp_jumlahData->id,
                "ijp_jumlah" => $ijp_jumlah,
                "tunjangan_id" => implode(',', $idTunjangan->all() ),
                "tunjangan_data" => $dataTunjangan,
                "tunjangan" => $dataTunjangan->sum('jumlah_ihsan'),
                "potongan_id" => implode(',', $idPotongan->all() ),
                "potongan_data" => $dataPotongan,
                "potongan" => $dataPotongan->sum('jumlah_ihsan'),
                "total" => round($total)

            ];
            return $z;
        });

        // return response()->json($data, 200);
        return Datatables::of($data)
        ->make(true);
    }
    public function loadData(Request $req){
        // return "asdd";
        // return response()->json($req, 200);
        $b = [
        'Lol',
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'];
        if ($buln = array_search($req->bulan, $b)) {
            // $buln = $buln+1;
        } else {
            $buln = $req->bulan;
        }
        // return $buln;
        $terakhir = Carbon::create($req->tahun, $buln)->lastOfMonth()->format('Y-m-d'); 

        $limit = $req->limit ? $req->limit : 10;
        $data = Pegawai::where(function($q) use($req){
            if ($req->id > 0) {
                $q->where('id','>',$req->id);
            }
            if ($req->tipe) {
                $q->where('tipe_pegawai_id', $req->tipe);
            }
            if ($req->jenis) {
                $q->where('jenis_pegawai_id', $req->jenis);
            }
            if ($req->cari) {
                $q->where('nama','LIKE', '%'.$req->cari.'%')
                ->orWhere('nik','LIKE', '%'.$req->cari.'%');
            }
        })
        ->orderBy('id','ASC')
        ->limit($limit)
        ->get();

        if ($req->cari) {
            $jumlah_siswa = Pegawai::where(function($q) use($req){
                    if ($req->id > 0) {
                        $q->where('id','<',$req->id);
                    }
                    if ($req->tipe) {
                        $q->where('tipe_pegawai_id', $req->tipe);
                    }
                    if ($req->jenis) {
                        $q->where('jenis_pegawai_id', $req->jenis);
                    }
                    if ($req->cari) {
                        $q->where('nama','LIKE', '%'.$req->cari.'%')
                        ->orWhere('nik','LIKE', '%'.$req->cari.'%');
                    }
                })->count();
        } else {
            $jumlah_siswa = Pegawai::where(function($q) use($req){
                    if ($req->tipe) {
                        $q->where('tipe_pegawai_id', $req->tipe);
                    }
                    if ($req->jenis) {
                        $q->where('jenis_pegawai_id', $req->jenis);
                    }
                })->count();
        }
        
        $load_siswa =  $data->count() > 0 ? ($req->ke * 10) + $data->count() : $jumlah_siswa;
        
        $output = '';
        $last_id = '';
        if (!$data->isEmpty()) {

            foreach ($data as $key => $q){
                $ip_jumlahData = MasterPenggajian::where('itp_id', 1)
                    ->where(function($dt) use($q) {
                        $dt->where('tipe_pegawai_id', $q->tipe_pegawai_id)
                        ->orWhereNull('tipe_pegawai_id');
                    })
                    ->where(function($dt) use($q) {
                        $dt->where('jenis_pegawai_id', $q->jenis_pegawai_id)
                        ->orWhereNull('jenis_pegawai_id');
                    })
                    ->where(function($dt) use($q) {
                        $dt->where(function($z) use($q) {
                            $z->where('pegawai_id', 'LIKE', "%,".$q->id.",%")
                            ->orWhere('pegawai_id', 'LIKE', $q->id)
                            ->orWhere('pegawai_id', 'LIKE', $q->id.",%")
                            ->orWhere('pegawai_id', 'LIKE', "%,".$q->id);
                        })
                        ->orWhereNull('pegawai_id');
                    })
                    ->where("status", 1)
                    ->where(function($q) use($terakhir) {
                        $q->whereDate('berlaku_sampai','>=', $terakhir)->orWhereNull('berlaku_sampai');
                    })
                    ->orderBy('pegawai_id', 'DESC')
                    ->first();
                if ($ip_jumlahData) {
                    $ip_jumlah = $ip_jumlahData->jumlah_ihsan;
                } else {
                    $ip_jumlah = 0;
                }
                $ijp_jumlahData = MasterPenggajian::where('itp_id', 2)
                    ->where(function($dt) use($q) {
                        $dt->where('tipe_pegawai_id', $q->tipe_pegawai_id)
                        ->orWhereNull('tipe_pegawai_id');
                    })
                    ->where(function($dt) use($q) {
                        $dt->where('jenis_pegawai_id', $q->jenis_pegawai_id)
                        ->orWhereNull('jenis_pegawai_id');
                    })
                    ->where(function($dt) use($q) {
                        $dt->where(function($z) use($q) {
                            $z->where('pegawai_id', 'LIKE', "%,".$q->id.",%")
                            ->orWhere('pegawai_id', 'LIKE', $q->id)
                            ->orWhere('pegawai_id', 'LIKE', $q->id.",%")
                            ->orWhere('pegawai_id', 'LIKE', "%,".$q->id);
                        })
                        ->orWhereNull('pegawai_id');
                    })
                    ->where("status", 1)
                    ->where(function($q) use($terakhir) {
                        $q->whereDate('berlaku_sampai','>=', $terakhir)->orWhereNull('berlaku_sampai');
                    })
                    ->orderBy('pegawai_id', 'DESC')
                    ->first();
                if ($ijp_jumlahData) {
                    $ijp_jumlah = $ijp_jumlahData->jumlah_ihsan;
                } else {
                    $ijp_jumlah = 0;
                }

                $dataTunjangan = DB::table('master_penggajians as mg')
                    ->leftJoin('ihsan_dan_potongans as ip', 'mg.itp_id', 'ip.id')
                    ->where('mg.tipe', 1)
                    ->where(function($dt) use($q) {
                        $dt->where('mg.tipe_pegawai_id', $q->tipe_pegawai_id)
                        ->orWhereNull('mg.tipe_pegawai_id');
                    })
                    ->where(function($dt) use($q) {
                        $dt->where('mg.jenis_pegawai_id', $q->jenis_pegawai_id)
                        ->orWhereNull('mg.jenis_pegawai_id');
                    })
                    ->where(function($dt) use($q) {
                        $dt->where(function($z) use($q) {
                            $z->where('mg.pegawai_id', 'LIKE', "%,".$q->id.",%")
                            ->orWhere('mg.pegawai_id', 'LIKE', $q->id)
                            ->orWhere('mg.pegawai_id', 'LIKE', $q->id.",%")
                            ->orWhere('mg.pegawai_id', 'LIKE', "%,".$q->id);
                        })
                        ->orWhereNull('mg.pegawai_id');
                    })
                    ->where("mg.status", 1)
                    ->where(function($q) use($terakhir) {
                        $q->whereDate('mg.berlaku_sampai','>=', $terakhir)->orWhereNull('mg.berlaku_sampai');
                    })
                    ->select('mg.id','mg.jumlah_ihsan', 'ip.nama')->get();
                $idTunjangan = $dataTunjangan->pluck('id');
                $dataPotongan = DB::table('master_penggajians as mg')
                                ->leftJoin('ihsan_dan_potongans as ip', 'mg.itp_id', 'ip.id')
                                ->where('mg.tipe', 0)
                                ->where(function($dt) use($q) {
                                    $dt->where('mg.tipe_pegawai_id', $q->tipe_pegawai_id)
                                    ->orWhereNull('mg.tipe_pegawai_id');
                                })
                                ->where(function($dt) use($q) {
                                    $dt->where('mg.jenis_pegawai_id', $q->jenis_pegawai_id)
                                    ->orWhereNull('mg.jenis_pegawai_id');
                                })
                                ->where(function($dt) use($q) {
                                    $dt->where(function($z) use($q) {
                                        $z->where('mg.pegawai_id', 'LIKE', "%,".$q->id.",%")
                                        ->orWhere('mg.pegawai_id', 'LIKE', $q->id)
                                        ->orWhere('mg.pegawai_id', 'LIKE', $q->id.",%")
                                        ->orWhere('mg.pegawai_id', 'LIKE', "%,".$q->id);
                                    })
                                    ->orWhere('mg.pegawai_id', '')
                                    ->orWhereNull('mg.pegawai_id');
                                })
                                ->where(function($q) use($terakhir) {
                                    $q->whereDate('mg.berlaku_sampai','>=', $terakhir)->orWhereNull('mg.berlaku_sampai');
                                })
                                ->where("mg.status", 1)
                                ->select('mg.id','mg.jumlah_ihsan', 'ip.nama', 'mg.is_persen')->get();
                
                
                $idPotongan = $dataPotongan->pluck('id');
                // MasterPenggajian::where('tipe', 1)->select('jumlah_ihsan');
                
                $jp = Penggajian::where('bulan', $req->bulan)->where('tahun', $req->tahun)->where('pegawai_id', $q->id)->first();
                if ($jp) {
                    $jpin = $jp->ihsan_jk_jp;
                } else {
                    $jpin = 0;
                }
                $tahun = Carbon::createFromFormat('Y-m-d', $q->tgl_masuk);
                $now = Carbon::now();
                // $mk = $now - $tahun;
                $mk = $tahun->diffInYears($now);
                $dataPotongan = $dataPotongan->map(function($q) use($ip_jumlah, $mk) {
                    if ($q->is_persen) {
                        $q->jumlah_ihsan = $mk * $ip_jumlah * $q->jumlah_ihsan/100;
                    }
                
                    return $q;
                });
                $total = ($mk * $ip_jumlah) + ($ijp_jumlah  * $jpin)+ $dataTunjangan->sum('jumlah_ihsan') - $dataPotongan->sum('jumlah_ihsan');

                    $q->id = $q->id;
                    $q->nama = $q->nama;
                    $q->tgl_masuk = $q->tgl_masuk;
                    $q->mk = $mk;
                    $q->ip_id = $ip_jumlah == 0 ? 0 : $ip_jumlahData->id;
                    $q->ip_jumlah = $mk * $ip_jumlah;
                    $q->jp = $jpin;
                    $q->ijp_id = $ijp_jumlah == 0 ? 0 : $ijp_jumlahData->id;
                    $q->ijp_jumlah = $ijp_jumlah;
                    $q->tunjangan_id = implode(',', $idTunjangan->all() );
                    $q->tunjangan_data = $dataTunjangan;
                    $q->tunjangan = $dataTunjangan->sum('jumlah_ihsan');
                    $q->potongan_id = implode(',', $idPotongan->all() );
                    $q->potongan_data = $dataPotongan;
                    $q->potongan = $dataPotongan->sum('jumlah_ihsan');
                    $q->total = round($total);

                $no = $key + 1;
                $nomor =  $req->ke == "0" ? $no : ($req->ke * 10) + $no;

                // Kebutuhan Tahun
                $tahun = Carbon::createFromFormat('Y-m-d', $q->tgl_masuk)->format('Y');

                // Kebutuhan tunjangan
                $listTunjangan = '';
                if ($dataTunjangan->count() > 0) {
                    $listTunjangan = 'title ="<ol class=`text-left p-0 pl-2`>';
                    foreach ($dataTunjangan as $index => $d){
                        $listTunjangan .= '<li>'.$d->nama.' : '.rupiah($d->jumlah_ihsan).'</li>';
                    }
                    $listTunjangan .= '</ol>"';
                }
                


                // Kebutuhan Potongan
                $listPotongan = '';
                if ($dataPotongan->count() > 0) {
                    $listPotongan = 'title ="<ol class=`text-left p-0 pl-2`>';
                    foreach ($dataPotongan as $index => $d){
                        $listPotongan .= '<li >'.$d->nama.' : '.rupiah($d->jumlah_ihsan).'</li>';
                    }
                    $listPotongan .= '</ol>"';
                }
                
                $output .= '
                    <tr>
                        <td>'.$nomor.'</td>
                        <td>'.$q->nama.'</td>
                        <td>'.$tahun.'</td>
                        <td>'.$mk.'</td>
                        <td>'.rupiah($mk * $ip_jumlah).'</td>
                        <td>
                            <input name="ijk_jp['.$q->id.']" 
                                type="number" value="'.$q->jp.'" 
                                min="0" id="edit_jam_kerja_'.$q->id.'" 
                                class="form-control" style="width: 70px;" 
                                oninput="setDataTableIjpJumlah('.$q->id.', this.value, '.$ijp_jumlah.', '.round($total).')">

                            <input name="id['.$q->id.']"  type="number" value="'.$q->id.'" hidden>
                            <input name="mk['.$q->id.']"  type="number" value="'.$q->mk.'" hidden>
                            <input name="ip_id['.$q->id.']"  type="text" value="'.$q->ip_id.'" hidden>
                            <input name="ijp_id['.$q->id.']"  type="text" value="'.$q->ijp_id.'" hidden>
                            <input name="tunjangan_id['.$q->id.']"  type="text" value="'.$q->tunjangan_id.'" hidden>
                            <input name="potongan_id['.$q->id.']"  type="text" value="'.$q->potongan_id.'" hidden>
                        </td>
                        <td>
                            <span id="ijp_jumlah_'.$q->id.'">'.rupiah($q->ijp_jumlah * $q->jp).'</span>
                        </td>
                        <td>
                            <a id="tunjangan_'.$q->id.'" data-placement="top" data-toggle="tooltip" data-html="true" '.$listTunjangan.'>'.rupiah($dataTunjangan->sum('jumlah_ihsan')).'</a>
                        </td>
                        <td>
                            <a id="potongan_'.$q->id.'" data-placement="top" data-toggle="tooltip2" data-html="true" '.$listPotongan.'>'.rupiah($dataPotongan->sum('jumlah_ihsan')).'</a>
                        </td>
                        <td>
                            <span id="total_'.$q->id.'">'.rupiah(round($total)).'</span>
                        </td>
                        <td>
                            <a  id="a-resume'.$q->id.'" href="/penggajian/resume-id/'.$req->bulan.'/'.$req->tahun.'/'.$q->id.'" class="btn btn-sm bg-success btn-labeled  btn-outline-success mt-2" target="_blank">
                                <b><i class="icon-printer"></i></b>
                                Slip
                            </a>
                        </td>

                    </tr>
                ';
                // return $z;
                $last_id = $q->id ;
            }
            $output .= '
            <tr id="load_more">
                    <td colspan="11">
                        <div class="text-center mb-2">
                            Menampilkan <b>'.$load_siswa.'</b> dari <b>'. $jumlah_siswa.'</b> Data Pegawai
                        </div>
                        <button type="button" name="load_more_button" class="btn btn-success form-control" data-id="' . $last_id . '" id="load_more_button">Load More</button>
                    </td>
            </tr>
            ';
        } else {
            $output .= '
            <tr id="load_more">
                <td colspan="11">
                    <div class="text-center mb-2">
                        Menampilkan <b>'.$load_siswa.'</b> dari <b>'. $jumlah_siswa.'</b> Data Pegawai
                    </div>
                    <div name="load_more_button" class="btn btn-info form-control">No Data Found</div>
                </td>
            </tr>';
        }
        return $output;

        return response()->json($data, 200);
        
    }
    public function index()
    {

        return view('pegawai::penggajian-lamaa');
    }
    public function index2()
    {

        return view('pegawai::penggajian');
    }

    public function cekPenggajian($bulan, $tahun){
        return Penggajian::where('bulan', $bulan)->where('tahun', $tahun)->count();
        // $data;
    }
    public function copasPenggajian($bulan, $tahun){
        $bulanCopas = $bulan - 1 == 0 ? 12 : $bulan -1;
        $tahunCopas = $bulan == 12 ? $tahun - 1 : $tahun;

        $dataLama = Penggajian::where('bulan', $bulanCopas)->where('tahun', $tahunCopas)
                    ->each(function($q) use($bulan, $tahun){
                        $data = new Penggajian();
                        $data->pegawai_id = $q->pegawai_id;
                        $data->masa_kerja = $q->masa_kerja;
                        $data->ihsan_pokok_id = $q->ihsan_pokok_id;
                        $data->ihsan_jk_jp = $q->ihsan_jk_jp;
                        $data->ihsan_jk_id = $q->ihsan_jk_id;
                        $data->tunjangan_id = $q->tunjangan_id;
                        $data->potongan_id = $q->potongan_id;
                        $data->bulan = $bulan;
                        $data->tahun = $tahun;
                        $data->save();
                    });
      

        return $dataLama ? 1 : 0;
        // $data;
    }
    public function show($id){
        return Penggajian::where("id",$id)->with("pegawai")->first();
    }

    public function store(Request $req)
    {

        // return response()->json($req, 200);
        foreach ($req->id as $v) {
           $sudahAda = Penggajian::where('pegawai_id',$v)->where('bulan', $req->bulan)->where('tahun', $req->tahun)->first();
           if ($sudahAda) {
               $data = Penggajian::find($sudahAda->id);
           } else {
               $data = new Penggajian();
           }
           
           $data->pegawai_id = $v;
           $data->masa_kerja = $req->mk[$v];
           $data->ihsan_pokok_id = $req->ip_id[$v];
           $data->ihsan_jk_jp = $req->ijk_jp[$v];
           $data->ihsan_jk_id = $req->ijp_id[$v];
           $data->tunjangan_id = $req->tunjangan_id[$v];
           $data->potongan_id = $req->potongan_id[$v];
           $data->bulan = $req->bulan;
           $data->tahun = $req->tahun;

           if ($sudahAda) {
                $res = $data->update();
            } else {
                $res = $data->save();
            }
        }

        $ses = [
            "success" => "Data Berhasil di Buat",
            "bulan" => $req->bulan,
            "tahun" => $req->tahun
        ];

        return response()->json($ses, 200);
        // return back()->with($ses);

    }

    

    public function daysDifference($endDate, $beginDate)
    {
        $date_parts1=explode("-", $beginDate);
        $date_parts2=explode("-", $endDate);
        return $date_parts2[0] - $date_parts1[0];
    
    }

    public function destroy($id)
    {
        //
        $data = Penggajian::find($id);
        if($data->status == 0){
            $data->delete();
        }else{
        return back()->withSuccess("Data tidak bisa di hapus");
           
        }
        

        return back()->withSuccess("Data Berhasil di hapus");
    }

    public function ubahStatus($id){
        $data = Penggajian::find($id);
        if($data->status == 0){
            $data->status = 1;
        }else{
            $data->status = 0;
        }
        $data->save();

        return back()->withSuccess("Data Berhasil di Ubah");

    }

    public function ubahJamkerja($id,$jam_kerja){

        $data = Penggajian::find($id);

        $p =  Pegawai::where("id",$data->pegawai_id)->first();

       
        $data->jam_kerja = $jam_kerja;
       
        $masaKerja = $this->daysDifference(date('Y-m-d'),$p->tgl_masuk);
        
        if($masaKerja > 0){
            $data->masa_kerja = $masaKerja;
        }else{
            $data->masa_kerja = 1;
        }

        // Tunjangan
        // 1. Get Tunjangan Untuk Semua Pegawai
        $tunjangan0 = MasterPenggajian::where("pegawai_id",null)
                    ->where("jenis_pegawai_id",null)
                    ->where("tipe_pegawai_id",null)
                    ->select("id","jumlah_ihsan","berlaku_sampai","dikalikan_jam_kerja","dikalikan_masa_kerja","tipe")
                    ->get()->toArray();
        
       
        // 2. Get Tunjangan untuk satu jenis pegawai
        $tunjangan1 = MasterPenggajian::where("pegawai_id",null)
                    ->where("jenis_pegawai_id",$p->jenis_pegawai_id)
                    ->where("tipe_pegawai_id",null)
                    ->select("id","jumlah_ihsan","berlaku_sampai","dikalikan_jam_kerja","dikalikan_masa_kerja","tipe")->get()
                    ->toArray();
        

        // 3. Get Tunjangan untuk satu tipe pegawai
        $tunjangan2 = MasterPenggajian::where("pegawai_id",null)
                    ->where("jenis_pegawai_id",null)
                    ->where("tipe_pegawai_id",$p->tipe_pegawai_id)
                    ->select("id","jumlah_ihsan","berlaku_sampai","dikalikan_jam_kerja","dikalikan_masa_kerja","tipe")->get()
                    ->toArray();


         // 4. Get Tunjangan untuk satu tipe dan satu jenis pegawai
         $tunjangan3 = MasterPenggajian::where("pegawai_id",null)
         ->where("jenis_pegawai_id",$p->jenis_pegawai_id)
         ->where("tipe_pegawai_id",$p->tipe_pegawai_id)
         ->select("id","jumlah_ihsan","berlaku_sampai","dikalikan_jam_kerja","dikalikan_masa_kerja","tipe")->get()
         ->toArray();

          // 5. Get Tunjangan untuk khusus pegawai
        $tunjangan4 = MasterPenggajian::where("pegawai_id",$p->pegawai_id)
                    ->where("jenis_pegawai_id",null)
                    ->where("tipe_pegawai_id",null)
                    ->select("id","jumlah_ihsan","berlaku_sampai","dikalikan_jam_kerja","dikalikan_masa_kerja","tipe")->get()
                    ->toArray();

                    
        $tunjanganData = array_merge($tunjangan0,$tunjangan1,$tunjangan2,$tunjangan3,$tunjangan4);
       
        $totalTunjangan = 0;
        $tunjanganId = [];

 
        $totalpotongan = 0;
        $potonganId = [];


        $keterangan = [];


        foreach(collect($tunjanganData)->unique("id") as $t){
             // Note:  Cek  Berlaku Sampai

           
                 
                if($t['tipe'] == 1){

               
                        if($t['berlaku_sampai'] !== null && date("Y-m-d") > $t['berlaku_sampai']){

                        }else{
                
                            // Note:  Cek Dikalikan Masa Kerja atau Jam Kerja
                
                            if($t['dikalikan_jam_kerja']){
                                $totalTunjangan += $jam_kerja * $t['jumlah_ihsan'];
                            }else if($t['dikalikan_masa_kerja']){
                                $totalTunjangan += $data->masa_kerja * $t['jumlah_ihsan'];
                            }else{
                                $totalTunjangan += $t['jumlah_ihsan'];
                            }


                            array_push($tunjanganId,$t['id']);
                        }
                }else if($t['tipe'] == 0){

                        if($t['berlaku_sampai'] !== null && date("Y-m-d") > $t['berlaku_sampai']){

                        }else{
                
                            // Note:  Cek Dikalikan Masa Kerja atau Jam Kerja
                
                            if($t['dikalikan_jam_kerja']){
                                $totalpotongan += $jam_kerja * $t['jumlah_ihsan'];
                            }else if($t['dikalikan_masa_kerja']){
                                $totalpotongan += $data->masa_kerja * $t['jumlah_ihsan'];
                            }else{
                                $totalpotongan += $t['jumlah_ihsan'];
                            }
                            array_push($potonganId,$t['id']);
                        }

                }


        }


        $data->tunjangan_id = implode(",",$tunjanganId);
        $data->tunjangan = $totalTunjangan;
        $data->potongan_id = implode(",",$potonganId);
        $data->potongan = $totalpotongan;
        $data->total_ihsan_diterima = $totalTunjangan - $totalpotongan;
        $data->save();

        return "sucess";

    }
}
