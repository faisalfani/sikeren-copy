<?php

namespace Modules\Pegawai\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use \Modules\Pegawai\Entities\IhsanDanPotongan;
use Modules\Pegawai\Entities\MasterPenggajian;

class ITPPegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function byTipe($tipe){
        return IhsanDanPotongan::where("tipe",$tipe)->get();
    }
    public function index()
    {
        $data = IhsanDanPotongan::all();
        $data = $data->map(function ($q) {
            switch ($q->tipe) {
                case '0':
                    $q->tipe_nama = 'Potongan';
                    break;
                
                case '1':
                    $q->tipe_nama = 'Tunjangan';
                    break;
                case '2':
                    $q->tipe_nama = 'Ihsan';
                    break;
                
                default:
                $q->tipe_nama = '?';
                    break;
            }

            $q->is_gajih = MasterPenggajian::where('itp_id', $q->id)->count();

            return $q;
        });
        // return $data;

        return view('pegawai::itp', compact("data"));
    }

    public function store(Request $req)
    {
        if ($req->jabatan == "on") {
            $jabatan = 1;
        } else {
            $jabatan = 0;
        }
        
    //   return response()->json($req, 200);
        if($req->edit){
            $data = IhsanDanPotongan::find($req->edit);
            $data->tipe = $req->tipe;
            $data->nama = $req->nama;
            $data->is_jabatan = $jabatan;
            $data->update();
            
        }else{
            // dd($req);
            $data = new IhsanDanPotongan;
            $data->tipe = $req->tipe;
            $data->nama = $req->nama;
            $data->is_jabatan = $jabatan;
            $data->save();

        }

        return back()->withSuccess("Data Berhasil di tambahkan");
    }

    public function destroy($id)
    {
        IhsanDanPotongan::find($id)->delete();
        return back()->withSuccess("Data Berhasil di hapus");

    }
}
