<?php

namespace Modules\Pegawai\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use \Modules\Pegawai\Entities\JenisPegawai;




class JenisPegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data = JenisPegawai::all();
        return view('pegawai::jenis_pegawai', compact("data"));
    }

    public function store(Request $req)
    {

      
        if($req->edit){
            $data = JenisPegawai::find($req->edit);
            // $data->kode = $req->kode;
            $data->nama = $req->nama;
            $data->update();
            
        }else{
            // dd($req);
            $data = new JenisPegawai;
            // $data->kode = $req->kode;
            $data->nama = $req->nama;
            $data->save();

        }

        return back()->withSuccess("Data Berhasil di tambahkan");
    }

    public function destroy($id)
    {
        JenisPegawai::find($id)->delete();
        return back()->withSuccess("Data Berhasil di hapus");

    }
}
