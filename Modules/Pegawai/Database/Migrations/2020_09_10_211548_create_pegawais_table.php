<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePegawaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pegawais', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string("nik");
            $table->string("password");
            
            $table->string("nama");
            $table->string("foto")->nullable();
            $table->date("tgl_masuk");
            $table->integer("tipe_pegawai_id");
            $table->integer("jenis_pegawai_id");
            $table->string("no_rek")->nullable();
            $table->string("bank")->nullable();
            $table->string("bio")->nullable();
            $table->string("no_hp")->nullable();
            $table->string("alamat")->nullable();
            $table->string("pin")->nullable();
            $table->integer("status")->default(1);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pegawais');
    }
}
