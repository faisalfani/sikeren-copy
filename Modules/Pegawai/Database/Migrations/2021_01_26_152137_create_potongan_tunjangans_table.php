<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePotonganTunjangansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('potongan_tunjangans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('penggajian_id');
            $table->integer('ihsan_dan_potongan_id');
            $table->double('jumlah_ihsan', 8, 2);
            $table->integer('jumlah_ihsan');
            $table->integer('jumlah_ihsan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('potongan_tunjangans');
    }
}
