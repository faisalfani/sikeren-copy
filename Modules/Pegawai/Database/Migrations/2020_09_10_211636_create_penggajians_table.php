<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenggajiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penggajians', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('pegawai_id');
            $table->integer('masa_kerja');
            $table->string('ihsan_pokok_id');
            $table->integer('ihsan_jk_jp');
            $table->string('ihsan_jk_id');
            $table->string('tunjangan_id');
            $table->string('potongan_id');
            $table->integer("bulan");
            $table->integer("tahun");
            $table->boolean('status')->default(false);            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penggajians');
    }
}
