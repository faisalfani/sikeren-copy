<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterPenggajiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_penggajians', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("tipe",1);
            $table->integer("itp_id");
            $table->integer("tipe_pegawai_id")->nullable();
            $table->integer("jenis_pegawai_id")->nullable();
            $table->integer("jumlah_ihsan");
            $table->boolean("dikalikan_masa_kerja");
            $table->boolean("dikalikan_jam_kerja");
            $table->integer("pegawai_id")->nullable();
            $table->date("berlaku_sampai")->nullable();
            $table->boolean("status")->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_penggajians');
    }
}
