@extends('layouts.app')
@section('title')
Penggajian
@endsection
@section('css-after') 
<meta name="_token" content="{{ csrf_token() }}">

<!-- Select2 -->
<link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.css">

<style>
  .sticky {
    position: fixed;
    /* margin: 0 50%; */
    bottom: 15px ;
    left: 0;
    z-index: 999;
    /* height: 50px; */
    width: 100%
  }
  .sticky > .btn-danger{
    display: block;
    margin: 0 auto;
    width: 50%;
    float: unset !important;
  }
</style>
@endsection
@section('content')
{{-- @if ($message = Session::get('success')) --}}
<div class="alert alert-success alert-dismissible" style="position: fixed;z-index: 99;right: 20px;display:none;width:23rem;" id="divMessage">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4><i class="icon fa fa-check"></i> Berhasil</h4>
  {{-- {{ $message }} --}}
  <span id="textMessage"></span>
  
</div>
{{-- @endif --}}
<form action="/penggajian" method="POST"  id="addform">
  @csrf
<div class="container-fluid ">
  <div class="row mb-3 ">
    <div class="col-md-12 mx-auto bg-white p-3">
      <div class="row ">
        <div class="col align-self-center">
          <h4 class="text-left"> Penggajian
              
          </h4>
        </div>
        <div class="col-10 align-items-end">
          <div class="row">

          
          <div class="col">
            <div class="form-group col-md-12 mb-0">
              <label> Bulan </label>
              <select required name="bulan" id="bulan" class="select2 form-control">
              @php
              $bulan = array (
              'Januari',
              'Februari',
              'Maret',
              'April',
              'Mei',
              'Juni',
              'Juli',
              'Agustus',
              'September',
              'Oktober',
              'November',
              'Desember'
              );
              @endphp 
              @foreach ($bulan as $key => $item)
              <option
              @if ($message = Session::get('bulan'))
                @if ($message == $key+1)
                value="{{$message}}"   
                selected
                @endif
                
              @else
                @if (Date('m') == $key+1)
                selected
                @endif
                value="{{$key+1}}"
              @endif
              
              >{{ $item }}</option>
              @endforeach
              </select>
           </div>
          </div>
          <div class="col">
            <div class="form-group col-md-12 mb-0">
              <label> Tahun </label>
              @php
              $y = Date('Y')-1;
              if(Date('m') < 7) {
              $tahunnn = Date('Y') - 1;
              }else{
              $tahunnn = Date('Y');
              }
              $tahunnn = Date('Y');
              @endphp
              <select required name="tahun" id="tahun" class="select2 form-control">
              @for ($i = 0; $i < 3; $i++)
              <option 
              @if ($message = Session::get('tahun'))
                @if ($message == $y+$i)
                selected
                @endif
                
              @else
                @if($y+$i == $tahunnn)
                selected 
                @endif 
              @endif 
              value="{{$y+$i}}">{{ $y+$i }}</option>
              @endfor
              </select>
           </div>
          </div>

          <div class="col pt-2">
            <button class="btn btn-primary mt-4 btn-labeled" id="btnSalin" type="button">
              <b><i class="fa fa-copy"></i></b>
              Salin dari bulan sebelumnya
            </button>
          </div>
          
          <div class="col pt-2">
            <div id="divBtnSimpan">
              <button type="submit" id="btn-tambah" class="mt-4  float-right btn btn-labeled btn-danger">
                <b><i class="fa fa-save"></i></b>
                Simpan
                </button>
            </div>
          </div>
          </div>
          <div class="row mt-3">
            <div class="col">
              <div class="btn-group">
                  <a id="a-resume" class="btn btn-sm bg-success btn-labeled  btn-outline-success mt-2" target="_blank">
                      <b><i class="icon-printer2"></i></b>
                      Slip Ihsan
                  </a>

                <a  id="a-laporan" class="btn btn-sm bg-green-900 btn-labeled btn-outline-success mt-2" target="_blank">
                  <b><i class="icon-printer"></i></b>
                  Laporan Ihsan
                  </a>
                <a  id="a-payroll" class="btn btn-sm bg-purple btn-labeled btn-outline-success mt-2" target="_blank">
                  <b><i class="icon-printer"></i></b>
                  Payroll Ihsan
                  </a>
                <a  id="a-payroll2" class="btn btn-sm bg-info btn-labeled btn-outline-success mt-2" target="_blank">
                  <b><i class="icon-printer"></i></b>
                  Payroll Ihsan v2
                  </a>
              </div>
            </div>
            <div class="col-2"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
   <div class="row " >
    
      
      <div class="col-md-12 mx-auto bg-white p-2">
         <div class="row">
           <div class="col">
            <div class="form-group col-md-12">
                <label> Tipe Pegawai </label>
                <select name="tipe_pegawai" class="select2 form-control" onchange="generateDataTable()" id="filterTipePegawai">
                  <option value="">Semua</option>
                  @foreach(\Modules\Pegawai\Entities\TipePegawai::get() as $key => $value)
                  <option value="{{$value->id}}">{{$value->nama}} ( {{$value->kode}} )</option>
                  @endforeach
                </select>
            </div>
           </div>
           <div class="col">
            <div class="form-group col-md-12">
                <label> Jenis Pegawai </label>
                <select name="jenis_pegawai" class="select2 form-control" onchange="generateDataTable()" id="filterJenisPegawai">
                  <option value="">Semua</option>
                  @foreach(\Modules\Pegawai\Entities\JenisPegawai::get() as $key => $value)
                  <option value="{{$value->id}}">{{$value->nama}}</option>
                  @endforeach
                </select>
            </div>
           </div>
         </div>
      </div>
      <div class="col-md-12 mx-auto bg-white p-2">
         <div class="table-responsive">
           <form id="formPenggajian">
            <table  class="table table-bordered table-hover" id="data-table-pegawai" style="cursor: pointer">
               <thead>
                  <tr>
                     <th class="text-center" rowspan="2">No</th>
                     <th class="text-center" rowspan="2">Nama</th>
                     <th class="text-center" rowspan="2">Tahun Masuk</th>
                     <th class="text-center" colspan="2">Ihsan Pokok</th>
                     <th class="text-center" colspan="2">Ihsan Jam Kerja</th>
                     <th class="text-center" rowspan="2">Tunjangan</th>
                     <th class="text-center" rowspan="2">Potongan</th>
                     <th class="text-center" rowspan="2">Total Ihsan</th>
                     <th class="text-center" rowspan="2" width="1%;" class="text-right">Cetak</th>
                  </tr>
                  <tr>
                    <th>MK</th>
                    <th>Jumlah</th>
                    <th>JP</th>
                    <th>Jumlah</th>
                  </tr>
               </thead>
               <tbody>
               </tbody>
            </table>
          </form>
         </div>
      </div>
   </div>
</div>
</form>
@endsection
@section('js-after')
<!-- Select2 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="../../plugins/select2/js/select2.full.min.js"></script>
<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script type="text/javascript">


$(document).ready(function(){
$('#addform').on('submit',function(e) {

$.ajax({
  url:'/penggajian',
  data:$(this).serialize(),
  type:'POST',
  success:function(data){
    document.getElementById('divMessage').style.display = 'block'
    document.getElementById('divMessage').style.opacity = 'unset'
    document.getElementById('textMessage').textContent = data.success
    document.getElementById("bulan").value = data.bulan;
    document.getElementById("tahun").value = data.tahun;

    removeMessage()
    // console.log(data);
  // $("#success").show().fadeOut(5000); //=== Show Success Message==
  },
  error:function(data){
  // $("#error").show().fadeOut(5000); //===Show Error Message====
  }
});
e.preventDefault(); //=== To Avoid Page Refresh and Fire the Event "Click"===
// console.log($(this).serialize());
});
});

window.onscroll = function() {myFunction()};

// Get the header
var header = document.getElementById("divBtnSimpan");

// Get the offset position of the navbar
var sticky = header.offsetTop;

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}

    document.addEventListener('DOMContentLoaded', function () {
      generateHref()
      
        /* ... */
    });
    removeMessage()
    function removeMessage(){
      window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            document.getElementById('divMessage').style.display = 'none'
        });
    }, 5000);
    }
   
   $(".select2").select2({
     theme: 'bootstrap4'
   })


     function getTahun(data){
      var d = new Date(data);
      return d.getFullYear();
     }

     function getMK(data){
      var d = new Date(data);
      var s = new Date();
      
      return s.getFullYear() - d.getFullYear();
     }

    

     let table = ""
     
    var $eventBulan = $("#bulan");
    $eventBulan.on("change", function (e) { 
      cekPenggajian()
      generateDataTable() 
      generateHref()
      
    });
    var $eventTahun = $("#tahun");
    $eventTahun.on("change", function (e) { 
      cekPenggajian()
      generateDataTable() 
      generateHref()
    });
    cekPenggajian()
    function cekPenggajian(){
      console.log("cekin");
        let bulan = document.getElementById("bulan").value;
        let tahun = document.getElementById("tahun").value;
      $.ajax({
        url:`/cek-penggajian/${bulan}/${tahun}`,
        data:$(this).serialize(),
        type:'GET',
        success:function(data){
          let x = document.getElementById("btnSalin");
          if (data == 0) {
            x.style.display = "block";
          } else {
            x.style.display = "none";
          }
          
          console.log("then",data);
        // $("#success").show().fadeOut(5000); //=== Show Success Message==
        },
        error:function(data){
        // $("#error").show().fadeOut(5000); //===Show Error Message====
        }
      });
    }

    function copasPenggajian(){
        let bulan = document.getElementById("bulan").value;
        let tahun = document.getElementById("tahun").value;
        document.getElementById("btnSalin").disabled = true;
        // var token = $('meta[name="_token"]').attr('content');;
        // console.log(token);
      $.ajax({
        url:`/cek-penggajian/${bulan}/${tahun}`,
        type:'POST',
        headers: {
          'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        },
        success:function(data){
          document.getElementById('divMessage').style.display = 'block'
          document.getElementById('divMessage').style.opacity = 'unset'
          document.getElementById('textMessage').textContent = data == 1 ? 'Data Berhasil di Copy' : 'Data Gagal di Copy'
          removeMessage()
          cekPenggajian()
          generateDataTable() 
          generateHref()
          document.getElementById("btnSalin").disabled = false;
          // let x = document.getElementById("btnSalin");
          // if (data == 0) {
          //   x.style.display = "block";
          // } else {
          //   x.style.display = "none";
          // }
          
          console.log(data);
        // $("#success").show().fadeOut(5000); //=== Show Success Message==
        },
        error:function(data){
        // $("#error").show().fadeOut(5000); //===Show Error Message====
        }
      });
    }

    var $eventTahun = $("#btnSalin");
    $eventTahun.on("click", function (e) { 
      copasPenggajian();
    });




     generateDataTable()
     function generateHref() {
        
        var tp = document.getElementById("filterTipePegawai").value;
        var jp = document.getElementById("filterJenisPegawai").value;
        let bulan = document.getElementById("bulan").value;
        let tahun = document.getElementById("tahun").value;
        document.getElementById("a-resume").href = `/penggajian/resume/${bulan}/${tahun}`;
        document.getElementById("a-laporan").href = `/penggajian/laporan/${bulan}/${tahun}?tipe=${tp}&jenis=${jp}`;
        document.getElementById("a-payroll").href = `/penggajian/payroll/${bulan}/${tahun}`;
        document.getElementById("a-payroll2").href = `/penggajian/payroll2/${bulan}/${tahun}`;
      }

    let dataFormin = []
    function generateDataTable(){
      var tp = document.getElementById("filterTipePegawai").value;
      var jp = document.getElementById("filterJenisPegawai").value;
      let bulan = document.getElementById("bulan").value;
      let tahun = document.getElementById("tahun").value;
      generateHref()
      if (table != "") {
         table.destroy();
      }
      table = $('#data-table-pegawai').DataTable({
              "drawCallback": function( settings, json ) {
                $('[data-toggle="tooltip"]').tooltip();
                $('[data-toggle="tooltip2"]').tooltip();
            },
            processing: true,
            serverSide: true,
            ajax: `/penggajian-datatables?tipe=${tp}&jenis=${jp}&bulan=${bulan}&tahun=${tahun}`,
            columns: [
                { data: function ( d, type, full, meta ) {
                    dataFormin.indexOf(d) === -1 ? dataFormin[d.id] = d : console.log("This item already exists");
                      return  meta.row + 1;
                }, name: 'tgl_masuk' },
                // { data: 'nama', name: 'nama' },
                { data: 'nama', name: 'nama' },
                { data: function ( data ) {
                    let tahun = getTahun(data.tgl_masuk)
                      return  tahun;
                }, name: 'tgl_masuk' },
                { data: function ( data ) {
                    // let mk = getMK(data.tgl_masuk)
                      return  data.mk;
                }, name: 'ip_mk' },
                { data: "ip_jumlah", render: $.fn.dataTable.render.number( '.', '.', 0, 'Rp. ' ) },
                { data: function(d){
                    // dataFormin.push(d)
                    
                    let form = `
                        <input name="ijk_jp[${d.id}]"  type="number" value="${d.jp}" min="0" id="edit_jam_kerja_${d.id}" class="form-control" style="width: 70px;" 
                        oninput="setDataTableIjpJumlah(${d.id}, this.value, ${d.ijp_jumlah}, ${d.total})">

                        <input name="id[${d.id}]"  type="number" value="${d.id}" hidden>
                        <input name="mk[${d.id}]"  type="number" value="${d.mk}" hidden>
                        <input name="ip_id[${d.id}]"  type="text" value="${d.ip_id}" hidden>
                        <input name="ijp_id[${d.id}]"  type="text" value="${d.ijp_id}" hidden>
                        <input name="tunjangan_id[${d.id}]"  type="text" value="${d.tunjangan_id}" hidden>
                        <input name="potongan_id[${d.id}]"  type="text" value="${d.potongan_id}" hidden>
                    `;
                    return form;

                }, name: 'ijk_jp' },
                { data: function(d){
                    
                    let form = `  
                        <span id="ijp_jumlah_${d.id}">${formatRupiah(d.ijp_jumlah * d.jp, "Rp. ")}</span>
                    `;
                    return form;

                }, name: 'ijk_jumlah' },
                { data: function(d){
                  let list = "<ol class='text-left p-0 pl-2'>";
                    for (let index = 0; index < d.tunjangan_data.length; index++) {
                      const element = d.tunjangan_data[index];
                      list += `<li class=''>${d.tunjangan_data[index].nama} : ${formatRupiah(d.tunjangan_data[index].jumlah_ihsan)}</li>`
                    }
                    list += "</ol>"
                    let form = `  
                        <a id="tunjangan_${d.id}" data-toggle="tooltip" data-html="true" title="${list}">${formatRupiah(d.tunjangan, "Rp. ")}</a>
                    `;
                    return form;
                }, name: 'tunjangan' },
                { data: function(d){
                  let list = "<ol class='text-left p-0 pl-2'>";
                    for (let index = 0; index < d.potongan_data.length; index++) {
                      const element = d.potongan_data[index];
                      list += `<li>${d.potongan_data[index].nama} : ${formatRupiah(d.potongan_data[index].jumlah_ihsan)}</li>`
                    }
                    list += "</ol>"
                    let form = `  
                        <a id="potongan_${d.id}" data-toggle="tooltip2" data-placement="top" data-html="true" title="${list}">${formatRupiah(d.potongan, "Rp. ")}</a>
                    `;
                    
                    return form;
                    

                }, name: 'potongan' },
                // { data: "potongan", render: $.fn.dataTable.render.number( '.', '.', 0, 'Rp. ' ) },
                // { data: 'potongan', name: 'potongan' },
                { data: function(d){
                    let form = `
                      
                        <span id="total_${d.id}">${formatRupiah(d.total, "Rp. ")}</span>
                      
                    `;
                    return form;

                }, name: 'ijk_jumlah' },
                { data: function(d){
                  let bulan = document.getElementById("bulan").value;
                  let tahun = document.getElementById("tahun").value;
                    let form = `
                      
                    <a  id="a-resume" href="/penggajian/resume-id/${bulan}/${tahun}/${d.id}" class="btn btn-sm bg-success btn-labeled  btn-outline-success mt-2" target="_blank">
                      <b><i class="icon-printer"></i></b>
                      Slip
                    </a>
                      
                    `;
                    return form;

                }, name: 'cetaka' },
            ]
      });
    }


     function setDataTableIjpJumlah(d, v, jumlah, total) {
       let nilaiBaru = v * parseInt(jumlah);
      console.log("baru",nilaiBaru);
      
      
      let spanIjpJumlah = document.getElementById("ijp_jumlah_"+d).innerHTML = formatRupiah(nilaiBaru, 'Rp. ');
      
      let totalBaru = (parseInt(total)) + nilaiBaru;
      console.log("totalBaru",totalBaru);
       let spanTotal= document.getElementById("total_"+d).innerHTML = formatRupiah(totalBaru, 'Rp. ');

     }


     function formatRupiah(angka, prefix){
      let angkain = angka.toString()
			var number_string = angkain.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
		}
    
</script>
@endsection