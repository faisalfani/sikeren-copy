@extends('layouts.app')

@section('title')
      Ihsan Tunjangan & Potongan
@endsection
@section('css-after') 
 <!-- DataTables -->
 <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.css">
 <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
@endsection

@section('content')

<div class="container ">
        <div class="row " >
          <div class="col-md-12 mx-auto col-lg-8 bg-white p-3">
            <h4 class="text-center"> Ihsan Tunjangan & Potongan

              <button onclick="tambahData()" type="button" class=" float-right btn btn-labeled btn-primary">
                <b><i class="fa fa-edit"></i></b>
                Tambah
              </button>
              
            </h4>
          </div>

          <div class="modal fade" id="m-form" tabindex="-1" role="dialog" aria-labelledby="m-form" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-body">
                  <h2>Ihsan Tunjangan & Potongan</h2>
                  <form action="/itp-pegawai" method="POST">
                    @csrf 
                    <div class="row">
                      
                      <div class="form-group col-md-12">
                          <label> Tipe</label>
                          <input id="edit" name="edit" type="hidden">   
                          {{-- <input required id="kode" placeholder="Kode" type="text" name="kode" class="form-control">    --}}

                          <select  name="tipe" id="tipe" class="form-control" required >
                            <option value="">Pilih Tipe</option>
                            <option value="1">Tunjangan</option>
                            <option value="0">Potongan</option>
                            <option value="2">Ihsan</option>
                          </select>
                      </div>
                  
                      <div class="form-group col-md-12">
                          <label> Nama </label>
                          <input required id="nama" placeholder="Nama" type="text" name="nama" class="form-control">   
                        
                    </div>
                    <div class="form-group col-md-12">
                      <label for="jabatan">Jabatan? </label>
                      <input name="jabatan" type="checkbox" checked data-toggle="toggle" data-off="Tidak" data-on="Ya" data-onstyle="success" data-offstyle="danger" data-width="100" data-height="20" id="jabatan">

                    </div>
                    
                      <div class="form-group col-md-12">                                                             
                          <button type="submit" type="button" class="btn bg-primary-c btn-block  mb-2">
                              <i class="fa fa-save"></i>
                              Simpan</button> 

                              <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Batal</button>


                      </div>
                  </div>
              </form>
          
          </div>
          </div>
        </div>
        </div>


            <div class="col-md-12 col-lg-8 mx-auto bg-white p-4">
              <div class="table-responsive">
              <table id="data-table" class="table table-bordered table-hover">
                <thead>  
                  <tr>
                    <th>Tipe</th>
                    <th>Nama</th>
                    <th class="text-right">Action</th>
                  </tr>
                 
                </thead>
                <tbody>
                  @foreach($data as $key => $value)
                  <tr>
                    <td>
                      {{ $value->tipe_nama}}
                    </td>
                    <td>
                      {{ $value->nama}}
                    </td>
                    <td>
                      <button onclick="edit({{ $value }})" class="btn bg-primary-c btn-sm float-right"><i class="fa fa-edit"></i></button>
                      @if ($value->is_gajih < 1)
                          
                      <a href="#" onclick="deleteData({{ $value->id }})" class="btn btn-danger btn-sm float-right  mr-2"><i class="fa fa-trash"></i></a>
                      @endif
                  
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            </div>
        </div>





    </div>
{{-- End Modal Tambah --}}
@endsection

@section('js-after')
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script type="text/javascript">



$("#data-table").dataTable();

function edit(data){
  if (data.is_jabatan == 0) {
    $('#jabatan').bootstrapToggle('off')
  } else {
    $('#jabatan').bootstrapToggle('on')
  }
  $("#m-form").modal("show")
  $("#nama").val(data.nama);
  $("#tipe").val(data.tipe);
  $("#edit").val(data.id);
}
function tambahData(){
  $('#jabatan').bootstrapToggle('off')
  $("#m-form").modal("show")
  $("#nama").val("");
  $("#tipe").val("");
  $("#edit").val("");
}

$("#m-form").submit(function(e){
  e.prevent.default();
  console.log("tess")
})

  function deleteData(id){
      swal({
        title: "Apakah Kamu Yakin?",
        text: "Pastikan data yang akan dihapus sudah valid",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Hapus",
        closeOnConfirm: false
      },
      function(){

        $.get("/hapus-itp-pegawai/"+id)
          .done((res) => {
            location.reload()
          });

      });
  }
</script>

@endsection