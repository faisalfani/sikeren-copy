@extends('layouts.app')

@section('title')
      Penggajian
@endsection
@section('css-after') 


   <!-- Select2 -->
   <link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">
   <link rel="stylesheet" href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <!-- DataTables -->
 <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.css">

@endsection

@section('content')

<div class="container-fluid ">
        <div class="row " >
          <div class="col-md-12 mx-auto bg-white p-3">
            <h4 class="text-left"> Penggajian
              <button type="button" id="btn-tambah" class=" float-right btn btn-labeled btn-primary">
                <b><i class="fa fa-edit"></i></b>
                Generate Gaji
              </button>
              
            </h4>
          </div>

          <div class="modal fade" id="m-form" tabindex="-1" role="dialog" aria-labelledby="m-form" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-body">
                  <h4>Generate Gaji</h4>
                  <form action="/penggajian" method="POST">
                    @csrf 
                    <div class="row">
                    <div class="col-md-12 row">
                   

                      <div class="col-md-6 row">
                              <div class="form-group col-md-12">
                                  <label> Pegawai </label>
                                  <select name="pegawai_id" id="pegawai_id" class=" form-control">
                                    <option value="">Semua</option>
                                  </select>
                              </div>

                              <div class="form-group col-md-12">
                                <label> Tipe Pegawai </label>
                                <select name="tipe_pegawai" class="select2 form-control">
                                  <option value="">Semua</option>
                      
                                  @foreach(\Modules\Pegawai\Entities\TipePegawai::get() as $key => $value)
                                     <option value="{{$value->id}}">{{$value->nama}} ( {{$value->kode}} )</option>
                                    @endforeach
                                  </select>
                            </div>
                            <div class="form-group col-md-12">
                              <label> Jenis Pegawai </label>
                              <select name="jenis_pegawai" class="select2 form-control">
                                <option value="">Semua</option>
                                  @foreach(\Modules\Pegawai\Entities\JenisPegawai::get() as $key => $value)
                                  <option value="{{$value->id}}">{{$value->nama}})</option>
                                @endforeach
                              </select>
                          </div>


                          <div class="form-group col-md-12">
                        
                            <label> Bulan </label>
                            <select required name="bulan" id="bulan" class="select2 form-control">
                   
                              @php
                                  $bulan = array (
                                      'Januari',
                                      'Februari',
                                      'Maret',
                                      'April',
                                      'Mei',
                                      'Juni',
                                      'Juli',
                                      'Agustus',
                                      'September',
                                      'Oktober',
                                      'November',
                                      'Desember'
                                    );
                              @endphp 
            
                              @foreach ($bulan as $key => $item)
                                  <option
            
                                  @if (Date('m') == $key+1)
                                      selected
                                  @endif
                                  value="{{$key+1}}"
                                  >{{ $item }}</option>
                              @endforeach
                            </select>
                        </div>
                      </div>

                      <div class="col-md-6 row">
                      
                    

                       <div class="form-group col-md-12">
                        <label> Tahun </label>
                        @php
                        $y = Date('Y')-1;
      
                        if(Date('m') < 7) {
                          $tahunnn = Date('Y') - 1;
                        }else{
                          $tahunnn = Date('Y');
                        }
      
                    @endphp
                    <select required name="tahun" id="tahun" class="select2 form-control">
                      @for ($i = 0; $i < 3; $i++)
                        <option 
                       
                        
                        @if($y+$i == $tahunnn)
                        
                        selected 
                        
                        @endif 
                        
                        value="{{$y+$i}}">{{ $y+$i }}</option>
                      @endfor
                    </select>
                    </div>
  
                      <div class="form-group col-md-12">
                        <label> Jam Kerja </label>
                        <input required id="jam_kerja" placeholder="Jam Kerja" type="text" name="jam_kerja" class="form-control">   
                      
                      </div>
                         

                      <div class="form-group col-md-12">                                                             
                        <button type="submit" type="button" class="btn btn-block bg-primary-c mb-2">
                            <i class="fa fa-save"></i>
                            Simpan</button> 

                            <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Batal</button>
                          
                    </div>
                    </div>

                   
                    </div>
                    
                     
                  </div>
              </form>
          
          </div>
          </div>
        </div>
        </div>


            <div class="col-md-12 mx-auto bg-white p-2">
              <div class="table-responsive">
              <table  class="table table-bordered table-hover" id="data-table-pegawai" style="cursor: pointer">
                <thead>  
                  <tr>
                    <th>Nama</th>
                    <th>Tahun</th>
                    <th>Bulan</th>
                    <th>Jam Kerja</th>
                    <th>Masa Kerja</th>
                    <th>Tunjangan</th>
                    <th>Potongan</th>
                    <th>Total Diterima</th>
                    <th>Status</th>
                    <th>Digenerate Tgl</th>
                    <th class="text-right">Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
            </div>
        </div>






        <div class="modal fade" id="detail_data" tabindex="-1" role="dialog" aria-labelledby="m-form" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">
                  <h5 class="text-success">Tunjangan</h5>
                  <div id="detail-tunjangan"></div>
                  <h5 class="text-danger">Potongan</h5>
                  <div id="detail-potongan"></div>

                  <br>
                  <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Tutup</button>

              </div>
            </div>
          </div>
        </div>




    </div>
@endsection

@section('js-after')



<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>

<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>




<script type="text/javascript">



$("#btn-tambah").click(function(){
  // alert("tess");
  tambahData()
})

function tambahData(){
 
  $("#m-form").modal("show")

  $("#pegawai_id").val("");
  $("#bulan").val("");
  $("#tahun").val("");

  $("#pegawai_id").prop('disabled', false);
  $("#bulan").prop('disabled', false);
  $("#tahun").prop('disabled', false);

  $("#jam_kerja").val("");
  


  $("#edit").val("");


  setCariPegawai()
  

}



function setCariPegawai(){
  $('#pegawai_id').select2({
    theme: 'bootstrap4',
    placeholder: 'Ketikan Nama / NIK',
    ajax: {
      url: '/cari-pegawai',
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        console.log(data)
            return {
              results:  $.map(data, function (item) {
                return {
                  text: item.nama,
                  id: item.id
                }
              })
            }
      }
    }
  });
}




 $('#data-table-pegawai').DataTable({
      processing: true,
      serverSide: true,
      ajax: '/penggajian-datatables',
      columns: [
          { data: 'nama', name: 'nama' },
          { data: 'tahun', name: 'tahun' },
          { data: 'bulan', name: 'bulan' },
          { data: function(d){
              let form = `
                <div class="row">
                  <input  tipe="text" value="${d.jam_kerja}" id="edit_jam_kerja_${d.id}" class="form-control col-md-9">
                  <button onclick="editJamKerja('${d.id}')" class="btn btn-success col-md-3 btn-sm"><i class="fa fa-save"></i></button>
                </div>
              `;
              return form;

          }, name: 'jam_kerja' },
          { data: 'masa_kerja', name: 'masa_kerja' },
          { data: function(d){
            return `<div onclick="detail('${d.list_tunjangan}','${d.list_potongan}')">Rp. ${formatRupiah(d.tunjangan)}
              
              <span class="text-success badge badge-sm">klik untuk detail</span>
              </div>
            
           `

          }, name: 'tunjangan' },
          { data: function(d){
            return `
            <div onclick="detail('${d.list_tunjangan}','${d.list_potongan}')">Rp. ${formatRupiah(d.potongan)}
              <span class="text-danger badge badge-sm">klik untuk detail</span>
              </div>
           
            `
            
          }, name: 'potongan' },
          { data: function(d){
            var totalis = formatRupiah(d.total_ihsan_diterima);
            return `<span onclick="detail('${d.list_tunjangan}','${d.list_potongan}')"> Rp ${totalis} </span>`;
          }, name: 'total_ihsan_diterima' },
          { data: function(d){

              if(d.status == 1){
                return `<span onclick="ubahStatus(${d.id})" class="badge badge-primary">Dibayarkan</span>`
              }else{
                return `<span onclick="ubahStatus(${d.id})" class="badge badge-default">Proses</span>`
              }

          }, name: 'status' },
          { data: 'updated_at', name: 'updated_at' },
          { data: function(d){
            return `
            <a href="#" onclick="deleteData('${d.id }')" class="btn btn-danger btn-sm float-right"><i class="fa fa-trash"></i></a>
            <button onclick="edit(`+d.id+`)" class="btn bg-primary-c btn-sm float-right mr-2"><i class="fa fa-edit"></i></button>
            `;
          } },
      ]
});



$(".select2").select2({
  theme: 'bootstrap4'
})

function edit(id){
    
  

    setCariPegawai()

    $.get("/penggajian/"+id)
        .done(data => {

              console.log(data)


              $("#m-form").modal("show")
             
              $("#bulan").val(data.bulan);
              $('#bulan').trigger('change');
              

              $("#tahun").val(data.tahun);
              $('#tahun').trigger('change');
              

              $("#jam_kerja").val(data.jam_kerja);


              
              $("#pegawai_id").val(data.pegawai_id);
              
              // Fetch the preselected item, and add to the control
              var studentSelect = $('#pegawai_id');

              var dataPegawai = data.pegawai

              if(dataPegawai){
                  // create the option and append to Select2
                var option = new Option(dataPegawai.nama, dataPegawai.id, true, true);
                    studentSelect.append(option).trigger('change');

                    // manually trigger the `select2:select` event
                    studentSelect.trigger({
                        type: 'select2:select',
                        params: {
                            data: {
                              text: dataPegawai.nama,
                              id: dataPegawai.id
                            }
                        }
                    });
              
              } 
            
              $("#pegawai_id").val(data.pegawai_id);



              $("#edit").val(data.id);
            
             

      });

    

}



function editJamKerja(id){
  // alert(id+"-"+$("#edit_jam_kerja_"+id).val())
  $.get("/update-jam-kerja/"+id+"/"+$("#edit_jam_kerja_"+id).val())
      .done(res => {
        location.reload()
      })
}
function detail(t,p){

  $("#detail_data").modal("show");

  let $data = t.split(",")

  let $res = '<ul>'
  $data.forEach(element => {
    if(element !== ' '){
     $res = $res + "<li>"+element+"</li>"
    }
  });
  
  $res = $res +'</ul>'
  $("#detail-tunjangan").html($res); 

  
  let $data1 = p.split(",")

  let $res1 = '<ul>'
  $data1.forEach(element => {
    if(element !== ' '){
      $res1 = $res1 + "<li>"+element+"</li>"
    }
  });
  
  $res1 = $res1 +'</ul>'
  $("#detail-potongan").html($res1); 

  

}

 function ubahStatus(id){
     
        $.get("/ubah-status-penggajian/"+id)
          .done((res) => {
            location.reload()
          });
 }
  function deleteData(id){
    console.log(id)
      swal({
        title: "Apakah Kamu Yakin?",
        text: "Pastikan data yang akan dihapus sudah valid",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Hapus",
        closeOnConfirm: false
      },
      function(){

        $.get("/hapus-penggajian/"+id)
          .done((res) => {
            location.reload()
          });

      });
  }
</script>

@endsection