@extends('layouts.app')

@section('title')
      Jenis Pegawai
@endsection
@section('css-after') 
 <!-- DataTables -->
 <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endsection

@section('content')

<div class="container ">
        <div class="row " >
          <div class="col-md-12 mx-auto col-lg-8 bg-white p-3">
            <h4 class="text-center">jenis Pegawai

              <button onclick="tambahData()" type="button" class=" float-right btn btn-labeled btn-primary">
                <b><i class="fa fa-edit"></i></b>
                Tambah
              </button>
              
            </h4>
          </div>

          <div class="modal fade" id="m-form" tabindex="-1" role="dialog" aria-labelledby="m-form" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-body">
                  <h2>jenis Pegawai</h2>
                  <form action="/jenis-pegawai" method="POST">
                    @csrf 
                    <div class="row">
                      
                      <div class="form-group col-md-12">
                          {{-- <label> Kode</label> --}}
                          <input id="edit" name="edit" type="hidden">   
                          {{-- <input required id="kode" placeholder="Kode" type="text" name="kode" class="form-control">    --}}
                      </div>
                  
                      <div class="form-group col-md-12">
                          <label> Nama </label>
                          <input required id="nama" placeholder="Nama" type="text" name="nama" class="form-control">   
                        
                    </div>
                    
                      <div class="form-group col-md-12">                                                             
                          <button type="submit" type="button" class="btn bg-primary-c btn-block  mb-2">
                              <i class="fa fa-save"></i>
                              Simpan</button> 
                              <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Batal</button>

                      </div>
                  </div>
              </form>
          
          </div>
          </div>
        </div>
        </div>


            <div class="col-md-12 col-lg-8 mx-auto bg-white p-4">
              <div class="table-responsive">
              <table id="data-table" class="table table-bordered table-hover">
                <thead>  
                  <tr>
                    {{-- <th>Kode</th> --}}
                    <th>Nama</th>
                    <th class="text-right">Action</th>
                  </tr>
                
                </thead>
                <tbody>
                  @foreach($data as $key => $value)
                  <tr>
                    {{-- <td>
                      {{ $value->kode}}
                    </td> --}}
                    <td>
                      {{ $value->nama}}
                    </td>
                    <td>
                      <a href="#" onclick="deleteData({{ $value->id }})" class="btn btn-danger btn-sm float-right"><i class="fa fa-trash"></i></a>
                      <button onclick="edit({{ $value }})" class="btn bg-primary-c btn-sm float-right mr-2"><i class="fa fa-edit"></i></button>
                  
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            </div>
        </div>





    </div>
{{-- End Modal Tambah --}}
@endsection

@section('js-after')

<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script type="text/javascript">

$("#data-table").dataTable();


function edit(data){
  console.log(data.id)
  $("#m-form").modal("show")
  $("#nama").val(data.nama);
  // $("#kode").val(data.kode);
  $("#edit").val(data.id);
}
function tambahData(){
  $("#m-form").modal("show")
  $("#nama").val("");
  // $("#kode").val("");
  $("#edit").val("");
}

$("#m-form").submit(function(e){
  e.prevent.default();
  console.log("tess")
})

  function deleteData(id){
      swal({
        title: "Apakah Kamu Yakin?",
        text: "Pastikan data yang akan dihapus sudah valid",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Hapus",
        closeOnConfirm: false
      },
      function(){

        $.get("/hapus-jenis-pegawai/"+id)
          .done((res) => {
            location.reload()
          });

      });
  }
</script>

@endsection