@extends('layouts.app')

@section('title')
      Data Pegawai
@endsection
@section('css-after') 

 <!-- DataTables -->
 <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.css">
   <!-- Select2 -->
   <link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">
   <link rel="stylesheet" href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
   <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
   
  <style>
    .sembunyi {
      display: none !important;
    }
  </style>

@endsection

@section('content')

<div class="container-fluid ">
        <div class="row " >
          <div class="col-md-12 mx-auto bg-white p-3">
            <h4 class="text-center"> Data Pegawai
              <button onclick="tambahData()" type="button" class=" float-right btn btn-labeled btn-primary">
                <b><i class="fa fa-edit"></i></b>
                Tambah
              </button>
              
            </h4>
          </div>

          <div class="modal fade" id="m-form" tabindex="-1" role="dialog" aria-labelledby="m-form" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-body">
                  <h2>Data Pegawai</h2>
                  <form action="/data-pegawai" method="POST">
                    @csrf 
                    <div class="row">
                      <div class="col row pr-2">
                        <div class="form-group col-md-12">
                          <label> Nama </label>
                          <input required id="nama" placeholder="Nama" type="text" name="nama" class="form-control">   
                        
                    </div>
                      </div>
                    </div>
                    <div class="row">
                      
                    <div class="col-md-6 row">
                        <div class="form-group col-md-12">
                            <label> NIY</label>
                            <input id="edit" name="edit" type="hidden">   
                            <input required id="nik" placeholder="NIY" type="text" name="nik" class="form-control" oninput="cekNik()">   
                            <div id="nikerror" class="py-2 text-red font-bold sembunyi">NIY/Username sudah digunakan oleh pegawai lain</div>
                        </div>
                    

  
                      <div class="form-group col-md-12">
                          <label> Alamat </label>
                          <input required id="alamat" placeholder="Alamat" type="text" name="alamat" class="form-control">   
                      </div>

                      <div class="form-group col-md-12">
                        <label> Pilih Bank</label> 
                         
                                <select class="data_bank select2 form-control" required name="nama_bank" id="nama_bank">
                                    
                                </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label> No Rek </label> 
                            <input required id="no_rek" placeholder="No Rek" type="text" name="no_rek" class="form-control">   
                        </div>
                        <div class="form-group col-md-12">
                          <label for="sertifikasi">Sertifikasi</label>
                          <input name="sertifikasi" type="checkbox" checked data-toggle="toggle" data-off="Ya" data-on="Tidak" data-onstyle="danger" data-offstyle="success" data-width="100" data-height="20" id="sertifikasi">

                        </div>
                      
                    </div>

                    <div class="col-md-6 row pr-0">
                        
                      <div class="form-group col-md-12 pr-0">
                        <label> No Hp </label>
                        <input required id="no_hp" placeholder="No HP" type="text" name="no_hp" class="form-control">   
                    </div>

                        <div class="form-group col-md-12 pr-0">
                          <div class="row">
                            <label class="col-3"> PIN </label> 
                            <div class="col text-right">
                              <input class="form-check-input" type="checkbox" value="Y" id="defaultCheck1" name="pin_is_password">
                              <label class="form-check-label" for="defaultCheck1">
                                Set PIN sebagai Password
                              </label>
                            </div>
                          </div>
                            
                            
                            <input required id="pin" placeholder="PIN" type="text" name="pin" class="form-control">   
                        </div>
                            <div class="form-group col-md-12 pr-0">
                                <label> Tgl Masuk </label>
                                <input required id="tgl_masuk" placeholder="Tgl Masuk" type="date" name="tgl_masuk" class="form-control">   
                            </div>
        
        
                            <div class="form-group col-md-12 pr-0">
                                <label> Tipe Pegawai </label> 
                                <select required class="form-control select2" name="tipe_pegawai" name="tipe_pegawai" id="tipe_pegawai">
                                  <option>Pilih Tipe Pegawai</option>
                                  @foreach($tipe as $key => $value)
                                  <option
                                            value="{{$value->id}}"
                                          
                                          >{{ $value->nama }} ({{ $value->kode }})
        
                                          </option>
                                                
                                        @endforeach
                                </select>
                            </div>
        
                            <div class="form-group col-md-12 pr-0">
                                <label> Jenis Pegawai </label> 
                                
                                <select required class="form-control select2" name="jenis_pegawai" name="jenis_pegawai" id="jenis_pegawai">
                                  <option>Pilih Jenis Pegawai</option>
                                        @foreach($jenis as $key => $value)
                                                <option value="{{$value->id}}">{{ $value->nama }}</option>
                                        @endforeach
                                </select>
                            </div>
        
                         
                    </div>
                    
                      <div class="form-group col-md-12">                                                             
                          <button type="submit" type="button" class="btn bg-primary-c btn-block  mb-2" id="simpanin">
                              <i class="fa fa-save"></i>
                              Simpan</button> 
                              <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Batal</button>

                      </div>
                  </div>
              </form>
          
          </div>
          </div>
        </div>
        </div>


            <div class="col-md-12 mx-auto bg-white p-2">
              <div class="table-responsive">
              <table  class="table table-bordered table-hover" id="data-table-pegawai">
                <thead>  
                  <tr>
                    <th>Foto</th>
                    <th>NIY</th>
                    <th>Nama</th>
                    <th>No HP</th>
                    <th>Alamat</th>
                    <th>Tgl Masuk</th>
                    <th>Tipe Pegawai</th>
                    <th>Jenis Pegawai</th>
                    <th>No Rek</th>
                    <th>PIN</th>
                    <th class="text-right">Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
            </div>
        </div>





    </div>
{{-- End Modal Tambah --}}
@endsection

@section('js-after')
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>

<script type="text/javascript">

async function cekNik(e){
  let nik = document.getElementById('nik').value
  let nikElemenet = document.getElementById('nik')
  let nikError = document.getElementById('nikerror')
  if (nik.length > 0) {
    $.get( "/pegawai/cek-nik/"+nik, function() {
      // alert( "success" );
    })
      .done(function(d) {
        if (d == 1) {
          nikError.classList.remove("sembunyi");
          document.getElementById("simpanin").disabled = true;
        } else {
          document.getElementById("simpanin").disabled = false;
          nikError.classList.add("sembunyi");

        }
      })
      .fail(function() {
        alert( "Terjadi Kesalahan saat pengecekan niy" );
      });
  }
  // console.log("e", nik);
}

 $('#data-table-pegawai').DataTable({
      // language : {
      //   'url': '/indonesian.json'
      // },
      processing: true,
      serverSide: true,
      ajax: '/data-pegawai-datatables',
      columns: [
        { data: function(d){
              let form = `
              <img src="/storage/pegawai/${d.foto}" class="rounded mx-auto d-block" alt="Foto" style="width:86px;">
              `;
              return form;

          }, name: 'foto' },
          // { data: 'foto', name: 'foto' },
          { data: 'nik', name: 'nik' },
          { data: 'nama', name: 'nama' },
          { data: 'no_hp', name: 'no_hp' },
          { data: 'alamat', name: 'alamat' },
          { data: 'tgl_masuk', name: 'tgl_masuk' },
          { data: 'tipe_pegawai', name: 'tipe_pegawai' },
          { data: 'jenis_pegawai', name: 'jenis_pegawai' },
          { data: 'no_rek', name: 'no_rek' },
          { data: 'pin', name: 'pin' },
          { data: function(d){
            return `
            <a href="#" onclick="deleteData('${d.id }')" class="btn btn-danger btn-sm float-right"><i class="fa fa-trash"></i></a>
            <button onclick="edit(`+d.id+`)" class="btn bg-primary-c btn-sm float-right mr-2"><i class="fa fa-edit"></i></button>
            `;
          } },
      ]
});



$.get("/databank.json")
    .done(res => {
        

        // console.log(res)

        var databank =  `<option>Pilih BANK</option>`;

        res.forEach(element => {
            databank = databank + `<option value="${element.name}">${element.name}</option>`;
        });

        $(".data_bank").html(databank)

    })





$('.select2').select2({
      theme: 'bootstrap4'
})


function edit(id){
 
$.get("/data-pegawai/"+id)
    .done(data => {

          $("#m-form").modal("show")
          $("#nama").val(data.nama);
          $("#nik").val(data.nik);
          $("#alamat").val(data.alamat);
          $("#nama_bank").val(data.bank);
          $("#no_rek").val(data.no_rek);
          $("#no_hp").val(data.no_hp);
          $("#pin").val(data.pin);
          $("#tgl_masuk").val(data.tgl_masuk);
          $("#tipe_pegawai").val(data.tipe_pegawai_id);
          $("#jenis_pegawai").val(data.jenis_pegawai_id);
          $("#edit").val(data.id);
          if (data.sertifikasi == 0) {
            $('#sertifikasi').bootstrapToggle('on')
            
          } else {
            $('#sertifikasi').bootstrapToggle('off')
            
          }

    })

}
function tambahData(){
  $('#sertifikasi').bootstrapToggle('on')
  dataInput = ''
  $("#m-form").modal("show")
  $("#nama").val("");
  $("#nik").val("");
  $("#alamat").val("");
  $("#nama_bank").val("");
  $("#no_rek").val("");
  $("#no_hp").val("");
  $("#pin").val("");
  $("#tgl_masuk").val("");
  $("#tipe_pegawai").val("");
  $("#jenis_pegawai").val("");
  $("#edit").val("");
}


  function deleteData(id){
      swal({
        title: "Apakah Kamu Yakin?",
        text: "Pastikan data yang akan dihapus sudah valid",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Hapus",
        closeOnConfirm: false
      },
      function(){

        $.get("/hapus-data-pegawai/"+id)
          .done((res) => {
            location.reload()
          });

      });
  }
</script>

@endsection