@extends('layouts.app')

@section('title')
      Master Penggajian
@endsection
@section('css-after') 


   <!-- Select2 -->
   <link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">
   <link rel="stylesheet" href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <!-- DataTables -->
 <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<style>
  td {
    white-space: nowrap;
}
</style>
@endsection

@section('content')

<div class="container-fluid ">
        <div class="row " >
          <div class="col-md-12 mx-auto bg-white p-3">
            <h4 class="text-left"> Master Penggajian
              <button type="button" id="btn-tambah" class=" float-right btn btn-labeled btn-primary">
                <b><i class="fa fa-edit"></i></b>
                Tambah
              </button>
              
            </h4>
          </div>

          <div class="modal fade" id="m-form" tabindex="-1" role="dialog" aria-labelledby="m-form" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-body">
                  <h4>Master Penggajian</h4>
                  <form action="/master-penggajian" method="POST">
                    @csrf 
                    <div class="row">
                      
                    <div class="col-md-6">
                        <div class="form-group">
                            <label> Tipe </label>
                            <input id="edit" name="edit" type="hidden">   
                            <input type="hidden" id="itptemp" name="itptemp">
                            <select  name="tipe" id="tipe" class="form-control" required >
                              <option value="">Pilih Tipe</option>
                              <option value="1">Tunjangan</option>
                              <option value="0">Potongan</option>
                              <option value="2">Ihsan</option>
                            </select>  
                        </div>
                    
                        <div class="form-group">
                            <label> Nama Tunjangan / Potongan </label>
                            <select  name="itp" id="itp" class="form-control" required ></select> 
                        </div>
  
                        
                        <div class="form-group">
                          <label> Tipe Pegawai </label> 
                          <select class="form-control select2" name="tipe_pegawai" name="tipe_pegawai" id="tipe_pegawai">
                                  <option value="">Semua</option>
                                  @foreach($tipe as $key => $value)
                                          <option value="{{$value->id}}">{{ $value->nama }} ({{ $value->kode }})</option>
                                  @endforeach
                          </select>
                      </div>
  
                      <div class="form-group col-md-12">
                          <label> Jenis Pegawai </label> 
                          <select class="form-control select2" name="jenis_pegawai" name="jenis_pegawai" id="jenis_pegawai">
                                  <option value="">Semua</option>
                                  @foreach($jenis as $key => $value)
                                          <option value="{{$value->id}}">{{ $value->nama }}</option>
                                  @endforeach
                          </select>
                      </div>
                      <div class="form-group col-md-12">
                          <label> Pegawai </label>
                          <select name="pegawai_id[]" id="pegawai_id" class=" form-control">
                            <option value="All">Semua</option>
                          </select>
                      </div>
                
                      
                    </div>

                    <div class="col-md-6 row">
                   

                      
  
                      <div class="form-group col-md-12">
                        <div class="row">
                          <div class="col">
                            <label id="textNominal"> Nominal </label>
                          </div>
                          <div class="col-4">
                            <div class="custom-control custom-switch">
                              <input type="checkbox" class="custom-control-input" id="customSwitch1" onchange="setTypeNominal(this)" name="is_persen">
                              <label class="custom-control-label" for="customSwitch1">Persen</label>
                            </div>
                          </div>
                        </div>
                        
                        <input class="form-control" required type="text" id="jumlah_ihsan" name="jumlah_ihsan" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
                        <p class="text-danger" id="textPersen" style="display: none">Gunakan titik untuk pecahan desimal; Contoh <b>2.5</b></p>
                      
                    </div>

                    <div class="form-group col-md-12">
                      <label> Berlaku Sampai</label>
                
                      <input id="berlaku_sampai" placeholder="Berlaku Sampai" type="date" name="berlaku_sampai" class="form-control">   


                  </div>

                  <div class="form-group col-md-12">
                      <label> Dikalikan Masa Kerja ? </label>
                      <br>
                      <input required type="radio" value="1" name="dikalikanmasakerja"  > Iya
                      <input required type="radio" value="0" name="dikalikanmasakerja" > Tidak
                      
                  </div>
                  <div class="form-group col-md-12">
                    <label> Dikalikan Jam Kerja ? </label>
                    <br>
                    <input required type="radio" value="1" name="dikalikanjamkerja" > Iya
                    <input required type="radio" value="0" name="dikalikanjamkerja"> Tidak
                </div>


                <div class="form-group col-md-12">
                  <label> Status </label>
                  <br>
                  <input required type="radio" value="1" name="status"  >Aktif
                  <input required type="radio" value="0" name="status"> Tidak
              </div>

                  


           
        
                         
                    </div>
                    
                      <div class="form-group col-md-12">                                                             
                          <button type="submit" type="button" class="btn btn-block bg-primary-c mb-2">
                              <i class="fa fa-save"></i>
                              Simpan</button> 
                              <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Batal</button>

                      </div>
                  </div>
              </form>
          
          </div>
          </div>
        </div>
        </div>


            <div class="col-md-12 mx-auto bg-white p-2">
              <div class="table-responsive">
              <table  class="table table-bordered table-hover" id="data-table-pegawai">
                <thead>  
                  <tr>
                    <th>Tipe</th>
                    <th>Nama</th>
                    <th style="white-space:nowrap;">Jumlah</th>
                    <th>Tipe Pegawai</th>
                    <th>Jenis Pegawai</th>
                    <th>Pegawai</th>
                    <th>Berlaku Sampai</th>
                    <th> (X) Masa Kerja</th>
                    <th> (X) Jam Kerja</th>
                    <th> Status</th>
                    <th class="text-right" style="min-width: 72px;white-space: nowrap !important;">Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
            </div>
        </div>





    </div>
{{-- End Modal Tambah --}}
@endsection

@section('js-after')



<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>

<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="/form-titik.js"></script>




<script type="text/javascript">

function setTypeNominal(e){
  // console.log("e", e.checked);
  let textNominal = document.getElementById("textNominal")
  // let textPersen = document.getElementById("textPersen")
  if (e.checked) {
    textNominal.innerHTML = "Persentase"
    document.getElementById("textPersen").style.display = "block"
  } else {
    document.getElementById("textPersen").style.display = "none"
    textNominal.innerHTML = "Nominal"

  }
}

$("#btn-tambah").click(function(){
  // alert("tess");
  tambahData()
})

function tambahData(){
 
  $("#m-form").modal("show")

  $("#tipe").val("");
  $("#itp").val("");
  $("#itptemp").val("")
  $("#jumlah_ihsan").val("");
  $("#tipe_pegawai").val("");
  $("#jenis_pegawai").val("");
  $("#pegawai_id").val("");
  $("#jumlah_ihsan").val("");
  $("#berlaku_sampai").val("");
  // $("#status").val("");


  
  $("input[name=dikalikanmasakerja][value='0']").prop('checked', true);
  $("input[name=dikalikanjamkerja][value='0']").prop('checked', true);
  $("input[name=status][value='1']").prop('checked', true);


  $("#edit").val("");


  setCariPegawai()
  

}



function setCariPegawai(){
  let selectedValuesTest = ["All"]
  $('#pegawai_id').select2({
    theme: 'bootstrap4',
    multiple: true,
    placeholder: 'Ketikan Nama / NIK',
    ajax: {
      url: '/cari-pegawai',
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        console.log("set cari pegawai",data)
            return {
              results:  $.map(data, function (item) {
                return {
                  text: item.nama,
                  id: item.id
                }
              })
            }
      }
    }
  });

  $('#pegawai_id').val(selectedValuesTest).trigger('change');
}

$('#data-table-pegawai').on( 'page.dt', function () {
  console.log("ASd");
  $('[data-toggle="tooltip"]').tooltip();
} );

 $('#data-table-pegawai').DataTable({
        "drawCallback": function( settings, json ) {
          $('[data-toggle="tooltip"]').tooltip();
      },
      processing: true,
      serverSide: true,
      ajax: '/master-penggajian-datatables',
      columns: [
          { data: function(d){
            if(d.tipe == 1){
              return `<span class="badge badge-primary bg-primary-c">Tunjangan</span>`
            }else if(d.tipe == 2){
              return `<span class="badge badge-warning bg-warning">Ihsan</span>`
            }else{
              return `<span class="badge badge-default">Potongan</span>`
            }
          }, name: 'tipe' },
          { data: 'nama', name: 'nama' },
          { data: function(d){
            if (d.is_persen) {
              return d.jumlah_ihsan+"%"
            }
            return "Rp. "+ formatRupiah(d.jumlah_ihsan)
          }, name: 'jumlah_ihsan' },
          { data: 'tipe_pegawai', name: 'tipe_pegawai' },
          { data: 'jenis_pegawai', name: 'jenis_pegawai' },
          { data: function(d){
            let list = ""
            if (d.list_pegawai == "All") {
               list = "Semua"
            } else {
               list = "<ol class='text-left p-0 pl-2'>";
              for (let index = 0; index < d.list_pegawai.length; index++) {
                const element = d.list_pegawai[index];
                list += `<li>${d.list_pegawai[index].nama}</li>`
              }
              list += "</ol>"
            }
            
              let form = `  
                  <a id="pegawai_${d.id}" data-toggle="tooltip" data-html="true" title="${list}" style="cursor: pointer;">${d.pegawai}</a>
              `;
              
              return form;
              

          }, name: 'pegawai' },
          // { data: 'pegawai', name: 'pegawai' },
          { data: 'berlaku_sampai', name: 'berlaku_sampai' },
          { data: function(d){

            if(d.dikalikan_masa_kerja == 1){
              return `<span class="badge badge-primary">Iya</span>`
            }else{
              return `<span class="badge badge-default">Tidak</span>`
            }

          }, name: 'dikalikanmasakerja' },
          { data: function(d){
            if(d.dikalikan_jam_kerja == 1){
              return `<span class="badge badge-primary">Iya</span>`
            }else{
              return `<span class="badge badge-default">Tidak</span>`
            }
          }, name: 'dikalikanjamkerja' },
          { data: function(d){
            if(d.status == 1){
              return `<span class="badge badge-success">Iya</span>`
            }else{
              return `<span class="badge badge-default">Tidak</span>`
            }
          }, name: 'status' },

          { data: function(d){
            return `
            <a href="#" onclick="deleteData('${d.id }')" class="btn btn-danger btn-sm float-right"><i class="fa fa-trash"></i></a>
            <button onclick="edit(`+d.id+`)" class="btn bg-primary-c btn-sm float-right mr-2"><i class="fa fa-edit"></i></button>
            `;
          } },
      ]
});





$("#tipe").change(function(){

 getItp()

});
function getItp(){
  $.get("/data-itp/"+$("#tipe").val())
    .done(res => {

      // console.log(res)
        
      var data =  ''

      res.forEach(element => {
          if($("#itptemp").val() == element.id){
            data = data + `<option selected value="${element.id}">${element.nama}</option>`;
          }else{
            data = data + `<option value="${element.id}">${element.nama}</option>`;
          }
      });

      $("#itp").html(data)
    
    });
}


$('.select2').select2({
      theme: 'bootstrap4'
})


function edit(id){
    
  // tambahData()

    setCariPegawai()

    $.get("/master-penggajian/"+id)
        .done(data => {


              console.log("datain", data);
              $("#m-form").modal("show")
              $("#tipe").val(data.tipe);


              getItp()

              $("#itptemp").val(data.itp_id);

              
              
              $("#tipe_pegawai").val(data.tipe_pegawai_id);
              $('#tipe_pegawai').trigger('change');
              $("#jenis_pegawai").val(data.jenis_pegawai_id);
              $("#jenis_pegawai").val(data.jenis_pegawai_id);
              $('#jenis_pegawai').trigger('change');
              // console.log("is", data.is_persen);
              if (data.is_persen) {
                document.getElementById('customSwitch1').checked = true
                // $('#customSwitch1').checked = true
              } else {
                document.getElementById('customSwitch1').checked = false

              }


              // Fetch the preselected item, and add to the control
              var studentSelect = $('#pegawai_id');
              studentSelect.select2('data', null);
              $('#pegawai_id').empty().trigger("change");
              var o = new Option("Semua", "All", true, true);
              studentSelect.append(o).trigger('change');
              var dataPegawai = data.pegawai

              if(dataPegawai){

                for (let index = 0; index < dataPegawai.length; index++) {
                  const el = dataPegawai[index];
                  var option = new Option(el.nama, el.id, true, true);
                  studentSelect.append(option).trigger('change');

                  // manually trigger the `select2:select` event
                  studentSelect.trigger({
                      type: 'select2:select',
                      params: {
                          data: {
                            text: el.nama,
                            id: el.id
                          }
                      }
                  });
                }
                  // create the option and append to Select2
                
              
              } 
              let arrPegawai = ["All"]
              if (data.pegawai_id) {
                let pegString = data.pegawai_id +''
                arrPegawai = pegString.split(",")
              } 
              // console.log("asd", data);
              $("#pegawai_id").val(arrPegawai);


              $("#jumlah_ihsan").val(data.jumlah_ihsan);
              $("#berlaku_sampai").val(data.berlaku_sampai);


              
              $("input[name=dikalikanmasakerja][value=" + data.dikalikan_masa_kerja + "]").prop('checked', true);
              $("input[name=dikalikanjamkerja][value=" + data.dikalikan_jam_kerja + "]").prop('checked', true);
              $("input[name=status][value=" + data.status + "]").prop('checked', true);

              // $("#status").val(data.status);
              $("#edit").val(data.id);

            

            
            

      });

    

}


  function deleteData(id){
    console.log(id)
      swal({
        title: "Apakah Kamu Yakin?",
        text: "Pastikan data yang akan dihapus sudah valid",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Hapus",
        closeOnConfirm: false
      },
      function(){

        $.get("/hapus-master-penggajian/"+id)
          .done((res) => {
            location.reload()
          });

      });
  }


  
</script>

@endsection