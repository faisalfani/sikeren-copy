<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group(function() {
    Route::get('/pegawai', 'PegawaiController@index');

    Route::get('/tipe-pegawai', 'TipePegawaiController@index');
    Route::post('/tipe-pegawai', 'TipePegawaiController@store');
    Route::get('/hapus-tipe-pegawai/{id}', 'TipePegawaiController@destroy');

    

    
    Route::get('/jenis-pegawai', 'JenisPegawaiController@index');
    Route::post('/jenis-pegawai', 'JenisPegawaiController@store');
    Route::get('/hapus-jenis-pegawai/{id}', 'JenisPegawaiController@destroy');


    
    Route::get('/itp-pegawai', 'ITPPegawaiController@index');
    Route::get('/data-itp/{tipe}', 'ITPPegawaiController@byTipe');
    Route::post('/itp-pegawai', 'ITPPegawaiController@store');
    Route::get('/hapus-itp-pegawai/{id}', 'ITPPegawaiController@destroy');



    Route::get('/data-pegawai/{id}', 'PegawaiController@show');
    Route::get('/data-pegawai', 'PegawaiController@index');
    Route::get('/data-pegawai-datatables', 'PegawaiController@json');
    Route::post('/data-pegawai', 'PegawaiController@store');
    Route::get('/hapus-data-pegawai/{id}', 'PegawaiController@destroy');


    
    Route::get('/master-penggajian/{id}', 'MasterPenggajianController@show');
    Route::get('/master-penggajian', 'MasterPenggajianController@index');
    Route::get('/master-penggajian-datatables', 'MasterPenggajianController@json');
    Route::post('/master-penggajian', 'MasterPenggajianController@store');
    Route::get('/hapus-master-penggajian/{id}', 'MasterPenggajianController@destroy');

    Route::get('/cari-pegawai', 'MasterPenggajianController@cariPegawai');



    
    
    Route::get('/penggajian/{id}', 'PenggajianController@show');
    
    // Route::get('/penggajian', 'PenggajianController@index');
    Route::get('/penggajian', 'PenggajianController@index2');
    Route::get('/penggajian-lama', 'PenggajianController@index');
    // Route::get('/penggajian-datatables', 'PenggajianController@json');
    Route::get('/penggajian-datatables', 'PenggajianController@json2');

    Route::post('/load-data-penggajian', "PenggajianController@loadData")->name('load-data-penggajian');


    Route::get('/cek-penggajian/{bulan}/{tahun}', 'PenggajianController@cekPenggajian');
    Route::post('/cek-penggajian/{bulan}/{tahun}', 'PenggajianController@copasPenggajian');
    Route::post('/penggajian', 'PenggajianController@store');
    Route::get('/hapus-penggajian/{id}', 'PenggajianController@destroy');
    Route::get('/ubah-status-penggajian/{id}', 'PenggajianController@ubahStatus');


    Route::get('/update-jam-kerja/{id}/{jam_kerja}','PenggajianController@ubahJamkerja');
    Route::get('/pegawai/cek-nik/{nik}','PegawaiController@cekNik');

});



