<?php

namespace Modules\Pegawai\Entities;

use Illuminate\Database\Eloquent\Model;

class JenisPegawai extends Model
{
    protected $fillable = [];
    public $timestamps = false;
}
