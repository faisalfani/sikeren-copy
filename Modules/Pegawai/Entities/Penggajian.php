<?php

namespace Modules\Pegawai\Entities;

use Illuminate\Database\Eloquent\Model;

class Penggajian extends Model
{
    protected $fillable = [];

    
    public function pegawai()
    {
        return $this->hasOne('Modules\Pegawai\Entities\Pegawai', 'id', 'pegawai_id');
    }

}
