<?php

namespace Modules\Pegawai\Entities;

use Illuminate\Database\Eloquent\Model;

class TipePegawai extends Model
{
    protected $fillable = [];
    public $timestamps = false;
}
