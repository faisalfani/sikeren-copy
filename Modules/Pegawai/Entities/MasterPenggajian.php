<?php

namespace Modules\Pegawai\Entities;

use Illuminate\Database\Eloquent\Model;

class MasterPenggajian extends Model
{
    protected $fillable = [];


    public function pegawai()
    {
        return $this->hasMany('Modules\Pegawai\Entities\Pegawai', 'id', 'pegawai_id');
    }

    public function itp()
    {
        return $this->hasOne('Modules\Pegawai\Entities\IhsanDanPotongan', 'id', 'itp_id');
    }
}
