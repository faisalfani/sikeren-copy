<?php

namespace Modules\Pegawai\Entities;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Pegawai extends Authenticatable implements JWTSubject
{
    
    protected $fillable = [];

    // public function getAuthPassword()
    // {
    //     return $this->pin;
    // }

    // public function setPasswordAttribute($value)
    // {
    //     $this->attributes['password'] = $value;
    // }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
