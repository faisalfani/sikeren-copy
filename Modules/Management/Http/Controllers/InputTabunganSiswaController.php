<?php

namespace Modules\Management\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\Siswa\Entities\Siswa;
use Modules\Keuangan\Entities\BayarSiswa;
use Modules\Keuangan\Entities\BiayaSiswa;
use Modules\Keuangan\Entities\Jurnal;
use Modules\Keuangan\Entities\PemasukanTabunganSiswa;
use Auth;

class InputTabunganSiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $req)
    {
        if($req->nis){
            $nis = $req->nis;
            $tahun = $req->tahun_ajaran;
            $siswa = Siswa::where("nis",$nis)->first();
        }else{
            $nis = "";
            $siswa = "";
            $tahun = date("Y");
        }

        return view('management::inputTabunganSiswa',compact("nis","tahun","siswa","req"));

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('management::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $req)
    {
        // id 4 = Tabsan
        $jml = 
        Jurnal::where("nis",$req->nis)->where("type",1)->where("id_jenis_biaya",99)->sum('jumlah')
        - Jurnal::where("nis",$req->nis)->where("type",0)->where("id_jenis_biaya",99)->sum('jumlah');


        $kode_trx = "TRX-". time();

        $new = new Jurnal;
        $new->nis = $req->nis;
        $new->kode_trx =  $kode_trx;
        $new->ref = $req->referensi;
        $new->jumlah = str_replace('.', '', $req->jumlah);
        $new->tgl_bayar = date("Y-m-d", strtotime($req->tgl_bayar));
        $new->nama_biaya = "TABUNGAN";
        $new->id_jenis_biaya = 99;
        $new->ket = $req->keterangan;
        $new->saldo_akhir = $jml +  str_replace('.', '', $req->jumlah);
        $new->type = 1;
        $new->save();

        return back()->with("success","Pembayaran berhasil ditambahkan");

    }
    public function storeKeluar(Request $req)
    {

          $tgl_bayarnya = date("Y-m-d", strtotime($req->tgl_bayar));
          $lastId =  (Jurnal::where("tgl_bayar",$tgl_bayarnya)->orderBy("id","DESC")->first()) ? Jurnal::where("tgl_bayar",$tgl_bayarnya)->selectRaw("right(kode_trx,3)")->orderBy("id","DESC")->first()->kode_trx : 1;
          $idsss = $lastId+1;
          $kode_trx = "TRX-". date("Ymd", strtotime($req->tgl_bayar)) ."".$idsss;
  
            $cekSaldo = 
            Jurnal::where("nis",$req->nis)->where("type",1)->where("id_jenis_biaya",99)->sum('jumlah')
            - Jurnal::where("nis",$req->nis)->where("type",0)->where("id_jenis_biaya",99)->sum('jumlah');
    

        $jml = str_replace('.', '', $req->jumlah);
        
        if($cekSaldo > $jml){
            $totalSave = $jml;
            $saldoAkhir = $cekSaldo - $jml;
        }else{
            $totalSave = $cekSaldo;
            $saldoAkhir = 0;
        }

        if($cekSaldo > 0){

                
            $new = new Jurnal;
            $new->nis = $req->nis;
            $new->kode_trx =  $kode_trx;
            $new->ref = $req->referensi;
            $new->jumlah = $totalSave;
            $new->tgl_bayar = date("Y-m-d", strtotime($req->tgl_bayar));
            $new->nama_biaya = "TABUNGAN";
            $new->id_jenis_biaya = 99;
            $new->ket = $req->keterangan;
            $new->saldo_akhir =  $saldoAkhir;
            $new->type = 0;
            $new->save();

            
            return back()->with("success","Pembayaran berhasil ditambahkan");
        }else{
            return back();
        }

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('management::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('management::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function hapusTabungan($id)
    {
        Jurnal::find($id)->delete();

        return "";
    }
}
