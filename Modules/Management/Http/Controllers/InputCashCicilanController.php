<?php

namespace Modules\Management\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Siswa\Entities\Siswa;
use Modules\Keuangan\Entities\BayarSiswa;
use Modules\Keuangan\Entities\BiayaSiswa;
use Modules\Keuangan\Entities\RincianBiaya;
use Modules\Keuangan\Entities\Jurnal;
use Modules\Keuangan\Entities\TotalBayarSiswa;
use Auth;
use PDF;
use DB;

class InputCashCicilanController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */


    public function bayarTagihanTunaiById($nis, $id)
    {

        if (Auth::id()) {
            $tagihan = explode("-", $id);
            // return $tagihan;
            $biayaId = [];

            $kode_trx = "TRX-" . time();

            $biayaSiswa = BiayaSiswa::where("nis", $nis)
                ->whereIn("id", $tagihan)
                ->whereRaw("nominal != dibayar")
                ->orderBy("tahun_id", "ASC")
                ->orderBy("bulan", "ASC")
                ->orderBy("prioritas", "ASC")
                ->get();


            if ($biayaSiswa) {

                $jumlahBayar = 0;

                foreach ($biayaSiswa as $b) {

                    $sisa = $b->nominal - $b->dibayar;

                    $edit = BiayaSiswa::find($b->id);
                    $edit->dibayar = $b->dibayar + $sisa;
                    $edit->save();

                    $new = new Jurnal;
                    $new->nis = $nis;
                    $new->kode_trx =  $kode_trx;
                    $new->ref = 1;
                    $new->jumlah = $sisa;
                    $new->tgl_bayar = date("Y-m-d");
                    $new->biaya_id = $b->id;
                    $new->nama_biaya = $b->nama;
                    $new->id_jenis_biaya = $edit->jenis_biaya_id;
                    $new->type = 1;
                    $new->save();

                    array_push($biayaId, $new->id);

                    $jumlahBayar = $jumlahBayar + $sisa;
                }

                $tbs = new TotalBayarSiswa;
                $tbs->kode_trx = $kode_trx;
                $tbs->nis = $nis;
                $tbs->ref = 1;
                $tbs->biaya_id = implode(",", $biayaId);
                $tbs->jumlah =  str_replace('.', '', $jumlahBayar);
                $tbs->tgl_bayar = date("Y-m-d");
                $tbs->save();


                // Hapus Data 
                $deleteH2h = DB::table("tagihan_h2hs")->where("nis", $nis)->delete();


                return "success";
            } else {
                return "";
            }
        } else {
            return "";
        }
    }
    public function dataStruk(Request $req, $id)
    {


        if ($req->print == 'true') {
            $pdf = PDF::loadView('pdf.struk', compact("id", "req"))
                ->setPaper(array(0, 0, 609.4488, 935.433), 'portrait');
            return $pdf->stream();
        } else {
            return view('pdf.struk', compact("id", "req"));
        }
    }
    public function index(Request $req)
    {
        // $students = BiayaSiswa::select('*', DB::raw('sum(nominal) as total_nominal'))
        // ->where('nis',$req->nis)->where("satuan","Daftar Ulang")->orderBy('created_at')->get()->groupBy('tahun_ajaran');


        // foreach ($dataBulanan as $b) {
        //     $bulanan[] =  $b->groupBy('bulan');
        // }

        // return response()->json($bulanan, 200);

        if ($req->nis) {
            $nis = $req->nis;
            $tahun = $req->tahun_ajaran;
            $siswa = Siswa::where("nis", $nis)->first();
            $daftarulang = BiayaSiswa::where('nis', $req->nis)->where("satuan", "Daftar Ulang")->orderBy('created_at')->get()->groupBy('tahun_ajaran');

            $bulanan = BiayaSiswa::select(
                'id',
                'bulan',
                'tahun_id',
                'nis',
                DB::raw('sum(nominal) as total_nominal'),
                DB::raw('sum(dibayar) as total_dibayar'),
                DB::raw('sum(nominal) - sum(dibayar) as total_sisa')
            )
                ->where('nis', $req->nis)
                ->where("satuan", "!=", "Daftar Ulang")
                ->groupBy('tahun_id', 'bulan')
                ->get();
            // return response()->json($bulanan, 200);
        } else {
            $nis = "";
            $siswa = "";
            $tahun = date("Y");
            $daftarulang = [];
            $bulanan = [];
        }

        return view('management::inputCashCicilan', compact("nis", "tahun", "siswa", "daftarulang", "bulanan"));
    }


    public function loadData(Request $req)
    {

        $data = BiayaSiswa::where('nis', $req->nis)
            ->where("satuan", '!=', "Daftar Ulang")
            ->where('bulan', $req->bulan)
            ->where('tahun_id', $req->tahun_id)
            ->orderBy('created_at')
            ->get();
        $output = '';
        if (!$data->isEmpty()) {
            foreach ($data as $key => $row) {
                $nomor = $key + 1;
                $data =  htmlspecialchars(json_encode($row), ENT_QUOTES, 'UTF-8');
                $sisa = $row->nominal - $row->dibayar;
                $button = "";
                if ($row->dibayar == 0) {
                    $button = '
                        <button onclick="editTagihan(' . $row->id . ',' . $row->nominal . ',' . $row->jenis_biaya_id . ')"
                            class="btn btn-primary btn-sm float-right">
                            <i class="fa fa-edit"></i>
                        </button>


                        <button onclick="hapusTagihan(' . $row->id . ')" class="btn btn-danger btn-sm float-right">
                            <i class="fa fa-trash"></i>
                        </button>
                    ';
                }

                $checkin = "";
                if ($sisa > 0) {
                    $checkin = '
                          <button onclick="addToH2h(' . $data . ')" id="add-h2h-' . $row->id . '"
                                class="btn btn-outline-secondary btn-sm mx-1 h2h-add-btn">
                                <i class="fa fa-square"></i>
                            </button>  
                            <button onclick="removeToH2h(' . $data . ')" id="remove-h2h-' . $row->id . '"
                                class="btn btn-primary btn-sm mx-1 d-none h2h-add-btn">
                                <i class="fa fa-check-square"></i>
                            </button>  
                        ';
                }
                $output .= '
                <tr>
                    <td>
                    ' . $checkin . '
                    ' . $nomor . '</td>
                    <td>
                        ' . $row->nama . ' (' . bulan_text($row->bulan) . ' - ' . $row->tahun_id . ')
                    </td>
                    <td>
                        ' . rupiah($row->nominal) . '
                    </td>
                    <td>
                        ' . rupiah($row->dibayar) . '
                    </td>
                    <td>
                        ' . rupiah($sisa) . '
                    </td>
                    <td id="btn-hapus-id-tagihan-' . $row->id . '">

                        ' . $button . '

                    </td>
                  </tr>
                ';
            }
        } else {
            $output = 'kosong';
        }
        return response()->json($output, 200);
    }

    public function hapusTagihan($id)
    {

        $tagihan = BiayaSiswa::find($id);

        if ($tagihan->dibayar > 0) {
            return "err";
        } else {
            $tagihan->delete();
            return "Sussess";
        }
    }
    public function hapusPembayaran($id)
    {

        $Total = TotalBayarSiswa::find($id);

        $jurnal = explode(",", $Total->biaya_id);
        foreach ($jurnal as $j) {
            $jj = Jurnal::find($j);
            $updateBiaya = BiayaSiswa::find($jj->biaya_id);
            $updateBiaya->dibayar = $updateBiaya->dibayar - $jj->jumlah;
            $updateBiaya->update();
            $jj->delete();
        }
        $Total->delete();

        return "success";
    }
    public function editTagihan(Request $req)
    {

        $tagihan = BiayaSiswa::find($req->id);

        if ($req->du_dk) {
            $siswa = Siswa::where("nis", $tagihan->nis)->first();
            $siswa->du_dk = $req->du_dk;
            $siswa->update();
        }


        $tagihan->nominal = str_replace('.', '', $req->nominal);



        if ($req->du_dk) {

            $tagihan->jenis_biaya_id = $req->du_dk;

            if ($req->du_dk == 0) {
                $tagihan->nama = 'Uang Makan (DU)';
            } else {
                $tagihan->nama = 'Uang Makan (DK)';
            }
        }
        if ($req->nama) {
            $tagihan->nama = $req->nama;
        }
        $tagihan->update();

        return back();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('management::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */

    public function searchNis(Request $req)
    {
        if ($req->has('q')) {
            $cari = $req->q;
            $data = Siswa::select('nis', 'nama')
                ->whereRaw('nama LIKE "%' . $cari . '%" OR nis LIKE "%' . $cari . '%" ')
                ->get();

            return $data;
        }
    }

    public function store(Request $req)
    {
        $jumlahBayar = str_replace('.', '', $req->jumlah_bayar);
        if ($req->referensi == '3') {
            $nis = $req->nis;
            $briva = infoVA($nis)->original;
            // dd($briva);
            // return response()->json($briva, 200);
            if ($briva['status'] == true) {
                $amount = (int)$briva['data']['amount'];
                $req->va = $req->nis;
                if ($jumlahBayar == $amount) {
                    hapusVA($req);
                } else {
                    return redirect()->back()->with(
                        'error',
                        'Jumlah Bayar dengan data di BRIVA tidak cocok. Briva:'
                            . rupiah($amount) . ', Jumlah Bayar: ' . rupiah($jumlahBayar)
                    );
                }
            } else {
                return redirect()->back()->with(
                    'error',
                    'Status pembayaran di Briva belum terbayarkan!'
                );
            }
            # code...
        }
        $kode_trx = "TRX-" . time();

        $biayaId = array();

        $biayaSiswa = BiayaSiswa::where("nis", $req->nis)
            ->whereRaw("nominal != dibayar")
            ->orderBy("tahun_id", "ASC")
            ->orderBy("bulan", "ASC")
            ->orderBy("prioritas", "ASC")
            ->get();

        $totalSisa = 0;



        if ($biayaSiswa) {

            foreach ($biayaSiswa as $b) {

                $sisa = $b->nominal - $b->dibayar;

                if ($jumlahBayar > $sisa) {

                    $edit = BiayaSiswa::find($b->id);
                    $edit->dibayar = $b->dibayar + $sisa;
                    $edit->save();

                    $new = new Jurnal;
                    $new->nis = $req->nis;
                    $new->kode_trx =  $kode_trx;
                    $new->ref = $req->referensi;
                    $new->jumlah = $sisa;
                    $new->tgl_bayar = date("Y-m-d", strtotime($req->tgl_bayar));
                    $new->biaya_id = $b->id;
                    $new->nama_biaya = $b->nama;
                    $new->id_jenis_biaya = $edit->jenis_biaya_id;
                    $new->type = 1;
                    $new->save();

                    array_push($biayaId, $new->id);

                    $jumlahBayar = $jumlahBayar - $sisa;
                } else {
                    if ($jumlahBayar > 0) {

                        $edit = BiayaSiswa::find($b->id);
                        $edit->dibayar = $b->dibayar + $jumlahBayar;
                        $edit->save();

                        $new = new Jurnal;
                        $new->nis = $req->nis;
                        $new->kode_trx = $kode_trx;
                        $new->ref = $req->referensi;
                        $new->jumlah = $jumlahBayar;
                        $new->tgl_bayar = date("Y-m-d", strtotime($req->tgl_bayar));
                        $new->biaya_id = $b->id;
                        $new->id_jenis_biaya = $edit->jenis_biaya_id;
                        $new->nama_biaya = $b->nama;
                        $new->type = 1;
                        $new->save();

                        array_push($biayaId, $new->id);

                        if ($req->referensi == '3') {
                            $h2h = DB::table("tagihan_h2hs")->where("nis", $req->nis)
                                ->where('ref', '3')->first();
                            $req->va = $req->nis;
                            hapusVA($req);
                        }

                        $jumlahBayar = 0;
                    } else {
                    }
                }
            }

            $tbs = new TotalBayarSiswa;
            $tbs->kode_trx = $kode_trx;
            $tbs->nis = $req->nis;
            $tbs->ref = $req->referensi;
            $tbs->biaya_id = implode(",", $biayaId);
            $tbs->jumlah =  str_replace('.', '', $req->jumlah_bayar);
            $tbs->tgl_bayar = date("Y-m-d", strtotime($req->tgl_bayar));
            $tbs->save();


            // Hapus Data 
            $deleteH2h = DB::table("tagihan_h2hs")->where("nis", $req->nis)->delete();


            return back()->with("success", "Pembayaran berhasil ditambahkan");
        } else {
            return back()->with("success", "Tidak ada Tagihan");
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('management::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('management::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
