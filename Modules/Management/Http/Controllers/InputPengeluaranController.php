<?php

namespace Modules\Management\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Keuangan\Entities\Jurnal;
use Modules\Keuangan\Entities\JenisBiaya;


class InputPengeluaranController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('management::inputPengeluaran');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('management::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $req)
    {
   
        
        $jml = 
            Jurnal::where("nis",$req->nis)
            ->where("id_jenis_biaya","!=",4)
            ->where("type",0)
            ->sum('jumlah')
            - Jurnal::where("nis",$req->nis)
            ->where("type",0)
            ->where("id_jenis_biaya","!=",4)
            ->sum('jumlah') 
            - str_replace('.', '', $req->jumlah);

            $kode_trx = "TRX-". time();

        
           $new = new Jurnal;
           $new->kode_trx = $kode_trx;
           $new->ref = $req->referensi;
           $new->id_jenis_biaya = $req->jenis_biaya;
           $new->nama_biaya = JenisBiaya::find($req->jenis_biaya)->nama;
           $new->ket = $req->keterangan;
           $new->type = 0;
           $new->jumlah = str_replace('.', '', $req->jumlah);
           $new->tgl_bayar = date("Y-m-d", strtotime($req->tanggal));
           $new->save();
           return back()->with("success","Pembayaran berhasil ditambahkan");
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('management::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('management::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
