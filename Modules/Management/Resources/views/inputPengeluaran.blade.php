@extends('layouts.app')

@section('title')
      Input Pengeluaran
@endsection
@section('css-after') 
  <!-- daterange picker -->
  <link rel="stylesheet" href="../../plugins/daterangepicker/daterangepicker.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
@endsection


@section('content')


<div class="container" style="min-height:400px">
  <div class="card card-outline  card-primary">
    <div class="card-header">
      <h3 class="card-title">Uang Keluar (Kredit)</h3>
      <div class="card-tools">
          <button  data-toggle="modal" data-target="#modal-default" class="btn btn-primary">
            <i class="fa fa-plus"></i>
            Tambah
          </button>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive p-0">
      <table class="table table-head-fixed">
        <thead>
          <tr>
              <th>No</th>
              <th>Referensi</th>
              <th>Jenis - Keterangan</th>
              <th>Nominal</th>
              <th>Tanggal</th>
              <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @php
              $totalKeluar = 0;
          @endphp
          @foreach (Modules\Keuangan\Entities\Jurnal::where("type",0)->where("id_jenis_biaya","!=",4)->get() as $key => $item)
            <tr>
              <td>{{$key+1}}</td>  
              <td>{{ ($item->ref == 1) ? "Cash" : "Transfer"}}</td>  
              <td>{{ $item->nama_biaya}} - {{ $item->ket }}</td>  
              <td>{{ rupiah($item->jumlah) }}</td>  
              <td>{{ tgl_indo($item->tgl_bayar) }}</td>
              <td>-</td>  
            </tr>    
            @php
                $totalKeluar += $item->jumlah;
            @endphp
          @endforeach
        </tbody>
      </table>
     
    </div>
    <h4 class="text-primary text-right px-4 py-2">
       Total : {{ rupiah($totalKeluar) }}
    </h4>
    <!-- /.card-body -->
  </div>



{{-- Modal --}}

<form action="/input-pengeluaran" method="POST">
        
<div class="modal fade" id="modal-default" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
     
      <div class="modal-content">
         
        <div class="modal-header">
          <h5 class="modal-title">Tambah  Pengeluaran</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
        
              @csrf
              <div class="row">
                <div class="input-group mb-3 col-md-4 d-none" >
                  <label class="col-md-12">Referensi</label>
                  <select value="1" name="referensi" class="form-control select2">
                    @foreach (DB::table("referensi")->get() as $key => $item)
                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="input-group mb-3 col-md-4">
                  <label class="col-md-12">Jenis Biaya</label>
                  <select name="jenis_biaya" class="form-control select2" required>
                      @foreach (Modules\Keuangan\Entities\JenisBiaya::get() as $key => $item)
                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                      @endforeach
                  </select>
                </div>
                <div class="form-group col-md-4">
                  <label>Keterangan</label>
                  <input  placeholder="Keterangan" name="keterangan" type="text" class="form-control">
                </div>
                <div class="form-group col-md-4">
                  <label>Jumlah</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Rp</span>
                    </div>
                    <input id="jumlah" placeholder="Rupiah" name="jumlah" type="text" required class="form-control">
                  </div>
                  </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Tanggal</label>
                    
                    <input required id="tanggal" class="form-control" type="text" name="tanggal">
      
                  </div>
                </div>
                <div class="col-md-4 mt-2">
                  <div class="form-group">
                  <button type="submit" class="btn btn-block btn-primary mt-4">
                    <i class="fa fa-save"></i>
                    Simpan</button>
                  </div>
                </div>
                </div>

              </div>
             
        </div>
      
      <!-- /.modal-content -->
    </div>

    <!-- /.modal-dialog -->
  </div>
</form>


  @endsection

@section('js-after')
<script src="../../plugins/moment/moment.min.js"></script>
<!-- date-range-picker -->
<script src="../../plugins/daterangepicker/daterangepicker.js"></script>
<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>

 <script>
  
rupiah('#jumlah')

$('#tanggal').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  });

  $('#tanggal1').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  });

$('.select2').select2({
      theme: 'bootstrap4'
})

 $("#tahun_ajaran").change(function(){
  location.href = "?" +$("#form-data").serialize()
 })
 
 </script>   
@endsection