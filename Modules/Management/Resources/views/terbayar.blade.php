
<div class="row">
  <div class="col-md-12 text-center">

      <a class="btn btn-primary " data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
          <i class="fa fa-list-ul"></i>
         Detail Pembayaran
        </a>
  </div>
</div>
<div class="collapse" id="collapseExample">
  <div class="card card-body  table-responsive">
  
              
              <table class="table table-head-fixed table-hover">
                <thead>
                  <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Nominal</th>
                      <th>Dibayar</th>
                      <th>Sisa</th>
                  </tr>
                </thead>
                <tbody>
                 
  @php
  $totalBayar = 0;
  $nominaldu = 0;
  $dibayardu = 0;
  $no = 1;
  $daftarUlang = Modules\Keuangan\Entities\BiayaSiswa::where("nis",$nis)
  ->where("satuan","Daftar Ulang")
  ->whereRaw("dibayar > 0")
  ->orderBy("bulan","ASC");
  @endphp
  
@if ($daftarUlang->count() > 0)
<tr class="text-bold" data-toggle="collapse" href="#daftarulang">
  <td>#</td>
  <td>
      Daftar Ulang (Juli)
  </td>
  <td>
      {{ rupiah($daftarUlang->sum("nominal")) }} 
  </td>
  <td>
      {{ rupiah($daftarUlang->sum("dibayar")) }} 

  </td>
  <td>

      {{ rupiah($daftarUlang->sum("nominal") - $daftarUlang->sum("dibayar")) }} 

  </td>
  <td class="text-right">
    <div class="btn btn-outline-primary btn-sm">
      <i class="fa fa-chevron-down"></i>
    </div>
  </td>
</tr>

@php
$no = 2;                      
@endphp    
              @foreach ($daftarUlang->get() as $key => $item)

                  <tr class="collapse active" id="daftarulang" >
                    <td>{{ $key+1 }}</td>          
                    <td>{{ $item->nama }}</td>  
                    <td>{{ rupiah($item->nominal) }}</td>  
                    <td>{{ rupiah($item->dibayar) }}</td>  
                    <td>{{ rupiah($item->nominal-$item->dibayar) }}</td>  
              </tr>   
              @php
                $totalBayar += $item->dibayar;
              @endphp
              @endforeach
  
@endif

  @php
      $tg = Modules\Keuangan\Entities\BiayaSiswa::where("nis",$nis)
                    ->where("satuan","!=","Daftar Ulang")
                    ->whereRaw("dibayar > 0 ")
                    ->orderBy("bulan","ASC");
      $bln = 0;
  @endphp



                  @foreach ($tg->get() as $key => $item)
               
            
                  @if ($bln !== $item->bulan)
                  
                      @php
                      $nom =   Modules\Keuangan\Entities\BiayaSiswa::where("nis",$nis)
                              ->where("satuan","!=","Daftar Ulang")
                              ->whereRaw("dibayar > 0")
                              ->where("bulan",$item->bulan)->sum("nominal");


                      $bay =  Modules\Keuangan\Entities\BiayaSiswa::where("nis",$nis)
                              ->where("satuan","!=","Daftar Ulang")
                              ->whereRaw("dibayar > 0")
                              ->where("bulan",$item->bulan)->sum("dibayar");
                      $sis =  rupiah($nom-$bay);
                      @endphp
          
                  
                  <tr class="text-bold" data-toggle="collapse" href="#bulan-{{$item->bulan}}">
                    <td>#</td>
                    <td>
                        Bulanan ({{ bulan_text($item->bulan)}})
                    </td>
                    <td>
                        {{ rupiah($nom) }} 
                    </td>
                    <td>
                        {{ rupiah($bay) }} 

                    </td>
                    <td>

                        {{ $sis }} 

                      </td>
                      <td class="text-right">
                        <div class="btn btn-outline-primary btn-sm">
                          <i class="fa fa-chevron-down"></i>
                        </div>
                      </td>
                  </tr>  
                      @php
                          $bln = $item->bulan;
                      @endphp
                  @endif
                    <tr class="collapse active" id="bulan-{{$item->bulan}}">
                      <td>{{ $key+1 }}</td>
                      <td>{{ $item->nama }} ({{ bulan_text($item->bulan) }})</td>  
                      <td>{{ rupiah($item->nominal) }}</td>  
                      <td>{{ rupiah($item->dibayar) }}</td>  
                      <td>{{ rupiah($item->nominal-$item->dibayar) }}</td>  
                      
                    </tr>   

                    @php
                        $totalBayar += $item->dibayar;
                    @endphp
                  @endforeach

                  <tr class="text-right">
                    <td colspan="6">
                        Total : {{ rupiah($totalBayar)}}          
                    </td>
                  </tr>
                </tbody>
              </table>
             
            </div>
     
  </div>
</div>