@extends('layouts.app')

@section('title')
    Input Tabungan
@endsection
@section('css-after')
    <!-- daterange picker -->
    <link rel="stylesheet"
        href="../../plugins/daterangepicker/daterangepicker.css">
    <!-- Select2 -->
    <link rel="stylesheet"
        href="../../plugins/select2/css/select2.min.css">
    <link rel="stylesheet"
        href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
@endsection


@section('content')

    @php

    if ($req->start and $req->end) {
        $start = date($req->start);
        $end = date($req->end);
    } else {
        $start = date('Y-m-d', strtotime('-1 month'));
        $end = date('Y-m-d');
    }

    @endphp

    <div class="container"
        style="min-height:400px">

        <div class="card card-outline card-primary">
            <div class="card-header">
                <h3 class="card-title">Tabungan Siswa</h3>

                <!-- /.card-tools -->

            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 pr-4">



                        <form id="form-data"
                            class="row"
                            action="/input-tabungan-siswa"
                            method="GET">
                            <div class="input-group mb-3 col-md-12">
                                <label class="col-md-12">NIS</label>

                                <select name="nis"
                                    value="@if ($nis) {{ $nis }} @endif"
                                    class="cari form-control"></select>

                                <div class="input-group-append">
                                    <button class="btn btn-outline-primary"
                                        type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-8">
                        @if ($siswa)
                            <div class="row">
                                <div class="col-md-4">
                                    Nama
                                    <br>
                                    <b>{{ $siswa->nama }}</b>
                                    <br>
                                    <small class="text-success-800">
                                        {{ $siswa->nis }}
                                    </small>
                                </div>
                                <div class="col-md-3">
                                    Jenjang
                                    <br>
                                    <b>{{ $siswa->jenjang }}
                                    </b>
                                    <br>
                                    @php
                                        $jj = $siswa->jenjang == 'MTS' ? 7 : 10;
                                    @endphp
                                    <small class="text-success-800">

                                        {{ $siswa->kelas }}
                                    </small>
                                </div>
                                <div class="col-md-5">
                                    <h4 class="btn btn-primary btn-block py-2">
                                        <i class="icon-warning"></i> PIN :
                                        {{ Modules\Siswa\Entities\Siswa::where('nis', $nis)->first()->pin }}
                                    </h4>
                                    <h4 class="btn btn-block btn-outline-primary py-2">
                                        <i class="fa fa-money-check mr-2"></i>
                                        Saldo
                                        {{ rupiah(
                                            Modules\Keuangan\Entities\Jurnal::where('nis', $nis)->where('id_jenis_biaya', 4)->where('type', 1)->sum('jumlah') -
                                                Modules\Keuangan\Entities\Jurnal::where('nis', $nis)->where('id_jenis_biaya', 4)->where('type', 0)->sum('jumlah'),
                                        ) }}
                                    </h4>
                                </div>
                            </div>
                        @else
                            @if ($nis !== '')
                                <br>
                                <div class="alert alert-danger text-center"> *NIS Tidak ditemukan </div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>

        @if ($siswa)

            <div class="card card-outline  card-primary">
                <div class="row p-3">

                    <div class="col-md-6">
                        <label>Periode</label>
                        <form class="row"
                            action="/"
                            method="GET">
                            <input name="nis"
                                value="{{ $nis }}"
                                type="hidden">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <input name="filter"
                                        type="text"
                                        class="form-control float-right"
                                        id="filterdate">
                                </div>
                            </div>
                            <div class="col-md-6 pt-1">
                                {{ tgl_indo($start) }} -
                                {{ tgl_indo($end) }}
                            </div>
                        </form>
                    </div>

                    <div class="col-md-6">
                        <div class="row float-right mt-3">
                            <div class="btn-group thesmallprint">
                                <button data-toggle="modal"
                                    data-target="#modal-default"
                                    class="btn bg-green-600 btn-labeled">
                                    <b><i class="icon-plus-circle2"></i></b>Uang Masuk (D)
                                </button>
                                <button data-toggle="modal"
                                    data-target="#modal-default0"
                                    class="btn bg-danger-600 btn-labeled">
                                    <b><i class="icon-minus-circle2"></i></b>Uang Keluar (K)
                                </button>
                                <a target="_blank"
                                    href="/cetak-tabungan/{{ $nis }}/{{ $start }}/{{ $end }}"
                                    class="btn bg-blue-600 btn-labeled printFunction btnLS"
                                    disabled="">
                                    <b><i class="icon-printer2"></i></b>Cetak
                                </a>
                            </div>

                        </div>

                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0 text-right">
                    <table class="table table-head-fixed">
                        <thead>
                            <tr>
                                <th style="width:50px">No</th>
                                <th>Tgl Bayar
                                    <br>Keterangan
                                </th>
                                <th>Tipe</th>
                                <th>Nominal</th>
                                <th>Saldo Akhir</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            @php
                                $totalBayar = 0;
                                $dtaa = Modules\Keuangan\Entities\Jurnal::where('nis', $nis)
                                    ->where(function ($q) {
                                        $q->where('id_jenis_biaya', 4)->orWhere('nama_biaya', 'TABUNGAN');
                                    })
                                    ->where('tgl_bayar', '>=', $start)
                                    ->where('tgl_bayar', '<=', $end)
                                    ->orderBy('created_at', 'DESC')
                                    ->get();
                                $no = 1;
                            @endphp
                            @foreach ($dtaa as $key => $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ tgl_indo($item['tgl_bayar']) }} : {{ jamber($item['created_at']) }}
                                        <br>
                                        {{ $item['ket'] }}
                                    </td>
                                    <td>{!! $item['type'] == 1 ? "<span class='label bg-blue-800'>D</span>" : "<span class='label bg-danger-800'>K</span>" !!}</td>

                                    <td> {{ $item['type'] == 1 ? '+' : '-' }} {{ rupiah($item['jumlah']) }}</td>
                                    <td>{{ rupiah($item['saldo_akhir']) }}</td>
                                    <td>
                                        @if ($key == 0)
                                            <button onclick="hapusTabungan({{ $item['id'] }})"
                                                class="btn btn-danger btn-sm">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        @endif

                                    </td>
                                </tr>
                            @endforeach

                            @if ($dtaa->count() == 0)
                                @php
                                    $dataTerakhir = Modules\Keuangan\Entities\Jurnal::where('nis', $nis)
                                        ->where('id_jenis_biaya', 4)
                                        ->orderBy('created_at', 'DESC')
                                        ->first();
                                @endphp
                                @if ($dataTerakhir)
                                    <tr>
                                        <td>Data Terakhir </td>
                                        <td>{{ tgl_indo($dataTerakhir->tgl_bayar) }} :
                                            {{ jamber($dataTerakhir->created_at) }}
                                            <br>
                                            {{ $dataTerakhir->ket }}
                                        </td>
                                        <td>{!! $dataTerakhir->type == 1 ? "<span class='label bg-blue-800'>D</span>" : "<span class='label bg-danger-800'>K</span>" !!}</td>

                                        <td> {{ $dataTerakhir->type == 1 ? '+' : '-' }}
                                            {{ rupiah($dataTerakhir->jumlah) }}</td>
                                        <td>{{ rupiah($dataTerakhir->saldo_akhir) }}</td>
                                        <td>
                                        </td>
                                    </tr>
                                @endif
                            @endif

                        </tbody>
                    </table>

                </div>
                <!-- /.card-body -->
            </div>





            {{-- Modal --}}

            <form action="/input-tabungan-siswa"
                method="POST">

                <div class="modal fade"
                    id="modal-default"
                    style="display: none;"
                    aria-hidden="true">
                    <div class="modal-dialog modal-md">

                        <div class="modal-content">

                            <div class="modal-header">
                                <h5 class="modal-title">Tambah Uang Masuk</h5>
                                <button type="button"
                                    class="close"
                                    data-dismiss="modal"
                                    aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                @csrf
                                <div class="row">
                                    <input name="nis"
                                        value="{{ $nis }}"
                                        type="hidden">
                                    <div class="col-md-12">
                                        <label class="col-md-12">Referensi</label>
                                        <select name="referensi"
                                            class="form-control select2">
                                            @foreach (DB::table('referensi')->get() as $key => $item)
                                                <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <label>Jumlah</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Rp</span>
                                            </div>
                                            <input autofocus
                                                id="jumlah"
                                                placeholder="Rupiah"
                                                name="jumlah"
                                                type="text"
                                                required
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-12 pb-3">
                                        <label>Keterangan</label>
                                        <textarea name="keterangan"
                                            class="form-control"></textarea>
                                    </div>

                                    <div class="col-md-4 d-none">
                                        <div class="form-group">
                                            <label>Tanggal</label>

                                            <input required
                                                id="tanggal"
                                                class="form-control"
                                                type="text"
                                                name="tgl_bayar">

                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <button type="submit"
                                            class="btn btn-primary btn-block">
                                            <i class="fa fa-save"></i>
                                            Simpan</button>
                                    </div>

                                </div>

                            </div>

                        </div>
                        <!-- /.modal-content -->
                    </div>

                    <!-- /.modal-dialog -->
                </div>
            </form>



            {{-- Penarikan --}}


            {{-- Modal --}}

            <form action="/input-tabungan-siswa/keluar"
                method="POST">

                <div class="modal fade"
                    id="modal-default0"
                    style="display: none;"
                    aria-hidden="true">
                    <div class="modal-dialog modal-md">

                        <div class="modal-content">

                            <div class="modal-header">
                                <h5 class="modal-title">Tambah Uang Keluar</h5>
                                <button type="button"
                                    class="close"
                                    data-dismiss="modal"
                                    aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                @csrf
                                <div class="row">
                                    <input name="nis"
                                        value="{{ $nis }}"
                                        type="hidden">
                                    <div class="input-group mb-3 col-md-12">
                                        <label class="col-md-12">Referensi</label>
                                        <select name="referensi"
                                            class="form-control select2">
                                            @foreach (DB::table('referensi')->get() as $key => $item)
                                                <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Jumlah</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Rp</span>
                                            </div>
                                            <input autofocus
                                                id="jumlah1"
                                                placeholder="Rupiah"
                                                name="jumlah"
                                                type="text"
                                                required
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4 d-none">
                                        <div class="form-group">
                                            <label>Tanggal</label>

                                            <input required
                                                id="tanggal1"
                                                class="form-control"
                                                type="text"
                                                name="tgl_bayar">

                                        </div>
                                    </div>
                                    <div class="col-md-12 pb-3">
                                        <label>Keterangan</label>
                                        <textarea name="keterangan"
                                            class="form-control"></textarea>
                                    </div>

                                    <div class="col-md-12">
                                        <button type="submit"
                                            class="btn btn-primary btn-block">
                                            <i class="fa fa-save"></i>
                                            Simpan</button>
                                    </div>

                                </div>

                            </div>

                        </div>
                        <!-- /.modal-content -->
                    </div>

                    <!-- /.modal-dialog -->
                </div>
            </form>

        @endif


    @endsection

    @section('js-after')
        <script src="../../plugins/moment/moment.min.js"></script>
        <!-- date-range-picker -->
        <script src="../../plugins/daterangepicker/daterangepicker.js"></script>
        <!-- Select2 -->
        <script src="../../plugins/select2/js/select2.full.min.js"></script>

        <script>
            function hapusTabungan(id) {
                swal({
                        title: "Apakah Kamu Yakin?",
                        text: "Pastikan data yang akan dihapus sudah valid",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Hapus",
                        closeOnConfirm: false
                    },
                    function() {

                        $.get("/hapus-tabungan/" + id)
                            .done((res) => {
                                location.reload()
                            })
                    });
            }




            var start = '{{ $start }}';
            var end = '{{ $end }}';


            $('#filterdate').daterangepicker({
                startDate: start,
                endDate: end,
                locale: {
                    format: 'YYYY-MM-D'
                },
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf(
                        'month')]
                }
            }, function(start, end) {
                location.href = "?nis={{ $nis }}&start=" + start.format('YYYY-MM-D') + "&end=" + end.format(
                    'YYYY-MM-D');
            });




            rupiah('#jumlah')
            rupiah('#jumlah1')

            $('#tanggal').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1901,
                maxYear: parseInt(moment().format('YYYY'), 10)
            });

            $('#tanggal1').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1901,
                maxYear: parseInt(moment().format('YYYY'), 10)
            });

            $('.select2').select2({
                theme: 'bootstrap4'
            })


            $('.cari').select2({
                theme: 'bootstrap4',
                placeholder: 'Cari...',
                ajax: {
                    url: '/searchNis',
                    dataType: 'json',
                    delay: 250,
                    processResults: function(data) {
                        console.log(data);
                        return {
                            results: $.map(data, function(item) {
                                return {
                                    text: item.nama,
                                    id: item.nis
                                }
                            })
                        };
                    },
                    cache: true
                }
            });

            $('.cari').on('select2:select', function(e) {
                location.href = "?" + $("#form-data").serialize()
            });

            $("#tahun_ajaran").change(function() {
                location.href = "?" + $("#form-data").serialize()
            })
        </script>
    @endsection
