@extends('layouts.app')

@section('title')
Input Cash Cicilan
@endsection
@section('css-after')
<!-- daterange picker -->
<link rel="stylesheet" href="../../plugins/daterangepicker/daterangepicker.css">
<!-- Select2 -->
<link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="../../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
<style>
  .table td,
  .table td {
    padding: 0
  }
</style>
@endsection


@section('content')


<div class="container" style="min-height:400px">

  <div class="card card-outline card-primary">
    <div class="card-header">
      <h3 class="card-title">Tagihan Siswa</h3>

      <!-- /.card-tools -->
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse">
          <i class="fas fa-minus"></i>
        </button>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <div class="row">
        <div class="col-md-3">

          <form id="form-data" class="row" action="/input-cash-cicil" method="GET">
            <div class="input-group mb-3 col-md-12">
              <label class="col-md-12">Cari Berdasarkan Nama / NISN</label>
              <select name="nis" class="cari form-control" value="@if ($nis){{ $nis }}@endif">

              </select>


              <div class="input-group-append">
                <button class="btn btn-outline-primary" type="submit">
                  <i class="fa fa-search"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-9 px-3">

          @if ($siswa)
          <div class="row">
            <div class="col-md-3">
              Nama
              (
              @if($siswa->jk == 'L')
              <b><i class="icon-man"></i></b>
              @else
              <b><i class="icon-woman"></i></b>
              @endif
              )
              <br>
              <b>{{ $siswa->nama }}</b>
              <br>
              <small class="text-success-600">
                NISN : {{ $siswa->nis }}
              </small>
              <br>
              PIN : <small style="border-radius:30px"
                class="mt-2 px-3 p-1 text-white bg-danger">{{ $siswa->pin }}</small>
            </div>

            @if ($siswa)
            <div class="col-md-3">
              Jenjang
              <br>
              <b>{{ $siswa->jenjang }} - ({{ ($siswa->baru_lama == 1 ? 'Intensif' : 'Qudama') }}) </b>
              <br>
              <small class="text-success-600">
                {{ $siswa->kelas }}
              </small>
              <br>
              <div class="row">
                <div class="col-md-12">
                  <small class="text-primary-600">
                    Uang Makan :
                    {{ ($siswa->du_dk == 0 ? 'DU' : 'DK') }}
                  </small>
                </div>
                <div class="col-md-12">
                  <small class="text-primary-600">
                    Laundry :
                    {{ ($siswa->laundry == 1 ? 'YA' : 'TIDAK') }}
                  </small>
                </div>
              </div>
            </div>
            <div class="col-md-6 text-right">
              <div class="btn-group thesmallprint">
                <button data-toggle="modal" data-target="#modal-default" type="button"
                  class="btn bg-success btn-labeled mt-2 btn-sm"><b><i class="icon-plus3"></i></b>Bayar
                </button>
                <a target="_BLANK" href="/laporan/persiswa?nis={{ $siswa->nis }}&type=pembayaran"
                  class="btn btn-primary btn-labeled mt-2 btn-sm" disabled><b><i class="icon-printer"></i></b>Cetak
                </a>
                <a href="#" onclick="generateBiayaSiswa('{{$siswa->nis}}')"
                  class="btn bg-warning-600 btn-labeled mt-2 btn-sm"><b><i class="icon-yelp"></i></b>Generate
                </a>
              </div>
            </div>
            @endif
          </div>
          @else
          @if ($nis !== '')
          <br>
          <div class="alert alert-danger text-center"> *NISN Tidak ditemukan </div>

          @endif
          @endif
        </div>
      </div>


      {{-- Disini --}}
      @if ($siswa)





      <div>

        <div class="card-body table-responsive ">




          <!-- Button trigger modal -->
          <button style="
                  position:fixed;
                  left:30px;
                  bottom:60px;
                  z-index:1000" type="button" class="btn btn-primary px-4" data-toggle="modal"
            data-target="#exampleModal">
            <i class="fa fa-shopping-cart"></i>
            <span id="total-h2h-icon"><span>
          </button>

          <!-- Modal -->
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content ">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Bayar Tagihan</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div id="tbl-menunggu-pembayaran"></div>
                  <div id="tbl-checkout-h2h"></div>
                </div>
              </div>
            </div>
          </div>
          @if(session()->has('error'))
          <div class="alert alert-warning p-2 text-center">
            {{ session()->get('error') }}
          </div>
          @endif
          @if(session()->has('success'))
          <div class="alert alert-success p-2 text-center">
            {{ session()->get('success') }}
          </div>
          @endif

          <table class="table table-head-fixed table-hover">
            <thead>
              <tr>
                <th>No (Bayar H2h)</th>
                <th>Nama</th>
                <th>Nominal</th>
                <th>Dibayar</th>
                <th>Sisa</th>
                <th class="text-right">Aksi</th>
              </tr>
            </thead>
            <tbody>

              @php
              $totalSisa = 0;
              $nominaldu = 0;
              $dibayardu = 0;
              $no = 1;


              $daftarUlangGroup = Modules\Keuangan\Entities\BiayaSiswa::
              where("nis",$nis)
              ->where("satuan","Daftar Ulang")
              ->groupBy("tahun_ajaran");


              $daftarUlang = Modules\Keuangan\Entities\BiayaSiswa::
              where("nis",$nis)
              ->where("satuan","Daftar Ulang");

              @endphp


              @if ($daftarUlang->count() > 0)

                @foreach($daftarUlangGroup->get() as $dafff)

                  @php
                  $dataxxx = Modules\Keuangan\Entities\BiayaSiswa::
                  where("nis",$nis)
                  ->where("satuan","Daftar Ulang")->where("tahun_ajaran",$dafff->tahun_ajaran);
                  @endphp


                  <tr class="text-bold" data-toggle="collapse" href="#daftarulang-{{$dafff->tahun_ajaran}}">
                    <td>#</td>
                    <td>
                      Daftar Ulang ( Tahun {{ $dafff->tahun_ajaran }} )
                    </td>
                    <td>
                      {{ rupiah($dataxxx->sum("nominal")) }}
                    </td>
                    <td>
                      {{ rupiah($dataxxx->sum("dibayar")) }}

                    </td>
                    <td>
                      @php

                      $tooot = (int)$dataxxx->sum("nominal") - (int)$dataxxx->sum("dibayar");

                      @endphp
                      {{ rupiah($tooot) }}

                    </td>
                    <td class="text-right">
                      <div class="btn btn-outline-primary btn-sm">
                        <i class="fa fa-chevron-down"></i>
                      </div>
                    </td>
                  </tr>

                  <tbody class="collapse" id="daftarulang-{{$dafff->tahun_ajaran}}">
                  @foreach ($dataxxx->get() as $key => $item)
                    <tr >
                      <td>
                        @if($item->nominal-$item->dibayar > 0)
                        <button onclick="addToH2h({{ $item }})" id="add-h2h-{{$item->id}}"
                          class="btn btn-outline-secondary btn-sm mx-1 h2h-add-btn">
                          <i class="fa fa-square"></i>
                        </button>
                        <button onclick="removeToH2h({{ $item }})" id="remove-h2h-{{$item->id}}"
                          class="btn btn-primary btn-sm mx-1 d-none h2h-add-btn">
                          <i class="fa fa-check-square"></i>
                        </button>
                        @endif
                        {{ $key+1 }}
                      </td>
                      <td>{{ $item->nama }} - {{ $item->tahun_id }}</td>
                      <td>{{ rupiah($item->nominal) }}</td>
                      <td>{{ rupiah($item->dibayar) }}</td>
                      <td>{{ rupiah($item->nominal-$item->dibayar) }}</td>
                      <td id="btn-hapus-id-tagihan-{{$item->id}}">
                        @if($item->dibayar == 0)
                        <button onclick="editTagihan('{{ $item->id }}','{{ $item->nominal }}','{{ $item->jenis_biaya_id }}')"
                          class="btn btn-primary btn-sm float-right">
                          <i class="fa fa-edit"></i>
                        </button>
                        <button onclick="hapusTagihan('{{ $item->id }}')" class="btn btn-danger btn-sm float-right">
                          <i class="fa fa-trash"></i>
                        </button>
                        @endif
                      </td>
                    </tr>
                    @php
                    $totalSisa += $item->nominal-$item->dibayar;
                    @endphp
                  @endforeach
                  </tbody>

                @endforeach

              @endif

              @php
              $thnn = Modules\Keuangan\Entities\BiayaSiswa::where("nis",$nis)
              ->where("satuan","!=","Daftar Ulang")
              ->groupBy("tahun_id")
              ->select("tahun_id")
              ->get();
              @endphp

              @foreach ($thnn as $thn0)

              @php
              $tg = Modules\Keuangan\Entities\BiayaSiswa::where("nis",$nis)
              ->where("satuan","!=","Daftar Ulang")
              ->where("tahun_id",$thn0->tahun_id)
              ->orderBy("bulan","ASC")
              ->orderBy("prioritas","ASC");
              $bln = 0;
              @endphp



              @foreach ($tg->get() as $key => $item)


              @if ($bln !== $item->bulan)

              @php
              $nom = Modules\Keuangan\Entities\BiayaSiswa::where("nis",$nis)
              ->where("satuan","!=","Daftar Ulang")
              ->where("tahun_id",$thn0->tahun_id)
              ->where("bulan",$item->bulan)->sum("nominal");


              $bay = Modules\Keuangan\Entities\BiayaSiswa::where("nis",$nis)
              ->where("satuan","!=","Daftar Ulang")
              ->where("tahun_id",$thn0->tahun_id)
              ->where("bulan",$item->bulan)->sum("dibayar");
              $sis = rupiah($nom-$bay);
              @endphp


              <tr class="text-bold" data-toggle="collapse" href="#bulan-{{$item->bulan}}-{{$thn0->tahun_id}}">
                <td>#</td>
                <td>
                  Bulanan ({{ bulan_text($item->bulan)}} - {{ $item->tahun_id }})
                </td>
                <td>
                  {{ rupiah($nom) }}
                </td>
                <td>
                  {{ rupiah($bay) }}

                </td>
                <td>

                  {{ $sis }}

                </td>
                <td class="text-right">
                  <div class="btn btn-outline-primary btn-sm">
                    <i class="fa fa-chevron-down"></i>
                  </div>
                </td>
              </tr>
              @php
              $bln = $item->bulan;
              @endphp
              @endif
              <tr class="collapse active" id="bulan-{{$item->bulan}}-{{$thn0->tahun_id}}">
                <td>
                  @if($item->nominal-$item->dibayar > 0)
                  <button onclick="addToH2h({{ $item }})" id="add-h2h-{{$item->id}}"
                    class="btn btn-outline-secondary btn-sm mx-1 h2h-add-btn">
                    <i class="fa fa-square"></i>
                  </button>
                  <button onclick="removeToH2h({{ $item }})" id="remove-h2h-{{$item->id}}"
                    class="btn btn-primary btn-sm mx-1 d-none h2h-add-btn">
                    <i class="fa fa-check-square"></i>
                  </button>
                  @endif

                  {{ $key+1 }}</td>
                <td>{{ $item->nama }} ({{ bulan_text($item->bulan) }} - {{ $item->tahun_id }})</td>
                <td>{{ rupiah($item->nominal) }}</td>
                <td>{{ rupiah($item->dibayar) }}</td>
                <td>{{ rupiah($item->nominal-$item->dibayar) }}</td>
                <td id="btn-hapus-id-tagihan-{{$item->id}}">

                  @if ($item->dibayar == 0)


                  <button onclick="editTagihan('{{ $item->id }}','{{ $item->nominal }}','{{ $item->jenis_biaya_id }}')"
                    class="btn btn-primary btn-sm float-right">
                    <i class="fa fa-edit"></i>
                  </button>


                  <button onclick="hapusTagihan('{{ $item->id }}')" class="btn btn-danger btn-sm float-right">
                    <i class="fa fa-trash"></i>
                  </button>

                  @endif


                </td>
              </tr>

              @php
              $totalSisa = $totalSisa + $item->nominal-$item->dibayar;
              @endphp
              @endforeach

              @endforeach


              <tr class="text-right">
                <td colspan="6">
                  <b>Sisa Tagihan : {{ rupiah($totalSisa)}}</b>
                </td>
              </tr>
            </tbody>
          </table>


        </div>
        <!-- /.card-body -->
      </div>

    </div>
    <div class="card card-outline card-primary">
      <div class="card-header">
        <h3 class="card-title">History Pembayaran</h3>
      </div>



      <!-- /.card-header -->
      <div class="card-body  table-responsive">

        <div class="table-responsive p-0" style="height: 200px;">
          <table class="table table-head-fixed">
            <thead>
              <tr>
                <th>No</th>
                <th>Kode Transaksi</th>
                <th>Metode Bayar</th>
                <th>Jumlah Bayar</th>
                <th>Tanggal Bayar</th>
                <th class="text-right">Action</th>
              </tr>
            </thead>
            <tbody>
              @php
              $totalbayar = 0;
              @endphp
              @foreach (Modules\Keuangan\Entities\TotalBayarSiswa::
              where("nis",$nis)
              ->orderBy("id","DESC")
              ->get() as $key => $item)
              <tr>
                <td>{{$key+1}}</td>
                <td>{{ $item->kode_trx }}</td>
                <td>{{ ($item->ref == 1) ? 'Cash' : 'Transfer' }}</td>
                <td>{{ rupiah($item->jumlah) }}</td>
                <td>{{ tgl_indo($item->tgl_bayar) }}</td>
                <td>

                  <button type="button" class="btn bg-danger btn-labeled btn-sm float-right"
                    onclick="hapusBayar('{{ $item->id }}')">
                    <b><i class="icon-bin2"></i></b> Batal
                  </button>
                  <a target="_BLANK" href="/cetak-struk/{{ $item->id }}?print=true"
                    class="btn bg-primary btn-labeled btn-sm float-right">
                    <b><i class="fa fa-print"></i></b> Cetak Struk
                  </a>
                </td>
              </tr>
              @php
              $totalbayar += $item->jumlah_bayar;
              @endphp
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
    </div>

  </div>







  {{-- Modal --}}

  <form id="form-cari" action="/input-cash-cicil" method="POST">

    <div class="modal fade" id="modal-default" style="display: none;" aria-hidden="true">
      <div class="modal-dialog modal-lg">

        <div class="modal-content">

          <div class="modal-header">
            <h5 class="modal-title">Bayar Tagihan Siswa</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Hapus">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">

            <div class="card-body table-responsive p-0">
              <table class="table table-head-fixed">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Sisa Tagihan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach (Modules\Keuangan\Entities\BiayaSiswa::
                  where("nis",$nis)
                  ->whereRaw("nominal != dibayar")
                  ->orderBy("tahun_id","ASC")
                  ->orderBy("bulan","ASC")
                  ->orderBy("prioritas","ASC")
                  ->get() as $key => $item)
                  <tr>
                    <td>{{$key+1}}</td>
                    <td>{{ $item->nama }} ({{ bulan_text($item->bulan) }} - {{ $item->tahun_id }})</td>
                    <td>{{ rupiah($item->nominal-$item->dibayar) }}</td>

                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>


            @csrf
            <div class="row">
              <input name="nis" value="{{ $nis }}" type="hidden">
              <div class="input-group mb-3 col-md-3">
                <label class="col-md-12">Total biaya</label>
                <input value="{{ rupiah($totalSisa) }}" type="text" class="form-control" disabled>
                <div class="input-group-append">
                  <button onclick="$('#jumlahBayar').val('{{ $totalSisa }}')" class="btn btn-outline-primary"
                    type="button">
                    <i class="fa fa-plus"></i>
                  </button>
                </div>
              </div>


              <div class="form-group col-md-3">
                <label>Jumlah Bayar</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Rp</span>
                  </div>
                  <input required name="jumlah_bayar" type="text" id="jumlahBayar" value="" class="form-control rupiah">
                </div>
              </div>
              <div class="col-md-3">
                <label> Metode Bayar</label>
                <select required name="referensi" class="form-control select2">
                  @foreach (DB::table("referensi")->get() as $key => $item)
                  <option value="{{ $item->id }}">{{ $item->nama }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Tanggal</label>

                  <input required id="tanggal" class="form-control" type="text" name="tgl_bayar">

                </div>
              </div>

            </div>

          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            {{--<button type="submit" class="btn btn-primary">--}}
            {{--<i class="fa fa-save"></i>--}}
            {{--Bayar & Simpan</button>--}}
            <button type="submit" class="btn bg-danger btn-labeled"><b><i class="icon-floppy-disk"></i></b>Simpan
            </button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>

      <!-- /.modal-dialog -->
    </div>
  </form>







  <div id="modal-generate_biaya_siswa" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">

            Generate Biaya Siswa
            ({{ tgl_indo(Date('Y-m-d'))}})
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">





          <div class="container bg-white ">

            <div class="row">
              <div class="col-md-12">




                <div class="col-md-12">

                  <div class="form-group">
                    <label>Tahun Ajaran</label>
                    @php
                    $y = Date('Y')-1;
                    @endphp
                    <select id="tahun_ajaran" class="form-control select2" style="width: 100%;">
                      @for ($i = 0; $i < 3; $i++) <option @if ($y+$i==Date('Y')) selected @endif value="{{$y+$i}}">Tahun
                        {{ $y+$i }} - {{ $y+$i+1 }}</option>
                        @endfor
                    </select>
                  </div>
                </div>


                <div class="col-md-12">

                  <div class="form-group">
                    <label>Bulan</label>
                    <select id="bulan" class="form-control select2" style="width: 100%;">

                      @php
                      $bulan = array (
                      'Januari',
                      'Februari',
                      'Maret',
                      'April',
                      'Mei',
                      'Juni',
                      'Juli',
                      'Agustus',
                      'September',
                      'Oktober',
                      'November',
                      'Desember'
                      );
                      @endphp

                      @foreach ($bulan as $key => $item)
                      <option @if (Date('m')==$key+1) selected @endif value="{{$key+1}}">{{ $item }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>



                <div class="col-md-12 d-none">
                  <div class="form-group">
                    <label>Tingkat</label>
                    <select required id="jenjang" class="form-control select2" style="width: 100%;">
                      @foreach (Modules\Keuangan\Entities\MasterBiaya::all() as $item)

                      <option @if ($item->tingkat == $siswa->jenjang)
                        selected
                        @endif
                        value="{{ $item->tingkat }}" >
                        {{ $item->tingkat }}
                      </option>

                      @endforeach

                    </select>
                  </div>
                </div>


                <div class="col-md-12 d-none">
                  <div class="form-group" id="list-kelas">
                    <label>Kelas</label>
                    <select id="kelas" class="form-control select2" style="width: 100%;">
                      <option value="100" selected="selected">Semua Kelas</option>
                      <option value="0">Kelas 7</option>
                      <option value="1">Kelas 8</option>
                      <option value="2">Kelas 9</option>
                    </select>
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label>Jenis Satuan</label>
                    <h6 id="jenis_satuan"></h6>
                  </div>
                </div>


                <div class="col-md-12">

                  <div class="form-group">
                    <label>NISN/NIPD</label>
                    <input value="{{$siswa->nis}}" type="text" class="form-control" id="nis" placeholder="NIS/NIPD">
                  </div>

                </div>

                <div class="col-md-12">
                  <label></label>
                  <div class="form-group mt-2">
                    <button onclick="generateBiaya()" class="btn bg-primary-c btn-block ">
                      <i class="fa fa-sync mr-2"></i>
                      Generate</button>
                  </div>
                </div>

              </div>
            </div>
            <br>
            <br>

            <div id="notif-status"></div>
          </div>

        </div>

      </div>
    </div>
  </div>


  @endif
</div>
</div>





<div id="modal-edit-tagihan" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Tagihan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form action="/edit-nominal-tagihan" method="POST">
          @csrf
          <input type="hidden" id="id-tagihan-edit" name="id">

          <div class="form-group clearfix" id="edit-du-dk">

          </div>

          <div class="form-group">
            <label> Nominal </label>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Rp</span>
              </div>
              <input name="nominal" placeholder="..." type="text" class="form-control" id="nominal-tagihan-edit">
            </div>
          </div>
          <button class="btn btn-primary">
            <i class="fa fa-save"></i>
            Simpan
          </button>
        </form>
      </div>
    </div>
  </div>
</div>




@endsection

@section('js-after')
<script src="../../plugins/moment/moment.min.js"></script>
<!-- date-range-picker -->
<script src="../../plugins/daterangepicker/daterangepicker.js"></script>
<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>

<!-- InputMask -->
<script src="../../plugins/inputmask/jquery.inputmask.bundle.js"></script>

<script>
  var h2hGenerateData = [];
var h2hMenungguData = [];
function generateH2h(){
  

  $("#proses-tagihan-btn").hide();
    $("#proses-tagihan-btn-loading").show();
  var dataId = []
   h2hGenerateData.forEach(i => {
     dataId.push(i.id)
    })
    let url = `@if($siswa)/api/v1/buatTagihanBris/{{$siswa->nis}}/{{$siswa->pin}}/@endif`+dataId.join("-");
  $.get(url)
    .done(res => {
      console.log(res)
      location.reload()

    })

}

function bayarTunai(){
  $("#proses-tagihan-btn").hide();
    $("#proses-tagihan-btn-loading").show();
  var dataId = []
   h2hGenerateData.forEach(i => {
     dataId.push(i.id)
    })
    let url = `/@if($siswa){{$siswa->nis}}@endif/bayar-tagihan-tunai-by-id/`+dataId.join("-");
  $.get(url)
    .done(res => {
      console.log(res)
      location.reload()

    })
}

function batalkanH2h(){
      let url = `@if($siswa)/api/v1/batalkanTagihanBris/{{$siswa->nis}}/{{$siswa->pin}}/@endif`+h2hMenungguData.id;
      $.get(url)
        .done(res => {
          console.log(res)
          location.reload()
        })
}

function getDataM(){
   let url = `@if($siswa)/api/v1/dataTagihan/{{$siswa->nis}}/{{$siswa->pin}}@endif`;

   $.get(url)
    .done(res => {
      if(res.info !== 'Tagihan tidak ditemukan'){
        
        h2hMenungguData = res.data
        
        $(".h2h-add-btn").hide()

      var resData = res.data.items

       var dataList = `<table class="table table-hover">
         <tr>
            <td colspan="2"><b>Menunggu Pembayaran ...</b></td>
          </tr>
          <tr>
            <td>Nama</td>
            <td class="text-right">Jumlah</td>
          </tr>
          `;
          var totalTa = 0;

          resData.forEach(i => {
            var sisaT = parseInt(i.nominal);
            totalTa = totalTa+sisaT;
            var list = `<tr><td> ${i.nama} </td> <td class="text-right"> 
            <span class="float-left">Rp.</span>
            ${formatRupiah(sisaT)} </td></tr>`;
            dataList += list

            $("#btn-hapus-id-tagihan-"+i.id).hide();
            
          })
          
          dataList += `
          <tr>
          <td><b>Total</b></td>
          <td class="text-right"><b>
          <span class="float-left">Rp.</span>
          ${formatRupiah(totalTa)} </b></td>
          </tr>`;
          
          dataList += `
          <tr>
            <td colspan="2" > <button onclick="batalkanH2h()" type="button" class="mt-3 btn btn-block btn-danger float-left">Batalkan</button></td>
          </tr>
          
          </table>`;
          $("#total-h2h-icon").html(resData.length)
         $("#tbl-menunggu-pembayaran").html(dataList)
      }
    })

 
}

getDataM();

function updateCheckOut(){
    var dataList = `<table class="table table-hover">
    <tr>
      <td>Nama</td>
      <td class="text-right">Jumlah</td>
    </tr>
    `;
    var totalTa = 0;

    h2hGenerateData.forEach(i => {
      var sisaT = i.nominal - i.dibayar;
      totalTa = totalTa+sisaT;
      var list = `<tr><td> ${i.nama} </td> <td class="text-right"> 
      <span class="float-left">Rp.</span>
      ${formatRupiah(sisaT)} </td></tr>`;
      dataList += list
    })
    
    dataList += `
    <tr>
    <td><b>Total</b></td>
    <td class="text-right"><b>
    <span class="float-left">Rp.</span>
     ${formatRupiah(totalTa)} </b></td>
    </tr>`;
    
    dataList += `

     <tr id="proses-tagihan-btn">
      <td>
          <button onclick="generateH2h()" type="button" class="btn btn-primary mt-3">Tagihkan Ke Bank</button>
      </td>
      <td>
          <button onclick="bayarTunai()" type="button" class="btn btn-secondary float-right mt-3">Bayar Tunai</button>
      </td>
      </tr>
             
    </table>`;

    $("#total-h2h-icon").html(h2hGenerateData.length)
    $("#tbl-checkout-h2h").html(dataList);

}
function addToCheck(item){
      var cekJikaAda = false;
      h2hGenerateData.forEach(i=> {
        if(i.id == item.id){
          cekJikaAda = true
        }
      })
    console.log(!cekJikaAda);
    if (!cekJikaAda) {
       $("#add-h2h-"+item.id).addClass("d-none");
       $("#remove-h2h-"+item.id).removeClass("d-none");

        h2hGenerateData.push(item)
    }else{
      
      var dataH2hBaru = [];
      h2hGenerateData.forEach(i=> {
        if(i.id !== item.id){
          dataH2hBaru.push(i)
        }
      })
      h2hGenerateData = dataH2hBaru
      
      $("#add-h2h-"+item.id).removeClass("d-none");
      $("#remove-h2h-"+item.id).addClass("d-none");
    }

  updateCheckOut()
}
function addToH2h(item){
    addToCheck(item)
}
function removeToH2h(item){
    addToCheck(item)

   
}
  
rupiah('#jumlahBayar');
rupiah("#nominal-tagihan-edit")

$('#tanggal').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  });

$('.select2').select2({
      theme: 'bootstrap4'
})

//  $("#tahun_ajaran").change(function(){
//   location.href = "?" +$("#form-data").serialize()
//  })


 function editTagihan(id,nominal,jb){
   $("#id-tagihan-edit").val(id);

  if(jb == 1 || jb == 2){

    var ck1 = '';
    var ck2 = '';

    if(jb == 1){
      ck1 = 'checked';
    }else if(jb == 2){
      ck2 = 'checked';
    }
    $("#edit-du-dk").html(`
      <label class="w-100"> Uang Makan</label>
          <div class="icheck-success d-inline">
            <input onclick="setval(400000)" ${ck1} value="0" type="radio" name="du_dk"  id="radioSuccess1">
            <label for="radioSuccess1">
              Dapur Umum (DU)
            </label>
          </div>
          <div class="icheck-success d-inline">
            <input onclick="setval(430000)" ${ck2} value="1" type="radio" name="du_dk" id="radioSuccess2">
            <label for="radioSuccess2">
                Dapur Keluarga (DK)
            </label>
          </div>
      `);
  }else{
    $("#edit-du-dk").html('');
  }


   $("#nominal-tagihan-edit").val(nominal);
   $("#modal-edit-tagihan").modal("show");
 }

 function setval(nilai){
    $("#nominal-tagihan-edit").val(nilai);
  }

 function hapusTagihan(id){

  swal({
      title: "Apakah Kamu Yakin?",
      text: "Pastikan data yang akan dihapus sudah valid",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Hapus",
      closeOnConfirm: false
    },
    function(){

      $.get("/hapus-tagihan/"+id)
        .done((res) => {
        location.reload()
        })
    });
 }

 function hapusBayar(id){

swal({
    title: "Apakah Kamu Yakin?",
    text: "Pastikan data yang akan dihapus sudah valid !",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-primary",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: false
  },
  function(){

    $.get("/hapus-pembayaran/"+id)
      .done((res) => {
        console.log(res);
       location.reload()
      })
  });
}

 

 $('.cari').select2({
    theme: 'bootstrap4',
    placeholder: 'Ketikan Nama / NISN',
    ajax: {
      url: '/searchNis',
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        console.log(data);
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.nama,
              id: item.nis
            }
          })
        };
      },
      cache: true
    }
  });

  $('.cari').on('select2:select', function (e) {
    location.href = "?" +$("#form-data").serialize()
  });



function generateBiayaSiswa(){
  $("#modal-generate_biaya_siswa").modal("show");
}
</script>



<script>
  $("#bulan").change(function(){
     if($(this).val() == 7){
       $("#jenis_satuan").html(" Daftar Ulang ");
     }else{
       $("#jenis_satuan").html(" Bulanan ");
     }
   });
 
   $("#jenjang").change(function(){
 
 var list = ``;
 
 if($(this).val() == 'MTS'){
   list = ` <label>Kelas</label>
             <select id="kelas" class="form-control select2" style="width: 100%;">
               <option value="100"  selected="selected">Semua Kelas</option>
               <option value="0">Kelas 7</option>
               <option value="1">Kelas 8</option>
               <option value="2">Kelas 9</option>
             </select>`;
 }else{
   list = ` <label>Kelas</label>
             <select id="kelas" class="form-control select2" style="width: 100%;">
               <option value="100"  selected="selected">Semua Kelas</option>                
               <option value="0">Kelas 10</option>
               <option value="1">Kelas 11</option>
               <option value="2">Kelas 12</option>
             </select>`;
 }
 $("#list-kelas").html(list);
 
 $('#kelas').select2({
       theme: 'bootstrap4'
 })
 
 })
 
 function generateBiaya(){
   
 
 
   var thn = $("#tahun_ajaran").val();
   var bulan = $("#bulan").val();
   var nis = $("#nis").val();
   var jenjang = $("#jenjang").val();
   var kelas = $("#kelas").val();
 
 
 
     $("#notif-status").html(`
       <div class="progress">
           <div id="pro" class="progress-bar progress-bar-striped" style="width:1%">
               1 %
           </div>
       </div>
     `);
 
     var width = 1;
     var idpro = setInterval(kondisiPro, 1);
 
     function kondisiPro() {
       if (width >= 65) {
         clearInterval(idpro);
       } else {
         width++; 
         pro.style.width = width + '%'; 
         pro.innerHTML = width + "%"; 
       }
     }
 
       var data = {
         _token: '{{ csrf_token() }}',
         tahun : thn,
         bulan : bulan,
         nis : nis,
         jenjang : jenjang,
         kelas : kelas
       }
 
 
       $.post("/keuangan/generate-biaya",data)
         .done((res) => {
             console.log(res);
           
             var r = JSON.parse(res);
 
 
             if(r.length > 0){
                 $("#notif-status").html(`
                 <div class="progress">
                     <div class="progress-bar progress-bar-striped" style="width:100%">
                         100%
                     </div>
                 </div>
                 <br>
                 <div class="alert alert-success" role="alert">
                   Biaya Siswa Berhasil Digenerate
                 </div>
                 `);  
               } else {
                 width = 100;
 
                 $("#notif-status").html(`
                   <div class="progress">
                       <div class="progress-bar progress-bar-striped" style="width:100%">
                           100%
                       </div>
                   </div>
                   <br>
                     <div class="alert alert-danger" role="alert">
                       Tidak Ada Data Yang Digenerate
                     </div>
                 `);
               }

               location.reload()
           
         })
 
 
 }
</script>
@endsection