<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group(function() {
    
    Route::get('/cetak-struk/{id}', 'InputCashCicilanController@dataStruk');

    Route::get('/input-cash-cicil', 'InputCashCicilanController@index');
    Route::post('/input-cash-cicil', 'InputCashCicilanController@store');

    Route::post('/load-data-input-cash-cicil', 'InputCashCicilanController@loadData')->name('load-data-input-cash-cicil');

    

    Route::get('/hapus-tagihan/{id}', 'InputCashCicilanController@hapusTagihan');
    Route::get('/hapus-pembayaran/{id}', 'InputCashCicilanController@hapusPembayaran');

    Route::post('/edit-nominal-tagihan','InputCashCicilanController@editTagihan');

    Route::get('/hapus-tabungan/{id}', 'InputTabunganSiswaController@hapusTabungan');

    Route::get('/input-tabungan-siswa', 'InputTabunganSiswaController@index');
    Route::post('/input-tabungan-siswa', 'InputTabunganSiswaController@store');
    Route::post('/input-tabungan-siswa/keluar', 'InputTabunganSiswaController@storeKeluar');

    Route::get("/input-pengeluaran","InputPengeluaranController@index");
    Route::post("/input-pengeluaran","InputPengeluaranController@store");
    Route::get("/searchNis",'InputCashCicilanController@searchNis');

    Route::get("/{nis}/bayar-tagihan-tunai-by-id/{id}",'InputCashCicilanController@bayarTagihanTunaiById');


});
