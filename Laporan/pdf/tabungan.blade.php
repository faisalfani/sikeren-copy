@extends('layouts.pdf')

@section('title')
      Laporan
@endsection
@section('css-after')

  @endsection
@section('content')

@include('comp.header_pdf')
@php

$tgl_awal = $start;
$tgl_akhir = $end;

$tgl_aw = $start;
$tgl_ak = $end;



$pr = tgl_indo($tgl_awal) ." - ". tgl_indo($tgl_akhir);


$where = "id_jenis_biaya = 4";
$whereJenis = "id = 4";


@endphp


            <div class="sub-header">
                <h3>Laporan Tabungan Siswa
                <br>
                {{ $siswa->nama }}
                </h3>
                <br>
                Periode {{ $pr }}
               
            </div>
                
            <table>
                <thead>
                    <tr>
                    <th colspan="5"> Titipan Tabungan Santri </b></th>
                    </tr>
                </thead>
                <tbody >
                <tr  style="text-align:center;">
                    <th><b>Jam</b></th>
                    <th><b>Tanggal</b></th>
                    <th><b>Uang Masuk</b></th>
                    <th><b>Uang Keluar</b></th>
                    <th style="text-align:right">Saldo Akhir</b></th>
                </tr>

                    @php
                         $jurnal = Modules\Keuangan\Entities\Jurnal::
                            where("nis",$nis)
                            ->where("tgl_bayar",">=",$start)
                            ->where("tgl_bayar","<=",$end)
                            ->where("id_jenis_biaya",'4')
                            ->get();
                            // dd($jurnal);
                    @endphp

                  @foreach ($jurnal as $item)
                   
                          <tr>
                              <td>
                                @php
                                $tgll = explode(" ",$item->created_at);

                                @endphp
                                Jam {{ $tgll[1]}} 
                              </td>
                            <td >
                                {{ tgl_indo($tgll[0]) }}  
                            </td>    
                           
                            <td style="text-align:right">
                                @if($item->type == 1)
                                {{ rupiah($item->jumlah) }} 
                                @endif
                            </td>
                            <td style="text-align:right">
                                @if($item->type == 0)
                                {{ rupiah($item->jumlah) }} 
                                @endif
                            </td>
                            <td style="text-align:right" >
                                {{ rupiah($item->saldo_akhir) }} 
                            </td>
                    </tr>  
                          
                  @endforeach
                     
                  
            </tbody>
            </table>
            <br>
            <br>
@endsection

@section('js-after')
@endsection