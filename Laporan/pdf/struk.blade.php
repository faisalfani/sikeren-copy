@extends('layouts.pdf')

@section('title')
    Laporan
@endsection
@section('css-after')
    <style>
        html {
            height: 215px;
            border-bottom: 1px solid #ccc
        }

    </style>
@endsection
@section('content')
    {{-- @for ($i = 0; $i < 3; $i++) --}}

    @include('comp.header_pdf')

    @php

    $tbs = Modules\Keuangan\Entities\TotalBayarSiswa::find($id);
    $siswa = Modules\Siswa\Entities\Siswa::where('nis', $tbs->nis)->first();
    $jurnal = Modules\Keuangan\Entities\Jurnal::where('kode_trx', $tbs->kode_trx)
        ->where('nis', $tbs->nis)
        ->get();

    $dataa = Modules\Keuangan\Entities\BiayaSiswa::where('nis', $tbs->nis);
    $totalTagihan = $dataa->sum('nominal');
    $totalDibayar = $dataa->sum('dibayar');

    @endphp

    <div class="sub-header"
        style="padding:0;margin:0;margin-top:-10px">

        <u>BUKTI PEMBAYARAN</u>
        <br>
        Kode {{ $tbs->kode_trx }}

    </div>

    <table style="font-size:12px;text-align:left;">
        <thead style="text-align:left;">
            <tr>
                <td style="background:#fff;border:0;"></td>
                <td style="background:#fff;border:0;">
                    NIS
                </td>
                <td colspan="2"
                    style="background:#fff;border:0;">
                    : <b>{{ $siswa->nis }}</b>
                </td>

                <td style="background:#fff;border:0;">
                    Tanggal
                </td>
                <td colspan="2"
                    style="background:#fff;border:0;">
                    : <b>{{ tgl_indo($tbs->tgl_bayar) }}</b>
                </td>
            </tr>
            <tr>
                <td style="background:#fff;border:0;"></td>

                <td style="background:#fff;border:0;">
                    Nama
                </td>
                <td colspan="2"
                    style="background:#fff;border:0;">
                    : <b>{{ $siswa->nama }}</b>
                </td>
                <td style="background:#fff;border:0;">
                    Jumlah Bayar
                </td>
                <td colspan="2"
                    style="background:#fff;border:0;">
                    : <b>{{ rupiah($tbs->jumlah) }}</b>
                </td>

            </tr>

            <tr>
                <td style="background:#fff;border:0;"></td>

                <td style="background:#fff;border:0;">
                    Kelas
                </td>
                <td colspan="2"
                    style="background:#fff;border:0;">
                    : <b>{{ $siswa->kelas }}</b>
                </td>
                <td style="background:#fff;border:0;">
                    Tunggakan
                </td>
                <td colspan="2"
                    style="background:#fff;border:0;">
                    : <b>{{ rupiah($totalTagihan - $totalDibayar) }}</b>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th style="background:#fff;border:0"></th>
                <th>No</td>
                <th colspan="3">Nama Biaya</th>
                <th colspan="2"
                    style="text-align:right">Jumlah</th>
                <th style="background:#fff;border:0"></th>
            </tr>
            @foreach ($jurnal as $key => $j)
                <tr>
                    <td style="background:#fff;border:0"></td>
                    <td>{{ $key + 1 }}</td>
                    <td colspan="3"> {{ $j->nama_biaya }}</td>
                    <td colspan="2"
                        style="text-align:right">{{ rupiah($j->jumlah) }}</td>
                    <td style="background:#fff;border:0"></td>

                </tr>
            @endforeach
            <tr>
                <td style="background:#fff;border:0"></td>
                <td colspan="6"
                    style="background:#fff;border:0">
                    <div style="float:left;width:250px">
                        <br>
                        <span style="margin-left:70px">Siswa,</span>

                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <hr style="width:70%;margin-left:5%">
                        <b style="text-align:center;float:left;margin-left:50px;">
                            {{ $siswa->nama }}
                        </b>
                    </div>
                    <div style="float:right;width:250px;">
                        Tasikmalaya, {{ tgl_indo($tbs->tgl_bayar) }}
                        <br>
                        <span style="margin-left:70px">Bendahara,</span>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <hr style="width:70%;margin-left:5%">

                        <b style="text-align:center;float:left;margin-left:100px;margin-top:20px">
                            {{-- {{  Auth::user()->name}} --}}
                        </b>

                    </div>
                </td>
            </tr>
        </tbody>
    </table>

    {{-- @endfor --}}
@endsection

@section('js-after')
@endsection
