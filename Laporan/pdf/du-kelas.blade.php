@extends('layouts.pdf')

@section('title')
      Laporan
@endsection
@section('css-after')

<style>
.page_break { page-break-before: always; }
</style>

  @endsection
@section('content')

@include('comp.header_pdf')

            <div class="sub-header">
                LAPORAN DAFTAR ULANG
                <br>
                Tahun Ajaran {{ $req->du_tahun_ajaran}} / {{ $req->du_tahun_ajaran+1}}
                <br>
               {{ $kelas->nama}}
               <br>
               @if($req->gender !== 'SEMUA')
                  ({{ ($req->gender == 'L') ? '(P) Putra' : '(Pi) Putri' }})
              @endif
            </div>
            
            <table >
                    <thead>  
                      <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Nominal</th>
                        <th>Bayar</th>
                        <th>Sisa Tagihan</th>
                      </tr>
                    </thead>
                    <tbody>

                      @php
                          $jmlNominal = 0;
                          $jmlBayar = 0;
                          $jmlSisa = 0;
                      @endphp
                    @foreach ($res as $key => $s)

                          @php
                            $jmlNominal += $s['nominal'];
                            $jmlBayar += $s['dibayar'];
                            $jmlSisa += $s['tagihan'];
                        @endphp
                        <tr>
                          <td> {{ $key+1 }}</td>
                          <td> {{ $s['nama'] }}</td>
                          <td> {{ rupiah($s['nominal']) }}</td>
                          <td> {{ rupiah($s['dibayar']) }}</td>
                          <td> {{ rupiah($s['tagihan']) }}</td>
                         
                        </tr>
                    @endforeach
                    <tr>
                      <td></td>
                      <td>Jumlah</td>
                      <td>{{rupiah($jmlNominal)}}</td>
                      <td>{{rupiah($jmlBayar)}}</td>
                      <td>{{rupiah($jmlSisa)}}</td>
                    </tr>
                    </tbody>
                  </table>


@endsection

@section('js-after')

@endsection