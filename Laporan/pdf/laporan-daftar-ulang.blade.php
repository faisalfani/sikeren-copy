@extends('layouts.pdf')

@section('title')
      Laporan
@endsection
@section('css-after')

<style>
body{
    
}
.page_break { page-break-before: always; }
</style>

  @endsection
@section('content')

@include('comp.header_pdf')
@php

$tgl  =     explode(" ",$req->filter);

$tgl_awal = $tgl[0];
$tgl_akhir = $tgl[2];


if($req->format_laporan == 'rentang'){
    if($req->bulan == 0){
        $pr = " Tahun " .$req->tahun_ajaran;

        $where = " YEAR(jurnals.tgl_bayar) = $req->tahun_ajaran";

    }else{
        $pr = bulan_text($req->bulan) ." ". $req->tahun_ajaran;
        
        $where = " YEAR(jurnals.tgl_bayar) = $req->tahun_ajaran AND MONTH(jurnals.tgl_bayar) = $req->bulan ";

    }

}else if($req->format_laporan == 'periode'){
    $pr = tgl_indo($tgl_awal) ." - ". tgl_indo($tgl_akhir);

    $where = " jurnals.tgl_bayar >= '$tgl_awal' AND jurnals.tgl_bayar <= '$tgl_akhir'";

}else{
    $pr = "Seluruh";

    $where = "jurnals.tgl_bayar";
}


if($req->tingkat !== 'SEMUA'){
    $where .= " AND s.jenjang = '$req->tingkat' ";
}

if($req->gender !== 'SEMUA'){
    $where .= " AND s.jk = '$req->gender' ";
}

if($req->ref > 0){
    $where .= " AND jurnals.ref = '$req->ref' ";
}


@endphp

            <div class="sub-header">
                LAPORAN DAFTAR ULANG
                @if($req->tingkat !== 'SEMUA')
                    {{ $req->tingkat }}
                @endif
                
                @if($req->gender !== 'SEMUA')
                    ({{ ($req->gender == 'L') ? '(P) Putra' : '(Pi) Putri' }})
                @endif
                <br>
                Tahun Ajaran {{ $req->du_tahun_ajaran }} /{{ $req->du_tahun_ajaran+1}}
                <br>
                Periode {{ $pr }}
               
            </div>
            <table>
                <thead>
                    <tr>
                        <th colspan="3">Pendapatan</th>
                    </tr>
                </thead>
                <tbody style="text-align:left;">
                <tr>
                    <th style="border-left:1px solid #ccc">No</th>
                    <th style="border-left:1px solid #ccc">Keterangan</th>
                    <th style="text-align:right;border-left:1px solid #ccc;border-right:1px solid #ccc"">Jumlah</th>
                </tr>
                @php
                    $totalPendapatan = 0;

                    $jenisBiaya =  Modules\Keuangan\Entities\Jurnal::
                    join("biaya_siswas as b","b.id","jurnals.biaya_id")
                    ->join("siswas as s","s.nis","jurnals.nis")
                    ->join("jenis_biayas as j","j.id","jurnals.id_jenis_biaya")
                    ->where("b.tahun_ajaran",$req->du_tahun_ajaran)
                    ->where("b.satuan","Daftar Ulang")
                    ->where("b.bulan",7)
                    ->where("jurnals.type",1)
                    ->whereRaw($where)
                    ->orderBy('prioritas')
                    ->select("j.*","b.prioritas")
                    ->distinct()
                    ->get();

                @endphp
                @foreach ($jenisBiaya->unique() as $key => $d)

                @php
                $jumlah =  Modules\Keuangan\Entities\Jurnal::
                    leftJoin("biaya_siswas as b","b.id","jurnals.biaya_id")
                    ->leftJoin("siswas as s","s.nis","jurnals.nis")
                    ->where("jurnals.id_jenis_biaya",$d->id)
                    ->where("b.tahun_ajaran",$req->du_tahun_ajaran)
                    ->where("b.satuan","Daftar Ulang")
                    ->where("b.bulan",7)
                    ->where("jurnals.type",1)
                    ->whereRaw($where)
                    ->select("jurnals.*")
                    ->sum("jurnals.jumlah");
            
                    $totalPendapatan += $jumlah; 
                @endphp
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>
                        
                        <a 
                        style="@if($req->print) color:black; @endif text-decoration:none"
                    href="/laporan-daftar-ulang-per-jenis/{{$d->id}}?data={{json_encode($req->all())}}&jumlah={{$jumlah}}" target="_blank">
                        {{ $d->nama }}
                        </a>
                        </td>
                    <td style="text-align:right;border-right:1px solid #ccc"> 
                        {{ rupiah($jumlah) }}
                    </td>
                </tr>

                @endforeach
                <tr>
                    <th> </th>
                    <th>
                        Total Pendapatan
                    </th>
                    <th style="text-align:right">
                        {{ rupiah($totalPendapatan) }}
                    </th>
                </tr>

            </tbody>
            </table>

@endsection

@section('js-after')
@endsection