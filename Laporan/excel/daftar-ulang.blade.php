
@php

$tgl  =     explode(" ",$req->filter);

$tgl_awal = $tgl[0];
$tgl_akhir = $tgl[2];


if($req->format_laporan == 'rentang'){
    if($req->bulan == 0){
        $pr = " Tahun " .$req->tahun_ajaran;

        $where = " YEAR(jurnals.tgl_bayar) = $req->tahun_ajaran";

    }else{
        $pr = bulan_text($req->bulan) ." ". $req->tahun_ajaran;
        
        $where = " YEAR(jurnals.tgl_bayar) = $req->tahun_ajaran AND MONTH(jurnals.tgl_bayar) = $req->bulan ";

    }

}else if($req->format_laporan == 'periode'){
    $pr = tgl_indo($tgl_awal) ." - ". tgl_indo($tgl_akhir);

    $where = " jurnals.tgl_bayar >= '$tgl_awal' AND jurnals.tgl_bayar <= '$tgl_akhir'";

}else{
    $pr = "Seluruh";

    $where = "jurnals.tgl_bayar";
}

if($req->tingkat !== 'SEMUA'){
    $where .= " AND s.jenjang = '$req->tingkat' ";
}

if($req->gender !== 'SEMUA'){
    $where .= " AND s.jk = '$req->gender' ";
}

if($req->ref > 0){
    $where .= " AND jurnals.ref = '$req->ref' ";
}

@endphp
@extends('layouts.excel')

@section('content')
<table>
        <tr>
            <td colspan="3" style="text-align:center;height:50px">
            {{-- <img class="logo" src="{{ url('/logo-pesantren.png') }}" alt="logo"> --}}
            <b style="font-size:110%">PONPES TERPADU  RIYADLUL ULUM WADDA'WAH</b>
            <br>
            <small >Condong, Ciberem, Kota Tasikmalaya, 
                <br>
                Telepon (0265) 7077821</small>
            <div style="height:3px;border-top:2px solid #ccc;border-bottom:2px solid #ccc;margin:10px 50px;margin-top:25px">
            </div>
        </td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td colspan="3" style="text-align:center">
                   <b> LAPORAN DAFTAR ULANG 

                    @if($req->tingkat !== 'SEMUA')
                        {{ $req->tingkat }}
                    @endif

                    @if($req->tingkat !== 'SEMUA')
                    ({{ ($req->gender == 'L') ? '(P) Putra' : '(Pi) Putri' }})
                    @endif
                   </b>
                   <br>
                   <b> Tahun Ajaran {{ $req->du_tahun_ajaran }} / {{ $req->du_tahun_ajaran+1 }}</b>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align:center">
                <b>Periode {{ $pr }}</b>
            </td>
        </tr>
        <tr>
                <td colspan="3"></td>
            </tr>
        <tr>
            <th style="text-align:center" colspan="3"><b>Pendapatan</b></th>
        </tr>
             
        <tr>
            <th style="width:5px" >No</th>
            <th style="width:30px;">Keterangan</th>
            <th style="width:30px;text-align:right;">Jumlah</th>
        </tr>
        @php
            $totalPendapatan = 0;
        @endphp
        @foreach (Modules\Keuangan\Entities\JenisBiaya::orderBy("nama","ASC")->get() as $key => $d)
            
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $d->nama }}</td>
            <td > 

                @php

                          $jumlah =  Modules\Keuangan\Entities\Jurnal::
                            join("biaya_siswas as b","b.id","jurnals.biaya_id")
                            ->where("jurnals.id_jenis_biaya",$d->id)
                            ->where("b.tahun_ajaran",$req->du_tahun_ajaran)
                            ->where("b.satuan","Daftar Ulang")
                            ->where("b.bulan",7)
                            ->where("jurnals.type",1)
                            ->whereRaw($where)
                            ->select("jurnals.*")
                            ->sum("jurnals.jumlah");
            
                    $totalPendapatan += $jumlah; 
                @endphp
                {{ $jumlah }}
            </td>
        </tr>

        @endforeach
        <tr>
            <th> </th>
            <th>
                <b>Total Pendapatan</b>
            </th>
            <th style="text-align:right">
               <b> {{ $totalPendapatan }}</b>
            </th>
        </tr>


</table>

                    @endsection