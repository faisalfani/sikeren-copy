@extends('layouts.excel')

@php

$tgl  =     explode(" ",$req->filter);

$tgl_awal = $tgl[0];
$tgl_akhir = $tgl[2];


if($req->format_laporan == 'rentang'){
    if($req->bulan == 0){
        $pr = " Tahun " .$req->tahun_ajaran;

        $where = " YEAR(tgl_bayar) = $req->tahun_ajaran";

    }else{
        $pr = bulan_text($req->bulan) ." ". $req->tahun_ajaran;
        
        $where = " YEAR(tgl_bayar) = $req->tahun_ajaran AND MONTH(tgl_bayar) = $req->bulan ";

    }

}else if($req->format_laporan == 'periode'){

    $pr = tgl_indo($tgl_awal) ." - ". tgl_indo($tgl_akhir);

    if($tgl_awal == $tgl_akhir){
        $pr = tgl_indo($tgl_awal);
    }

    $where = " tgl_bayar >= '$tgl_awal' AND tgl_bayar <= '$tgl_akhir'";

}else{
    $pr = "Seluruh";

    $where = "tgl_bayar";
}


@endphp

@section('content')
    

            <table >
                <thead>
                <tr>
                        <td colspan="3" style="text-align:center;height:50px">
                        {{-- <img class="logo" src="{{ url('/logo-pesantren.png') }}" alt="logo"> --}}
                        <b style="font-size:110%">PONPES TERPADU  RIYADLUL ULUM WADDA'WAH</b>
                        <br>
                        <small >Condong, Ciberem, Kota Tasikmalaya, 
                            <br>
                            Telepon (0265) 7077821</small>
                        <div style="height:3px;border-top:2px solid #ccc;border-bottom:2px solid #ccc;margin:10px 50px;margin-top:25px">
                        </div>
                    </td>
                    </tr>
                    <tr>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align:center">
                                <b> LAPORAN UANG SETORAN </b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align:center">
                            <b>Periode {{ $pr }}</b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"></td>
                    </tr>
                
                            
                    <tr>
                        <th style="width:5px" ><b>No</b></th>
                        <th style="width:30px;"><b>Keterangan</b></th>
                        <th style="width:30px;text-align:right;"><b>Jumlah</b></th>
                    </tr>
                </thead>
                <tbody style="text-align:left;">
               
                @php
                    $totalPendapatan = 0;
                @endphp
                @foreach (Modules\Keuangan\Entities\JenisBiaya::orderBy("nama","ASC")->get() as $key => $d)
                    
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $d->nama }}</td>
                    <td style="text-align:right;border-right:1px solid #ccc"> 

                        @php
    
                            $jumlah =  Modules\Keuangan\Entities\Jurnal::
                            where("id_jenis_biaya",$d->id)
                            ->where("type",1)
                            ->whereRaw($where)
                            ->sum("jumlah");
                    
                            $totalPendapatan += $jumlah; 
                        @endphp
                        {{ $jumlah }}
                    </td>
                </tr>

                @endforeach
                <tr>
                    <th></th>
                    <th>
                        Total Pendapatan
                    </th>
                    <th style="text-align:right">
                        {{ $totalPendapatan }}
                    </th>
                </tr>

            </tbody>
            </table>


@endsection