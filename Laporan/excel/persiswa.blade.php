
@extends('layouts.excel')

@section('content')

@php


$siswa = Modules\Siswa\Entities\Siswa::where("nis",$req->nis)->first();

$tgl  =     explode(" ",$req->filter);
$tgl_awal = $tgl[0];
$tgl_akhir = $tgl[2];


if($req->format_laporan == 'rentang'){
    if($req->bulan == 0){
        $pr = " Tahun " .$req->tahun_ajaran;

        $tgl_awal = date("$req->tahun_ajaran-01-01");
        $tgl_akhir = date("$req->tahun_ajaran-12-31");

        $where = "created_at >= '$tgl_awal' AND created_at <= '$tgl_akhir'";

    }else{
        $pr = bulan_text($req->bulan) ." ". $req->tahun_ajaran;

        $tgl_awal = date("$req->tahun_ajaran-$req->bulan-01");
        $tgl_akhir = date("$req->tahun_ajaran-$req->bulan-31");

        $where = "created_at >= '$tgl_awal' AND created_at <= '$tgl_akhir'";

   
    }

}else if($req->format_laporan == 'periode'){
    $pr = tgl_indo($tgl_awal) ." - ". tgl_indo($tgl_akhir);
    $where = "created_at >= '$tgl_awal' AND created_at <= '$tgl_akhir'";

}else{
    
    $pr = "Semua";
    $where = "id";

}


@endphp


            @if ($req->type == 'tagihan')
                @php
                    $dataa = Modules\Keuangan\Entities\BiayaSiswa::where("nis",$req->nis)
                    ->whereRaw($where)
                    ->whereRaw("nominal != dibayar");

                    $data = $dataa->get();

                    $totalTagihan = $dataa->sum("nominal");
                    $totalDibayar = $dataa->sum("dibayar");
                @endphp
            @else 
                @php
                    $dataa = Modules\Keuangan\Entities\BiayaSiswa::where("nis",$req->nis)
                    ->whereRaw($where)
                    ->whereRaw("dibayar > 0");

                    $data = $dataa->get();
                    $totalTagihan = $dataa->sum("nominal");
                    $totalDibayar = $dataa->sum("dibayar");


                @endphp
            @endif
          
            <table style="font-size:14px;text-align:left;">
            <thead>
                    <tr>
                            <td colspan="7" style="text-align:center;height:50px">
                                <b style="font-size:110%">PONPES TERPADU  RIYADLUL ULUM WADDA'WAH</b>
                                <br>
                                <small >Condong, Ciberem, Kota Tasikmalaya, 
                                    <br>
                                    Telepon (0265) 7077821</small>
                                <div style="height:3px;border-top:2px solid #ccc;border-bottom:2px solid #ccc;margin:10px 50px;margin-top:25px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7"></td>
                        </tr>
                        <tr>
                            <td colspan="7" style="text-align:center">
                                    <b> 
                                    @if ($req->type == 'tagihan')
            
                                    LAPORAN TAGIHAN SISWA
                                    @else 
                        
                                    LAPORAN PEMBAYARAN SISWA
                                    @endif
                            
                                    </b>
                            </td>
                        </tr>
                        <tr>
                                <td colspan="7"></td>
                            </tr>
                        <tr>
                            <td colspan="7" style="text-align:center">
                                <b>Periode {{ $pr }}</b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7"></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align:right;">
                                NIS
                            </td>
                            <td colspan="2" style="">
                                : <b>{{ $siswa->nis}}</b>
                            </td>
                              
                            <td  style="">
                                Tagihan 
                            </td>
                            <td colspan="2"  style="">
                                : <b>{{ rupiah($totalTagihan)}}</b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align:right;">
                                Nama
                            </td>
                            <td colspan="2" style="">
                                : <b>{{ $siswa->nama}}</b>
                            </td>
                            <td  style="">
                                    Dibayar 
                            </td>
                            <td colspan="2"  style="" >
                                : <b>{{ rupiah($totalDibayar)}}</b>
                            </td>
                                
                    </tr>
                            
                    <tr>
                        <td colspan="2" style="text-align:right;">
                                Kelas
                            </td>
                            <td colspan="2"  style="">
                                : <b>{{ $siswa->kelas}}</b>
                            </td>
                        <td  style="">
                            Tunggakan 
                        </td>
                        <td colspan="2"  style="" >
                            : <b>{{ rupiah($totalTagihan-$totalDibayar)}}</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:16px" colspan="7"></td>
                    </tr>
                </thead>
                <tbody style="text-align:left;font-size: 12px">
                <tr  style="text-align:center;">
                    <th style="width:5px">No</th>
                    <th style="width:25px">Nama Biaya</th>
                    <th style="width:10px">Jenis</th>
                    <th style="width:20px">Bulan</th>
                    <th style="width:20px">Tagihan</th>
                    <th style="width:20px">Dibayar</th>
                    <th style="width:20px">Tunggakan</th>
                </tr>
                @foreach ($data as $key => $item)
                    <tr>
                        <td>
                            {{ $key+1}}
                        </td>
                        <td>
                            {{ $item->nama}}
                        </td>
                        <td>
                            {{ $item->satuan }}
                        </td>
                        <td>
                            {{ bulan_text($item->bulan) }} {{ $item->tahun_id}}
                        </td>
                        <td>
                            {{ rupiah($item->nominal) }}
                        </td>
                        <td>
                            {{ rupiah($item->dibayar) }}
                        </td>
                        <td>
                            {{ rupiah($item->nominal-$item->dibayar) }}
                        </td>
                    </tr>
                @endforeach

            </tbody>
            </table>


@endsection