@extends('layouts.excel')
@section('content')

            <table >
                    <thead>  
                        <thead>
                            <tr>
                                    <td colspan="5" style="text-align:center;height:50px">
                                    {{-- <img class="logo" src="{{ url('/logo-pesantren.png') }}" alt="logo"> --}}
                                    <b style="font-size:110%">PONPES TERPADU  RIYADLUL ULUM WADDA'WAH</b>
                                    <br>
                                    <small >Condong, Ciberem, Kota Tasikmalaya, 
                                        <br>
                                        Telepon (0265) 7077821</small>
                                    <div style="height:3px;border-top:2px solid #ccc;border-bottom:2px solid #ccc;margin:10px 50px;margin-top:25px">
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <td colspan="5"></td>
                                </tr>
                                <tr>
                                    <td colspan="5" style="text-align:center">
                                            <b> LAPORAN DAFTAR ULANG</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" style="text-align:center">
                                            <b>Tahun Ajaran {{ $req->du_tahun_ajaran}} / {{ $req->du_tahun_ajaran+1}} </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" style="text-align:center">
                                        <b>{{ $kelas->nama }}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" style="text-align:center">
                                        @if($req->gender !== 'SEMUA')
                                            ({{ ($req->gender == 'L') ? '(P) Putra' : '(Pi) Putri' }})
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5"></td>
                                </tr>
                                <tr>
                                  <th style="width:5px" ><b>No</b></th>
                                  <th style="width:30px;"><b>Keterangan</b></th>
                                  <th style="width:30px;">Nominal</th>
                                  <th style="width:30px;">Bayar</th>
                                  <th  style="width:30px;text-align:right;">Sisa Tagihan</th>
                                </tr>
                    </thead>
                    <tbody>

                        @php
                            $jmlNominal = 0;
                            $jmlBayar = 0;
                            $jmlSisa = 0;
                        @endphp
                      @foreach ($res as $key => $s)
  
                            @php
                              $jmlNominal += $s['nominal'];
                              $jmlBayar += $s['dibayar'];
                              $jmlSisa += $s['tagihan'];
                          @endphp
                          <tr>
                            <td> {{ $key+1 }}</td>
                            <td> {{ $s['nama'] }}</td>
                            <td> {{ $s['nominal'] }}</td>
                            <td> {{ $s['dibayar'] }}</td>
                            <td> {{ $s['tagihan'] }}</td>
                           
                          </tr>
                      @endforeach
                      <tr>
                        <td></td>
                        <td>Jumlah</td>
                        <td>{{$jmlNominal}}</td>
                        <td>{{$jmlBayar}}</td>
                        <td>{{$jmlSisa}}</td>
                      </tr>
                      </tbody>
                  </table>


@endsection