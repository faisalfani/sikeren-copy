@extends('layouts.app')

@section('title')
      Laporan
@endsection
@section('css-after')
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="../../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
 <!-- Select2 -->
 <link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">
 <link rel="stylesheet" href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

<link rel="stylesheet" href="../../plugins/daterangepicker/daterangepicker.css">
<style>
table{
    background: #fff;
    border-radius: 10px;
}
table tr td,table tr th{
    padding: 2px 10px;
}
</style>
@endsection
@section('content')


@php
    
    if($req->start AND $req->end){
        $start = date($req->start);
        $end = date($req->end);
        $where = "BETWEEN ".$start." AND ".$end;

    }else{
        $start = date('Y-m-d', strtotime("-1 month"));
        $end = date('Y-m-d');
        $where = "BETWEEN ".$start." AND ".$end;    
    }

@endphp
<div class="container bg-white py-4">

<h3 class="text-center">Laporan Per Siswa</h3>                                 
<form id="form-laporan" class="row" action="/laporan-persiswa" >
  <div class="col-md-6 offset-lg-3">
      <div class="row">

          <div class="col-md-12">
           
              <div class="form-group" >
                  <label>NIS / Nama</label>
                  <select 
                  @if ($req->nis)
                      value="{{$req->nis}}"
                  @endif
                   id="nis" required name="nis" class="cari form-control" >
                    @if ($req->nis)
                      <option value="{{$req->nis}}">{{$req->nis}}</option>
                  @endif
                  </select>
              </div>

              <div class="form-group">
                    <div class="icheck-success d-inline">
                      <input @if ($req->type == 'tagihan')
                      checked
                      @endif value="tagihan" type="radio" name="type" checked id="radioSuccess1">
                      <label for="radioSuccess1">
                        Tagihan
                      </label>
                    </div>
                    <div class="icheck-success d-inline">
                      <input @if ($req->type == 'pembayaran')
                      checked
                      @endif type="radio" name="type" value="pembayaran" id="radioSuccess2">
                      <label for="radioSuccess2">
                        Pembayaran
                      </label>
                    </div>

                </div>

                <div class="form-group">
                    <label>Pilih Format Laporan</label>
                    <select name="format_laporan" required id="format-laporan" class="form-control select2" style="width: 100%;">
                        <option value="">
                            Pilih
                        </option>
                        <option selected value="semua">
                            Semua
                        </option>
                        <option value="rentang">
                          Rentang (Bulan/Tahun)
                        </option>
                        <option value="periode">
                          Periode ( Tanggal )
                        </option>
                    </select>
                  </div>
                
    
              <div class="row" id="rentang">

          <div class="col-md-6">
           
              <div class="form-group">
                  <label>Tahun</label>
                  @php
                      $y = Date('Y')-1;
                  @endphp
                  <select  name="tahun_ajaran" id="tahun_ajaran" class="form-control select2" style="width: 100%;">
                   
                    @for ($i = 0; $i < 3; $i++)
                      <option  @if ($y+$i == Date('Y')) selected @endif value="{{$y+$i}}" value="{{$y+$i}}">Tahun {{ $y+$i }}</option>
                    @endfor
                  </select>
                </div>
              </div>
    
    
              <div class="col-md-6">
               
                <div class="form-group">
                    <label>Bulan</label>
                    <select  name="bulan" id="bulan" class="form-control select2" style="width: 100%;">
                        <option value="">
                            Pilih
                        </option>
                      @php
                          $bulan = array (
                              'Januari',
                              'Februari',
                              'Maret',
                              'April',
                              'Mei',
                              'Juni',
                              'Juli',
                              'Agustus',
                              'September',
                              'Oktober',
                              'November',
                              'Desember'
                            );
                      @endphp 
    
                      <option
                          value="0"
                      >Semua Bulan</option>
                      @foreach ($bulan as $key => $item)
                          <option
                          @if (Date('m') == $key+1)
                            selected
                        @endif
                          value="{{$key+1}}"
                          >{{ $item }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                

      </div>
    </div>
          <div class="col-md-12" id="periode">
              <div class="form-group">
                 


                    <label>
                
                    Periode          
                        </label>
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="far fa-calendar-alt"></i>
                              </span>
                            </div>
                              <input required name="filter" type="text" class="form-control float-right" id="filterdate">
                          </div>

                          
                </div>
          </div>

          <div class="col-md-12 mt-2">
              <button class="btn btn-primary mt-4 btn-block">
                <i class="fa fa-print"></i> Lihat
              </button>
          </div>
         
      </div>
  </div>
</form>

</div>
@endsection

@section('js-after')

    <!-- InputMask -->
<script src="../../plugins/inputmask/jquery.inputmask.bundle.js"></script>
<script src="../../plugins/moment/moment.min.js"></script>
<!-- date-range-picker -->
<script src="../../plugins/daterangepicker/daterangepicker.js"></script>
<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>

<script>



$('#nis').select2({
    theme: 'bootstrap4',
    placeholder: 'Cari...',
    ajax: {
      url: '/searchNis',
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        console.log(data);
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.nama,
              id: item.nis
            }
          })
        };
      },
      cache: true
    }
  });





var start = '{{ $start }}';
var end = '{{ $end }}';

$("#rentang").hide();
$("#periode").hide();


$("#format-laporan").change(function(){
  if($(this).val() == 'rentang'){
    $("#rentang").show();
    $("#periode").hide();
  }else if($(this).val() == 'periode'){
    $("#rentang").hide();
    $("#periode").show();
  }else{
    $("#rentang").hide();
    $("#periode").hide();
  }
})

$('#filterdate').daterangepicker({
    startDate: start,
    endDate: end,
    locale: {
        format: 'YYYY-MM-D'
    },
    ranges: {
       'Today': [moment(), moment()],
       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
       'Last 7 Days': [moment().subtract(6, 'days'), moment()],
       'Last 30 Days': [moment().subtract(29, 'days'), moment()],
       'This Month': [moment().startOf('month'), moment().endOf('month')],
       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
}, function(start,end){
      // location.href = "?start="+start.format('YYYY-MM-D') +"&end="+end.format('YYYY-MM-D');     
});

    

$("#form-laporan").submit(function(e){
    e.preventDefault();
    var link  = $(this).attr("action") +"?"+ $(this).serialize()
    window.open(link, "_blank"); 
  });

</script>
@endsection