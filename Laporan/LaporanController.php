<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use Modules\Master\Entities\Kelas;
use Modules\Siswa\Entities\Siswa;
use Modules\Keuangan\Entities\BiayaSiswa;
use Modules\Keuangan\Entities\JenisBiaya;
use DB;
use Excel;
use App\Exports\LkExport;
use App\Exports\PerJenisExport;
use App\Exports\UangSetoranExport;
use App\Exports\PerKelasExport;
use App\Exports\DUPerKelasExport;
use App\Exports\JurnalExport;
use App\Exports\PerSiswaExport;
use App\Exports\Tabungan;
use App\Exports\DaftarUlang;
use App\Exports\DaftarUlangPerJenis;

class LaporanController extends Controller
{
    //
    public function index(Request $req){

        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.lk',compact("req"));
            return $pdf->stream();
            
        }else if($req->print == 'excel'){
           
            $nama_file = 'laporan_LK_'.date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new LkExport($req), $nama_file);
            
        }else{
           
            return view("pdf.lk",compact("req"));
        }
    }
    
    function cetakperJenis(Request $req){
        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.laporan-perjenis',compact("req"));
            return $pdf->stream();
            
        }else if($req->print == 'excel'){
            $nama_file = 'laporan_PerJenis_'.date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new PerJenisExport($req), $nama_file);
            
        }else{
            return view("pdf.laporan-perjenis",compact("req"));
        }
    }

    
    public function cetakperKelas(Request $req,$id){

        $kelas = Kelas::find($id);
        $siswa = Siswa::where("id_kelas",$id)->get();
        $res = array();
        foreach($siswa as $s){
            $tagihan = BiayaSiswa::where("nis",$s->nis)
                ->where("nominal","!=","dibayar")
                ->get();
            $sisa = 0;

            foreach($tagihan as $t){
                $sisa += $t->nominal - $t->dibayar;    
            }
            $data = [
                    "nis" => $s->nis, 
                    "nama" => $s->nama,
                "jk" => $s->jk,
                    "tagihan" => $sisa,
                ];
            array_push($res,$data);
        }

        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.laporan-perkelas',compact('req','res','kelas'));
            return $pdf->stream();
            
        }else if($req->print == 'excel'){
          
            $nama_file = 'laporan_PerKelas_'.date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new PerKelasExport($req,$res,$kelas), $nama_file);
            
        }else{
            return view('pdf.laporan-perkelas',compact('req','res','kelas'));
        }
       
        
     }
    public function lk(Request $req){
        return view("laporan.lk",compact('req'));
    }

    public function perJenis(Request $req){
        return view("laporan.perjenis",compact('req'));
    }

  

    public function jurnal(Request $req){
        return view("laporan.jurnal",compact('req'));
    }

       //
    public function pdfjurnal(Request $req){

        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.laporan-jurnal',compact("req"));
            return $pdf->stream();
        }else if($req->print == 'excel'){
            $nama_file = 'laporan_Jurnal_'.date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new JurnalExport($req), $nama_file);
            
        }else{
            return view("pdf.laporan-jurnal",compact("req"));
        }
    }
    

    // Pemasukan
       
        public function pemasukan(Request $req){
            return view("laporan.pemasukan",compact('req'));
        }
       public function pdfpemasukan(Request $req){

        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.pemasukan',compact("req"));
            return $pdf->stream();
        }else if($req->print == 'excel'){
            
            $nama_file = 'Uang_Setoran_'.date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new UangSetoranExport($req), $nama_file);
            
        }
        else{
            return view("pdf.pemasukan",compact("req"));
        }
    }

     // Persiswa
       
    public function persiswa(Request $req){
        return view("laporan.persiswa",compact('req'));
    }
    public function pdfpersiswa(Request $req){

        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.persiswa',compact("req"));
            return $pdf->stream();
            
        }else if($req->print == 'excel'){
            
            $nama_file = 'Laporan_PerSIswa_'.date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new PerSiswaExport($req), $nama_file);
            
        }
        else{
            return view("pdf.persiswa",compact("req"));
        }
    }


    public function pdftabungan(Request $req,$nis,$start,$end){

        $siswa = Siswa::where("nis",$nis)->first();
        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.tabungan',compact("req","nis","start","end","siswa"));
            return $pdf->stream();
            
        }else if($req->print == 'excel'){
            
            $nama_file = 'Laporan_Tabungan_Siswa_'. $nis . date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new Tabungan($req,$nis,$start,$end,$siswa), $nama_file);
            
        }
        else{
            return view("pdf.tabungan",compact("req","nis","start","end","siswa"));
        }
    }


    public function DaftarUlang(Request $req){
        return view("laporan.daftar-ulang",compact('req'));
    }
    public function CetakDaftarUlang(Request $req){
        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.laporan-daftar-ulang',compact("req"));
            return $pdf->stream();
        }else if($req->print == 'excel'){
            $nama_file = 'laporan_daftar_ulang_'.date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new DaftarUlang($req), $nama_file);
        }else{
            return view("pdf.laporan-daftar-ulang",compact("req"));
        }
    }

    public function CetakDaftarUlangPerJenis(Request $req,$id){

        $req1 = json_decode($req->data);

        $jenis = JenisBiaya::find($id);

        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.laporan-daftar-ulang-per-jenis',compact("req","req1","jenis"));
            return $pdf->stream();
        }else if($req->print == 'excel'){
            $nama_file = 'laporan_daftar_ulang_per_jenis'.date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new DaftarUlangPerJenis($req,$req1,$jenis), $nama_file);
        }else{
            return view("pdf.laporan-daftar-ulang-per-jenis",compact("req","req1",'jenis'));
        }
    }

    public function DUKelas(Request $req){
        return view("laporan.daftar-ulang-kelas",compact('req'));
    }

    public function cetakDUKelas(Request $req,$id){

        
            $kelas = Kelas::find($id);
            
            
            if($req->gender !== 'SEMUA'){
                $siswa = Siswa::
                where("id_kelas",$id)
                ->where("jk",$req->gender)
                ->orderBy("nama","ASC")
                ->get();
            }else{
                $siswa = Siswa::
                    where("id_kelas",$id)
                    ->orderBy("nama","ASC")
                    ->get();
            }
            $res = array();
            foreach($siswa as $s){
                $tagihan = BiayaSiswa::
                    where("nis",$s->nis)
                    ->where("bulan",7)
                    ->where("satuan","Daftar Ulang")
                    ->where("tahun_ajaran",$req->du_tahun_ajaran)
                    ->get();
                $sisa = 0;
                $dibayar = 0;
                $nominal = 0;

                foreach($tagihan as $t){
                    $sisa += $t->nominal - $t->dibayar;    
                    $dibayar += $t->dibayar;    
                    $nominal += $t->nominal;    
                }
                $data = [
                        "nis" => $s->nis, 
                        "nama" => $s->nama,
                        "jk" => $s->jk,
                        "tagihan" => $sisa,
                        "dibayar" => $dibayar,
                        "nominal" => $nominal,
                    ];
                array_push($res,$data);
            }

            if($req->print == 'true'){
                $pdf = PDF::loadView('pdf.du-kelas',compact('req','res','kelas'));
                return $pdf->stream();
                
            }else if($req->print == 'excel'){
            
                $nama_file = 'laporan_DU_Kelas_'.date('Y-m-d_H-i-s').'.xlsx';
                return Excel::download(new DUPerKelasExport($req,$res,$kelas), $nama_file);
                
            }else{
                return view('pdf.du-kelas',compact('req','res','kelas'));
            }
    }
}
