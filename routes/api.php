<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', 'Auth\LoginController@authMiddleware');

Route::get('/{nis}/{pin}', 'aplikasiController@index');
Route::get('/{nis}/{pin}/tabungan', 'aplikasiController@tabungan');
Route::get('/{nis}/{pin}/pembayaran', 'aplikasiController@pembayaran');


Route::post('/updateAvatar/{nis}/{pin}', 'aplikasiController@updateAvatar');
Route::post('/updatePin/{nis}/{pin}', 'aplikasiController@updatePin');

Route::post('/pegawai/login', 'LoginPegawai@login');
Route::group([

  'middleware' => ['assign.guard:pegawai'],
  'prefix' => 'pegawai'

], function () {

  
  Route::post('user', 'PegawaiAuthController@me');
  Route::post('user/update', 'PegawaiAuthController@updatePegawai');
  Route::post('home', 'PegawaiAuthController@home');
  Route::post('pendapatan', 'PegawaiAuthController@pendapatanBulanIni');
  Route::post('sejarah', 'PegawaiAuthController@sejarahPenggajian');
  Route::post('diterima', 'PegawaiAuthController@statusDiterima');
  Route::post('foto', 'PegawaiAuthController@uploadFoto');
  Route::post('logout', 'PegawaiAuthController@logout');

});

