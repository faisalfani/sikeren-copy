<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Auth::routes();

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
  ]);


// Route::get('/aplikasi-siswa', function(){
//     return view('siswa');
// });

Route::get('/tabletSiswa', 'tabletSiswaController@index');
Route::get("/searchNisSiswa",'tabletSiswaController@searchNis');

Route::get('chart-line', 'HomeController@chartLine');
Route::get('chart-line-ajax', 'HomeController@chartLineAjax');

Route::middleware("auth")->group(function(){

    Route::get('/home', 'HomeController@baseHome');
    Route::get('/', 'HomeController@baseHome');

    Route::get('/profile', 'HomeController@index');

   

    Route::post('/edit-profile', 'HomeController@editProfile');



    
    Route::get('/laporan', 'LaporanController@index');
    Route::get('/laporan/lk', 'LaporanController@lk');


    Route::get('/laporan/per-jenis', 'LaporanController@perJenis');
    Route::get('/laporan-perjenis', 'LaporanController@cetakperJenis');


    Route::get('/laporan-perkelas/{id}', 'LaporanController@cetakperKelas');
    Route::get('/laporan-tagihan-siswa-lulus', 'LaporanController@cetakTagihanSiswaLulus');
    Route::get('/laporan-saldo-tabungan-siswa', 'LaporanController@saldoTabunganSiswa');
    

    
    Route::get('/laporan/jurnal', 'LaporanController@jurnal');
    Route::get('/laporan-jurnal', 'LaporanController@pdfjurnal');

    Route::get('/laporan/pemasukan', 'LaporanController@pemasukan');
    Route::get('/laporan-pemasukan', 'LaporanController@pdfpemasukan');


    Route::get('/laporan/tagihan', 'LaporanController@setoran');
    Route::get('/laporan-tagihan', 'LaporanController@pdfsetoran');
    Route::get('/data-kelas/{id}', 'LaporanController@dataKelas');

    Route::get('/laporan/persiswa', 'LaporanController@persiswa');
    Route::get('/laporan-persiswa', 'LaporanController@pdfpersiswa');


    Route::get('/laporan/daftar-ulang', 'LaporanController@DaftarUlang');
    Route::get('/laporan-daftar-ulang', 'LaporanController@CetakDaftarUlang');
    Route::get('/laporan-daftar-ulang-per-jenis/{id}','LaporanController@CetakDaftarUlangPerJenis');

    Route::get('laporan/daftar-ulang/kelas', 'LaporanController@DUKelas');

    Route::get('/laporan-daftar-ulang/kelas/{id}', 'LaporanController@cetakDUKelas');

    Route::get('/cetak-tabungan/{nis}/{start}/{end}', 'LaporanController@pdftabungan');


    Route::post('/import-tagihan', 'HomeController@importTagihan');

    Route::get('/penggajian/resume/{bulan}/{tahun}', 'LaporanController@resume')->name("penggajian-resume");
    Route::get('/penggajian/resume-id/{bulan}/{tahun}/{id}', 'LaporanController@resumeId')->name("penggajian-resume-id");
    Route::get('/penggajian/laporan/{bulan}/{tahun}', 'LaporanController@laporanPenggajian');
    Route::get('/penggajian/payroll/{bulan}/{tahun}', 'LaporanController@payrollPenggajian');
    Route::get('/penggajian/payroll2/{bulan}/{tahun}', 'LaporanController@payrollPenggajianLama');
        
});
