var staticCacheName = "pwa-sikern-v2" + new Date().getTime();
var filesToCache = [
  "/offline",
  "/dist/css/adminlte.min.css",
  "/plugins/fontawesome-free/css/all.min.css",
  "/plugins/icheck-bootstrap/icheck-bootstrap.min.css",
  "/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css",
  "/plugins/select2/css/select2.min.css",
  "/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css",
  "/dist/js/adminlte.min.js",
  "/plugins/jquery/jquery.min.js",
  "/plugins/bootstrap/js/bootstrap.bundle.min.js",
  "/plugins/select2/js/select2.full.min.js",
  "/plugins/datatables/jquery.dataTables.min.js",
  "/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js",
  // '/images/icons/icon-72x72.png',
  "/images/icons/icon-96x96.png",
  "/images/icons/icon-128x128.png",
  "/images/icons/icon-144x144.png",
  "/images/icons/icon-152x152.png",
  "/images/icons/icon-192x192.png",
  "/images/icons/icon-384x384.png",
  "/images/icons/icon-512x512.png",
];

// Cache on install
self.addEventListener("install", (event) => {
  this.skipWaiting();
  event.waitUntil(
    caches.open(staticCacheName).then((cache) => {
      return cache.addAll(filesToCache);
    })
  );
});

// Clear cache on activate
self.addEventListener("activate", (event) => {
  event.waitUntil(
    caches.keys().then((cacheNames) => {
      return Promise.all(
        cacheNames
          .filter((cacheName) => cacheName.startsWith("pwa-"))
          .filter((cacheName) => cacheName !== staticCacheName)
          .map((cacheName) => caches.delete(cacheName))
      );
    })
  );
});

// Serve from Cache
self.addEventListener("fetch", (event) => {
  event.respondWith(
    caches
      .match(event.request)
      .then((response) => {
        return response || fetch(event.request);
      })
      .catch(() => {
        return caches.match("offline");
      })
  );
});
