import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        data: localStorage.getItem('data')
    },
    mutations: {
      change(state, data) {
        let save = JSON.stringify(data)
        state.data = save
        localStorage.setItem('data', save);
      }
    },
    getters: {
      data: () => {
        return JSON.parse(localStorage.getItem('data'))
      }
    }
})