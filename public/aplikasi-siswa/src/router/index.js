import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Tagihan from '../views/Tagihan.vue'
import History from '../views/History.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'login',
    component: Login
  },{
    path: '/tagihan',
    name: 'tagihan',
    component: Tagihan
  },{
    path: '/history-pembayaran',
    name: 'hostory',
    component: History
  }
]

const router = new VueRouter({
  routes
})

export default router
