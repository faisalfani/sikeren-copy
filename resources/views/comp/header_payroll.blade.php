<br>
<table style="width: 100%;margin:0;padding:0;">
    <tr>
        <td style="padding:0;margin:0;border: none;width:20%;">
            <img class="logo"
                src="https://ponpescondong.com/logo-pesantren.png"
                alt="logo">
        </td>
        <td style="padding:0;margin:0;border: none;">
            <br>
            <p style="font-weight:500;text-transform:uppercase;font-size:18px;margin:0;padding:0;">DATA IHSAN MTS/MA
                TERPADU RIYADLUL ULUM WADDA'WAH CONDONG
                <br>
                BULAN {{ bulan_text($req->bulan) }} {{ $req->tahun }}
            </p>
        </td>
    </tr>
</table>
<div style="height:3px;border-top:2px solid #ccc;border-bottom:2px solid #ccc;margin:10px 50px;margin-top:25px">
</div>
