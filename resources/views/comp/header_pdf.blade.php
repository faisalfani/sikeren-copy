@php
$info = Modules\Core\Entities\CoreInfo::first();
@endphp

@php

if($req->print == 'true'){

@endphp
        <style>
            .kecilin {
                font-size: 12px !important;
            }
            .printin {
                margin-top:-70px;
            }
            @page { margin-left: 0px; margin-right: 0; }
            body { margin-left: 0px; margin-right: 0; }
            .page_break { page-break-before: always; }
        </style>
@php
}
@endphp

<nav class="header printin">
    <table style="width:100%;border:none;padding:0;margin:0;">
        <tr style="border:none;">
            @if ($info->left_logo_status)
                <td style="border:none;width:20%;">
                    <img src="{{ $req->print? $_SERVER['DOCUMENT_ROOT'] . '/images/custom/' . $info->left_logo: url('/images/custom') . '/' . $info->left_logo }}"
                        alt="logo Kiri"
                        style="width:80px;">
                </td>
            @endif

            <td style="border:none;text-align:center">

                <span style="font-size:24px">{{ $info->nama }}<span>
                        <br>
                        <div style="margin-top:8px;font-size:12px;">{{ $info->alamat }}

                        </div>
                        @if ($info->no_telp)
                            <div style="margin-top:4px;font-size:12px;">Telepon {{ $info->no_telp }}</div>
                        @endif

            </td>
            @if ($info->right_logo_status)
                <td style="border:none;width:20%;">
                    <img src="{{ $req->print? $_SERVER['DOCUMENT_ROOT'] . '/images/custom/' . $info->right_logo: url('/images/custom') . '/' . $info->right_logo }}"
                        alt="logo kanan"
                        style="width:80px;">
                </td>
            @endif
        </tr>
    </table>


    <div style="height:3px;border-top:2px solid #ccc;border-bottom:2px solid #ccc;margin:10px 0 15px;width:100%;">
    </div>

</nav>
