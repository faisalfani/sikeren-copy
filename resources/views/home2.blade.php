@php
  
  if($req->start AND $req->end){
    $start = date($req->start);
    $end = date($req->end);
    $where = "BETWEEN ".$start." AND ".$end;

  }else{
    $start = date('Y-m-d');
    $end = date('Y-m-d');
    $where = "BETWEEN ".$start." AND ".$end;    
  }

  $pengeluaran =  Modules\Keuangan\Entities\Jurnal::
  where("type",0)
  ->where("tgl_bayar",">=",$start)
  ->where("tgl_bayar","<=",$end)
  ->sum("jumlah");

  $tagihan_n =  Modules\Keuangan\Entities\BiayaSiswa::
  // where("tgl_bayar",">=",$start)
  // ->where("tgl_bayar","<=",$end)
  sum("nominal");

  $tagihan_b =  Modules\Keuangan\Entities\BiayaSiswa::
  // where("tgl_bayar",">=",$start)
  // ->where("tgl_bayar","<=",$end)
  sum("dibayar");

  $tagihan = $tagihan_n - $tagihan_b;

  
  $saldo = Modules\Keuangan\Entities\Jurnal::
  where("type",1)
  ->where("tgl_bayar","<=",$end)
  ->sum("jumlah") 
  - 
  Modules\Keuangan\Entities\Jurnal::
  where("type",0)
  ->where("tgl_bayar","<=",$end)
  ->sum("jumlah");

  $pemasukan =  Modules\Keuangan\Entities\Jurnal::
  where("type",1)
  ->where("tgl_bayar",">=",$start)
  ->where("tgl_bayar","<=",$end)
  ->sum('jumlah');

  $pemasukan1 =  Modules\Keuangan\Entities\Jurnal::
    where("type",1)
    ->where("tgl_bayar",">=",$start)
    ->where("tgl_bayar","<=",$end)
    ->get();
  

  foreach ($pemasukan1 as $key => $p) {
    $cek = Modules\Siswa\Entities\Siswa::where("nis",$p->nis)->first();
    if(!$cek){
    }
  }

$levelnya = Auth::user()->role_id;

@endphp

@extends('layouts.app')

@section('css-after')
    <!-- daterange picker -->
  <link rel="stylesheet" href="../../plugins/daterangepicker/daterangepicker.css">
  <style>
    .bordered{
      border: 0.5px solid rgb(50, 58, 53);
    }
    .hoverin{
      cursor: pointer;
    }
    .dualima{
      max-width: 250px !important;

    }
    .dropdownin{
      padding: 10px !important;
      min-width: 350px !important;
    }
    .chartin{
      height:250px !important;
      width: 100% !important;
    }
  </style>
@endsection
@section('title')
      Home
@endsection
@section('content')
@if (($levelnya == 1) or ($levelnya == 7))


<div class="">
  <ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item" role="presentation">
      <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Grafik</a>
    </li>
    <li class="nav-item" role="presentation">
      <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Tabel</a>
    </li>
  </ul>
  <div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active pt-2" id="home" role="tabpanel" aria-labelledby="home-tab">
      <div class="row">
        <div class="col-md-4">
            {{-- Awal Chart Pembayaran Bebas/Daftar Ulang --}}
            <div class="card">
              <div class="card-header m-0 p-2">
                  <div style="display:flex;">
                    <div class="">
                        <h6 class="m-0 p-0 ">Pembayaran Jenis Pembayaran Bebas/Daftar Ulang</h6>
                        <small class="m-0 p-0"><span id="labelBebanPembayaranBebas">Semua Jenis</span> <span id="labelTahunPembayaranBebas">Semua Tahun</span></small>
                    </div>
                    <div class="d-flex flex-fill flex-row-reverse float-right" style="gap: 12px;">
                        <i class="fas fa-sync hoverin" onclick="panggilDatajenisPembayaranBebas()"></i>
                        <div class="dropdown" style="margin-top: -4px;">
                          <i class="fas fa-filter" type="button" id="dropdownMenuBebas" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          </i>
                          <div class="dropdown-menu dropdownin" aria-labelledby="dropdownMenuBebas">
                              <input id="jenisBiayaBebas" onchange="jenisBiayaBebasAct(this)" type="text" list="jenisBiaya" class="form-control" placeholder="Semua Jenis Biaya">
                              <datalist id="jenisBiaya">
                                <option data-value="" value="Semua Jenis Biaya"></option>
                                @foreach ($jenis_biaya as $jb)
                                <option data-value="{{$jb->id}}" value="{{$jb->nama}}"></option>
                                @endforeach
                              </datalist>
                              <input id="tahunBiayaBebas" onchange="tahunBiayaBebasAct(this)" type="text" list="tahunAkademikList" class="form-control mt-2" placeholder="Semua Tahun Ajaran">
                          </div>
                        </div>
                    </div>
                  </div>
              </div>
              <div class="card-body p-2">
                  <div class="row">
                    <div class="col-6">
                        <canvas id="myChart" ></canvas>
                    </div>
                    <div class="col-6">
                        <div class="table-responsive p-0">
                          <table class="table table-bordered m-0">
                              <thead>
                                <tr>
                                    <th colspan="2">
                                      <span class="mr-2" style="display:inline-block;width:15px;height:15px;background:rgba(255, 99, 132, 0.2);border: 1px solid rgba(255, 99, 132, 1);border-radius:0 50% 0 0"></span>Sudah Lunas
                                    </th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                    <td class="m-0 p-1 text-center">Siswa</td>
                                    <td class="m-0 p-1 text-center">Nominal</td>
                                </tr>
                                <tr>
                                    <td class="m-0 p-1 text-center" id="chart1LunasSiswa">0</td>
                                    <td class="m-0 p-1 text-center" id="chart1LunasUang">Rp. 0</td>
                                </tr>
                              </tbody>
                          </table>
                          <div class="table-responsive p-0">
                              <table class="table table-bordered m-0">
                                <thead>
                                    <tr>
                                      <th colspan="2"><span class="mr-2" style="display:inline-block;width:15px;height:15px;background:rgba(54, 162, 235, 0.2);border: 1px solid rgba(54, 162, 235, 1);border-radius:0 50% 0 0"></span>
                                          Belum Lunas
                                      </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                      <td class="m-0 p-1 text-center">Siswa</td>
                                      <td class="m-0 p-1 text-center">Nominal</td>
                                    </tr>
                                    <tr>
                                      <td class="m-0 p-1 text-center" id="chart1HutangSiswa">0</td>
                                      <td class="m-0 p-1 text-center" id="chart1HutangUang">Rp. 0</td>
                                    </tr>
                                </tbody>
                              </table>
                          </div>
                        </div>
                    </div>
                  </div>
              </div>
              {{-- Akhir Chart Pembayaran Bebas/Daftar Ulang --}}
            </div>
        </div>
        <div class="col-md-4">
            {{-- Awal Chart Pembayaran Bulanan --}}
            <div class="card">
              <div class="card-header m-0 p-2">
                  <div style="display:flex;">
                    <div class="">
                        <h6 class="m-0 p-0 ">Pembayaran Jenis Pembayaran Bulanan</h6>
                        <small class="m-0 p-0"><span id="labelBebanPembayaranBulanan">Semua Jenis</span> <span id="labelTahunPembayaranBulanan">Semua Tahun</span></small>
                    </div>
                    <div class="d-flex flex-fill flex-row-reverse float-right" style="gap: 12px;">
                        <i class="fas fa-sync hoverin" onclick="panggilDatajenisPembayaranBulanan()"></i>
                        <div class="dropdown" style="margin-top: -4px;">
                          <i class="fas fa-filter" type="button" id="dropdownMenuBulanan" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          </i>
                          <div class="dropdown-menu dropdownin" aria-labelledby="dropdownMenuBulanan">
                              <input id="jenisBiayaBulanan" onchange="jenisBiayaBulananAct(this)" type="text" list="jenisBiaya" class="form-control" placeholder="Semua Jenis Biaya">
                              <datalist id="jenisBiaya">
                                <option data-value="" value="Semua Jenis Biaya"></option>
                                @foreach ($jenis_biaya as $jb)
                                <option data-value="{{$jb->id}}" value="{{$jb->nama}}"></option>
                                @endforeach
                              </datalist>
                              <input id="tahunBiayaBulanan" onchange="tahunBiayaBulananAct(this)" type="text" list="tahunAkademikList" class="form-control mt-2" placeholder="Semua Tahun Ajaran">
                          </div>
                        </div>
                    </div>
                  </div>
              </div>
              <div class="card-body p-2">
                  <div class="row">
                    <div class="col-6">
                        <canvas id="myChart2" ></canvas>
                    </div>
                    <div class="col-6">
                        <div class="table-responsive p-0">
                          <table class="table table-bordered m-0">
                              <thead>
                                <tr>
                                    <th colspan="2">
                                      <span class="mr-2" style="display:inline-block;width:15px;height:15px;background:rgba(255, 99, 132, 0.2);border: 1px solid rgba(255, 99, 132, 1);border-radius:0 50% 0 0"></span>Sudah Lunas
                                    </th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                    <td class="m-0 p-1 text-center">Siswa</td>
                                    <td class="m-0 p-1 text-center">Nominal</td>
                                </tr>
                                <tr>
                                    <td class="m-0 p-1 text-center" id="chart2LunasSiswa">0</td>
                                    <td class="m-0 p-1 text-center" id="chart2LunasUang">Rp. 0</td>
                                </tr>
                              </tbody>
                          </table>
                        </div>
                        <div class="table-responsive p-0">
                          <table class="table table-bordered m-0">
                              <thead>
                                <tr>
                                    <th colspan="2"><span class="mr-2" style="display:inline-block;width:15px;height:15px;background:rgba(54, 162, 235, 0.2);border: 1px solid rgba(54, 162, 235, 1);border-radius:0 50% 0 0"></span>
                                      Belum Lunas
                                    </th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                    <td class="m-0 p-1 text-center">Siswa</td>
                                    <td class="m-0 p-1 text-center">Nominal</td>
                                </tr>
                                <tr>
                                    <td class="m-0 p-1 text-center" id="chart2HutangSiswa">0</td>
                                    <td class="m-0 p-1 text-center" id="chart2HutangUang">Rp. 0</td>
                                </tr>
                              </tbody>
                          </table>
                        </div>
                    </div>
                  </div>
              </div>
              {{-- Akhir Chart Pembayaran Bulanan --}}
            </div>
        </div>
        <div class="col-md-4">
          <div class="row m-0 p-0">
            <div class="col-6">
              {{-- Awal Chart Kepatuhan Bebas --}}
                <div class="card">
                  <div class="card-header m-0 p-2">
                    <div style="display:flex;">
                      <div class="text-center flex-fill">
                        <h6 class="m-0 p-0 ">Kepatuhan Siswa</h6>
                        <small class="m-0 p-0">Jenis Bebas</small>
                      </div>
                      <div class="float-right" style="gap: 12px;">
                        <i class="fas fa-sync hoverin" onclick="panggilKepatuhanBebas()"></i>
                      </div>
                    </div>
                  </div>
                  <div class="card-body p-2">
                    <canvas id="myChart3"></canvas>
                  </div>
                </div>
              {{-- Akhir Chart Kepatuhan Bebas --}}
            </div>
            <div class="col-6">
              {{-- Awal Chart Kepatuhan Bulanan --}}
                <div class="card">
                  <div class="card-header m-0 p-2">
                    <div style="display:flex;">
                      <div class="text-center flex-fill">
                        <h6 class="m-0 p-0 ">Kepatuhan Siswa</h6>
                        <small class="m-0 p-0">Jenis Bulanan</small>
                      </div>
                      <div class="float-right" style="gap: 12px;">
                        <i class="fas fa-sync hoverin" onclick="panggilKepatuhanBulanan()"></i>
                      </div>
                    </div>
                  </div>
                  <div class="card-body p-2">
                    <canvas id="myChart4"></canvas>
                  </div>
                </div>
              {{-- Akhir Chart Kepatuhan Bulanan --}}
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-5">
          {{-- Awal Chart Realisasi Bulanan --}}
            <div class="card h-100">
              <div class="card-header m-0 p-2">
                <div style="display:flex;">
                  <div class="text-center flex-fill">
                    <h6 class="m-0 p-0 ">Realisasi Beban Bulanan Semua Jenis <span id="labelTahunRealisasi">Semua Tahun Ajaran</span></h6>
                    {{-- <small class="m-0 p-0">Jenis Bebas</small> --}}
                  </div>
                  <div class="float-right d-flex" style="gap: 12px;">
                    
                    <div class="dropdown" style="margin-top: -4px;">
                      <i class="fas fa-filter" type="button" id="dropdownMenuRealisasi" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      </i>
                      <div class="dropdown-menu dropdownin" aria-labelledby="dropdownMenuRealisasi">
                          {{-- <input id="jenisBiayaBulanan" onchange="jenisBiayaBulananAct(this)" type="text" list="jenisBiaya" class="form-control" placeholder="Semua Jenis Biaya"> --}}
                          {{-- <datalist id="jenisBiaya">
                            <option data-value="" value="Semua Jenis Biaya"></option>
                            @foreach ($jenis_biaya as $jb)
                            <option data-value="{{$jb->id}}" value="{{$jb->nama}}"></option>
                            @endforeach
                          </datalist> --}}
                          <input id="tahunRealisasi" onchange="tahunRealisasiAct(this)" type="text" list="tahunAkademikList" class="form-control mt-2" placeholder="Semua Tahun Ajaran">
                      </div>
                    </div>
                    <i class="fas fa-sync hoverin" onclick="panggilrealisasiBeban()"></i>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <canvas id="myChart5"></canvas>
              </div>
            </div>
          {{-- Akhir Chart Realisasi Bulanan --}}
        </div>
        <div class="col-md-7">
          {{-- Awal Chart Jumlah Siswa --}}
            <div class="card h-100">
              <div class="card-header m-0 p-2">
                <div style="display:flex;">
                  <div class="text-center flex-fill">
                    <h6 class="m-0 p-0 ">Jumlah siswa</h6>
                  </div>
                  <div class="float-right" style="gap: 12px;">
                    <i class="fas fa-sync hoverin" onclick="panggiljumlahSiswa()"></i>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-5">
                    <div class="form-group text-center">
                      <label for="">Tahun Ajaran</label>
                      <input id="tahunAkademikInput" onchange="tahunAkademikAct(this)" type="text" list="tahunAkademikList" class="form-control" placeholder="Semua Tahun Ajaran">
                      <datalist id="tahunAkademikList">
                        <option value="Semua Tahun Ajaran" data-value=""></option>
                        @foreach ($tahun_akademik as $ta)
                            <option data-value="{{$ta->tahun_ajaran}}" value="{{$ta->tahun_akademik}}" ></option>
                        @endforeach
                      </datalist>
                    </div>
                  </div>
                  <div class="col-2">
                    <div class="form-group text-center">
                      <label for="">Jumlah Kelas</label>
                      <h4 id="tahunAkademikJumlahKelas" class="mb-0 text-center" style="font-size: 24px">{{$data_kelas->count()}}</h4>
                    </div>
                  </div>
                  <div class="col-5">
                    <div class="form-group text-center">
                      <label for="">Data Kelas</label>
                      <input id="dataKelasInput" onchange="dataKelaskAct(this)" type="text" list="dataKelasList" class="form-control" placeholder="Semua Data Kelas">
                      <datalist id="dataKelasList">
                        <option value="Semua Data Kelas"></option>
                        @foreach ($data_kelas as $dk)
                            <option value="{{$dk->nama}}"></option>
                        @endforeach
                      </datalist>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-4">
                    <div class="card">
                      <div class="card-header text-center bg-success">
                        Aktif
                      </div>
                      <div class="card-body p-2">
                        <div class="row">
                          <div class="col text-center" id="lAktif">L : {{$data_siswa['aktif'] ? nomorin($data_siswa['aktif']->laki) : '0'}}</div>
                          <div class="col text-center" id="pAktif">P : {{$data_siswa['aktif'] ? nomorin($data_siswa['aktif']->perempuan) : '0'}}</div>
                        </div>
                        <h3 class="text-center mb-0" style="font-size:50px;" id="tAktif">{{$data_siswa['aktif'] ? nomorin($data_siswa['aktif']->total) : '0'}}</h3>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="card">
                      <div class="card-header text-center bg-danger">
                        Alumni
                      </div>
                      <div class="card-body p-2">
                        <div class="row">
                          <div class="col text-center" id="lAlumni">L : {{$data_siswa['alumni'] ? nomorin($data_siswa['alumni']->laki) : '0'}}</div>
                          <div class="col text-center" id="pAlumni">P : {{$data_siswa['alumni'] ? nomorin($data_siswa['alumni']->perempuan) : '0'}}</div>
                        </div>
                        <h3 class="text-center mb-0" style="font-size:50px;" id="tAlumni">{{$data_siswa['alumni'] ? nomorin($data_siswa['alumni']->total) : '0'}}</h3>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="card">
                      <div class="card-header text-center bg-info">
                        Berhenti
                      </div>
                      <div class="card-body p-2">
                        <div class="row">
                          <div class="col text-center" id="lBerhenti">L : {{$data_siswa['berhenti'] ? nomorin($data_siswa['berhenti']->laki) : '0'}}</div>
                          <div class="col text-center" id="pBerhenti">P : {{$data_siswa['berhenti'] ? nomorin($data_siswa['berhenti']->perempuan) : '0'}}</div>
                        </div>
                        <h3 class="text-center mb-0" style="font-size:50px;" id="tBerhenti">{{$data_siswa['berhenti'] ? nomorin($data_siswa['berhenti']->total) : '0'}}</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          {{-- Akhir Chart Jumlah Siswa --}}
        </div>
      </div>
    </div>
    <div class="tab-pane fade pt-2" id="profile" role="tabpanel" aria-labelledby="profile-tab">
      {{-- <section class="content container">
        <div class="container-fluid"> --}}
            <div class="row">
              <div class="col-md-12">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                  <div class="card">
                    <div class="card-header">
                        <div class="row pt-3">
                          <div class="col-md-10">
                              <form class="row" id="form-filter" action="/" method="GET">
                                <div class="col-md-4">
                                    <div class="input-group">
                                      <div class="input-group-prepend">
                                          <span class="input-group-text">
                                          <i class="far fa-calendar-alt"></i>
                                          </span>
                                      </div>
                                      <input name="filter" type="text" class="form-control float-right" id="filterdate">
                                    </div>
                                </div>
                                <div class="col-md-4 pt-1">
                                    {{ tgl_indo($start)}} -
                                    {{ tgl_indo($end)}}
                                </div>
                              </form>
                              <!-- /.input group -->
                          </div>
                          <div class="col-md-2 d-none d-lg-block">
                              <div class="card-tools text-right">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                                </button>
                              </div>
                          </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <!-- Info boxes -->
                        <div class="row">
                          <div class="col-12 col-sm-6 col-md-3">
                              <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1">
                                <i class="fas fa-credit-card"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Saldo</span>
                                    <span class="info-box-number">
                                    {{ rupiah($saldo) }}
                                    </span>
                                </div>
                                <!-- /.info-box-content -->
                              </div>
                              <!-- /.info-box -->
                          </div>
                          <!-- /.col -->
                          <div class="col-12 col-sm-6 col-md-3">
                              <div class="info-box mb-3">
                                <span class="info-box-icon bg-success elevation-1"><i class="fas fa-credit-card"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Pemasukan</span>
                                    <span class="info-box-number">
                                    {{ rupiah($pemasukan)}}
                                    </span>
                                </div>
                                <!-- /.info-box-content -->
                              </div>
                              <!-- /.info-box -->
                          </div>
                          <!-- /.col -->
                          <!-- fix for small devices only -->
                          <div class="clearfix hidden-md-up"></div>
                          <div class="col-12 col-sm-6 col-md-3">
                              <div class="info-box mb-3">
                                <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-credit-card"></i></span>
                                <!-- <a href="/input-pengeluaran" class="info-box-content">-->
                                <a href="#" class="info-box-content">
                                <span class="info-box-text">Pengeluaran</span>
                                <span class="info-box-number">
                                {{ rupiah($pengeluaran)}}
                                </span>
                                </a>
                                <!-- /.info-box-content -->
                              </div>
                              <!-- /.info-box -->
                          </div>
                          <!-- /.col -->
                          <div class="col-12 col-sm-6 col-md-3">
                              <div class="info-box mb-3">
                                <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-credit-card"></i></span>
                                {{--<a href="/keuangan/biaya-siswa" class="info-box-content">--}}
                                <a href="#" class="info-box-content">
                                <span class="info-box-text">Semua Tagihan</span>
                                <span class="info-box-number">
                                {{ rupiah($tagihan)}}
                                </span>
                                </a>
                                <!-- /.info-box-content -->
                              </div>
                              <!-- /.info-box -->
                          </div>
                          <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="card-footer">
                        <!-- /.row -->
                    </div>
                  </div>
                  <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="row">
              <div class="col-md-6">
                  <!-- PRODUCT LIST -->
                  <div class="card ">
                    <div class="card-header">
                        <h3 class="card-title">Pemasukan terbaru</h3>
                        <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-card-widget="collapse">
                          <i class="fas fa-minus"></i>
                          </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <ul class="products-list product-list-in-card pl-2 pr-2">
                          @foreach (
                          Modules\Keuangan\Entities\Jurnal::where("type",1)
                          ->orderBy("updated_at","DESC")->limit(5)->get() as $d)
                          <li class="item">
                              <a href="javascript:void(0)" class="product-title">
                              {{ $d->nama_biaya }}   
                              <span class="badge badge-warning float-right">
                              {{ tgl_indo($d->tgl_bayar) }} 
                              </span></a>
                              <span class="product-description">
                              {{ rupiah($d->jumlah) }} :  {{ $d->nis}} 
                              </span>
                          </li>
                          @endforeach
                        </ul>
                    </div>
                  </div>
                  <!-- /.card -->
              </div>
              <div class="col-md-6">
                  <!-- PRODUCT LIST -->
                  <div class="card ">
                    <div class="card-header">
                        <h3 class="card-title">Pengeluaran Terbaru</h3>
                        <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-card-widget="collapse">
                          <i class="fas fa-minus"></i>
                          </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <ul class="products-list product-list-in-card pl-2 pr-2">
                          @foreach (Modules\Keuangan\Entities\Jurnal::where("type",0)
                          ->orderBy("updated_at","DESC")->limit(5)->get() as $d)
                          <li class="item">
                              <a href="javascript:void(0)" class="product-title">
                              {{ $d->nama_biaya }}   
                              <span class="badge badge-warning float-right">
                              {{ tgl_indo($d->tgl_bayar) }} 
                              </span></a>
                              <span class="product-description">
                              {{ rupiah($d->jumlah) }}
                              </span>
                          </li>
                          @endforeach
                        </ul>
                    </div>
                  </div>
                  <!-- /.card -->
              </div>
            </div>
            <!-- /.row -->
        {{-- </div> --}}
        <!--/. container-fluid -->
      {{-- </section> --}}
    </div>
  </div>
</div>


    
@else

    <section class="content container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">

                            <div class="row pt-3">

                                <div class="col-md-10">
                                    Dashboard
                                    <!-- /.input group -->
                                </div>

                                <div class="col-md-2 d-none d-lg-block">
                                    <div class="card-tools text-right">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                            <i class="fas fa-minus"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->



                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
    </section>
@endif
@endsection


@section('js-after')
    <!-- PAGE PLUGINS -->
<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="../../plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="../../plugins/inputmask/jquery.inputmask.bundle.js"></script>
<script src="../../plugins/moment/moment.min.js"></script>
<!-- date-range-picker -->
<script src="../../plugins/daterangepicker/daterangepicker.js"></script>

<script>





var start = '{{ $start }}';
var end = '{{ $end }}';


$('#filterdate').daterangepicker({
    startDate: start,
    endDate: end,
    locale: {
        format: 'YYYY-MM-D'
    },
    ranges: {
       'Today': [moment(), moment()],
       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
       'Last 7 Days': [moment().subtract(6, 'days'), moment()],
       'Last 30 Days': [moment().subtract(29, 'days'), moment()],
       'This Month': [moment().startOf('month'), moment().endOf('month')],
       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
}, function(start,end){
      location.href = "?start="+start.format('YYYY-MM-D') +"&end="+end.format('YYYY-MM-D');     
});


  $("#form-laporan").submit(function(e){
    e.preventDefault();
    var link  = $(this).attr("action") +"?"+ $(this).serialize()
    window.open(link, "_blank"); 
  });

</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.4.1/chart.min.js" integrity="sha512-5vwN8yor2fFT9pgPS9p9R7AszYaNn0LkQElTXIsZFCL7ucT8zDCAqlQXDdaqgA1mZP47hdvztBMsIoFxq/FyyQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
  
var ctx = document.getElementById('myChart');
var jenisPembayaranBebas = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ['Lunas', 'Belum Lunas'],
        datasets: [{
            data: [1,1],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
            ],
            borderWidth: 1
        }]
    },
    options: {
      responsive: true,
      plugins: {
        legend: {
          display: false,
        },
        title: {
          display: false,
        },
        tooltip: {
          callbacks: {
            label: function(context) {
                var label = context.dataset.label || '0';
                // console.log('context',context);
                if (label) {
                    label += ' Siswa';
                }
                if (context.parsed.y !== null) {
                    label = context.parsed;
                }
                return new Intl.NumberFormat().format(label) + " Siswa";
            }
          }
        }
      }
    },
});

var ctx2 = document.getElementById('myChart2');
var jenisPembayaranBulanan = new Chart(ctx2, {
    type: 'doughnut',
    data: {
        labels: ['Lunas', 'Belum Lunas'],
        datasets: [{
            data: [1,1],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
            ],
            borderWidth: 1
        }]
    },
    options: {
      responsive: true,
      plugins: {
        legend: {
          display: false,
        },
        title: {
          display: false,
        },
        tooltip: {
          callbacks: {
            label: function(context) {
                var label = context.dataset.label || '0';

                if (label) {
                    label += ' Siswa';
                }
                if (context.parsed.y !== null) {
                    label = context.parsed;
                }
                return new Intl.NumberFormat().format(label) + " Siswa";
            }
          }
        }
      }
    },
});

var ctx3 = document.getElementById('myChart3');
ctx3.height = 350
var kepatuhanBebas = new Chart(ctx3, {
    type: 'bar',
    data: {
        labels: ['Kepatuhan'],
        datasets: [{
            data: [1],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
            ],
            borderWidth: 1
        }]
    },
    options: {
      responsive: true,
      plugins: {
        legend: {
          display: false,
        },
        title: {
          display: false,
        },
        tooltip: {
          callbacks: {
            label: function(context) {
                var label = context.dataset.label || '0';

                if (label) {
                    label += '%';
                }
                if (context.parsed.y !== null) {
                    label = context.parsed.y;
                }
                return label + "%";
            }
          }
        }
      },
      scales : {
          y : {
              min: 0,
              max: 100,
          }
      }
      
    },
});
var ctx4 = document.getElementById('myChart4');
ctx4.height = 350
var kepatuhanBulanan = new Chart(ctx4, {
    type: 'bar',
    data: {
        labels: ['Kepatuhan'],
        datasets: [{
            data: [1],
            backgroundColor: [
                'rgba(54, 162, 235, 0.2)',
            ],
            borderColor: [
                'rgba(54, 162, 235, 1)',
            ],
            borderWidth: 1
        }]
    },
    options: {
      responsive: true,
      plugins: {
        legend: {
          display: false,
        },
        title: {
          display: false,
        },
        tooltip: {
          callbacks: {
            label: function(context) {
                var label = context.dataset.label || '0';

                if (label) {
                    label += '%';
                }
                if (context.parsed.y !== null) {
                    label = context.parsed.y;
                }
                return label + "%";
            }
          }
        }
      },
      scales : {
          y : {
              min: 0,
              max: 100,
          }
      }
      
    },
});

var ctx5 = document.getElementById('myChart5');
// ctx5.height = 200
var realisasiBeban = new Chart(ctx5, {
    type: 'line',
    data: {
        labels: ['Juli', 'Agustus', 'September', 'Oktober','November', 'Desember', 'Januari','Februari', 'Maret', 'April', 'Mei', 'Juni'],
        datasets: [{
            data: [1,2,3,4,5,1,7,8,9,10,11,12],
            backgroundColor: [
                'rgba(54, 162, 235, 0.2)',
            ],
            borderColor: [
                'rgba(54, 162, 235, 1)',
            ],
            borderWidth: 1,
            fill:'start'
        }]
    },
    options: {
      responsive: true,
      plugins: {
        legend: {
          display: false,
        },
        title: {
          display: false,
        },
        tooltip: {
          callbacks: {
            label: function(context) {
                var label = context.dataset.label || '0';

                if (label) {
                    label += '%';
                }
                if (context.parsed.y !== null) {
                    label = context.parsed.y;
                }
                return label + "%";
            }
          }
        },
        filler: {
          propagate: false,
        },
      },
      scales : {
          y : {
              min: 0,
              max: 100,
          }
      },
      elements : {
        line : {
          tension : 0.4
        }
      }
      
    },
});
// var cobain = document.getElementById('myChart5');
// cobain.height = 200
// window.addEventListener('beforeprint', () => {
//   realisasiBeban.resize(600, 600);
// });
// window.addEventListener('afterprint', () => {
//   realisasiBeban.resize();
// });
function panggilDatajenisPembayaranBebas() {
  let tahun ="" 
  let jenis = ""
  let t = document.getElementById('tahunBiayaBebas').value
  if (t) {
    tahun = document.querySelector("#tahunAkademikList option[value='"+t+"']").dataset.value;
  }
  let j = document.getElementById('jenisBiayaBebas').value
  if (j) {
    jenis = document.querySelector("#jenisBiaya option[value='"+j+"']").dataset.value;
  }  

  let url = "{{url('/chart/pembayaran-bebas')}}?";    
  if (jenis.length > 0) {
    url += "jenis="+jenis+"&";
  } 

  if (tahun.length > 0) {
    url += "tahun="+tahun;
  }  

  fetch(url)
  .then(response => response.json())
  .then(data => {
    // jenisPembayaranBebas.data.datasets[0].data = [1,1];
    let datain = [data.lunas_siswa, data.hutang_siswa]
    jenisPembayaranBebas.data.datasets[0].data = datain;
    jenisPembayaranBebas.update()
    document.getElementById('chart1LunasSiswa').innerHTML = data.lunas_siswa
    document.getElementById('chart1LunasUang').innerHTML = data.lunas_uang
    document.getElementById('chart1HutangSiswa').innerHTML = data.hutang_siswa
    document.getElementById('chart1HutangUang').innerHTML = data.hutang_uang
  });
}
function panggilDatajenisPembayaranBulanan() {
  let tahun = ""
  let jenis = ""
  let t = document.getElementById('tahunBiayaBulanan').value
  if (t) {
    tahun = document.querySelector("#tahunAkademikList option[value='"+t+"']").dataset.value;
  }
  let j = document.getElementById('jenisBiayaBulanan').value
  if (j) {
    jenis = document.querySelector("#jenisBiaya option[value='"+j+"']").dataset.value;
  }

  let url = "{{url('/chart/pembayaran-bulanan')}}?";    
  if (jenis.length > 0) {
    url += "jenis="+jenis+"&";
  } 

  if (tahun.length > 0) {
    url += "tahun="+tahun;
  }

  console.log(url);

  fetch(url)
  .then(response => response.json())
  .then(data => {
    // jenisPembayaranBebas.data.datasets[0].data = [1,1];    
    let datain = [data.lunas_siswa, data.hutang_siswa]
    jenisPembayaranBulanan.data.datasets[0].data = datain;
    jenisPembayaranBulanan.update()
    document.getElementById('chart2LunasSiswa').innerHTML = data.lunas_siswa
    document.getElementById('chart2LunasUang').innerHTML = data.lunas_uang
    document.getElementById('chart2HutangSiswa').innerHTML = data.hutang_siswa
    document.getElementById('chart2HutangUang').innerHTML = data.hutang_uang
  });
}
function panggilKepatuhanBebas() {
  fetch("{{url('/chart/kepatuhan-bebas')}}")
  .then(response => response.json())
  .then(data => {
    // jenisPembayaranBebas.data.datasets[0].data = [1,1];
    let datain = [data.persen]
    kepatuhanBebas.data.datasets[0].data = datain;
    kepatuhanBebas.update()
  });
}
function panggilKepatuhanBulanan() {
  fetch("{{url('/chart/kepatuhan-bulanan')}}")
  .then(response => response.json())
  .then(data => {
    // jenisPembayaranBebas.data.datasets[0].data = [1,1];
    let datain = [data.persen]
    kepatuhanBulanan.data.datasets[0].data = datain;
    kepatuhanBulanan.update()
  });
}
function panggilrealisasiBeban() {
  let tahun = ""
  let t = document.getElementById('tahunRealisasi').value
  if (t) {
    tahun = document.querySelector("#tahunAkademikList option[value='"+t+"']").dataset.value;
  }

  let url = "{{url('/chart/realisasi-kepatuhan')}}?";      
  if (tahun.length > 0) {
    url += "tahun="+tahun;
  }
  
  fetch(url)
  .then(response => response.json())
  .then(data => {
    // jenisPembayaranBebas.data.datasets[0].data = [1,1];
    let datain = [data.persen]
    realisasiBeban.data.datasets[0].data = data;
    realisasiBeban.update()
  });
}
function tahunAkademikAct(e){
  let ta = e.value.split('/');
  if (ta == 'Semua Tahun Ajaran') {
    ta = 'Semua'
  } else {
    ta = ta[0]
  }
  fetch("{{url('/chart/data-kelas')}}/"+ta)
  .then(response => response.json())
  .then(data => {
    // jenisPembayaranBebas.data.datasets.data = [1,1];
    // console.log("d", data);

    document.getElementById('tahunAkademikJumlahKelas').innerHTML = data.length
    document.getElementById('dataKelasList').innerHTML = ''
    let options = '';
    for (var i = 0; i < data.length; i++) {
      options += '<option value="' + data[i].nama + '" />';
    }

    document.getElementById('dataKelasList').innerHTML = options;
    document.getElementById('dataKelasInput').value = "Semua Data Kelas"
    getJumlahSiswa(ta, 'Semua')
  });
}
function dataKelaskAct(e){
  let kelas, tahun = '';
  if (e.value == 'Semua Tahun Ajaran') {
    kelas = 'Semua'
  } else {
    kelas = e.value
    document.getElementById('tahunAkademikJumlahKelas').innerHTML = "1"
  }

  tahun = document.getElementById('tahunAkademikInput').value
  if (tahun == 'Semua Tahun Ajaran' || tahun == '') {
    tahun = 'Semua'
  } else {
    tahun = tahun.split('/');
    tahun = tahun[0]
  }
    getJumlahSiswa(tahun, kelas)
    
}

function panggiljumlahSiswa(){
  // let kelas, tahun = '';
  let kelas = document.getElementById('dataKelasInput').value
  if (kelas == 'Semua Tahun Ajaran' || kelas == '') {
    kelas = 'Semua'
  } 
  tahun = document.getElementById('tahunAkademikInput').value
  if (tahun == 'Semua Tahun Ajaran' || tahun == '') {
    tahun = 'Semua'
  } else {
    tahun = tahun.split('/');
    tahun = tahun[0]
  }
    getJumlahSiswa(tahun, kelas)
}

function getJumlahSiswa(tahun, kelas){
  fetch("{{url('/chart/jumlah-siswa')}}/"+tahun+"/"+kelas)
  .then(response => response.json())
  .then(data => {
    // jenisPembayaranBebas.data.datasets.data = [1,1];
      data.aktif ?document.getElementById('lAktif').innerHTML = nomorin(data.aktif.laki) : false;
      data.aktif ?document.getElementById('pAktif').innerHTML = nomorin(data.aktif.perempuan) : false;
      data.aktif ?document.getElementById('tAktif').innerHTML = nomorin(data.aktif.total) : false;
      data.alumni ?document.getElementById('lAlumni').innerHTML = nomorin(data.alumni.laki) : false;
      data.alumni ?document.getElementById('pAlumni').innerHTML = nomorin(data.alumni.perempuan) : false;
      data.alumni ?document.getElementById('tAlumni').innerHTML = nomorin(data.alumni.total) : false;
      data.berhenti ?document.getElementById('lBerhenti').innerHTML = nomorin(data.berhenti.laki) : false;
      data.berhenti ?document.getElementById('pBerhenti').innerHTML = nomorin(data.berhenti.perempuan) : false;
      data.berhenti ?document.getElementById('tBerhenti').innerHTML = nomorin(data.berhenti.total) : false;

    
  });
}

function jenisBiayaBebasAct(e){
  // console.log(e.getAttribute('data-value'));
  var labelin = e.value;
  let label = document.getElementById('labelBebanPembayaranBebas')
  label.innerHTML = labelin
  // var valuein = document.querySelector("#jenisBiaya option[value='"+labelin+"']").dataset.value;
  // console.log("2sen", valuein)
  panggilDatajenisPembayaranBebas()
}
function tahunBiayaBebasAct(e){
  var labelin = e.value;
  let label = document.getElementById('labelTahunPembayaranBebas')
  label.innerHTML = labelin
  // var valuein = document.querySelector("#jenisBiaya option[value='"+labelin+"']").dataset.value;
  // console.log("2sen", valuein)
  panggilDatajenisPembayaranBebas()
}
function jenisBiayaBulananAct(e){
  var labelin = e.value;
  let label = document.getElementById('labelBebanPembayaranBulanan')
  label.innerHTML = labelin
  // var valuein = document.querySelector("#jenisBiaya option[value='"+labelin+"']").dataset.value;
  // console.log("2sen", valuein)
  panggilDatajenisPembayaranBulanan()
}
function tahunBiayaBulananAct(e){
  var labelin = e.value;
  let label = document.getElementById('labelTahunPembayaranBulanan')
  label.innerHTML = labelin
  // var valuein = document.querySelector("#jenisBiaya option[value='"+labelin+"']").dataset.value;
  // console.log("2sen", valuein)
  panggilDatajenisPembayaranBulanan()
}
function tahunRealisasiAct(e){
  var labelin = e.value;
  let label = document.getElementById('labelTahunRealisasi')
  label.innerHTML = labelin
  // var valuein = document.querySelector("#jenisBiaya option[value='"+labelin+"']").dataset.value;
  // console.log("2sen", valuein)
  panggilrealisasiBeban()
}
function nomorin(angka){
  return new Intl.NumberFormat().format(angka)
}
setTimeout(() => {
   panggilDatajenisPembayaranBebas()
   panggilDatajenisPembayaranBulanan()
   panggilKepatuhanBebas()
   panggilKepatuhanBulanan()
   panggilrealisasiBeban()
}, 2000);

</script>
@endsection