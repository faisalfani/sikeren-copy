<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @laravelPWA
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Aplikasi Siswa Tagihan</title>

    <style>
    *{
      margin: 0;
      padding:0;
      box-sizing: border-box;
    }
    body{
      overflow: hidden;
    }
    .box{
      border-radius: 10px;
      min-height: 700px;
      width: 26%;
      min-width: 320px;
      position: absolute;
      left: 36%;
      box-shadow: 1px 2px 3px #ccc
    }
    .bg-img{
      position: absolute;
      top: 0;
      left: 0;
      z-index: -1;
      width: 100%
    }
    .logo-sikeren{
      position: absolute;
      top: 30px;
      left: 30px;
      width: 200px;
    }
    .card-box{
      margin: 35px;
      margin-top: 120px;
      border-radius: 15px;
      min-height: 500px;
      box-shadow: 1px 2px 3px #ccc;
      background: #fff;
      padding: 10px;
    }
    .title{
      position: absolute;
      top: 90px;
      left: 50px;
      color: white
    }
    @media screen and (max-width:600px){
      .box{
        width: 100%;
        left: 0;
        border-radius: 0;
        box-shadow: 0;
      }
    }
    .card-list{
      border-radius: 10px;
      margin-bottom: 5px;
      padding: 12px;
      box-shadow: 1px 1px 3px #ccc;
    }
    .card-list:hover{
      opacity: .8;
    }
    </style>
  </head>
  <body>
    
    <div class="row">
      <div class="box">
        <img class="bg-img" src="/bg.png" alt="Background">
        <img class="logo-sikeren" src="/logo.png" alt="Logo">
        <h6 class="title">Tagihan</h6>
        <div class="card-box">
          <div class="card-body">
              <div class="row">
              @php
              $totalSisa = 0;
              @endphp
              @foreach (Modules\Keuangan\Entities\BiayaSiswa::where("nis",$nis)->orderBy("created_at","ASC")->get() as $key => $item)
                  <div class="col-md-12 card-list">
                  <h6>{{ $item->nama }}
                    <br>
                  <small>({{ rupiah($item->nominal - $item->dibayar) }})</small></h6>
                  </div>
                @php
                    $totalSisa += $item->nominal-$item->dibayar;
                @endphp
              @endforeach
            </div>
          </div>
        </div>
        
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  
    <script>
 
    </script>
  </body>
</html>