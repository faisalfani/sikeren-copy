@php

  if($req->start AND $req->end){
    $start = date($req->start);
    $end = date($req->end);
    $where = "BETWEEN ".$start." AND ".$end;

  }else{
    $start = date('Y-m-d');
    $end = date('Y-m-d');
    $where = "BETWEEN ".$start." AND ".$end;    
  }

  $pengeluaran =  Modules\Keuangan\Entities\Jurnal::
  where("type",0)
  ->where("tgl_bayar",">=",$start)
  ->where("tgl_bayar","<=",$end)
  ->sum("jumlah");

  $tagihan_n =  Modules\Keuangan\Entities\BiayaSiswa::
  // where("tgl_bayar",">=",$start)
  // ->where("tgl_bayar","<=",$end)
  sum("nominal");

  $tagihan_b =  Modules\Keuangan\Entities\BiayaSiswa::
  // where("tgl_bayar",">=",$start)
  // ->where("tgl_bayar","<=",$end)
  sum("dibayar");

  $tagihan = $tagihan_n - $tagihan_b;

  
  $saldo = Modules\Keuangan\Entities\Jurnal::
  where("type",1)
  ->where("tgl_bayar","<=",$end)
  ->sum("jumlah") 
  - 
  Modules\Keuangan\Entities\Jurnal::
  where("type",0)
  ->where("tgl_bayar","<=",$end)
  ->sum("jumlah");

  $pemasukan =  Modules\Keuangan\Entities\Jurnal::
  where("type",1)
  ->where("tgl_bayar",">=",$start)
  ->where("tgl_bayar","<=",$end)
  ->sum('jumlah');

  $pemasukan1 =  Modules\Keuangan\Entities\Jurnal::
    where("type",1)
    ->where("tgl_bayar",">=",$start)
    ->where("tgl_bayar","<=",$end)
    ->get();
  

  foreach ($pemasukan1 as $key => $p) {
    $cek = Modules\Siswa\Entities\Siswa::where("nis",$p->nis)->first();
    if(!$cek){
    }
  }



@endphp

@extends('layouts.app')

@section('css-after')
    <!-- daterange picker -->
  <link rel="stylesheet" href="../../plugins/daterangepicker/daterangepicker.css">
@endsection
@section('title')
      Home
@endsection
@section('content')
    @php
    $levelnya = Auth::user()->role_id;
    if (($levelnya == 1) or ($levelnya == 7)) {
    @endphp
    <section class="content container">
        <div class="container-fluid">
        
  
            
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                    <div class="card-header">

                      <div class="row pt-3">

                        <div class="col-md-10">
                              <form class="row" id="form-filter" action="/" method="GET">
                                  <div class="col-md-4">
                                        <div class="input-group">
                                          <div class="input-group-prepend">
                                            <span class="input-group-text">
                                              <i class="far fa-calendar-alt"></i>
                                            </span>
                                          </div>
                                            <input name="filter" type="text" class="form-control float-right" id="filterdate">
                                        </div>
                                  </div>
                                  <div class="col-md-4 pt-1">
                                    {{ tgl_indo($start)}} -
                                    {{ tgl_indo($end)}}
                                  </div>
                            </form>
                            <!-- /.input group -->
                          </div>

                        <div class="col-md-2 d-none d-lg-block">
                          <div class="card-tools text-right">
                              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                              <i class="fas fa-minus"></i>
                              </button>
                          </div>
                        </div>
                      </div>
                    </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <!-- Info boxes -->
          <div class="row">
              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-info elevation-1">
                      <i class="fas fa-credit-card"></i></span>
    
                  <div class="info-box-content">
                    <span class="info-box-text">Saldo</span>
                    <span class="info-box-number">
                     {{ rupiah($saldo) }}
                    </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->
              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-success elevation-1"><i class="fas fa-credit-card"></i></span>
    
                  <div class="info-box-content">
                    <span class="info-box-text">Pemasukan</span>
                    <span class="info-box-number">
                      {{ rupiah($pemasukan)}}
                    </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->
    
              <!-- fix for small devices only -->
              <div class="clearfix hidden-md-up"></div>
    
              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-credit-card"></i></span>
    
                  <!-- <a href="/input-pengeluaran" class="info-box-content">-->
                    <a href="#" class="info-box-content">
                    <span class="info-box-text">Pengeluaran</span>
                    <span class="info-box-number">
                        {{ rupiah($pengeluaran)}}
                    </span>
                  </a>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->
              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-credit-card"></i></span>
    
                  {{--<a href="/keuangan/biaya-siswa" class="info-box-content">--}}
                    <a href="#" class="info-box-content">
                    <span class="info-box-text">Semua Tagihan</span>
                    <span class="info-box-number">
                        {{ rupiah($tagihan)}}
                    </span>
                  </a>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
<div class="card-footer">
                                <!-- /.row -->
                                </div>
               
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
  
        <div class="row">
            <div class="col-md-6">
              <!-- PRODUCT LIST -->
              <div class="card ">
                <div class="card-header">
                  <h3 class="card-title">Pemasukan Terbaru</h3>
  
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>
                  
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                  <ul class="products-list product-list-in-card pl-2 pr-2">
                    
                    @foreach (
                      Modules\Keuangan\Entities\Jurnal::where("type",1)
                      ->orderBy("updated_at","DESC")->limit(5)->get() as $d)
                        
                    <li class="item">
                   
                        <a href="javascript:void(0)" class="product-title">
                            {{ $d->nama_biaya }}   
                         
                          <span class="badge badge-warning float-right">

                              {{ tgl_indo($d->tgl_bayar) }} 
                                                   
                          </span></a>
                        <span class="product-description">
                            {{ rupiah($d->jumlah) }} :  {{ $d->nis}} 
                        </span>
                    </li>


                    @endforeach
                  
                  </ul>
                </div>
             
              </div>
              <!-- /.card -->

            </div>

            <div class="col-md-6">
                <!-- PRODUCT LIST -->
                <div class="card ">
                  <div class="card-header">
                    <h3 class="card-title">Pengeluaran Terbaru</h3>
    
                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                      </button>
                    
                    </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body p-0">
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                      
                      @foreach (Modules\Keuangan\Entities\Jurnal::where("type",0)
                      ->orderBy("updated_at","DESC")->limit(5)->get() as $d)
                          
                      <li class="item">
                     
                          <a href="javascript:void(0)" class="product-title">
                              {{ $d->nama_biaya }}   
                           
                            <span class="badge badge-warning float-right">
  
                                {{ tgl_indo($d->tgl_bayar) }} 
                                                     
                            </span></a>
                          <span class="product-description">
                              {{ rupiah($d->jumlah) }}
                          </span>
                      </li>
  
  
                      @endforeach
                    
                    </ul>
                  </div>
               
                </div>
                <!-- /.card -->
  
              </div>
  


          </div>
          <!-- /.row -->
        </div><!--/. container-fluid -->
      </section>
      @php } else { //jika diluar level keuangan @endphp
    <section class="content container">
        <div class="container-fluid">



            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">

                            <div class="row pt-3">

                                <div class="col-md-10">
                                    Dashboard
                                    <!-- /.input group -->
                                </div>

                                <div class="col-md-2 d-none d-lg-block">
                                    <div class="card-tools text-right">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                            <i class="fas fa-minus"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->



                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
    </section>
      @php } @endphp
@endsection


@section('js-after')
    <!-- PAGE PLUGINS -->
<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="../../plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="../../plugins/inputmask/jquery.inputmask.bundle.js"></script>
<script src="../../plugins/moment/moment.min.js"></script>
<!-- date-range-picker -->
<script src="../../plugins/daterangepicker/daterangepicker.js"></script>

<script>





var start = '{{ $start }}';
var end = '{{ $end }}';


$('#filterdate').daterangepicker({
    startDate: start,
    endDate: end,
    locale: {
        format: 'YYYY-MM-D'
    },
    ranges: {
       'Today': [moment(), moment()],
       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
       'Last 7 Days': [moment().subtract(6, 'days'), moment()],
       'Last 30 Days': [moment().subtract(29, 'days'), moment()],
       'This Month': [moment().startOf('month'), moment().endOf('month')],
       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
}, function(start,end){
      location.href = "?start="+start.format('YYYY-MM-D') +"&end="+end.format('YYYY-MM-D');     
});


  $("#form-laporan").submit(function(e){
    e.preventDefault();
    var link  = $(this).attr("action") +"?"+ $(this).serialize()
    window.open(link, "_blank"); 
  });

</script>
@endsection