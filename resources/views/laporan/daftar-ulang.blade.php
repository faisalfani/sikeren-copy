@extends('layouts.app')

@section('title')
    Laporan
@endsection
@section('css-after')
    <link rel="stylesheet"
        href="../../plugins/daterangepicker/daterangepicker.css">
    <style>
        table {
            background: #fff;
            border-radius: 10px;
        }

        table tr td,
        table tr th {
            padding: 2px 10px;
        }

    </style>
@endsection
@section('content')
    @php

    if ($req->start and $req->end) {
        $start = date($req->start);
        $end = date($req->end);
        $where = 'BETWEEN ' . $start . ' AND ' . $end;
    } else {
        $start = date('Y-m-d', strtotime('-1 month'));
        $end = date('Y-m-d');
        $where = 'BETWEEN ' . $start . ' AND ' . $end;
    }

    @endphp
    <div class="container">

        <h3 class="text-center">Laporan Daftar Ulang</h3>
        <form id="form-laporan"
            class="row"
            action="/laporan-daftar-ulang">
            <div class="col-md-8 offset-lg-2">
                <div class="row">



                    <div class="col-md-6">

                        <div class="form-group">
                            <label>Tahun Ajaran </label>
                            @php
                                $y = Date('Y') - 1;
                                
                                if (Date('m') < 7) {
                                    $tahunnn = Date('Y') - 1;
                                } else {
                                    $tahunnn = Date('Y');
                                }
                                
                            @endphp
                            <select name="du_tahun_ajaran"
                                class="form-control select2"
                                style="width: 100%;">
                                @for ($i = 0; $i < 3; $i++)
                                    <option @if ($y + $i == $tahunnn) selected @endif
                                        value="{{ $y + $i }}">Tahun Ajaran {{ $y + $i }} / {{ $y + $i + 1 }}
                                    </option>
                                @endfor
                            </select>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tingkat</label>
                            <select name="tingkat"
                                class="form-control">
                                <option selected
                                    value="SEMUA">SEMUA</option>
                                <option value="MTS">MTS</option>
                                <option value="MA">MA</option>
                            </select>
                        </div>
                    </div>



                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Gender</label>
                            <select name="gender"
                                class="form-control">
                                <option selected
                                    value="SEMUA">SEMUA</option>
                                <option value="L">Putra (P)</option>
                                <option value="P">Putri (Pi)</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Ref</label>
                            <select name="ref"
                                class="form-control">
                                <option selected
                                    value="0">SEMUA</option>
                                <option value="1">Cash</option>
                                <option value="2">Transfer</option>
                            </select>
                        </div>
                    </div>



                    <div class="col-md-6">


                        <div class="form-group">
                            <label>Pilih Format Laporan</label>
                            <select name="format_laporan"
                                required
                                id="format-laporan"
                                class="form-control select2"
                                style="width: 100%;">
                                <option value="">
                                    Pilih
                                </option>
                                <option value="seluruh">
                                    Seluruh
                                </option>
                                <option value="rentang">
                                    Rentang (Bulan/Tahun)
                                </option>
                                <option value="periode">
                                    Periode ( Tanggal )
                                </option>
                            </select>
                        </div>


                        <div class="row"
                            id="rentang">

                            <div class="col-md-6">

                                <div class="form-group">
                                    <label>Tahun</label>
                                    @php
                                        $y = Date('Y') - 1;
                                    @endphp
                                    <select name="tahun_ajaran"
                                        id="tahun_ajaran"
                                        class="form-control select2"
                                        style="width: 100%;">

                                        @for ($i = 0; $i < 3; $i++)
                                            <option @if ($y + $i == Date('Y')) selected @endif
                                                value="{{ $y + $i }}"
                                                value="{{ $y + $i }}">Tahun {{ $y + $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>




                            <div class="col-md-6">

                                <div class="form-group">
                                    <label>Bulan</label>
                                    <select name="bulan"
                                        id="bulan"
                                        class="form-control select2"
                                        style="width: 100%;">

                                        @php
                                            $bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                                        @endphp

                                        <option value="0">Semua Bulan</option>
                                        @foreach ($bulan as $key => $item)
                                            <option @if (Date('m') == $key + 1) selected @endif
                                                value="{{ $key + 1 }}">{{ $item }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>



                        </div>
                    </div>
                    <div class="col-md-12"
                        id="periode">
                        <div class="form-group">



                            <label>

                                Periode
                            </label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="far fa-calendar-alt"></i>
                                    </span>
                                </div>
                                <input required
                                    name="filter"
                                    type="text"
                                    class="form-control float-right"
                                    id="filterdate">
                            </div>


                        </div>
                    </div>

                    <div class="col-md-12 mt-2">
                        <button class="btn btn-primary mt-4 btn-block">
                            <i class="fa fa-print"></i> Lihat
                        </button>
                    </div>

                </div>
            </div>
        </form>

    </div>
@endsection

@section('js-after')
    <!-- InputMask -->
    <script src="../../plugins/inputmask/jquery.inputmask.bundle.js"></script>
    <script src="../../plugins/moment/moment.min.js"></script>
    <!-- date-range-picker -->
    <script src="../../plugins/daterangepicker/daterangepicker.js"></script>
    <script>
        var start = '{{ $start }}';
        var end = '{{ $end }}';

        $("#rentang").hide();
        $("#periode").hide();


        $("#format-laporan").change(function() {
            if ($(this).val() == 'rentang') {
                $("#rentang").show();
                $("#periode").hide();
            } else if ($(this).val() == 'periode') {
                $("#rentang").hide();
                $("#periode").show();
            } else {
                $("#rentang").hide();
                $("#periode").hide();
            }
        })

        $('#filterdate').daterangepicker({
            startDate: start,
            endDate: end,
            locale: {
                format: 'YYYY-MM-D'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf(
                    'month')]
            }
        }, function(start, end) {
            // location.href = "?start="+start.format('YYYY-MM-D') +"&end="+end.format('YYYY-MM-D');     
        });



        $("#form-laporan").submit(function(e) {
            e.preventDefault();
            var link = $(this).attr("action") + "?" + $(this).serialize()
            window.open(link, "_blank");
        });
    </script>
@endsection
