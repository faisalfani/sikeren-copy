@extends('layouts.excel')
@php
    function rupiahin($angka){
      return $angka;
      $hasil_rupiah = number_format($angka,0,',','.');
      return $hasil_rupiah;
    
    }

    $totalCell = (count($header['potongan']) * 2) + 12;
@endphp
@section('content')
<table >
    <tr>
        <td colspan="{{$totalCell}}" style="text-align:center;height:50px">
            {{-- <img class="logo" src="{{ url('/logo-pesantren.png') }}" alt="logo"> --}}
            <b style="font-size:110%">PONPES TERPADU  RIYADLUL ULUM WADDA'WAH</b>
            <br>
            <small >Condong, Ciberem, Kota Tasikmalaya, 
                <br>
                Telepon (0265) 7077821</small>
            <div style="height:3px;border-top:2px solid #ccc;border-bottom:2px solid #ccc;margin:10px 50px;margin-top:25px">
            </div>
        </td>
    </tr>
    <tr>
      <th rowspan="2" class="" style="text-align:center;">No</th>
      <th rowspan="2" class="" style="text-align:center;">Nama Nasabah</th>
      <th rowspan="2" class="" style="text-align:center;">Nomor Rekening</th>
      <th rowspan="2" class="" style="text-align:center;">Nama Bank</th>
      <th rowspan="2" class="" style="text-align:center;" colspan="2">Jumlah Pemasukan</th>
      <th  class="" style="text-align:center;" colspan="{{$header['potongan']->count() < 1 ? '2' :count($header['potongan']) * 2}}">POTONGAN</th>
      <th rowspan="2" class="" style="text-align:center;" colspan="2">TOTAL IHSAN</th>
      <th rowspan="2" class="" style="text-align:center;">KET</th>
    </tr>
    <tr>
      {{-- <th colspan="2">POKOK</th>
      <th colspan="2">LAIN2</th> --}}
      @foreach ($header['potongan'] as $item)
          <th colspan="2">{{$item->nama}}</th>
      @endforeach
      @if ($header['potongan']->count() < 1)
        <th colspan="2">-</th>
      @endif
    </tr>

    
    @foreach ($res as $key => $v)
        <tr>
          <td>{{$key + 1}}</td>
          <td style="text-align: left;">{{$v['nama']}}</td>
          <td style="text-align: left;">&nbsp;{{$v['no_rek']}}</td>
          <td style="text-align: left;">&nbsp;{{$v['bank']}}</td>
          {{-- <td style="">Rp </td>
          <td style=" text-align: right !important;">{{rupiahin($v['pokok'])}}</td>
          <td style="">Rp </td>
          <td style=" text-align: right !important;">{{rupiahin($v['lain2'])}}</td> --}}
          <td style="">Rp </td>
          <td style=" text-align: right !important;">{{rupiahin($v['total'])}}</td>
          @foreach ($v['potongan'] as $item)
            <td style="">Rp </td>
            <td style=" text-align: right !important;">{{rupiahin($item->jumlah_ihsan)}}</td>
          @endforeach
          @if (count($v['potongan']) < 1)
          <td style="">Rp </td>
          <td style=" text-align: right !important;">-</td>
          @endif
          <td style="">Rp </td>
          <td style=" text-align: right !important;">{{rupiahin($v['diterima'])}}</td>
          <td style=""></td>
        </tr>
    @endforeach

    <tr>
      <td colspan="4"></td>
      {{-- <td style="">Rp </td>
      <td style=" text-align: right !important;">{{rupiahin($jumlah->pokok)}}</td>
      <td style="">Rp </td>
      <td style=" text-align: right !important;">{{rupiahin($jumlah->lain2)}}</td> --}}
      <td style="">Rp </td>
      <td style=" text-align: right !important;">{{rupiahin($jumlah->total)}}</td>
      @foreach ($jumlah->potongan as $item)
        <td style="">Rp </td>
        <td style=" text-align: right !important;">{{rupiahin($item)}}</td>
      @endforeach
      @if (count($jumlah->potongan) < 1)
        <td style="">Rp </td>
        <td style=" text-align: right !important;">-</td>
      @endif
      <td style="">Rp </td>
      <td style=" text-align: right !important;">{{rupiahin($jumlah->diterima)}}</td>
      <td style=""></td>
    </tr>

  </table>
@endsection