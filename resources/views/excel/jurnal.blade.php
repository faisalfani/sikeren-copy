
@extends('layouts.excel')

@section('content')

@php
$tgl  =     explode(" ",$req->filter);
$tgl_awal = $tgl[0];
$tgl_akhir = $tgl[2];

$tgl_aw = $tgl[0];
$tgl_ak = $tgl[2];



if($req->format_laporan == 'rentang'){
    if($req->bulan == 0){
        $pr = " Tahun " .$req->tahun_ajaran;

        $tgl_awal = date("$req->tahun_ajaran-01-01");
        $tgl_akhir = date("$req->tahun_ajaran-12-31");

        $tgl_aw = date("$req->tahun_ajaran-01-01");
        $tgl_ak = date("$req->tahun_ajaran-12-31");

    }else{
        $pr = bulan_text($req->bulan) ." ". $req->tahun_ajaran;

        $tgl_awal = date("$req->tahun_ajaran-$req->bulan-01");
        $tgl_akhir = date("$req->tahun_ajaran-$req->bulan-31");

        $tgl_aw = date("$req->tahun_ajaran-$req->bulan-01");
        $tgl_ak = date("$req->tahun_ajaran-$req->bulan-31");
   
    }

}else{
    $pr = tgl_indo($tgl_awal) ." - ". tgl_indo($tgl_akhir);

}



$saldo_akhir = Modules\Keuangan\Entities\Jurnal::
                    where("ref",1)
                    ->where("tgl_bayar","<",$tgl_aw)
                    ->sum("jumlah")
            - 
            Modules\Keuangan\Entities\Jurnal::
                    where("ref",2)
                    ->where("tgl_bayar","<",$tgl_aw)
                    ->sum("jumlah");

@endphp

            <table>
                <thead>
                <tr>
                        <td colspan="5" style="text-align:center;height:50px">
                        {{-- <img class="logo" src="{{ url('/logo-pesantren.png') }}" alt="logo"> --}}
                        <b style="font-size:110%">PONPES TERPADU  RIYADLUL ULUM WADDA'WAH</b>
                        <br>
                        <small >Condong, Ciberem, Kota Tasikmalaya, 
                            <br>
                            Telepon (0265) 7077821</small>
                        <div style="height:3px;border-top:2px solid #ccc;border-bottom:2px solid #ccc;margin:10px 50px;margin-top:25px">
                        </div>
                    </td>
                    </tr>
                    <tr>
                        <td colspan="5"></td>
                    </tr>
                    <tr>
                        <td colspan="5" style="text-align:center">
                                <b> LAPORAN JURNAL </b>
                        </td>
                    </tr>
                    <tr>
                            <td colspan="5"></td>
                        </tr>
                    <tr>
                        <td colspan="5" style="text-align:center">
                            <b>Periode {{ $pr }}</b>
                        </td>
                    </tr>
                    <tr>
                            <td colspan="5"></td>
                        </tr>
                 
                            
                    <tr>
                            <th style="width:20px">Tanggal</th>
                            <th style="width:25px">Keterangan</th>
                            <th style="width:20px">D</th>
                            <th style="width:20px">K</th>
                            <th style="width:20px">Saldo</th>
                    </tr>
                </thead>
                <tbody style="text-align:left;">

                <tr>
                    <td >
                        {{-- {{ tgl_indo($tgl_aw) }}     --}}
                    </td>    
                    <td>
                        Saldo Bulan Lalu
                    </td>
                    <td style="text-align:right">
                            
                    </td>
                    <td style="text-align:right">
                        
                    </td>
                    <td>
                        {{ rupiah($saldo_akhir)}}
                    </td>   
                </tr>  
               
                @while(strtotime($tgl_aw) <= strtotime($tgl_ak))
                    @php
                    $jenisBiaya = Modules\Keuangan\Entities\JenisBiaya::orderBy("nama","ASC")->get();

                    $tgl_cek = '';
                                        
                    $jml_bank = Modules\Keuangan\Entities\Jurnal::
                    where("ref",2)
                    ->where("tgl_bayar",$tgl_aw)
                    ->sum("jumlah");
                    @endphp
                    
                    @if ($jml_bank > 0)
                        
                     <tr>
                        <td >
                            {{ tgl_indo($tgl_aw) }}    
                        </td>    
                        <td>
                            Setoran Transfer
                        </td>
                        <td style="text-align:right">
                                
                        </td>
                        <td style="text-align:right">
                           
                            {{ rupiah($jml_bank) }} 
                        </td>
                        <td>
                            {{ rupiah($saldo_akhir-=$jml_bank)}}
                        </td>   
                    </tr>    
                    @endif
                    @foreach ($jenisBiaya  as $item)

                    @php
                         $jurnal = Modules\Keuangan\Entities\Jurnal::
                            where("ref",1)
                            ->where("type",1)
                            ->where("tgl_bayar",$tgl_aw)
                            ->where("id_jenis_biaya",$item->id);
                    @endphp
                    @if ($jurnal->get()->count() > 0)
                     
                          <tr>
                            <td >
                                @if ($jml_bank == 0)
                                    
                                {{ tgl_indo($tgl_aw) }}  
                                
                             
                                @endif  
                            </td>    
                            <td>
                                {{ $item->nama }}    
                            </td>
                            <td style="text-align:right">
                                {{ rupiah($jurnal->sum('jumlah')) }} 
                            </td>
                            <td style="text-align:right">

                            </td>
                            <td>
                                {{ rupiah($saldo_akhir+=$jurnal->sum('jumlah'))}}
                            </td>
                    </tr>  
                       
                    @endif 
                    @php
                        $tgl_cek = $tgl_aw;
                        $jml_bank == 0;
                    @endphp
                    @endforeach
                
                    @php
                        $tgl_aw = date ("Y-m-d", strtotime("+1 day", strtotime($tgl_aw)));
                     
                    @endphp
                @endwhile
                

            </tbody>
            </table>
@endsection