@extends('layouts.excel')

@section('title')
      Laporan
@endsection

@section('content')
@php

$tgl_awal = $start;
$tgl_akhir = $end;

$tgl_aw = $start;
$tgl_ak = $end;



$pr = tgl_indo($tgl_awal) ." - ". tgl_indo($tgl_akhir);


$where = "id_jenis_biaya = 4";
$whereJenis = "id = 4";


@endphp

            
            <table>
                    <tr>
                        <td colspan="5" style="text-align:center;height:50px">
                            {{-- <img class="logo" src="{{ url('/logo-pesantren.png') }}" alt="logo"> --}}
                            <b style="font-size:110%">PONPES TERPADU  RIYADLUL ULUM WADDA'WAH</b>
                            <br>
                            <small >Condong, Ciberem, Kota Tasikmalaya, 
                                <br>
                                Telepon (0265) 7077821</small>
                            <div style="height:3px;border-top:2px solid #ccc;border-bottom:2px solid #ccc;margin:10px 50px;margin-top:25px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5"></td>
                    </tr>
                    <tr>
                        <td colspan="5" style="text-align:center">
                               <b> LAPORAN TABUNGAN SISWA </b>
                               <br>
                               {{ $siswa->nama }}
                              
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5"></td>
                    </tr>
                    <tr>
                        <td colspan="5" style="text-align:center">
                            <b>Periode {{ $pr }}</b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5"></td>
                    </tr>
                    <tr  style="text-align:center;">
                        <th><b>Jam</b></th>
                        <th><b>Tanggal</b></th>
                        <th><b>Uang Masuk</b></th>
                        <th><b>Uang Keluar</b></th>
                        <th style="text-align:right">Saldo Akhir</th>
                    </tr>

                    @php
                         $jurnal = Modules\Keuangan\Entities\Jurnal::
                            where("nis",$nis)
                            ->where("tgl_bayar",">=",$start)
                            ->where("tgl_bayar","<=",$end)
                            ->where("id_jenis_biaya",'4')
                            ->get();
                            // dd($jurnal);
                    @endphp

                  @foreach ($jurnal as $item)
                   
                          <tr>
                              <td>
                                @php
                                $tgll = explode(" ",$item->created_at);
                                @endphp
                                {{ $tgll[1]}} 
                              </td>
                            <td >
                                {{ tgl_indo($tgll[0]) }}  
                            </td>    
                           
                            <td style="text-align:right">
                                @if($item->type == 1)
                                {{ rupiah($item->jumlah) }} 
                                @endif
                            </td>
                            <td style="text-align:right">
                                @if($item->type == 0)
                                {{ rupiah($item->jumlah) }} 
                                @endif
                            </td>
                            <td style="text-align:right" >
                                {{ rupiah($item->saldo_akhir) }} 
                            </td>
                    </tr>  
                          
                  @endforeach

            </table>
@endsection

@section('js-after')
@endsection