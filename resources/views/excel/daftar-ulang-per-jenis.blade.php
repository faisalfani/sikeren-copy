
@php

$tgl  =     explode(" ",$req1->filter);

$tgl_awal = $tgl[0];
$tgl_akhir = $tgl[2];


if($req1->format_laporan == 'rentang'){
    if($req1->bulan == 0){
        $pr = " Tahun " .$req1->tahun_ajaran;

        $where = " YEAR(jurnals.tgl_bayar) = $req1->tahun_ajaran";

    }else{
        $pr = bulan_text($req1->bulan) ." ". $req1->tahun_ajaran;
        
        $where = " YEAR(jurnals.tgl_bayar) = $req1->tahun_ajaran AND MONTH(jurnals.tgl_bayar) = $req1->bulan ";

    }

}else if($req1->format_laporan == 'periode'){
    $pr = tgl_indo($tgl_awal) ." - ". tgl_indo($tgl_akhir);

    $where = " jurnals.tgl_bayar >= '$tgl_awal' AND jurnals.tgl_bayar <= '$tgl_akhir'";

}else{
    $pr = "Seluruh";

    $where = "jurnals.tgl_bayar";
}

if($req1->tingkat !== 'SEMUA'){
    $where .= " AND s.jenjang = '$req1->tingkat' ";
}

if($req1->gender !== 'SEMUA'){
    $where .= " AND s.jk = '$req1->gender' ";
}

if($req1->ref > 0){
    $where .= " AND jurnals.ref = '$req1->ref' ";
}

@endphp
@extends('layouts.excel')

@section('content')
<table>
        <tr>
            <td colspan="3" style="text-align:center;height:50px">
            {{-- <img class="logo" src="{{ url('/logo-pesantren.png') }}" alt="logo"> --}}
            <b style="font-size:110%">PONPES TERPADU  RIYADLUL ULUM WADDA'WAH</b>
            <br>
            <small >Condong, Ciberem, Kota Tasikmalaya, 
                <br>
                Telepon (0265) 7077821</small>
            <div style="height:3px;border-top:2px solid #ccc;border-bottom:2px solid #ccc;margin:10px 50px;margin-top:25px">
            </div>
        </td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td colspan="3" style="text-align:center">
                   <b> LAPORAN DAFTAR ULANG 
                    @if($req1->tingkat !== 'SEMUA')
                        {{ $req1->tingkat }}
                    @endif
                    @if($req1->tingkat !== 'SEMUA')
                    ({{ ($req->gender == 'L') ? '(P) Putra' : '(Pi) Putri' }})
                    @endif
                   </b>
                   <br>
                   <b> {{ $jenis->nama }}</b>
                   <br>
                   <b> Tahun Ajaran {{ $req1->du_tahun_ajaran }} / {{ $req1->du_tahun_ajaran+1 }}</b>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align:center">
                <b>Periode {{ $pr }}</b>
            </td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
             
        <tr>
            <th style="width:5px" >No</th>
            <th style="width:30px;">Nama</th>
            <th style="width:30px;text-align:right;">Jumlah</th>
        </tr>
        @php
                $data =  Modules\Keuangan\Entities\Jurnal::
                join("biaya_siswas as b","b.id","jurnals.biaya_id")
                ->join("siswas as s","s.nis","jurnals.nis")
                ->where("jurnals.id_jenis_biaya",$jenis->id)
                ->where("b.tahun_ajaran",$req1->du_tahun_ajaran)
                ->where("b.satuan","Daftar Ulang")
                ->where("b.bulan",7)
                ->where("jurnals.type",1)
                ->whereRaw($where)
                ->select("jurnals.*","s.nama as nama_siswa")
                ->orderBy("s.nama","ASC")
                ->get();
        @endphp
        @foreach ($data as $key => $d)
            
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $d->nama_siswa }}</td>
            <td>
                {{ $d->jumlah }}
            </td>
        </tr>

        @endforeach
        <tr>
            <th> </th>
            <th>
                <b>Total Pendapatan</b>
            </th>
            <th style="text-align:right">
               <b> {{ $req->jumlah }}</b>
            </th>
        </tr>


</table>

                    @endsection