@extends('layouts.excel')
@section('content')

            <table >
                    <thead>  
                        <thead>
                            <tr>
                                    <td colspan="3" style="text-align:center;height:50px">
                                    {{-- <img class="logo" src="{{ url('/logo-pesantren.png') }}" alt="logo"> --}}
                                    <b style="font-size:110%">PONPES TERPADU  RIYADLUL ULUM WADDA'WAH</b>
                                    <br>
                                    <small >Condong, Ciberem, Kota Tasikmalaya, 
                                        <br>
                                        Telepon (0265) 7077821</small>
                                    <div style="height:3px;border-top:2px solid #ccc;border-bottom:2px solid #ccc;margin:10px 50px;margin-top:25px">
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align:center">
                                            <b> LAPORAN TAGIHAN KELAS</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align:center">
                                        <b>{{ $kelas->nama }}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                  <th style="width:5px" ><b>No</b></th>
                                  <th style="width:30px;"><b>Keterangan</b></th>
                                  <th style="width:30px;text-align:right;"><b>Tagihan</b></th>
                                </tr>
                    </thead>
                    <tbody>
                    @foreach ($res as $key => $s)
                        <tr>
                          <td> {{ $key+1 }}</td>
                          <td> {{ $s['nama'] }}</td>
                          <td> {{ $s['tagihan'] }}</td>
                         
                        </tr>
                    @endforeach
                    </tbody>
                  </table>


@endsection