
@extends('layouts.excel')

@section('content')


@php
$tgl  =     explode(" ",$req->filter);
$tgl_awal = $tgl[0];
$tgl_akhir = $tgl[2];

$tgl_aw = $tgl[0];
$tgl_ak = $tgl[2];



if($req->format_laporan == 'rentang'){
    if($req->bulan == 0){
        $pr = " Tahun " .$req->tahun_ajaran;

        $tgl_awal = date("$req->tahun_ajaran-01-01");
        $tgl_akhir = date("$req->tahun_ajaran-12-31");

        $tgl_aw = date("$req->tahun_ajaran-01-01");
        $tgl_ak = date("$req->tahun_ajaran-12-31");

    }else{
        $pr = bulan_text($req->bulan) ." ". $req->tahun_ajaran;

        $tgl_awal = date("$req->tahun_ajaran-$req->bulan-01");
        $tgl_akhir = date("$req->tahun_ajaran-$req->bulan-31");

        $tgl_aw = date("$req->tahun_ajaran-$req->bulan-01");
        $tgl_ak = date("$req->tahun_ajaran-$req->bulan-31");
   
    }

}else{
    $pr = tgl_indo($tgl_awal) ." - ". tgl_indo($tgl_akhir);

}


if($req->jenis_biaya == 0){
    $where = 'id';
    $whereJenis = 'id';
}else{
    $where = "id_jenis_biaya = '$req->jenis_biaya'";
    $whereJenis = "id = '$req->jenis_biaya'";
}


@endphp


<table>
        <tr>
            <td colspan="5" style="text-align:center;height:50px">
            {{-- <img class="logo" src="{{ url('/logo-pesantren.png') }}" alt="logo"> --}}
            <b style="font-size:110%">PONPES TERPADU  RIYADLUL ULUM WADDA'WAH</b>
            <br>
            <small >Condong, Ciberem, Kota Tasikmalaya, 
                <br>
                Telepon (0265) 7077821</small>
            <div style="height:3px;border-top:2px solid #ccc;border-bottom:2px solid #ccc;margin:10px 50px;margin-top:25px">
            </div>
        </td>
        </tr>
        <tr>
            <td colspan="5"></td>
        </tr>
        <tr>
            <td colspan="5" style="text-align:center">
                   <b> LAPORAN PER JENIS BIAYA </b>
            </td>
        </tr>
        <tr>
            <td colspan="5"></td>
        </tr>
        <tr>
            <td colspan="5" style="text-align:center">
                <b>Periode {{ $pr }}</b>
            </td>
        </tr>
        <tr>
            <td colspan="5"></td>
        </tr>
</table>
            @foreach ($jenisBiaya = Modules\Keuangan\Entities\JenisBiaya::orderBy("nama","ASC")
            ->whereRaw($whereJenis)
            ->get() as $item)

                @php
                    $saldo_akhir = Modules\Keuangan\Entities\Jurnal::
                            where("ref",1)
                            ->where("tgl_bayar","<",$tgl_aw)
                            ->where("id_jenis_biaya",$item->id)
                            ->sum("jumlah")
                    - 
                    Modules\Keuangan\Entities\Jurnal::
                            where("ref",2)
                            ->where("tgl_bayar","<",$tgl_aw)
                            ->where("id_jenis_biaya",$item->id)
                            ->sum("jumlah");
                @endphp
                
            <table>
                <thead>
                    <tr>
                        <td colspan="5"></td>
                    </tr>
                    <tr>
                        <th colspan="5" style="text-align:center"><b> {{ $item->nama }} </b></th>
                    </tr>
                    <tr>
                        <td colspan="5"></td>
                    </tr>
                </thead>
                <tbody >
                <tr  style="text-align:center;">
                    <th style="font-weight:bold;width:20px">Tanggal</th>
                    <th style="font-weight:bold;width:30px">Keterangan</th>
                    <th style="font-weight:bold;width:15px">D</th>
                    <th style="font-weight:bold;width:15px">K</th>
                    <th style="font-weight:bold;text-align:right;width:20px">Saldo</th>
                </tr>
                <tr>
                    <td>

                    </td>    
                    <td>
                        Saldo Bulan Lalu
                    </td>
                    <td style="text-align:right">
                            
                    </td>
                    <td style="text-align:right">
                        
                    </td>
                    <td style="text-align:right">
                        {{ $saldo_akhir}}
                    </td>   
                </tr>  
            
                @while(strtotime($tgl_aw) <= strtotime($tgl_ak))
                    @php

                    $tgl_cek = '';
                                        
                    $jml_bank = Modules\Keuangan\Entities\Jurnal::
                    where("ref",2)
                    ->where("tgl_bayar",$tgl_aw)
                    ->whereRaw($where)
                    ->sum("jumlah");
                    @endphp
                    
                    
                    
                    @if ($jml_bank > 0)
                        
                     <tr>
                        <td >
                            {{ tgl_indo($tgl_aw) }}    
                        </td>    
                        <td>
                            Setoran Transfer
                        </td>
                        <td style="text-align:right">
                                
                        </td>
                        <td style="text-align:right">
                           
                            {{ $jml_bank }} 
                        </td>
                        <td >
                            {{ $saldo_akhir -= $jml_bank}}
                        </td>   
                    </tr>    
                    @endif
                    @foreach ($jenisBiaya  as $item)

                    @php
                         $jurnal = Modules\Keuangan\Entities\Jurnal::
                            where("ref",1)
                            ->where("type",1)
                            ->where("tgl_bayar",$tgl_aw)
                            ->where("id_jenis_biaya",$item->id);
                    @endphp
                    @if ($jurnal->get()->count() > 0)
                     
                          <tr>
                            <td >
                                @if ($jml_bank == 0)
                                    
                                {{ tgl_indo($tgl_aw) }}  
                                
                             
                                @endif  
                            </td>    
                            <td>
                                {{ $item->nama }}    
                            </td>
                            <td style="text-align:right">
                                {{ $jurnal->sum('jumlah') }} 
                            </td>
                            <td style="text-align:right">

                            </td>
                            <td style="text-align:right" >
                                {{ $saldo_akhir+=$jurnal->sum('jumlah')}}
                            </td>
                    </tr>  
                       
                    @endif 
                    @php
                        $tgl_cek = $tgl_aw;
                        $jml_bank == 0;
                    @endphp
                    @endforeach
                
                    @php
                        $tgl_aw = date ("Y-m-d", strtotime("+1 day", strtotime($tgl_aw)));
                     
                    @endphp
                @endwhile
                

            </tbody>
            </table>
            <br>
            <br>

@endforeach
@endsection