@extends('layouts.excel')
@section('content')
@php
    $data = ['colspan' => "6", 
              'judul' => "Laporan Saldo Tabungan Siswa (TABSAN)"];
@endphp
            <table >
                        <thead>
                            @include('excel.comp.header_excel', $data)
                              <tr>
                                <th rowspan="2" style="width:5px">No</th>
                                <th rowspan="2" style="width:15px">NIS</th>
                                <th rowspan="2" style="width:35px">Nama</th>
                                <th colspan="3">SALDO</th>
                              </tr>
                              <tr>
                                <th style="width:15px">Masuk</th>
                                <th style="width:15px">Keluar</th>
                                <th style="width:15px">Sisa</th>
                              </tr>
                    </thead>
                    <tbody class="isi">
                    @php
                      $totalMasuk = 0;
                      $totalKeluar = 0;
                      $totalSisa = 0;
                      $result = json_decode($res, true);
                    @endphp
                    @foreach ($result as $key => $s)
                    @php
                      $totalMasuk += $s['masuk'];
                      $totalKeluar += $s['keluar'];
                      $totalSisa += $s['saldo'];
                    @endphp
                        <tr>
                          <td style="text-align: center;"> {{ $key+1 }}</td>
                          <td> {{ $s['nis'] }}</td>
                          <td> {{ $s['nama'] }}</td>
                          <td> {{ $s['masuk'] }}</td>
                          <td> {{ $s['keluar'] }}</td>
                          <td> {{ $s['saldo'] }}</td>
                        </tr>
                    @endforeach
                     <tr>
                          <td colspan="3" style="text-align:right">Total</td>
                          <td> {{ $totalMasuk }}</td>
                          <td> {{ $totalKeluar }}</td>
                          <td> {{ $totalSisa }}</td>
                        </tr>
                      <tr>
                        <td colspan="6" style="font-size:10px;">Di cetak oleh : {{ $user}}</td>
                      </tr>
                    </tbody>
                  </table>


@endsection