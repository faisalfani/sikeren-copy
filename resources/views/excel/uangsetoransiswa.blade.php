@extends('layouts.excel')

@php
$tgl = explode(' ', $req->filter);

$tgl_awal = $tgl[0];
$tgl_akhir = $tgl[2];

if ($req->format_laporan == 'rentang') {
    if ($req->bulan == 0) {
        $pr = ' Tahun ' . $req->tahun_ajaran;

        $where = " tahun_ajaran = $req->tahun_ajaran";
    } else {
        $pr = bulan_text($req->bulan) . ' ' . $req->tahun_ajaran;

        $where = " tahun_ajaran = $req->tahun_ajaran AND bulan = $req->bulan ";
    }
} elseif ($req->format_laporan == 'periode') {
    $pr = tgl_indo($tgl_awal) . ' - ' . tgl_indo($tgl_akhir);

    if ($tgl_awal == $tgl_akhir) {
        $pr = tgl_indo($tgl_awal);
    }
    $awal = explode('-', $tgl_awal);
    $akhir = explode('-', $tgl_akhir);

    $bulan_awal = (int) $awal[1];
    $bulan_akhir = (int) $akhir[1];
    $where = " (bulan >= '$bulan_awal' AND tahun_ajaran >= '$awal[0]') AND (bulan <= '$bulan_akhir' AND tahun_ajaran <= '$akhir[0]')";
} else {
    $pr = 'Seluruh';

    $where = 'bulan';
}

if ($req->jenjang != 'All') {
    switch ($req->jenjang) {
        case '1':
            $where .= " AND jenjang = 'MTS'";
            break;

        default:
            $where .= " AND jenjang = 'MA'";
            break;
    }
    // $where += $req->jenjang;
}

if ($req->kelas != 'All') {
    $where .= " AND id_kelas = '$req->kelas'";
}

$data = DB::table('siswas')
    ->leftJoin('biaya_siswas', 'siswas.nis', '=', 'biaya_siswas.nis')
    ->where('no_hp', '>', 0)
    ->select('biaya_siswas.nis', 'siswas.nama', 'siswas.kelas', 'siswas.jenjang', DB::raw('sum(biaya_siswas.nominal) - sum(biaya_siswas.dibayar) as totalNominal'))
    ->groupBy('siswas.nis')
    ->having('totalNominal', '>', 0)
    ->whereRaw($where)
    ->orderBy('siswas.kelas', 'ASC')
    ->orderBy('siswas.nis', 'ASC')
    ->get();

if ($req->jenjang != 'All') {
    if ($req->jenjang == 1) {
        $jenjangData = 'MTS';
    } else {
        $jenjangData = 'MA';
    }
} else {
    $jenjangData = '';
}
$kelasData = '';
if ($req->kelas != 'All') {
    $kelasData = Modules\Master\Entities\Kelas::find($req->kelas);
}

@endphp

@section('content')
    <table>
        <thead>
            <tr>
                <td colspan="6"
                    style="text-align:center;height:50px">
                    {{-- <img class="logo" src="{{ url('/logo-pesantren.png') }}" alt="logo"> --}}
                    <b style="font-size:110%">PONPES TERPADU RIYADLUL ULUM WADDA'WAH 2</b>
                    <br>
                    <small>Condong, Ciberem, Kota Tasikmalaya,
                        <br>
                        Telepon (0265) 7077821</small>
                    <div
                        style="height:3px;border-top:2px solid #ccc;border-bottom:2px solid #ccc;margin:10px 50px;margin-top:25px">
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="6"></td>
            </tr>
            <tr>
                <td colspan="6"
                    style="text-align:center">
                    <b> Laporan Tagihan Keuangan Siswa </b>
                </td>
            </tr>

            <tr>
                <td colspan="6"
                    style="text-align:center">
                    <b>Periode {{ $pr }}</b>
                </td>
            </tr>
            <tr>
                <td colspan="6"
                    style="text-align:center">
                    @if ($jenjangData)
                        <b>Jenjang {{ $jenjangData }} {{ $kelasData ? 'Kelas ' . $kelasData->nama : '' }}</b>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="6"></td>
            </tr>


            <tr>
                <th style="border-left:1px solid #ccc">No</th>
                <th style="border-left:1px solid #ccc">NISN</th>
                <th style="border-left:1px solid #ccc">Nama</th>
                <th style="border-left:1px solid #ccc">Kelas</th>
                <th style="border-left:1px solid #ccc">Jenjang</th>
                <th style="text-align:right;border-left:1px solid #ccc;border-right:1px solid #ccc">Tagihan</th>
            </tr>
        </thead>
        <tbody>

            @foreach ($data as $key => $d)
                <tr>
                    <td class="kecilin"
                        style="text-align: center;">{{ $key + 1 }}</td>
                    <td class="kecilin">{{ $d->nis }}</td>
                    <td class="kecilin"
                        style="white-space:nowrap">{{ $d->nama }}</td>
                    <td class="kecilin"
                        style="white-space:nowrap">{{ $d->kelas }}</td>
                    <td class="kecilin"
                        style="text-align: center">{{ $d->jenjang }}</td>
                    <td class="kecilin"
                        style="text-align:right;border-right:1px solid #ccc;white-space:nowrap;"> {{ $d->totalNominal }}
                    </td>
                </tr>
            @endforeach

            <tr>
                <th colspan="5">
                    Total Tagihan
                </th>
                <th style="text-align:right;white-space:nowrap;">
                    {{ $data->sum('totalNominal') }}
                </th>
            </tr>
        </tbody>
    </table>
@endsection
