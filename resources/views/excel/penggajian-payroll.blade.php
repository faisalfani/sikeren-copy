@extends('layouts.excel')
@php
    function rupiahin($angka){
      return $angka;
      $hasil_rupiah = number_format($angka,0,',','.');
      return $hasil_rupiah;
    
    }

    $totalCell = (count($header['potongan']) * 2) + 12;
@endphp
@section('content')
<table >
    <tr>
        <td colspan="{{$totalCell}}" style="text-align:center;height:50px">
            {{-- <img class="logo" src="{{ url('/logo-pesantren.png') }}" alt="logo"> --}}
            <b style="font-size:110%">PONPES TERPADU  RIYADLUL ULUM WADDA'WAH</b>
            <br>
            <small >Condong, Ciberem, Kota Tasikmalaya, 
                <br>
                Telepon (0265) 7077821</small>
            <div style="height:3px;border-top:2px solid #ccc;border-bottom:2px solid #ccc;margin:10px 50px;margin-top:25px">
            </div>
        </td>
    </tr>
    <tr>
      <th class="" style="text-align:center; width: 1%;">No</th>
      <th class="" style="text-align:center;">Nama Nasabah</th>
      <th class="" style="text-align:center;">Nomor Rekening</th>
      <th class="" style="text-align:center;" colspan="2">TOTAL IHSAN</th>
      <th class="" style="text-align:center;">KET</th>
    </tr>

    
    @foreach ($res as $key => $v)
        <tr>
          <td>{{$key + 1}}</td>
          <td style="text-align: left;">{{$v['nama']}}</td>
          <td style="text-align: left;">&nbsp;{{$v['no_rek']}}</td>
          <td style="width:1%;">Rp </td>
          <td style="text-align: right !important;">{{rupiahin($v['diterima'])}}</td>
          <td style="width:100px;"></td>
        </tr>
    @endforeach
  </table>
@endsection