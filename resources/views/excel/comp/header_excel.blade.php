@php
    $info = Modules\Core\Entities\CoreInfo::first();
@endphp
<tr>
   <td colspan="{{$data['colspan']}}"></td>
</tr>
<tr>
   <td colspan="{{$data['colspan']}}" style="text-align:center;height:80px">
      {{-- <img class="logo" src="{{ url('/logo-pesantren.png') }}" alt="logo"> --}}
      <b style="font-size:110%">{{$info ? $info->nama : 'CV Alphabet'}}</b>
      <br>
      <small >{{$info ? $info->alamat : 'Lorem ipsum dolor sit amet'}},
        @if ($info->no_telp)
            <br>
            Telepon {{$info->no_telp}}
        @endif
        </small>
      <div style="height:3px;border-top:2px solid #ccc;border-bottom:2px solid #ccc;margin:10px 50px;margin-top:25px">
      </div>
   </td>
</tr>
<tr>
   <td colspan="{{$data['colspan']}}"></td>
</tr>
<tr>
   <td colspan="{{$data['colspan']}}" style="text-align:center">
      <b>{{$data['judul']}}</b>
   </td>
</tr>
<tr>
   <td colspan="{{$data['colspan']}}"></td>
</tr>