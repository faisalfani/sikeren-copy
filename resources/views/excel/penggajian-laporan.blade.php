@extends('layouts.excel')
@php
function rupiahin($angka)
{
    return $angka;
    $hasil_rupiah = number_format($angka, 0, ',', '.');
    return $hasil_rupiah;
}

$totalCell = count($header['tunjangan']) * 2 + count($header['potongan']) * 2 + 14;
@endphp
@section('content')
    <table>
        <tr>
            <td colspan="{{ $totalCell }}"
                style="text-align:center;height:50px">
                {{-- <img class="logo" src="{{ url('/logo-pesantren.png') }}" alt="logo"> --}}
                <b style="font-size:110%;text-transform:uppercase;">{{ $req->tipe ? $header['tipe']->nama : '' }}</b>
                <br>
                <small>MTS-MA TERPADU RIYADLUL 'ULUM
                    <br>
                    PESANTREN RIYADLUL 'ULUM WADDA'WAH</small>
                <div
                    style="height:3px;border-top:2px solid #ccc;border-bottom:2px solid #ccc;margin:10px 50px;margin-top:25px">
                </div>
            </td>
        </tr>
        <tr>
            <th style="text-align:center; width: 1%;"
                rowspan="2">No</th>
            <th style="text-align:center;"
                rowspan="2">Nama</th>
            <th style="text-align:center;"
                rowspan="2">Tahun</th>
            <th class=""
                style="text-align:center;width:1%;"
                rowspan="2">Sertifikasi</th>
            <th style="text-align:center;"
                colspan="3">IHSAN POKOK</th>
            <th style="text-align:center;"
                colspan="3">IHSAN JAM KERJA</th>
            <th style="text-align:center;"
                colspan="{{ count($header['tunjangan']) * 2 + 2 }}">TUNJANGAN</th>
            <th style="text-align:center;"
                colspan="{{ count($header['potongan']) * 2 }}">POTONGAN</th>
            <th style="text-align:center;"
                rowspan="2"
                colspan="2">TOTAL IHSAN</th>
        </tr>
        <tr>
            <th style="text-align:center;">MK</th>
            <th style="text-align:center;"
                colspan="2">Jumlah</th>
            <th style="text-align:center;">JP</th>
            <th style="text-align:center;"
                colspan="2">Jumlah</th>
            <th style="text-align:center;"
                colspan="2">Jabatan</th>

            @foreach ($header['tunjangan'] as $item)
                <th style="text-align:center;"
                    colspan="2">{{ $item->nama }}</th>
            @endforeach
            @foreach ($header['potongan'] as $item)
                <th style="text-align:center;"
                    colspan="2">{{ $item->nama }}</th>
            @endforeach

        </tr>
        @php
            $no = 0;
        @endphp
        @foreach ($res as $key => $z)
            <tr>
                <td colspan="{{ $totalCell }}"
                    style="text-align: left;padding:2px;background:#dede">{{ $key }}</td>
            </tr>
            @foreach ($z as $v)
                <tr>
                    <td>{{ $no + 1 }}</td>
                    <td style="text-align: left !important;">{{ $v['nama'] }}</td>
                    <td>{{ $v['tahun'] }}</td>
                    <td>{{ $v['sertifikasi'] == 1 ? 'Y' : 'T' }}</td>
                    <td>{{ $v['mk'] }}</td>
                    <td style="width: 1%;">Rp</td>
                    <td style="text-align: right;">{{ rupiahin($v['ip_jumlah']) }}</td>
                    <td>{{ $v['jp'] }}</td>
                    <td style="width: 1%;">Rp</td>
                    <td style="text-align: right;">{{ rupiahin($v['ijk_jumlah']) }}</td>
                    <td style="width: 1%;">Rp</td>
                    <td style="text-align: right;">{{ rupiahin($v['tunjangan_jabatan']) }}</td>
                    @foreach ($v['tunjangan'] as $item)
                        <td style="width: 1%;">Rp</td>
                        <td style="text-align: right;">{{ rupiahin($item->jumlah_ihsan) }}</td>
                    @endforeach
                    @foreach ($v['potongan'] as $item)
                        <td style="width: 1%;">Rp</td>
                        <td style="text-align: right;">{{ rupiahin($item->jumlah_ihsan) }}</td>
                    @endforeach
                    <td style="width: 1%;">Rp</td>
                    <td style="text-align: right;">{{ rupiahin($v['total']) }}</td>
                </tr>
                @php
                    $no++;
                @endphp
            @endforeach
        @endforeach
        <tr>
            <td colspan="5"></td>
            <td style="width: 1%;font-weight:bold;">Rp</td>
            <td style="text-align: right;font-weight:bold;">{{ rupiahin($jumlah->ip_jumlah) }}</td>
            <td></td>
            <td style="width: 1%;font-weight:bold;">Rp</td>
            <td style="text-align: right;font-weight:bold;">{{ rupiahin($jumlah->ijk_jumlah) }}</td>
            <td style="width: 1%;font-weight:bold;">Rp</td>
            <td style="text-align: right;font-weight:bold;">{{ rupiahin($jumlah->tunjangan_jabatan) }}</td>
            @foreach ($jumlah->data_jumlah_tunjangan as $item)
                <td style="width: 1%;font-weight:bold;">Rp</td>
                <td style="text-align: right;font-weight:bold;">{{ rupiahin($item) }}</td>
            @endforeach
            @foreach ($jumlah->data_jumlah_potongan as $item)
                <td style="width: 1%;font-weight:bold;">Rp</td>
                <td style="text-align: right;font-weight:bold;">{{ rupiahin($item) }}</td>
            @endforeach
            <td style="width: 1%;font-weight:bold;">Rp</td>
            <td style="text-align: right;font-weight:bold;">{{ rupiahin($jumlah->total) }}</td>
        </tr>
        <tr>
            <td colspan="{{ $totalCell }}"
                class="b-none"><br><br><br>Tasikmalaya, {{ tgl_indo($req->tanggal) }} <br><br></td>
        </tr>
        @php
            $sisaBagi = $totalCell % 4;
            $hasilBagi = ($totalCell - $sisaBagi) / 4;
            
            if ($sisaBagi > 1) {
                $sisaLagi = $sisaBagi % 2;
                $kanan = (($sisaBagi - $sisaLagi) / 2) * 2;
                $kiri = $sisaLagi;
            } else {
                $kiri = $sisaBagi;
                $kanan = 0;
            }
        @endphp
        <tr>
            @if ($kiri > 0)
                <td colspan="{{ $kiri }}"
                    class="b-none"></td>
            @endif

            <td style="text-align: center;"
                colspan="{{ $hasilBagi }}"
                class="b-none"
                rowspan="7">
                PIMPINAN PESANTREN
                <br>
                <br>
                <br>
                <br>
                <br>

                <b>KH. DIDING DARUL FALAH</b>
            </td>
            <td style="text-align: center;"
                colspan="{{ $hasilBagi }}"
                class="b-none"
                rowspan="7">
                KEPALA MA TERPADU
                <br>
                <br>
                <br>
                <br>
                <br>

                <b>H. MAHMUD FARID, Drs., M.Pd</b>
            </td>
            <td style="text-align: center;"
                colspan="{{ $hasilBagi }}"
                class="b-none"
                rowspan="7">
                KEPALA MTS TERPADU
                <br>
                <br>
                <br>
                <br>
                <br>

                <b>H. ENDANG RAHMAT, Drs.</b>
            </td>
            <td style="text-align: center;"
                colspan="{{ $hasilBagi }}"
                class="b-none"
                rowspan="7">
                BENDAHARA,
                <br>
                <br>
                <br>
                <br>
                <br>

                <b>NOVIYANTI, A.Md</b>
            </td>
            @if ($kanan > 0)
                <td colspan="{{ $kanan }}"
                    class="b-none"></td>
            @endif
        </tr>
    </table>
@endsection
