@php
  $info = Modules\Core\Entities\CoreInfo::first();
  // dd($info);
  $tahun = date("Y");
  $pattern = "/tahunSekarang/i";
  $info->copyright_before_login = preg_replace($pattern, $tahun, $info->copyright_before_login);
  $info->copyright_after_login = preg_replace($pattern, $tahun, $info->copyright_after_login);
@endphp
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  
  <title>  {{ config('app.name', 'LARAVEL') }} | Log in</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  @laravelPWA
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="../../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link href="../../dist/css/adminlte.min.css" rel="stylesheet">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@include('style.bg')
<style>
  body{
        /* Center and scale the image nicely */
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        background: {{ $info->walpaper_image ? 'url(./images/custom/'.$info->walpaper_image.')' : 'url(./img/bgs.jpg)'}};
        color:white;
        /*background-color:darkgreen;*/
    }
.bg-primary-c, .btn-primary{
    background: {{$info->color_primary ? $info->color_primary : '#00695C'}};
    color: #fff
}

</style>
</head>

<body>
<!--Hey! This is the original version
of Simple CSS Waves-->

<div class="header">

    <!--Content before waves-->
    <div class="inner-header flex">



<div class="login-box m-0">
    <br>
    <br>
    <br>
    <a href="/tabletSiswa" 
    style="position:absolute;top:30px;right:30px;z-index:1000000000000000"
    class="btn btn-outline-primary">
    <i class="fa fa-arrow-right"></i> 
    App Siswa</a>
     <div class="card shadow-lg" style="z-index:10000;opacity:.9">
       <div class="card-body login-card-body" >
           <a href="/" class="text-dark">
         <div class="row">
           <div class="col-md-12 text-center">
             <img src="{{ $info->logo ? '/images/custom/'.$info->logo : '/img/logo.png'}}" alt="Logo Pesantren Condong" width="150" class="img-fluid">
           </div>
         </div>
         <p class="login-box-msg pt-2">
        
             <h5 class="text-center"> Sistem Informasi Keuangan Pesantren <br>
              <b>(SIKEREN)</b>  <br>
               <h6 class="text-center">{{$info->nama ? $info->nama : 'SANTREN.ID'}}</h6>
               </h5>
         </p>
       </a>
         <form action="/login" method="post">
           @csrf


           @include('comp.notif')

           <div class="input-group mb-3">
   
             <label class="w-100"> Username </label> 
             <input placeholder="Username / Email" id="identity" type="text" class="form-control @error('identity') is-invalid @enderror" name="identity" value="{{ old('identity') }}" required autocomplete="identity" autofocus> 
                  <div class="input-group-prepend">
                       <div class="input-group-text">
                       <span class="fas fa-user"></span>
                       </div>
                   </div>
                   </div>
                   @error('identity')
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $message }}</strong>
                   </span>
               @enderror
           <div class="input-group mb-3">
             <label class="w-100"> Password </label>
               <input placeholder="Password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
   
               <div class="input-group-append">
                       <div class="input-group-text">
                       <span class="fas fa-lock"></span>
                       </div>
                   </div>
   
                   @error('password')
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $message }}</strong>
                   </span>
               @enderror
           </div>
           <div class="row">
             <div class="col-8">
               <div class="icheck-primary">
                 <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                 <label for="remember">
                   Ingat Saya
                 </label>
               </div>
             </div>
             <!-- /.col -->
             <div class="col-4">
               <button type="submit" class="btn btn-primary btn-block btn-flat">Masuk</button>
             </div>
             <!-- /.col -->
           </div>
         </form>
   
       </div>
       <!-- /.login-card-body -->
     </div>
   </div>
   <!-- /.login-box -->

   

    </div>
    
    <br/><br/><br/>
    <!--Waves Container-->
    <div>
    <svg class="waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
    viewBox="0 24 150 28" preserveAspectRatio="none" shape-rendering="auto">
    <defs>
    <path id="gentle-wave" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z" />
    </defs>
    <g class="parallax">
    <use xlink:href="#gentle-wave" x="48" y="0" fill="rgba(255,255,255,0.7" />
    <use xlink:href="#gentle-wave" x="48" y="3" fill="rgba(255,255,255,0.5)" />
    <use xlink:href="#gentle-wave" x="48" y="5" fill="rgba(255,255,255,0.3)" />
    <use xlink:href="#gentle-wave" x="48" y="7" fill="#fff" />
    </g>
    </svg>
    </div>
    <!--Waves end-->
    
    </div>
    <!--Header ends-->
    
    <!--Content starts-->
    <div class="content flex">
  @if ($info->copyright_before_login)
    <p> {!! $info->copyright_before_login !!} </p>
  @else
    <p>Copyright &copy; 2019 CV Alphabet</p>
    
  @endif
    </div>
    <!--Content ends-->
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 2500);
</script>    
</body>
</html>