@php
  $info = Modules\Core\Entities\CoreInfo::first();
  // dd($info);
  $tahun = date("Y");
  $pattern = "/tahunSekarang/i";
  $info->copyright_before_login = preg_replace($pattern, $tahun, $info->copyright_before_login);
  $info->copyright_after_login = preg_replace($pattern, $tahun, $info->copyright_after_login);
     $nis = '';
    if($req->nis){
        $nis = $req->nis;
        $siswa = Modules\Siswa\Entities\Siswa::where("nis",$nis)->first();
    }else{  
        $siswa = '';
    }

    
    if($req->start AND $req->end){
    $start = date($req->start);
    $end = date($req->end);
  }else{
    $start = date('Y-m-d', strtotime("-1 month"));
    $end = date('Y-m-d');
  }


@endphp
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  
  <title>  {{ $info->nama ? $info->nama : 'SIKEREN' }} | SISWA</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  @laravelPWA
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="../../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link href="../../dist/css/adminlte.min.css" rel="stylesheet">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
 <!-- Select2 -->
 <link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">
 <link rel="stylesheet" href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
 <!-- daterange picker -->
 <link rel="stylesheet" href="../../plugins/daterangepicker/daterangepicker.css">

<style>

/*
 * Globals
 */

/* Custom default button */
.btn-secondary,
.btn-secondary:hover,
.btn-secondary:focus {
  color: #333;
  text-shadow: none; /* Prevent inheritance from `body` */
  background-color: #fff;
  border: .05rem solid #fff;
}


/*
 * Base structure
 */

html,
body {
  height: 100%;
  align-items: center;
}



.tab-pane{
    padding: 20px 10px
}


</style>
</head>

<body >


    @if(!$siswa)
    <style>
    #form-data{
        position: absolute;
        top: 150px;
        width: 60%;
        margin: auto;
        left:20%;
        z-index: 100px;
    }
    
    </style>
     
     @endif
     
     <header class="w-100 ">
                <!-- Navbar -->
                  <nav  class="navbar navbar-expand navbar-dark bg-primary
                  border-bottom-0
                  ">
                      <a href="/" class="navbar-brand">
                          <img src="{{ $info->logo_after_login ? '/images/custom/'.$info->logo_after_login : '/logo.png'}}" alt="AdminLTE Logo" class="brand-image">
      
                      </a>
                  </nav>
              </header>
<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">

      
        <main role="main" class="inner cover">
                <form id="form-data" class="row text-center">
                    @if(!$siswa)
                    <div class="col-md-12 form-group mb-5">
                        
                      <img src="{{ $info->logo ? '/images/custom/'.$info->logo : '/img/logo.png'}}" alt="Logo Pesantren Condong" width="150" class="img-fluid mb-3">
                      <h5 class="text-center"> Sistem Informasi Keuangan Pesantren <br>
                        <b>(SIKEREN)</b>  <br>
                         <h6 class="text-center">{{$info->nama ? $info->nama : 'SANTREN.ID'}}</h6>
                         </h5>
                    </div>  
                    @endif
                    
                    <div class="input-group mb-3 @if ($siswa) col-md-10 @else col-md-12 @endif">
                        <span class="input-group-prepend">
                            <button type="button" class="btn btn-primary">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                        <select name="nis" class="cari form-control" 
                        value="@if ($nis){{ $nis }}@endif"
                        autofocus
                        >
                    
                        </select>
                    </div>
                    @if ($siswa)
                    <div class="col-md-2">
                        <a class="btn btn-block btn-outline-primary" href="?nis=">
                            <i class="fa fa-sign-in"></i>
                            Keluar
                        </a>
                    </div>
                    @endif
                </form>



                
        @if ($siswa)
        <div class="row text-center mb-4">
          <div class="col-md-6">
              Nama
              <br>
              <b>{{ $siswa->nama }}</b>
              <br>
              <small class="text-success-600">
                  {{ $siswa->nis }}
              </small>
          </div>
          <div class="col-md-6">
              Jenjang
              <br>
              <b>{{ $siswa->jenjang }}
              </b>
              <br>
              <small class="text-success-600">
                  {{ $siswa->kelas }}
              </small>
          </div>

        </div>

      
                <!-- /.card -->
                <div class="card card-primary card-outline">
                    <div class="card-body">
                        <ul class="nav nav-pills" id="custom-content-below-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link
                            @if($req->menu !== 'tabungan')  active  @endif
                            " id="custom-content-below-home-tab" data-toggle="pill" href="#custom-content-below-home" role="tab" aria-controls="custom-content-below-home"
                             >
                                Tagihan
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link
                            @if($req->menu == 'tabungan')  active  @endif
                            " id="custom-content-below-profile-tab" data-toggle="pill" href="#custom-content-below-profile" role="tab" aria-controls="custom-content-below-profile" 
                           >
                                Tabungan
                            </a>
                        </li>
                        </ul>
                        <div class="tab-content" id="custom-content-below-tabContent">
                        <div class="tab-pane fade @if($req->menu == 'tagihan' || !$req->menu) show active @endif" id="custom-content-below-home" role="tabpanel" aria-labelledby="custom-content-below-home-tab">
                            
                                <div class="card-body table-responsive " >
  
                                        
                                        <table class="table table-head-fixed table-hover">
                                          <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Nominal</th>
                                                <th>Dibayar</th>
                                                <th>Sisa</th>
                                                <th class="text-right">Aksi</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                           
                            @php
                            $totalSisa = 0;
                            $nominaldu = 0;
                            $dibayardu = 0;
                            $no = 1;
                            $daftarUlang = Modules\Keuangan\Entities\BiayaSiswa::where("nis",$nis)
                            ->where("satuan","Daftar Ulang")
                            ->whereRaw("dibayar != nominal")
                            ->orderBy("bulan","ASC");
                            @endphp
                            
                          
                            @if ($daftarUlang->count() > 0)
                            
                          <tr class="text-bold" data-toggle="collapse" href="#daftarulang">
                            <td>#</td>
                            <td>
                                Daftar Ulang (Juli)
                            </td>
                            <td>
                                {{ rupiah($daftarUlang->sum("nominal")) }} 
                            </td>
                            <td>
                                {{ rupiah($daftarUlang->sum("dibayar")) }} 
                          
                            </td>
                            <td>
                          
                                {{ rupiah($daftarUlang->sum("nominal") - $daftarUlang->sum("dibayar")) }} 
                          
                            </td>
                            <td class="text-right">
                              <div class="btn btn-outline-primary btn-sm">
                                <i class="fa fa-chevron-down"></i>
                              </div>
                            </td>
                          </tr>
                          
                          @php
                          $no = 2;                      
                          @endphp    
                          
                          
                                        @foreach ($daftarUlang->get() as $key => $item)
                          
                                            <tr class="collapse active" id="daftarulang" >
                                              <td>{{ $key+1 }}</td>          
                                              <td>{{ $item->nama }}</td>  
                                              <td>{{ rupiah($item->nominal) }}</td>  
                                              <td>{{ rupiah($item->dibayar) }}</td>  
                                              <td>{{ rupiah($item->nominal-$item->dibayar) }}</td>  
                                              <td>
                                                  {{-- <button onclick="editTagihan('{{ $item->id }}','{{ $item->nominal }}','{{ $item->jenis_biaya_id }}')" 
                                                      class="btn btn-primary btn-sm float-right">
                                                      <i class="fa fa-edit"></i>
                                                    </button>  
                                                    <button onclick="hapusTagihan('{{ $item->id }}')" 
                                                        class="btn btn-danger btn-sm float-right">
                                                        <i class="fa fa-trash"></i>
                                                      </button>     --}}
                                              </td>  
                                        </tr>   
                                        @php
                                          $totalSisa += $item->nominal-$item->dibayar;
                                        @endphp
                                        @endforeach
                            
                          @endif
                          
                            @php
                                $tg = Modules\Keuangan\Entities\BiayaSiswa::where("nis",$nis)
                                              ->where("satuan","!=","Daftar Ulang")
                                              ->orderBy("bulan","ASC");
                                $bln = 0;
                            @endphp
                          
                          
                          
                                            @foreach ($tg->get() as $key => $item)
                                         
                                      
                                            @if ($bln !== $item->bulan)
                                            
                                                @php
                                                $nom =   Modules\Keuangan\Entities\BiayaSiswa::where("nis",$nis)
                                                        ->where("satuan","!=","Daftar Ulang")
                                                        ->where("bulan",$item->bulan)->sum("nominal");
                          
                          
                                                $bay =  Modules\Keuangan\Entities\BiayaSiswa::where("nis",$nis)
                                                        ->where("satuan","!=","Daftar Ulang")
                                                        ->where("bulan",$item->bulan)->sum("dibayar");
                                                $sis =  rupiah($nom-$bay);
                                                @endphp
                                    
                                            
                                            <tr class="text-bold" data-toggle="collapse" href="#bulan-{{$item->bulan}}">
                                              <td>#</td>
                                              <td>
                                                  Bulanan ({{ bulan_text($item->bulan)}} {{ $item->tahun_id }})
                                              </td>
                                              <td>
                                                  {{ rupiah($nom) }} 
                                              </td>
                                              <td>
                                                  {{ rupiah($bay) }} 
                          
                                              </td>
                                              <td>
                          
                                                  {{ $sis }} 
                          
                                                </td>
                                                <td class="text-right">
                                                  <div class="btn btn-outline-primary btn-sm">
                                                    <i class="fa fa-chevron-down"></i>
                                                  </div>
                                                </td>
                                            </tr>  
                                                @php
                                                    $bln = $item->bulan;
                                                @endphp
                                            @endif
                                              <tr class="collapse  show" id="bulan-{{$item->bulan}}">
                                                <td>{{ $key+1 }}</td>
                                                <td>{{ $item->nama }} ({{ bulan_text($item->bulan) }})</td>  
                                                <td>{{ rupiah($item->nominal) }}</td>  
                                                <td>{{ rupiah($item->dibayar) }}</td>  
                                                <td>{{ rupiah($item->nominal-$item->dibayar) }}</td>  
                                                <td>
                                                </td>  
                                              </tr>   
                          
                                              @php
                                                  $totalSisa = $totalSisa + $item->nominal-$item->dibayar;
                                              @endphp
                                            @endforeach
                          
                                            <tr class="text-right">
                                              <td colspan="5">
                                                  <b>Sisa Tagihan : {{ rupiah($totalSisa)}}</b>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                       
                          
                                      </div>
                                      <!-- /.card-body -->

                        </div>
                        <div class="tab-pane fade @if($req->menu == 'tabungan') show active @endif" id="custom-content-below-profile" role="tabpanel" aria-labelledby="custom-content-below-profile-tab">
                               
                                <div class="card ">
                                    <div class="row p-3">
                                      
                                        <div class="col-md-8">
                                              <label >Periode</label>
                                              <form class="row" action="/" method="GET">
                                                <input name="nis" value="{{ $nis }}" type="hidden">
                                                <div class="col-md-6">
                                                      <div class="input-group">
                                                        <div class="input-group-prepend">
                                                          <span class="input-group-text">
                                                            <i class="far fa-calendar-alt"></i>
                                                          </span>
                                                        </div>
                                                          <input name="filter" type="text" class="form-control float-right" id="filterdate">
                                                      </div>
                                                </div>
                                                <div class="col-md-6 pt-1">
                                                  {{ tgl_indo($start)}} -
                                                  {{ tgl_indo($end)}}
                                                </div>
                                          </form>
                                        </div>
                          
                                     
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body table-responsive p-0 text-right">
                                      <table class="table table-head-fixed">
                                        <thead>
                                          <tr>
                                              <th >No</th>
                                              <th >Tgl Bayar</th>
                                              <th>Tipe</th>
                                              <th>Nominal</th>
                                              <th>Saldo Akhir</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                        
                                          @php
                                              $totalBayar = 0;
                                              $dtaa =  Modules\Keuangan\Entities\Jurnal::
                                              where("nis",$nis)
                                              ->where("id_jenis_biaya",4)
                                              ->where("tgl_bayar",">=",$start)
                                              ->where("tgl_bayar","<=",$end)
                                              ->orderBy("created_at","DESC")
                                              ->get();
                                              $no = 1;
                                          @endphp
                                          @foreach ($dtaa as $key => $item)
                                            <tr>
                                              <td>{{$no++}}</td>  
                                              <td>{{ tgl_indo($item['tgl_bayar']) }}
                                              <br>
                                                  <sup> {{ jamber($item['created_at']) }}</sup>
                                              </td>
                                              <td>{!! ($item['type'] == 1) ? "<span class='label bg-blue-800'>D</span>" : "<span class='label bg-danger-800'>K</span>" !!}</td>
                                              {{-- <td>{{ ($item['referensi'] == 1) ? 'Cash' : 'Transfer' }}</td>   --}}
                                              <td> {{ ($item['type'] == 1) ? '+' : '-' }} {{ rupiah($item['jumlah']) }}</td>  
                                              <td>{{ rupiah($item['saldo_akhir']) }}</td>
                                              
                                            </tr>
                                          @endforeach
                                        </tbody>
                                      </table>
                                     
                                    </div>
                                    <!-- /.card-body -->
                                  </div>



                        </div>
          
    
    

  @endif

  
        </main>
</div>



    

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>


<script src="../../plugins/moment/moment.min.js"></script>
<!-- date-range-picker -->
<script src="../../plugins/daterangepicker/daterangepicker.js"></script>

<script>

    

var start = '{{ $start }}';
var end = '{{ $end }}';


$('#filterdate').daterangepicker({
    startDate: start,
    endDate: end,
    locale: {
        format: 'YYYY-MM-D'
    },
    ranges: {
       'Today': [moment(), moment()],
       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
       'Last 7 Days': [moment().subtract(6, 'days'), moment()],
       'Last 30 Days': [moment().subtract(29, 'days'), moment()],
       'This Month': [moment().startOf('month'), moment().endOf('month')],
       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
}, function(start,end){
      location.href = "?menu=tabungan&nis={{$nis}}&start="+start.format('YYYY-MM-D') +"&end="+end.format('YYYY-MM-D');     
});



$('#tanggal').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  });

  $('#tanggal1').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  });


 $('.cari').select2({
    theme: 'bootstrap4',
    placeholder: 'Masukkan Nama / NISN',
    ajax: {
      url: '/searchNisSiswa',
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        console.log(data);
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.nama,
              id: item.nis
            }
          })
        };
      },
      cache: true
    }
  }).focus();

  $('.cari').on('select2:select', function (e) {
    location.href = "?" +$("#form-data").serialize()
  });

</script>    
</body>
</html>