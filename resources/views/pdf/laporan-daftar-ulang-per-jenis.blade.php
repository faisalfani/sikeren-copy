@extends('layouts.pdf')

@section('title')
      Laporan
@endsection
@section('css-after')

<style>
body{
    
}
.page_break { page-break-before: always; }
</style>

  @endsection
@section('content')

@include('comp.header_pdf')
@php

$tgl  =     explode(" ",$req1->filter);

$tgl_awal = $tgl[0];
$tgl_akhir = $tgl[2];


if($req1->format_laporan == 'rentang'){
    if($req1->bulan == 0){
        $pr = " Tahun " .$req1->tahun_ajaran;

        $where = " YEAR(jurnals.tgl_bayar) = $req1->tahun_ajaran";

    }else{
        $pr = bulan_text($req1->bulan) ." ". $req1->tahun_ajaran;
        
        $where = " YEAR(jurnals.tgl_bayar) = $req1->tahun_ajaran AND MONTH(jurnals.tgl_bayar) = $req1->bulan ";

    }

}else if($req1->format_laporan == 'periode'){
    $pr = tgl_indo($tgl_awal) ." - ". tgl_indo($tgl_akhir);

    $where = " jurnals.tgl_bayar >= '$tgl_awal' AND jurnals.tgl_bayar <= '$tgl_akhir'";

}else{
    $pr = "Seluruh";

    $where = "jurnals.tgl_bayar";
}

if($req1->tingkat !== 'SEMUA'){
    $where .= " AND s.jenjang = '$req1->tingkat' ";
}


if($req1->gender !== 'SEMUA'){
    $where .= " AND s.jk = '$req->gender' ";
}

if($req1->ref > 0){
    $where .= " AND jurnals.ref = '$req1->ref' ";
}

@endphp

            <div class="sub-header">
                LAPORAN DAFTAR ULANG
                @if($req1->tingkat !== 'SEMUA')
                    {{ $req1->tingkat }}
                @endif
                @if($req1->gender !== 'SEMUA')
                      ({{ ($req->gender == 'L') ? '(P) Putra' : '(Pi) Putri' }})
                @endif
                <br>
                {{ $jenis->nama }}
                <br>
                Tahun Ajaran {{ $req1->du_tahun_ajaran }} /{{ $req1->du_tahun_ajaran+1}}
                <br>
                Periode {{ $pr }}
               
            </div>
            <table>
                <tbody style="text-align:left;">
                <tr>
                    <th style="border-left:1px solid #ccc">No</th>
                    <th style="border-left:1px solid #ccc">Kelas</th>
                    <th style="border-left:1px solid #ccc">Nama</th>
                    <th style="text-align:right;border-left:1px solid #ccc;border-right:1px solid #ccc"">Jumlah</th>
                </tr>
                @php
                    $data =  Modules\Keuangan\Entities\Jurnal::
                    join("biaya_siswas as b","b.id","jurnals.biaya_id")
                    ->join("siswas as s","s.nis","jurnals.nis")
                    ->where("jurnals.id_jenis_biaya",$jenis->id)
                    ->where("b.tahun_ajaran",$req1->du_tahun_ajaran)
                    ->where("b.satuan","Daftar Ulang")
                    ->where("b.bulan",7)
                    ->where("jurnals.type",1)
                    ->whereRaw($where)
                    ->select("jurnals.*","s.nama as nama_siswa","s.kelas")
                    ->orderBy("s.jenjang","DESC")
                    ->get();
            
                    // dd($data);
                @endphp
                @foreach ($data as $key => $d)
                    
                <tr>
                    <td>{{$key+1}}</td>
                    <td>
                        {{ $d->kelas }}
                    </td>
                    <td>
                        {{ $d->nama_siswa }}
                    </td>
                    <td style="text-align:right;border-right:1px solid #ccc"> 
                        {{ rupiah($d->jumlah) }}
                    </td>
                </tr>
                
                @endforeach

                <tr>
                    <th> </th>
                    <th> </th>
                    <th>
                        Jumlah
                    </th>
                    <th style="text-align:right">
                        {{ rupiah($req->jumlah) }}
                    </th>
                </tr>

            </tbody>
            </table>

@endsection

@section('js-after')
@endsection