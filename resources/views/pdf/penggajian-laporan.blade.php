@extends('layouts.pdf')

@section('title')
    Laporan
@endsection
@section('css-after')
    <style>
        /* .page-break {
            page-break-after: always;
        } */
        .break-before {
            page-break-before: always;
        }

        @page {
            margin: 10px 5px;
        }

        #content {
            padding: 0 15px;
        }

        /* body{
                  font-family: 'Times New Roman', Times, serif;
                  font-size: 14px;
                  margin:0;
                 }
                 html {
                  margin-left: 0px;
                 }
                 @page{
                   margin: 0px;
                 } */
        /* body { margin: 0;padding: 0 !important; } */
        .arialin {
            font-family: 'Times New Roman', Times, serif !important;
            font-size: 11px;
        }

        .b-none {
            border: none;
        }

        .page_break {
            page-break-before: always;
        }

        table tr td {
            font-size: 4px;
            padding: 0 1px;
            margin: 0;
            text-align: center
        }

        table tr th {
            font-size: 5px;
            text-align: center
                /* font-weight: normal; */
        }

    </style>
@endsection
@section('content')
    @php
    function rupiahin($angka)
    {
        $hasil_rupiah = number_format($angka, 0, ',', '.');
        return $hasil_rupiah;
    }
    $totalCell = count($header['tunjangan']) * 2 + count($header['potongan']) * 2 + 14;
    @endphp

    <div class="header"
        style="{{ $req->print ? 'padding-top:0;margin-top:0;' : '' }}">
        <img class="logo"
            src="https://ponpescondong.com/logo-pesantren.png"
            alt="logo">
        {{-- <span style="font-size:110%;text-transform:uppercase;">{{$req->tipe ? $header['tipe']->nama : ''}}<span> --}}
        <br>
        <small style="font-weight:bold">MTS-MA TERPADU RIYADLUL 'ULUM
            <br>
            PESANTREN RIYADLUL 'ULUM WADDA'WAH</small>
        <div style="height:3px;border-top:2px solid #ccc;border-bottom:2px solid #ccc;margin:10px 10px 0px;margin-top:25px">
        </div>

    </div>
    <div id="content">
        <table style="width:100%;border:none;padding:0;margin:0;">
            <tr>
                <th class=""
                    style="margin:0;padding:0 1px;text-align:center; width: 1%;"
                    rowspan="2">No</th>
                <th class=""
                    style="margin:0;padding:0 1px;text-align:center;"
                    rowspan="2">Nama</th>
                <th class=""
                    style="margin:0;padding:0 1px;text-align:center;"
                    rowspan="2">Tahun</th>
                <th class=""
                    style="margin:0;padding:0 1px;text-align:center;width:1%;"
                    rowspan="2">Sertifikasi</th>
                <th class=""
                    style="margin:0;padding:0 1px;text-align:center;"
                    colspan="3">IHSAN POKOK</th>
                <th class=""
                    style="margin:0;padding:0 1px;text-align:center;"
                    colspan="3">IHSAN JAM KERJA</th>
                <th class=""
                    style="margin:0;padding:0 1px;text-align:center;"
                    colspan="{{ count($header['tunjangan']) * 2 + 2 }}">TUNJANGAN</th>
                <th class=""
                    style="margin:0;padding:0 1px;text-align:center;"
                    colspan="{{ count($header['potongan']) * 2 }}">POTONGAN</th>
                <th class=""
                    style="margin:0;padding:0 1px;text-align:center;"
                    rowspan="2"
                    colspan="2">TOTAL IHSAN</th>
            </tr>
            <tr>
                <th class=""
                    style="margin:0;padding:0 1px;text-align:center;">MK</th>
                <th class=""
                    style="margin:0;padding:0 1px;text-align:center;"
                    colspan="2">Jumlah</th>
                <th class=""
                    style="margin:0;padding:0 1px;text-align:center;">JP</th>
                <th class=""
                    style="margin:0;padding:0 1px;text-align:center;"
                    colspan="2">Jumlah</th>
                <th class=""
                    style="margin:0;padding:0 1px;text-align:center;"
                    colspan="2">Jabatan</th>

                @foreach ($header['tunjangan'] as $item)
                    <th class=""
                        style="margin:0;padding:0 1px;text-align:center;"
                        colspan="2">{{ $item->nama }}</th>
                @endforeach
                @foreach ($header['potongan'] as $item)
                    <th class=""
                        style="margin:0;padding:0 1px;text-align:center;"
                        colspan="2">{{ $item->nama }}</th>
                @endforeach

            </tr>
            @php
                $no = 0;
            @endphp
            @foreach ($res as $key => $z)
                <tr>
                    <td colspan="{{ $totalCell }}"
                        style="text-align: left;padding:2px;background:#dede">{{ $key }}</td>
                </tr>
                @foreach ($z as $v)
                    <tr>
                        <td style="margin: 0; padding:0 1px;">{{ $no + 1 }}</td>
                        <td style="text-align: left !important;margin: 0; padding:0 1px;">{{ $v['nama'] }}</td>
                        <td style="margin: 0; padding:0 1px;">{{ $v['tahun'] }}</td>
                        <td style="margin: 0; padding:0 1px;">{{ $v['sertifikasi'] == 1 ? 'Y' : 'T' }}</td>
                        <td style="margin: 0; padding:0 1px;">{{ $v['mk'] }}</td>
                        <td style="width: 1%;border-right:none;margin: 0; padding:0 1px;">Rp</td>
                        <td style="text-align: right;border-left:none;margin: 0; padding:0 1px;">
                            {{ rupiahin($v['ip_jumlah']) }}</td>
                        <td style="margin: 0; padding:0 1px;">{{ $v['jp'] }}</td>
                        <td style="width: 1%;border-right:none;margin: 0; padding:0 1px;">Rp</td>
                        <td style="text-align: right;border-left:none;margin: 0; padding:0 1px;">
                            {{ rupiahin($v['ijk_jumlah']) }}</td>
                        <td style="width: 1%;border-right:none;margin: 0; padding:0 1px;">Rp</td>
                        <td style="text-align: right;border-left:none;">{{ rupiahin($v['tunjangan_jabatan']) }}</td>
                        @foreach ($v['tunjangan'] as $item)
                            <td style="width: 1%;border-right:none;margin: 0; padding:0 1px;">Rp</td>
                            <td style="text-align: right;border-left:none;margin: 0; padding:0 1px;">
                                {{ rupiahin($item->jumlah_ihsan) }}</td>
                        @endforeach
                        @foreach ($v['potongan'] as $item)
                            <td style="width: 1%;border-right:none;margin: 0; padding:0 1px;">Rp</td>
                            <td style="text-align: right;border-left:none;margin: 0; padding:0 1px;">
                                {{ rupiahin($item->jumlah_ihsan) }}</td>
                        @endforeach
                        <td style="width: 1%;border-right:none;margin: 0; padding:0 1px;">Rp</td>
                        <td style="text-align: right;border-left:none;margin: 0; padding:0 1px;">
                            {{ rupiahin($v['total']) }}</td>
                    </tr>
                    @php
                        $no++;
                    @endphp
                @endforeach
            @endforeach
            <tr>
                <td colspan="5"></td>
                <td style="width: 1%;border-right:none;font-weight:bold;margin: 0; padding:0 1px;">Rp</td>
                <td style="text-align: right;border-left:none;font-weight:bold;margin: 0; padding:0 1px;">
                    {{ rupiahin($jumlah->ip_jumlah) }}</td>
                <td></td>
                <td style="width: 1%;border-right:none;font-weight:bold;margin: 0; padding:0 1px;">Rp</td>
                <td style="text-align: right;font-weight:bold;border-left:none;margin: 0; padding:0 1px;">
                    {{ rupiahin($jumlah->ijk_jumlah) }}</td>
                <td style="width: 1%;border-right:none;font-weight:bold;margin: 0; padding:0 1px;">Rp</td>
                <td style="text-align: right;font-weight:bold;border-left:none;margin: 0; padding:0 1px;">
                    {{ rupiahin($jumlah->tunjangan_jabatan) }}</td>
                @foreach ($jumlah->data_jumlah_tunjangan as $item)
                    <td style="width: 1%;border-right:none;font-weight:bold;margin: 0; padding:0 1px;">Rp</td>
                    <td style="text-align: right;font-weight:bold;border-left:none;margin: 0; padding:0 1px;">
                        {{ rupiahin($item) }}</td>
                @endforeach
                @foreach ($jumlah->data_jumlah_potongan as $item)
                    <td style="width: 1%;border-right:none;font-weight:bold;margin: 0; padding:0 1px;">Rp</td>
                    <td style="text-align: right;font-weight:bold;border-left:none;margin: 0; padding:0 1px;">
                        {{ rupiahin($item) }}</td>
                @endforeach
                <td style="width: 1%;border-right:none;font-weight:bold;margin: 0; padding:0 1px;">Rp</td>
                <td style="text-align: right;font-weight:bold;border-left:none;margin: 0; padding:0 1px;">
                    {{ rupiahin($jumlah->total) }}</td>
            </tr>
            <tr>
                <td colspan="{{ $totalCell }}"
                    class="b-none"><br><br><br>Tasikmalaya, {{ tgl_indo($req->tanggal) }} <br><br></td>
            </tr>
            @php
                $sisaBagi = $totalCell % 4;
                $hasilBagi = ($totalCell - $sisaBagi) / 4;
                
                if ($sisaBagi > 1) {
                    $sisaLagi = $sisaBagi % 2;
                    $kanan = (($sisaBagi - $sisaLagi) / 2) * 2;
                    $kiri = $sisaLagi;
                } else {
                    $kiri = $sisaBagi;
                    $kanan = 0;
                }
            @endphp
            <tr>
                @if ($kiri > 0)
                    <td colspan="{{ $kiri }}"
                        class="b-none"></td>
                @endif

                <td colspan="{{ $hasilBagi }}"
                    class="b-none">
                    PIMPINAN PESANTREN
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <b>KH. DIDING DARUL FALAH</b>
                </td>
                <td colspan="{{ $hasilBagi }}"
                    class="b-none">
                    KEPALA MA TERPADU
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <b>H. MAHMUD FARID, Drs., M.Pd</b>
                </td>
                <td colspan="{{ $hasilBagi }}"
                    class="b-none">
                    KEPALA MTS TERPADU
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <b>H. ENDANG RAHMAT, Drs.</b>
                </td>
                <td colspan="{{ $hasilBagi }}"
                    class="b-none">
                    BENDAHARA,
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <b>NOVIYANTI, A.Md</b>
                </td>
                @if ($kanan > 0)
                    <td colspan="{{ $kanan }}"
                        class="b-none"></td>
                @endif
            </tr>
        </table>
    </div>
@endsection

@section('js-after')
@endsection
