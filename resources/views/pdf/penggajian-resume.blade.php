@extends('layouts.pdf')


@section('title')
    Laporan
@endsection
@section('css-after')
    <style>
        body {}

        .arialin {
            font-family: 'Times New Roman', Times, serif !important;
            font-size: 11px;
        }

        .b-none {
            border: none;
        }

        .page_break {
            page-break-before: always;
        }

        @page {
            margin: 0;
        }

        body {
            margin: 0;
        }

    </style>
@endsection
@section('content')
    @php
    function rupiahin($angka)
    {
        $hasil_rupiah = number_format($angka, 0, ',', '.');
        return $hasil_rupiah;
    }
    @endphp
    @foreach ($res as $key => $v)
        <div style="width: 100%;height:45%;margin:0 !important;padding:23px !important">

            <div class="arialin"
                style="margin: 0 !important; background:#dedede;">
                <table style="width:100%;border:none;padding:0;margin:0;">
                    <tr>
                        <td class="b-none"
                            style="width: 1%;"></td>
                        {{-- Width width:6.5%; default --}}
                        <td class="b-none"
                            style=""></td>
                        <td class="b-none"
                            style=""></td>
                        <td class="b-none"
                            style="width: 1%;"></td>
                        <td class="b-none"
                            style=""></td>
                        <td class="b-none"
                            style=""></td>
                        <td class="b-none"
                            style="width: 1%;"></td>
                        <td class="b-none"
                            style=""></td>
                        <td class="b-none"
                            style="width:1% !important;"></td>
                        <td class="b-none"
                            style="width: 1%;"></td>
                        <td class="b-none"
                            style=""></td>
                        <td class="b-none"
                            style=""></td>
                        <td class="b-none"
                            style=""></td>
                        <td class="b-none"
                            style=""></td>
                        <td class="b-none"
                            style="width: 1%;"></td>
                        <td class="b-none"
                            style="width: 1%;"></td>
                        <td class="b-none"
                            style=""></td>
                    </tr>
                    <tr>
                        <td class="b-none"
                            colspan="3"
                            rowspan="3"
                            style="text-align: center;"><img style="height: 50px;"
                                src="https://ponpescondong.com/logo-pesantren.png"
                                alt="logo"></td>
                        <td class="b-none"
                            colspan="14"
                            style="text-align: center;font-size:12px;margin:0 !important;padding:0 !important;font-weight:bold;">
                            SLIP IHSAN PENDIDIKAN DAN TENAGA KEPENDIDIKAN</td>
                    </tr>
                    <tr>
                        <td class="b-none"
                            colspan="14"
                            style="text-align: center;font-size:12px;margin:0 !important;padding:0 !important;font-weight:bold;">
                            MTS-MA TERPADU RIYADLUL ULUM WADDA'WAH</td>
                    </tr>
                    <tr>
                        <td class="b-none"
                            colspan="14"
                            style="text-align: center;font-size:12px;margin:0 !important;padding:0 !important;font-weight:bold;">
                            PONPES TERPADU RIYADLUL ULUM WADDA'WAH</td>
                    </tr>
                    <tr>
                        <td class="b-none"
                            colspan="17"
                            style="border-top: 2px solid black;"></td>
                    </tr>
                    <tr style="padding:0;margin:0;">
                        <td class="b-none"
                            style="font-weight:bold; font-size:11px;"
                            colspan="4">No</td>
                        <td class="b-none"
                            style="font-weight:bold; font-size:11px;"
                            colspan="4">: {{ $key + 1 }}</td>
                        <td class="b-none"
                            style="font-weight:bold; font-size:11px;"></td>
                        <td class="b-none"
                            style="font-weight:bold; font-size:11px;"
                            colspan="5">Tahun Masuk Kerja</td>
                        <td class="b-none"
                            style="font-weight:bold; font-size:11px;"
                            colspan="3">: {{ $v['mk'] }}</td>
                    </tr>
                    <tr>
                        <td class="b-none"
                            style="font-weight:bold; font-size:11px;"
                            colspan="4">Nama Lengkap</td>
                        <td class="b-none"
                            style="font-weight:bold; font-size:11px;"
                            colspan="4">: {{ $v['nama'] }}</td>
                        <td class="b-none"
                            style="font-weight:bold; font-size:11px;"></td>
                        <td class="b-none"
                            style="font-weight:bold; font-size:11px;"
                            colspan="5">Status Kepegawaian</td>
                        <td class="b-none"
                            style="font-weight:bold; font-size:11px;"
                            colspan="3">: {{ $v['kepegawaian'] }}</td>
                    </tr>
                    <tr>
                        <td class="b-none"
                            style="font-weight:bold; font-size:11px;"
                            colspan="4">Jabatan</td>
                        <td class="b-none"
                            style="font-weight:bold; font-size:11px;"
                            colspan="4">: {{ $v['jabatan'] }}</td>
                        <td class="b-none"
                            style="font-weight:bold; font-size:11px;"></td>
                        <td class="b-none"
                            style="font-weight:bold; font-size:11px;"
                            colspan="5"></td>
                        <td class="b-none"
                            style="font-weight:bold; font-size:11px;"></td>
                        <td class="b-none"
                            style="font-weight:bold; font-size:11px;"
                            colspan="2"></td>
                    </tr>

                    <tr>
                        <td class="b-none"
                            style="font-weight:bold; text-align:center;"
                            colspan="17">KOMPONEN IHSAN <br></td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold; text-align:center;"
                            colspan="8">PENDAPATAN</td>
                        <td style=""></td>
                        <td style="font-weight:bold; text-align:center;"
                            colspan="8">POTONGAN</td>
                    </tr>

                    @for ($i = 0; $i < $v['maksimal']; $i++)
                        <tr>
                            <td>{{ $i + 1 }}</td>
                            <td colspan="5"
                                style="width: 25% !important;">
                                {{ count($v['pendapatan']) > $i ? $v['pendapatan'][$i]['nama'] : '' }} </td>
                            <td>{{ count($v['pendapatan']) > $i ? ':' : '' }}</td>
                            <td>
                                @if (count($v['pendapatan']) > $i)
                                    <span style="float:left;">Rp</span>
                                    <span style="float:right;">{{ rupiahin($v['pendapatan'][$i]['jumlah']) }}</span>
                                @endif
                            </td>
                            <td></td>
                            <td>{{ count($v['potongan']) > $i ? $i + 1 : '' }}</td>
                            <td style="width: 25% !important;"
                                colspan="4">{{ count($v['potongan']) > $i ? $v['potongan'][$i]['nama'] : '' }}</td>
                            <td>{{ count($v['potongan']) > $i ? ':' : '' }}</td>
                            <td style="border-right: none;">{{ count($v['potongan']) > $i ? 'Rp' : '' }}</td>
                            <td style="text-align: right;border-left:none;">
                                @if (count($v['potongan']) > $i)
                                    {{ rupiahin($v['potongan'][$i]['jumlah']) }}
                                @endif
                            </td>
                        </tr>
                    @endfor

                    <tr>
                        <td style="font-weight:bold;text-align:right;"
                            colspan="7">JUMLAH PENDAPATAN IHSAN</td>
                        <td style="font-weight:bold;">
                            <span style="float:left;">Rp</span>
                            <span style="float:right;">{{ rupiahin($v['jumlah_pendapatan']) }}</span>
                        </td>
                        <td style="font-weight:bold;"></td>
                        <td style="font-weight:bold;text-align:right;"
                            colspan="6">JUMLAH POTONGAN IHSAN</td>
                        <td style="border-right: none;">Rp</td>
                        <td style="font-weight:bold; text-align: right;border-left:none;">
                            {{ rupiahin($v['jumlah_potongan']) }}
                        </td>

                    </tr>
                    <tr>
                        <td colspan="8"
                            style="font-weight:bold;text-align: right;">TOTAL IHSAN</td>
                        <td style="font-weight:bold;">:</td>
                        <td style="font-weight:bold;"
                            colspan="8">{{ rupiah($v['total']) }}</td>
                    </tr>
                    <tr>
                        <td class="b-none"
                            colspan="17"
                            style="font-size: 8px;"><br>CATATAN :</td>
                    </tr>
                    <tr>
                        <td style="padding-top: 0 !important; margin: 0 !important; font-size:8px; text-align:right;"
                            class="b-none">1</td>
                        <td style="padding-left:0 !important; padding-top: 0 !important; margin: 0 !important; font-size:8px;"
                            class="b-none"
                            colspan="16">IHSAN DIEVALUASI SECARA BERKALA SESUAI DENGAN LOYALITAS, DEDIKASI, DAN KINERJA</td>
                    </tr>
                    <tr>
                        <td style="padding-top: 0 !important; margin: 0 !important; font-size:8px; text-align:right;"
                            class="b-none">2</td>
                        <td style="padding-left:0 !important; padding-top: 0 !important; margin: 0 !important; font-size:8px;"
                            class="b-none"
                            colspan="16">KONFIRMASI DAN KLARIFIKASI IHSAN LANGSUNG KEPADA BAGIAN KEUANGAN</td>
                    </tr>
                    <tr>
                        <td style="padding-top: 0 !important; margin: 0 !important; font-size:8px; text-align:right;"
                            class="b-none">3</td>
                        <td style="padding-left:0 !important; padding-top: 0 !important; margin: 0 !important; font-size:8px;"
                            class="b-none"
                            colspan="16">ZISWA DIAMBIL DARI IHSAN POKOK, DIGUNAKAN UNTUK MEMBANTU SISWA YANG KURANG MAMPU
                            DAN KEGIATAN SOSIAL LAINNYA</td>
                    </tr>
                    <tr>
                        <td class="b-none"
                            colspan="17"><br></td>
                    </tr>
                    <tr>
                        <td class="b-none"
                            colspan="17"
                            style="text-align:center;">Tasikmalaya, {{ tgl_indo($data['tanggal']) }}</td>
                    </tr>
                    <tr>
                        <td class="b-none"
                            colspan="17"
                            style="text-align:center;">Bagian Keuangan,</td>
                    </tr>

                    <tr>
                        <td class="b-none"
                            colspan="17"
                            style="text-align:center;">
                            <br><br><br><br><br>
                            <b>NOVIYANTI, AMD</b>
                            <br><br>
                        </td>
                    </tr>


                </table>
            </div>

        </div>



        @if (($key + 1) % 2 == 0)
            <div class="page_break"></div>
        @else
            <div style="border-top: 1px dotted black; width:100%;"></div>
        @endif
    @endforeach
@endsection

@section('js-after')
@endsection
