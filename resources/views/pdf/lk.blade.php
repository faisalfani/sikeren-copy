@extends('layouts.pdf')

@section('title')
      Laporan
@endsection
@section('css-after')

<style>
body{
    
}
.page_break { page-break-before: always; }
</style>

  @endsection
@section('content')

@include('comp.header_pdf')
@php

$tgl  =     explode(" ",$req->filter);

$tgl_awal = $tgl[0];
$tgl_akhir = $tgl[2];


if($req->format_laporan == 'rentang'){
    if($req->bulan == 0){
        $pr = " Tahun " .$req->tahun_ajaran;

        $where = " YEAR(tgl_bayar) = $req->tahun_ajaran";

    }else{
        $pr = bulan_text($req->bulan) ." ". $req->tahun_ajaran;
        
        $where = " YEAR(tgl_bayar) = $req->tahun_ajaran AND MONTH(tgl_bayar) = $req->bulan ";

    }

}else if($req->format_laporan == 'periode'){
    $pr = tgl_indo($tgl_awal) ." - ". tgl_indo($tgl_akhir);

    $where = " tgl_bayar >= '$tgl_awal' AND tgl_bayar <= '$tgl_akhir'";

}else{
    $pr = "Seluruh";

    $where = "tgl_bayar";
}


@endphp

            <div class="sub-header">
                LAPORAN KEUANGAN KAS 
                <br>
                <br>
                Periode {{ $pr }}
               
            </div>
            <table>
                <thead>
                    <tr>
                        <th colspan="3">Pendapatan</th>
                    </tr>
                </thead>
                <tbody style="text-align:left;">
                <tr>
                    <th style="border-left:1px solid #ccc">No</th>
                    <th style="border-left:1px solid #ccc">Keterangan</th>
                    <th style="text-align:right;border-left:1px solid #ccc;border-right:1px solid #ccc"">Jumlah</th>
                </tr>
                @php
                    $totalPendapatan = 0;
                @endphp
                @foreach (Modules\Keuangan\Entities\JenisBiaya::orderBy("nama","ASC")->get() as $key => $d)
                    
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $d->nama }}</td>
                    <td style="text-align:right;border-right:1px solid #ccc"> 

                        @php
    
                            $jumlah =  Modules\Keuangan\Entities\Jurnal::
                            where("id_jenis_biaya",$d->id)
                            ->where("type",1)
                            ->whereRaw($where)
                            ->sum("jumlah");
                    
                            $totalPendapatan += $jumlah; 
                        @endphp
                        {{ rupiah($jumlah) }}
                    </td>
                </tr>

                @endforeach
                <tr>
                    <th> </th>
                    <th>
                        Total Pendapatan
                    </th>
                    <th style="text-align:right">
                        {{ rupiah($totalPendapatan) }}
                    </th>
                </tr>

            </tbody>
            </table>


            <div class="page_break"></div>
<br>
<br>
            <table>
                    <thead>
                        <tr>
                            <th colspan="3">Pengeluaran</th>
                        </tr>
                    </thead>
                    <tbody style="text-align:left;">
                    <tr>
                        <th style="border-left:1px solid #ccc">No</th>
                        <th style="border-left:1px solid #ccc">Keterangan</th>
                        <th style="text-align:right;border-left:1px solid #ccc;border-right:1px solid #ccc"">Jumlah</th>
                    </tr>
                    @php
                        $totalPengeluaran = 0;
                    @endphp
                    @foreach (Modules\Keuangan\Entities\JenisBiaya::orderBy("nama","ASC")->get() as $key => $d)
                        
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $d->nama }}</td>
                        <td style="text-align:right;border-right:1px solid #ccc"> 
    
                            @php
        
                                $jumlah =  Modules\Keuangan\Entities\Jurnal::
                                where("id_jenis_biaya",$d->id)
                                ->where("type",0)
                                ->whereRaw($where)
                                ->sum("jumlah");
                        
                                $totalPengeluaran += $jumlah; 
                            @endphp
                            {{ rupiah($jumlah) }}
                        </td>
                    </tr>
    
                    @endforeach
                    <tr>
                        <th> </th>
                        <th>
                            Total Pengeluaran
                        </th>
                        <th style="text-align:right">
                            {{ rupiah($totalPengeluaran) }}
                        </th>
                    </tr>
    
                </tbody>
                </table>

                <div class="page_break"></div>
                <br>
                <br>


                <table>
                        <thead>
                            <tr>
                                <th colspan="3">Kesimpulan</th>
                            </tr>
                        </thead>
                        <tbody style="text-align:left;">
                        <tr>
                            <th style="border-left:1px solid #ccc">No</th>
                            <th style="border-left:1px solid #ccc">Keterangan</th>
                            <th style="text-align:right;border-left:1px solid #ccc;border-right:1px solid #ccc"">Jumlah</th>
                        </tr>
                        @php
                            $totalK = 0;
                        @endphp
                        @foreach (Modules\Keuangan\Entities\JenisBiaya::orderBy("nama","ASC")->get() as $key => $d)
                            
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>Saldo {{ $d->nama }}</td>
                            <td style="text-align:right;border-right:1px solid #ccc"> 
        
                                @php
            
                                    $jumlah =  Modules\Keuangan\Entities\Jurnal::
                                    where("id_jenis_biaya",$d->id)
                                    ->where("type",1)
                                    ->whereRaw($where)
                                    ->sum("jumlah") - Modules\Keuangan\Entities\Jurnal::
                                    where("id_jenis_biaya",$d->id)
                                    ->where("type",0)
                                    ->whereRaw($where)
                                    ->sum("jumlah");
                            
                                    $totalK += $jumlah; 
                                @endphp
                                {{ rupiah($jumlah) }}
                            </td>
                        </tr>
        
                        @endforeach
                        <tr>
                            <th> </th>
                            <th>
                                Total Kesimpulan
                            </th>
                            <th style="text-align:right">
                                {{ rupiah($totalK) }}
                            </th>
                        </tr>
        
                    </tbody>
                    </table>

@endsection

@section('js-after')
@endsection