@extends('layouts.pdf')

@section('title')
    Laporan
@endsection
@section('css-after')
@endsection
@section('content')

    @include('comp.header_pdf')
    @php
    $tgl = explode(' ', $req->filter);
    $tgl_awal = $tgl[0];
    $tgl_akhir = $tgl[2];

    $tgl_aw = $tgl[0];
    $tgl_ak = $tgl[2];

    if ($req->format_laporan == 'rentang') {
        if ($req->bulan == 0) {
            $pr = ' Tahun ' . $req->tahun_ajaran;

            $tgl_awal = date("$req->tahun_ajaran-01-01");
            $tgl_akhir = date("$req->tahun_ajaran-12-31");

            $tgl_aw = date("$req->tahun_ajaran-01-01");
            $tgl_ak = date("$req->tahun_ajaran-12-31");
        } else {
            $pr = bulan_text($req->bulan) . ' ' . $req->tahun_ajaran;

            $tgl_awal = date("$req->tahun_ajaran-$req->bulan-01");
            $tgl_akhir = date("$req->tahun_ajaran-$req->bulan-31");

            $tgl_aw = date("$req->tahun_ajaran-$req->bulan-01");
            $tgl_ak = date("$req->tahun_ajaran-$req->bulan-31");
        }
    } else {
        $pr = tgl_indo($tgl_awal) . ' - ' . tgl_indo($tgl_akhir);
    }

    $saldo_akhir =
        Modules\Keuangan\Entities\Jurnal::where('ref', 1)
            ->where('tgl_bayar', '<', $tgl_aw)
            ->sum('jumlah') -
        Modules\Keuangan\Entities\Jurnal::where('ref', 2)
            ->where('tgl_bayar', '<', $tgl_aw)
            ->sum('jumlah');

    @endphp


    <div class="sub-header">
        JURNAL HARIAN
        <br>
        Periode {{ $pr }}

    </div>
    <table>
        <tbody style="text-align:left;">
            <tr style="text-align:center;">
                <th>Tanggal</th>
                <th>Keterangan</th>
                <th>D</th>
                <th>K</th>
                <th>Saldo</th>
            </tr>

            <tr>
                <td>
                    {{ tgl_indo($tgl_aw) }}
                </td>
                <td>
                    Saldo Bulan Lalu
                </td>
                <td style="text-align:right">

                </td>
                <td style="text-align:right">

                </td>
                <td>
                    {{ rupiah($saldo_akhir) }}
                </td>
            </tr>

            @while (strtotime($tgl_aw) <= strtotime($tgl_ak))
                @php
                    $jenisBiaya = Modules\Keuangan\Entities\JenisBiaya::orderBy('nama', 'ASC')->get();
                    
                    $tgl_cek = '';
                    
                    $jml_bank = Modules\Keuangan\Entities\Jurnal::where('ref', 2)
                        ->where('tgl_bayar', $tgl_aw)
                        ->sum('jumlah');
                @endphp

                @if ($jml_bank > 0)
                    <tr>
                        <td>
                            {{ tgl_indo($tgl_aw) }}
                        </td>
                        <td>
                            Setoran Transfer
                        </td>
                        <td style="text-align:right">

                        </td>
                        <td style="text-align:right">

                            {{ rupiah($jml_bank) }}
                        </td>
                        <td>
                            {{ rupiah($saldo_akhir -= $jml_bank) }}
                        </td>
                    </tr>
                @endif
                @foreach ($jenisBiaya as $item)
                    @php
                        $jurnal = Modules\Keuangan\Entities\Jurnal::where('ref', 1)
                            ->where('type', 1)
                            ->where('tgl_bayar', $tgl_aw)
                            ->where('id_jenis_biaya', $item->id);
                    @endphp
                    @if ($jurnal->get()->count() > 0)
                        <tr>
                            <td>
                                @if ($jml_bank == 0)
                                    {{ tgl_indo($tgl_aw) }}
                                @endif
                            </td>
                            <td>
                                {{ $item->nama }}
                            </td>
                            <td style="text-align:right">
                                {{ rupiah($jurnal->sum('jumlah')) }}
                            </td>
                            <td style="text-align:right">

                            </td>
                            <td>
                                {{ rupiah($saldo_akhir += $jurnal->sum('jumlah')) }}
                            </td>
                        </tr>
                    @endif
                    @php
                        $tgl_cek = $tgl_aw;
                        $jml_bank == 0;
                    @endphp
                @endforeach

                @php
                    $tgl_aw = date('Y-m-d', strtotime('+1 day', strtotime($tgl_aw)));
                    
                @endphp
            @endwhile


        </tbody>
    </table>

@endsection

@section('js-after')
@endsection
