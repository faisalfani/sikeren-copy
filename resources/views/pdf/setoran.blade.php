@extends('layouts.pdf')

@section('title')
      Laporan
@endsection
@section('css-after')

<style>
    @page { margin-left: 0px; margin-right: 0; }
body { margin-left: 0px; margin-right: 0; }
.page_break { page-break-before: always; }

</style>
@php
    if($req->print == 'true'){

@endphp
        <style>
            .kecilin {
                font-size: 12px !important;
            }
            .printin {
                margin-top:-70px;
            }
        </style>
@php
    }
@endphp
  @endsection
@section('content')

    @include('comp.header_pdf')



@php

$tgl  =     explode(" ",$req->filter);

        $tgl_awal = $tgl[0];
        $tgl_akhir = $tgl[2];
        

        if($req->format_laporan == 'rentang'){
            if($req->bulan == 0){
                $pr = " Tahun " .$req->tahun_ajaran;
        
                $where = " tahun_ajaran = $req->tahun_ajaran";
        
            }else{
                $pr = bulan_text($req->bulan) ." ". $req->tahun_ajaran;
                
                $where = " tahun_ajaran = $req->tahun_ajaran AND bulan = $req->bulan ";
        
            }
        
        }else if($req->format_laporan == 'periode'){
        
            $pr = tgl_indo($tgl_awal) ." - ". tgl_indo($tgl_akhir);
        
            if($tgl_awal == $tgl_akhir){
                $pr = tgl_indo($tgl_awal);
            }
            $awal = explode('-', $tgl_awal);
            $akhir = explode('-', $tgl_akhir);
            
            $bulan_awal = (int)$awal[1];
            $bulan_akhir = (int)$akhir[1];
            $where = " (bulan >= '$bulan_awal' AND tahun_ajaran >= '$awal[0]') AND (bulan <= '$bulan_akhir' AND tahun_ajaran <= '$akhir[0]')";
        
        }else{
            $pr = "Seluruh";
        
            $where = "bulan";
        }
        
        if ($req->jenjang != 'All') {
            switch ($req->jenjang) {
                case '1':
                    $where .= " AND jenjang = 'MTS'";
                    break;
                
                default:
                    $where .= " AND jenjang = 'MA'";
                    break;
            }
            // $where += $req->jenjang;
        } 
        
        if ($req->kelas != 'All') {
            $where .= " AND id_kelas = '$req->kelas'";
        } 

        $data = DB::table('siswas')
            ->leftJoin('biaya_siswas','siswas.nis','=','biaya_siswas.nis')
            ->where('no_hp','>',0)
            ->select('biaya_siswas.nis','siswas.nama','siswas.kelas','siswas.jenjang', DB::raw('sum(biaya_siswas.nominal) - sum(biaya_siswas.dibayar) as totalNominal'))
            ->groupBy('siswas.nis')
            ->having('totalNominal','>',0)
            ->whereRaw($where)
            ->orderBy('siswas.kelas', 'ASC')
            ->orderBy('siswas.nis', 'ASC')
            ->get();

if ($req->jenjang != 'All') {
    if ($req->jenjang == 1) {
        $jenjangData = 'MTS';
    } else {
        $jenjangData = 'MA';
    }
} else {
    $jenjangData = '';
}
$kelasData = '';
if ($req->kelas != 'All') {
    $kelasData = Modules\Master\Entities\Kelas::find($req->kelas);
}


@endphp

            <div class="sub-header">
                Laporan Tagihan Keuangan Siswa
                <br>
                Periode {{ $pr }}
                <br>
                @if ($jenjangData)
                    <b>Jenjang {{$jenjangData}} {{$kelasData ? 'Kelas '.$kelasData->nama : ''}}</b>
                @endif
                
               
            </div>

            <table style="width: 100%;table-layout: fixed; font-size: 12px;">
                <thead style="text-align:left;">
                    <tr>
                        <th style="border-left:1px solid #ccc; width: 3%;">No</th>
                        <th style="border-left:1px solid #ccc;width:12%;">NISN</th>
                        <th style="border-left:1px solid #ccc; width:40%;">Nama</th>
                        <th style="border-left:1px solid #ccc;">Kelas</th>
                        <th style="border-left:1px solid #ccc; width:10%;">Jenjang</th>
                        <th style="text-align:right;border-left:1px solid #ccc;border-right:1px solid #ccc; "">Tagihan</th>
                    </tr>
                </thead>
                <tbody>
                    
                    @foreach ($data as $key => $d)
                        
                    <tr>
                        <td class="kecilin" style="text-align: center; padding:0 1px;">{{ $key+1 }}</td>
                        <td class="kecilin">{{ $d->nis }}</td>
                        <td class="kecilin" style="white-space:nowrap">{{ $d->nama}}</td>
                        <td class="kecilin" style="white-space:nowrap">{{ $d->kelas}}</td>
                        <td class="kecilin" style="text-align: center">{{ $d->jenjang}}</td>
                        <td class="kecilin" style="text-align:right;border-right:1px solid #ccc;white-space:nowrap;"> {{ rupiah($d->totalNominal) }}</td>
                    </tr>

                    @endforeach

                    <tr>
                        <th colspan="5">
                            Total Tagihan
                        </th>
                        <th style="text-align:right;white-space:nowrap;">
                            {{ rupiah($data->sum('totalNominal')) }}
                        </th>
                    </tr>
                </tbody>
            </table>            


@endsection

@section('js-after')
@endsection