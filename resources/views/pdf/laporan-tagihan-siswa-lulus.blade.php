@extends('layouts.pdf')

@section('title')
    Laporan
@endsection
@section('css-after')
    <style>
        .page_break {
            page-break-before: always;
        }

    </style>
@endsection
@section('content')
    @include('comp.header_pdf')

    <div class="sub-header">
        LAPORAN TAGIHAN Siswa Lulus
    </div>
    <table style="font-size:12px;">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Tagihan</th>
            </tr>
        </thead>
        <tbody>
            @php
                $total = 0;
            @endphp
            @foreach ($res as $key => $s)
                @php
                    $total += $s['tagihan'];
                @endphp
                <tr>
                    <td> {{ $key + 1 }}</td>
                    <td> {{ $s['nama'] }}</td>
                    <td> {{ rupiah($s['tagihan']) }}</td>
                </tr>
            @endforeach
            <tr>
                <td colspan="2"
                    style="text-align:right">Total</td>
                <td> {{ rupiah($total) }}</td>
            </tr>
        </tbody>
    </table>
@endsection

@section('js-after')
@endsection
