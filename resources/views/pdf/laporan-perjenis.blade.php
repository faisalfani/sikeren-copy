@extends('layouts.pdf')

@section('title')
    Laporan 2
@endsection
@section('css-after')
@endsection
@section('content')
    @include('comp.header_pdf')
    @php
    $tgl = explode(' ', $req->filter);
    $tgl_awal = $tgl[0];
    $tgl_akhir = $tgl[2];

    $tgl_aw = $tgl[0];
    $tgl_ak = $tgl[2];

    if ($req->format_laporan == 'rentang') {
        if ($req->bulan == 0) {
            $pr = ' Tahun ' . $req->tahun_ajaran;

            $tgl_awal = date("$req->tahun_ajaran-01-01");
            $tgl_akhir = date("$req->tahun_ajaran-12-31");

            $tgl_aw = date("$req->tahun_ajaran-01-01");
            $tgl_ak = date("$req->tahun_ajaran-12-31");
        } else {
            $pr = bulan_text($req->bulan) . ' ' . $req->tahun_ajaran;

            $tgl_awal = date("$req->tahun_ajaran-$req->bulan-01");
            $tgl_akhir = date("$req->tahun_ajaran-$req->bulan-31");

            $tgl_aw = date("$req->tahun_ajaran-$req->bulan-01");
            $tgl_ak = date("$req->tahun_ajaran-$req->bulan-31");
        }
    } else {
        $pr = tgl_indo($tgl_awal) . ' - ' . tgl_indo($tgl_akhir);
    }

    if ($req->jenis_biaya == 0) {
        $where = 'id';
        $whereJenis = 'id';
    } else {
        $where = "id_jenis_biaya = '$req->jenis_biaya'";
        $whereJenis = "id = '$req->jenis_biaya'";
    }

    @endphp


    <div class="sub-header">
        LAPORAN PER JENIS BIAYA
        <br>
        <br>
        Periode {{ $pr }}

    </div>

    @foreach ($jenisBiaya = Modules\Keuangan\Entities\JenisBiaya::orderBy('nama', 'ASC')->whereRaw($whereJenis)->get()
        as $item)
        @php
            $saldo_akhir =
                Modules\Keuangan\Entities\Jurnal::where('ref', 1)
                    ->where('tgl_bayar', '<', $tgl_aw)
                    ->where('id_jenis_biaya', $item->id)
                    ->sum('jumlah') -
                Modules\Keuangan\Entities\Jurnal::where('ref', 2)
                    ->where('tgl_bayar', '<', $tgl_aw)
                    ->where('id_jenis_biaya', $item->id)
                    ->sum('jumlah');
        @endphp

        <table style="font-size: 12px;">
            <thead>
                <tr>
                    <th colspan="5"> {{ $item->nama }}</b></th>
                </tr>
            </thead>
            <tbody>
                <tr style="text-align:center;">
                    <th><b>Tanggal</b></th>
                    <th><b>Keterangan</b></th>
                    <th><b>D</b></th>
                    <th><b>K</b></th>
                    <th style="text-align:right">Saldo Akhir</b></th>
                </tr>
                <tr>
                    <td>

                    </td>
                    <td>
                        Saldo Bulan Lalu
                    </td>
                    <td style="text-align:right">

                    </td>
                    <td style="text-align:right">

                    </td>
                    <td style="text-align:right">
                        {{ rupiah($saldo_akhir) }}
                    </td>
                </tr>

                @while (strtotime($tgl_aw) <= strtotime($tgl_ak))
                    @php
                        
                        $tgl_cek = '';
                        
                        $jml_bank = Modules\Keuangan\Entities\Jurnal::where('ref', 2)
                            ->where('tgl_bayar', $tgl_aw)
                            ->whereRaw($where)
                            ->sum('jumlah');
                    @endphp



                    @if ($jml_bank > 0)
                        <tr>
                            <td>
                                {{ tgl_indo($tgl_aw) }}
                            </td>
                            <td>
                                Setoran Transfer
                            </td>
                            <td style="text-align:right">

                            </td>
                            <td style="text-align:right">

                                {{ rupiah($jml_bank) }}
                            </td>
                            <td>
                                {{ rupiah($saldo_akhir -= $jml_bank) }}
                            </td>
                        </tr>
                    @endif
                    @foreach ($jenisBiaya as $item)
                        @php
                            $jurnal = Modules\Keuangan\Entities\Jurnal::where('ref', 1)
                                ->where('type', 1)
                                ->where('tgl_bayar', $tgl_aw)
                                ->where('id_jenis_biaya', $item->id);
                        @endphp
                        @if ($jurnal->get()->count() > 0)
                            <tr>
                                <td>
                                    @if ($jml_bank == 0)
                                        {{ tgl_indo($tgl_aw) }}
                                    @endif
                                </td>
                                <td>
                                    {{ $item->nama }}
                                </td>
                                <td style="text-align:right">
                                    {{ rupiah($jurnal->sum('jumlah')) }}
                                </td>
                                <td style="text-align:right">

                                </td>
                                <td style="text-align:right">
                                    {{ rupiah($saldo_akhir += $jurnal->sum('jumlah')) }}
                                </td>
                            </tr>
                        @endif
                        @php
                            $tgl_cek = $tgl_aw;
                            $jml_bank == 0;
                        @endphp
                    @endforeach

                    @php
                        $tgl_aw = date('Y-m-d', strtotime('+1 day', strtotime($tgl_aw)));
                        
                    @endphp
                @endwhile


            </tbody>
        </table>
        <br>
        <br>
    @endforeach
@endsection

@section('js-after')
@endsection
