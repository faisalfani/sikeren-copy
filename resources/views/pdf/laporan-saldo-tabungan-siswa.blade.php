@php
    $tombolin = [
      "pdf" => false
];
@endphp
@extends('layouts.pdf', $tombolin)

@section('title')
      Laporan
@endsection
@section('css-after')
  @if ($req->print == 'true')
      <style>
        .isi > tr > td {
          font-size:11px;
        }
        .atas > tr > th {
          font-size:12px;
        }
        @page { margin: 0px; padding: 5px;}
        body { margin: 0px; padding: 5px;}
      </style>
  @endif
  <style>
    
  .page_break { page-break-before: always; }

    

  </style>

  @endsection
@section('content')

@include('comp.header_pdf')

{{-- @php
    // dd($res);
    $info = Modules\Core\Entities\CoreInfo::first();
@endphp --}}
            <div class="sub-header">
                Laporan Saldo Tabungan Siswa (TABSAN)
            </div>
            <table >
                    <thead class="atas">  
                      <tr>
                        <th rowspan="2">No</th>
                        <th rowspan="2">NIS</th>
                        <th rowspan="2">Nama</th>
                        <th colspan="3">SALDO</th>
                      </tr>
                      <tr>
                        <th>Masuk</th>
                        <th>Keluar</th>
                        <th>Sisa</th>
                      </tr>
                    </thead>
                    <tbody class="isi">
                    @php
                      $totalMasuk = 0;
                      $totalKeluar = 0;
                      $totalSisa = 0;
                      $result = json_decode($res, true);
                    @endphp
                    @foreach ($result as $key => $s)
                    @php
                      $totalMasuk += $s['masuk'];
                      $totalKeluar += $s['keluar'];
                      $totalSisa += $s['saldo'];
                    @endphp
                        <tr>
                          <td style="text-align: center;"> {{ $key+1 }}</td>
                          <td> {{ $s['nis'] }}</td>
                          <td> {{ $s['nama'] }}</td>
                          <td> {{ rupiah($s['masuk']) }}</td>
                          <td> {{ rupiah($s['keluar']) }}</td>
                          <td> {{ rupiah($s['saldo']) }}</td>
                        </tr>
                    @endforeach
                     <tr>
                          <td colspan="3" style="text-align:right">Total</td>
                          <td> {{ rupiah($totalMasuk) }}</td>
                          <td> {{ rupiah($totalKeluar) }}</td>
                          <td> {{ rupiah($totalSisa) }}</td>
                        </tr>
                      <tr>
                        <td colspan="6" style="border: none;padding-top:3px;font-size:10px;">Di cetak oleh : {{ $user}}</td>
                      </tr>
                    </tbody>
                  </table>


@endsection

@section('js-after')

@endsection