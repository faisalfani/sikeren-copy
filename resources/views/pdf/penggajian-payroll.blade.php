@extends('layouts.pdf')

@section('title')
      Laporan
@endsection
@section('css-after')

<style>
body{
    
}
.arialin{
  font-family: 'Times New Roman', Times, serif !important;
  font-size: 11px;
}
.b-none{
  border: none;
}
.page_break { page-break-before: always; }
table tr td{
  font-size: 7px;
  padding: 0 1px;
  margin: 0;
  text-align:center
}
table tr th{
  font-size: 10px;
  text-align:center
  /* font-weight: normal; */
}
@page { margin: 0; }
body { margin: 0; }
</style>

@endsection
@section('content')

@php
    function rupiahin($angka){
	
      $hasil_rupiah = number_format($angka,0,',','.');
      return $hasil_rupiah;
    
    }
@endphp

@include('comp.header_payroll')
<div style="margin: 0 50px;">
  <table style="width:100%;border:none;padding:0;margin:0;">
    <tr>
      <th class="" style="text-align:center; width: 1%;">No</th>
      <th class="" style="text-align:center;">Nama Nasabah</th>
      <th class="" style="text-align:center;">Nomor Rekening</th>
      <th class="" style="text-align:center;" colspan="2">TOTAL IHSAN</th>
      <th class="" style="text-align:center;width:100px;">KET</th>
    </tr>
    
    @foreach ($res as $key => $v)
        <tr>
          <td>{{$key + 1}}</td>
          <td style="text-align: left;">{{$v['nama']}}</td>
          <td style="text-align: left;">{{$v['no_rek']}}</td>
          
          <td style="width:1%;border-right:none;">Rp </td>
          <td style="border-left:none; text-align: right !important;">{{rupiahin($v['diterima'])}}</td>
          <td style="width:100px;"></td>
        </tr>
    @endforeach
    <tr>
      <td colspan="3" style="text-align: right;">Jumlah</td>
      <td style="width:1%;border-right:none;">Rp </td>
      <td style="border-left:none; text-align: right !important;">{{rupiahin($jumlah->diterima)}}</td>
      <td style="width:100px;"></td>
    </tr>
    
  </table>
</div>
@endsection

@section('js-after')
@endsection