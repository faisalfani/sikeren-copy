@extends('layouts.pdf')

@section('title')
    Laporan
@endsection
@section('css-after')
@endsection
@section('content')
    @include('comp.header_pdf')

    @php

    $siswa = Modules\Siswa\Entities\Siswa::where('nis', $req->nis)->first();

    $tgl = explode(' ', $req->filter);
    $tgl_awal = $tgl[0];
    $tgl_akhir = $tgl[2];

    if ($req->format_laporan == 'rentang') {
        if ($req->bulan == 0) {
            $pr = ' Tahun ' . $req->tahun_ajaran;

            $tgl_awal = date("$req->tahun_ajaran-01-01");
            $tgl_akhir = date("$req->tahun_ajaran-12-31");

            $where = "created_at >= '$tgl_awal' AND created_at <= '$tgl_akhir'";
        } else {
            $pr = bulan_text($req->bulan) . ' ' . $req->tahun_ajaran;

            $tgl_awal = date("$req->tahun_ajaran-$req->bulan-01");
            $tgl_akhir = date("$req->tahun_ajaran-$req->bulan-31");

            $where = "created_at >= '$tgl_awal' AND created_at <= '$tgl_akhir'";
        }
    } elseif ($req->format_laporan == 'periode') {
        $pr = tgl_indo($tgl_awal) . ' - ' . tgl_indo($tgl_akhir);
        $where = "created_at >= '$tgl_awal' AND created_at <= '$tgl_akhir'";
    } else {
        $pr = 'Semua';
        $where = 'id';
    }

    @endphp


    <div class="sub-header">

        @if ($req->type == 'tagihan')
            LAPORAN TAGIHAN SISWA
        @else
            LAPORAN PEMBAYARAN SISWA
        @endif


        <br>
        Periode {{ $pr }}

    </div>

    @if ($req->type == 'tagihan')
        @php
            $dataa = Modules\Keuangan\Entities\BiayaSiswa::where('nis', $req->nis)
                ->whereRaw($where)
                ->whereRaw('nominal != dibayar');
            
            $data = $dataa->get();
            
            $totalTagihan = $dataa->sum('nominal');
            $totalDibayar = $dataa->sum('dibayar');
        @endphp
    @else
        @php
            $dataa = Modules\Keuangan\Entities\BiayaSiswa::where('nis', $req->nis)
                ->whereRaw($where)
                ->whereRaw('dibayar > 0');
            
            $data = $dataa->get();
            $totalTagihan = $dataa->sum('nominal');
            $totalDibayar = $dataa->sum('dibayar');
            
        @endphp
    @endif

    <table style="font-size:14px;text-align:left;">
        <tr>
            <td colspan="2"
                style="background:#fff;border:0;text-align:right;">
                NIS
            </td>
            <td colspan="2"
                style="background:#fff;border:0;">
                : <b>{{ $siswa->nis }}</b>
            </td>

            <td style="background:#fff;border:0;">
                Tagihan
            </td>
            <td colspan="2"
                style="background:#fff;border:0;">
                : <b>{{ rupiah($totalTagihan) }}</b>
            </td>
        </tr>
        <tr>
            <td colspan="2"
                style="background:#fff;border:0;text-align:right;">
                Nama
            </td>
            <td colspan="2"
                style="background:#fff;border:0;">
                : <b>{{ $siswa->nama }}</b>
            </td>
            <td style="background:#fff;border:0;">
                Dibayar
            </td>
            <td colspan="2"
                style="background:#fff;border:0;">
                : <b>{{ rupiah($totalDibayar) }}</b>
            </td>

        </tr>

        <tr>
            <td colspan="2"
                style="background:#fff;border:0;text-align:right;">
                Kelas
            </td>
            <td colspan="2"
                style="background:#fff;border:0;">
                : <b>{{ $siswa->kelas }}</b>
            </td>
            <td style="background:#fff;border:0;">
                Tunggakan
            </td>
            <td colspan="2"
                style="background:#fff;border:0;">
                : <b>{{ rupiah($totalTagihan - $totalDibayar) }}</b>
            </td>
        </tr>

    </table>
    <table>
        <tbody style="text-align:left;font-size: 12px">
            <tr style="text-align:center;">
                <th>No</th>
                <th>Nama Biaya</th>
                <th>Jenis</th>
                <th>Bulan</th>
                <th>Tagihan</th>
                <th>Dibayar</th>
                <th>Tunggakan</th>
            </tr>
            @foreach ($data as $key => $item)
                <tr>
                    <td>
                        {{ $key + 1 }}
                    </td>
                    <td>
                        {{ $item->nama }}
                    </td>
                    <td>
                        {{ $item->satuan }}
                    </td>
                    <td>
                        {{ bulan_text($item->bulan) }} {{ $item->tahun_id }}
                    </td>
                    <td>
                        {{ rupiah($item->nominal) }}
                    </td>
                    <td>
                        {{ rupiah($item->dibayar) }}
                    </td>
                    <td>
                        {{ rupiah($item->nominal - $item->dibayar) }}
                    </td>
                </tr>
            @endforeach

        </tbody>
    </table>
@endsection

@section('js-after')
@endsection
