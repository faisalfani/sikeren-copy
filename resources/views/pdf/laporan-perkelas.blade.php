@extends('layouts.pdf')

@section('title')
      Laporan
@endsection
@section('css-after')

<style>
.page_break { page-break-before: always; }
</style>

  @endsection
@section('content')

@include('comp.header_pdf')

            <div class="sub-header">
                LAPORAN TAGIHAN KELAS
                <br>
               {{ $kelas->nama}}
            </div>
            <table >
                    <thead>  
                      <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Tagihan</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach ($res as $key => $s)
                        <tr>
                          <td> {{ $key+1 }}</td>
                          <td> {{ $s['nama'] }}</td>
                          <td> {{ rupiah($s['tagihan']) }}</td>
                         
                        </tr>
                    @endforeach
                    </tbody>
                  </table>


@endsection

@section('js-after')

@endsection