@extends('layouts.pdf')

@section('title')
    Laporan
@endsection
@section('css-after')
    <style>
        .break-before {
            page-break-before: always;
        }

        @page {
            margin: 10px 5px;
        }

        #content {
            padding: 0 15px;
        }

        .arialin {
            font-family: 'Times New Roman', Times, serif !important;
            font-size: 11px;
        }

        .b-none {
            border: none;
        }

        .page_break {
            page-break-before: always;
        }

        table tr td {
            font-size: 5px;
            padding: 0 1px;
            margin: 0;
            text-align: center
        }

        table tr th {
            font-size: 6px;
            text-align: center
                /* font-weight: normal; */
        }

    </style>
@endsection
@section('content')
    @php
    function rupiahin($angka)
    {
        $hasil_rupiah = number_format($angka, 0, ',', '.');
        return $hasil_rupiah;
    }
    @endphp
    @foreach ($res as $k => $value)
        <table style="width: 100%;margin:0;padding:0;">
            <tr>
                <td style="padding:0;margin:0;border: none;width:20%;">
                    <img class="logo"
                        src="https://ponpescondong.com/logo-pesantren.png"
                        alt="logo">
                </td>
                <td style="padding:0;margin:0;border: none;">
                    <br>
                    <p style="font-weight:500;text-transform:uppercase;font-size:18px;margin:0;padding:0;">DATA IHSAN
                        MTS/MA TERPADU RIYADLUL ULUM WADDA'WAH CONDONG
                        <br>
                        BULAN {{ bulan_text($req->bulan) }} {{ $req->tahun }}
                        <br>
                        {{ $k }}
                    </p>

                </td>
            </tr>
        </table>
        <div style="height:3px;border-top:2px solid #ccc;border-bottom:2px solid #ccc;margin:10px 50px;">
        </div>
        <div id="content">

            <table style="width:100%;border:none;padding:0;margin:0;">
                <tr>
                    <th class=""
                        style="margin:0;padding:0 1px;text-align:center; width: 1%;"
                        rowspan="2">No</th>
                    <th class=""
                        style="margin:0;padding:0 1px;text-align:center;"
                        rowspan="2">Nama Nasabah</th>
                    <th class=""
                        style="margin:0;padding:0 1px;text-align:center;"
                        rowspan="2">Nomor Rekening</th>
                    {{-- <th class="" style="margin:0;padding:0 1px;text-align:center;" colspan="4">IHSAN</th> --}}
                    <th class=""
                        style="margin:0;padding:0 1px;text-align:center;"
                        rowspan="2"
                        colspan="2">TOTAL PENDAPATAN</th>
                    <th class=""
                        style="margin:0;padding:0 1px;text-align:center;"
                        colspan="{{ $header['potongan']->count() < 1 ? '2' : count($header['potongan']) * 2 }}">POTONGAN
                    </th>
                    <th class=""
                        style="margin:0;padding:0 1px;text-align:center;"
                        rowspan="2"
                        colspan="2">TOTAL IHSAN</th>
                    <th class=""
                        style="margin:0;padding:0 1px;text-align:center;"
                        rowspan="2">KET</th>
                </tr>
                <tr>
                    {{-- <th colspan="2">POKOK</th>
      <th colspan="2">LAIN2</th> --}}
                    @foreach ($header['potongan'] as $item)
                        <th colspan="2">{{ $item->nama }}</th>
                    @endforeach
                    @if ($header['potongan']->count() < 1)
                        <th colspan="2">-</th>
                    @endif
                </tr>

                @foreach ($value as $key => $v)
                    <tr>
                        <td style="margin:0;padding:0 1px;">{{ $key + 1 }}</td>
                        <td style="margin:0;padding:0 1px;text-align: left;">{{ $v['nama'] }}</td>
                        <td style="margin:0;padding:0 1px;text-align: left;">{{ $v['no_rek'] }}</td>
                        {{-- <td style="width:1%;border-right:none;">Rp </td>
          <td style="border-left:none; text-align: right !important;">{{rupiahin($v['pokok'])}}</td>
          <td style="width:1%;border-right:none;">Rp </td>
          <td style="border-left:none; text-align: right !important;">{{rupiahin($v['lain2'])}}</td> --}}
                        <td style="width:1%;border-right:none;margin:0;padding:0 1px;">Rp </td>
                        <td style="margin:0;padding:0 1px;border-left:none; text-align: right !important;">
                            {{ rupiahin($v['total']) }}</td>
                        @foreach ($v['potongan'] as $item)
                            <td style="margin:0;padding:0 1px;width:1%;border-right:none;">Rp </td>
                            <td style="margin:0;padding:0 1px;border-left:none; text-align: right !important;">
                                {{ rupiahin($item->jumlah_ihsan) }}</td>
                        @endforeach
                        @if (count($v['potongan']) < 1)
                            <td style="margin:0;padding:0 1px;width:1%;border-right:none;">Rp </td>
                            <td style="margin:0;padding:0 1px;border-left:none; text-align: right !important;">-</td>
                        @endif
                        <td style="margin:0;padding:0 1px;width:1%;border-right:none;">Rp </td>
                        <td style="margin:0;padding:0 1px;border-left:none; text-align: right !important;">
                            {{ rupiahin($v['diterima']) }}</td>
                        <td style="margin:0;padding:0 1px;width:100px;"></td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="3"
                        style="margin:0;padding:0 1px;"></td>
                    {{-- <td style="width:1%;border-right:none;">Rp </td>
      <td style="border-left:none; text-align: right !important;">{{rupiahin($jumlah->pokok)}}</td>
      <td style="width:1%;border-right:none;">Rp </td>
      <td style="border-left:none; text-align: right !important;">{{rupiahin($jumlah->lain2)}}</td> --}}
                    <td style="margin:0;padding:0 1px;width:1%;border-right:none;">Rp </td>
                    <td style="margin:0;padding:0 1px;border-left:none; text-align: right !important;">
                        {{ rupiahin($jumlah->total) }}</td>
                    @foreach ($jumlah->potongan as $item)
                        <td style="margin:0;padding:0 1px;width:1%;border-right:none;">Rp </td>
                        <td style="margin:0;padding:0 1px;border-left:none; text-align: right !important;">
                            {{ rupiahin($item) }}</td>
                    @endforeach
                    @if (count($jumlah->potongan) < 1)
                        <td style="margin:0;padding:0 1px;width:1%;border-right:none;">Rp </td>
                        <td style="margin:0;padding:0 1px;border-left:none; text-align: right !important;">-</td>
                    @endif
                    <td style="margin:0;padding:0 1px;width:1%;border-right:none;">Rp </td>
                    <td style="margin:0;padding:0 1px;border-left:none; text-align: right !important;">
                        {{ rupiahin($jumlah->diterima) }}</td>
                    <td style="margin:0;padding:0 1px;width:100px;"></td>
                </tr>

            </table>
        </div>
        <div class="page_break"></div>
    @endforeach
@endsection

@section('js-after')
@endsection
