@extends('layouts.app')

@section('title')
      My Profile
@endsection
@section('content')

<form action="/edit-profile" method="POST">
    @csrf
<div class="container pt-4">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card card-success">
                <div class="card-header">
                        Profil Saya
                </div>
                <div class="card-body">
                       
                       <div class="w-100">
                           @include('comp.notif')
                       </div>
                       <div class="form-group">
                            <label> Nama </label> 
                            <input type="text" name="name" value="{{ Auth::user()->name }}" class="form-control">
                       </div>
                       <div class="form-group">
                            <label> username </label> 
                            <input type="text" name="username" value="{{ Auth::user()->username }}"  class="form-control">
                        </div>
                       <div class="form-group">
                            <label> Email </label> 
                            <input type="text" name="email" value="{{ Auth::user()->email }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label> Password Baru</label>
                            <input placeholder="Password Baru" type="text" value="" name="password" class="form-control">                            
                            <small id="emailHelp" class="form-text">
                            Isi Jika ingin merubah password dan kosongkan jika tidak
                            </small>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn bg-primary-c btn-block float-right">
                                <i class="fa fa-save"></i>
                                Save</button>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

</form>
<br>
<br>
<br>
@endsection
