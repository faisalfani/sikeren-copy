<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta http-equiv="x-ua-compatible" content="ie=edge">
   <title>  @yield('title')</title>
   <link rel="stylesheet" href="/plugins/fontawesome-free/css/all.min.css">
   <style>
    *{
      box-sizing: border-box
    }
  table{
      font-family: Arial,Helvetica Neue,Helvetica,sans-serif; 
      background: #fff;
      padding: 10px 35px;
      border-collapse: collapse
  }

table tr th{background-color: #f2f2f2;}

table th {background-color: #ddd;}
table tr:hover {background-color: #ddd;}

  table tr td,table tr th{
      padding: 1px 10px;
      border:1px solid #ccc;
  }
  table tr td{
    border-left:1px solid #ccc
  }
  table{
      width: 100%;
  }
  thead{
      text-align: center;
  }
  .logo{
    position: absolute;
    left: 90px;
    width: 60px;
  }
  .header{
    position: relative;
    text-align: center;
    width: 100%;
    padding: 10px;
    padding-top: 50px;
    font-weight: bold
  }
  .sub-header{
    text-align: center;
    width: 100%;
    padding-bottom: 20px;
    font-weight: bold
  }
 
   </style>
  
  @yield('css-after')
  
</head>
<body>
  @if ($req->print !== 'true')
  <style>
    *{
      padding: 0;
      margin: 0;
      
    }
    .header{

      padding: 0px !important;
    }
  .navbar{
    background: #00695C;
    position: fixed;
    top: 0;
    width: 100%;
    padding: 10px 5%;
    z-index: 10000000
  }
  .btn{
    padding: 10px 35px;
    cursor: pointer;
    margin: 0px 10px;
  }
  .btn:hover{
    opacity: .8;
  }
  .left{
    float: left
  }
  .right{
    float: right
  }
  main{
    margin-top: 100px;
    width: 80%;
    margin-left: 10%;
    margin-bottom: 100px;
    min-width: 800px
  }

  table{
      width: 86%;
      margin-left: 7%
  }
  .logo{
    left: 80px;
  }

  </style>    
<header class="navbar">

  
  <div onclick="goBack()" class="btn right" style="color:red;background:#fff">
      <i class="fa fa-times"></i> Close
    </div>

    @if (Route::current()->getName() != "penggajian-resume")
        
    
  <div class="btn right" style="color:#00695C;background:#fff" onclick="printdata('excel')">
    <i class="fa fa-print"></i> Excel
  </div>
  @endif

  @if (( empty($tombolin) ? true : $tombolin['pdf']) == true )
      <div class="btn right" style="color:#00695C;background:#fff" onclick="printdata('pdf')">
    <i class="fa fa-print"></i> PDF 
  </div>
  @endif
  

  

</header>

@endif
<main>
  @yield('content')
</main>

<script>
function printdata(tipe){
  var link = window.location.href;
  if(tipe == 'pdf'){
    if(link.includes('?')){
      window.location.href = link + "&print=true";
    }else{
      window.location.href = link + "?print=true";
    }
  }else{
    if(link.includes('?')){
      window.location.href = link + "&print=excel";
    }else{
      window.location.href = link + "?print=excel";
    }
  }
  
}

function goBack(){
 window.close();
}

</script>
@yield('js-after')
</body>
</html>
