@php
  $info = Modules\Core\Entities\CoreInfo::first();
  $tahun = date("Y");
  $pattern = "/tahunSekarang/i";
  $info->copyright_before_login = preg_replace($pattern, $tahun, $info->copyright_before_login);
  $info->copyright_after_login = preg_replace($pattern, $tahun, $info->copyright_after_login);
  // dd($info);
@endphp

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  @laravelPWA

  <link rel="icon" sizes="64x64" href="{{ $info->favicon ? '/images/custom/'.$info->favicon : '/img/icon.png'}}">
  <link rel="apple-touch-icon" href="{{ $info->favicon ? '/images/custom/'.$info->favicon : '/img/icon.png'}}">
  
  <title>  {{ $info->nama ? $info->nama : 'SIKEREN' }} | @yield('title')</title>
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <link rel="stylesheet" href="../../css/app.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">
  <!-- style tambahan -->
    <link href="/style/core.min.css" rel="stylesheet" type="text/css">
    <!-- yg diatas konflik -->
    <link href="/style/styles.css" rel="stylesheet" type="text/css">
    <link href="/style/colors.min.css" rel="stylesheet" type="text/css">
    {{--<link href="style/bootstrap.min.css" rel="stylesheet" type="text/css">--}}
    {{--<link href="style/components.min.css" rel="stylesheet" type="text/css">--}}

<style>
.content-wrapper{
    /* Center and scale the image nicely */
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    margin-top:calc(6.7rem - 1px) !important;
    /* background: url(https://www.roomtoreward.org/wp-content/uploads/office-blur.jpg); */
}
.bg-primary-c, .btn-primary{
    background: {{$info->color_primary ? $info->color_primary : '#00695C'}};
    color: #fff
}
.bg-primary-c:hover{
  color: #f1f1f1
}
.bg-secondary-c{
   background:{{$info->color_secondary ? $info->color_secondary : '#4CAF50'}};
}
.navigation-tab {
  height: 60px;
  width: 100%;
  background-color: #fff;
  box-shadow: 0 50px 30px 0 rgba(0, 0, 0, 0.175);
  overflow: hidden;
  border: 1px solid #fff;
  display: flex;
  position: fixed;
  flex-shrink: 0;
  bottom: 0;
  z-index: 1000;
  opacity: 0;
}
.navigation-tab-item {
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 20%;
  flex-shrink: 0;
  cursor: pointer;
  transition: 0.3s;
  position: relative;
  z-index: 2;
  font-size: 10px;
  color: black;
  text-align: center
}
.navigation-tab-item i{
  width: 100%;
  float: left;
}
.navigation-tab-item.active {
    width: 20%;
    color: #00695C;
}

.navigation-tab-overlay {
  border-bottom: 2px solid #00695C;
  height: 100%;
  width: 20%;
  position: absolute;
  left: 0;
  top: 0;
  transition: 0.3s;
}
.navigation-tab__icon {
  display: block;
  /* color: #4298e7; */
  transition-duration: 0.3s;
}


@media (max-width:800px){
    .navigation-tab{
        opacity: 1;
    }
    #nav-menu{
      display: none
    }
}



.nav-menu-active{
  position: fixed;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  display: block;
  z-index: 1000000;
  background:#00695C;
  float: left;
  padding: 20px 12px;
  padding-top: 30%;
  overflow-y: scroll

}
.nav-menu-active ul li{
  padding: 10px 12px;
}
.nav-menu-active .nav-item{
  padding: 10px 12px;

}
.btn-close-menu{
  z-index: 100000000;
  position: fixed;
  top: 30px;
  right: 30px
}

.dropdown-item{
  font-size: 15px
}

#right-navbar-link{
  font-size:15px;
  height: 65px;
  display: flex;
  align-items:center;
  
}

#right-navbar-link:hover{
  background-color: #00000050;
}

.second-navbar{
  margin-top: 4rem;
  background: white;
  display: flex;
  align-items:center;
  padding:0;

}

.second-navbar a{
  color: black;
  font-size: 15px;
  font-weight: normal;
  display: flex;
  align-items: center;
  column-gap: 8px;
}

.second-navbar a:hover{
  color:white;
  background: {{$info->color_primary ? $info->color_primary : '#00695C'}};

}

.main-header{
  padding:0;
}



@media (max-width:480px){
   .content-wrapper{
     margin-top: calc(3.5rem - 1px) !important;
   }

   .second-navbar{
     margin-top: 0 !important
   }
   .second-navbar a{
     color: white !important
   }

   .second-navbar .dropdown-item{
     color: black !important
   }

  .main-header{
    padding: 7px
  }

  #right-navbar-link{
    display: none
  }
}


.brand-image{
  margin-left: 1rem;
}

#dropdownSubMenus{
  display: flex;
  column-gap: 8px;
  align-items:center
}

.title-wrapper{
  display: flex;
  justify-content: space-between;
  align-items:center;
}

.breadcrumb{
  font-size:12px
}

.breadcrumb-title{
  display: flex;
  column-gap: 8px;
  align-items: center;
  margin-left:0.5rem
}

.breadcrumb-title small{
  font-size: 15px
}


.breadcrumb .breadcrumb-item{
  padding-left:0
}

.breadcrumb .breadcrumb-item:not(:first-child)::before{
  content: ">";
  padding: 0 5px;
  color: #ccc
}


  </style>
@yield('css-after')
</head>
<body class="hold-transition layout-top-nav layout-navbar-fixed">
<div class="wrapper">

<header class="main-header flex-column">

  <!-- Navbar -->
  <nav  class="main-header  navbar navbar-expand navbar-dark bg-primary-c
  border-bottom-0
  ">
    <div class="container">
      <a href="/" class="navbar-brand">
        <img src="{{ $info->logo_after_login ? '/images/custom/'.$info->logo_after_login : '/logo.png'}}" alt="AdminLTE Logo" class="brand-image"
            >
      </a>
      <a class="d-block d-lg-none btn btn-outline-primary text-white" onclick="activeMenu()" href="#">
      Menu
      </a>     
     

       <!-- Right navbar links -->
      <div id="right-navbar-link">
        <ul  class="navbar-nav ml-auto d-none d-lg-block">
                   

                    <li class="dropdown user user-menu dropdown-hover">
                    <a class="nav-link" id="dropdownSubMenus" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle" href="#" >
                      <i class="far fa-user"></i> {{ Auth::user()->name}} <i class="caret"></i>
                    </a>
                    <ul  aria-labelledby="dropdownSubMenus" class="dropdown-menu border-0 shadow dropdown-submenu dropdown-hover">
                    <li class="user-header bg-light-blue"> 
                      <img src="{{ !Auth::user()->avatar ? '/img/logo.png' : Auth::user()->avatar}}" class="img-circle" alt="User Image">
                                        <p> {{ Auth::user()->role->nama}} <small>Login Terakhir :<br>
                                                Rabu, 20 April 2022, 02:28:04</small> </p>
                    </li>
                    <li class="user-footer">
                      <div class="float-left">
                          <a href="/profile" class="btn btn-default btn-flat">Edit Profil</a>
                        </div>
                      <div class="float-right">
                        
                        <a class="btn bg-red btn-flat" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();">
                          {{ __('Logout') }}
                      </a>
          
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                      </div>
                    </li>
                </ul>
            </div>


    </div>
  </nav>
  <nav  class="second-navbar navbar navbar-expand  bg-primary-c
  border-bottom-0
  ">
    <div class="container">
      <a class="btn btn-danger text-white btn-close-menu" onclick="closeMenu()" href="#">
        Tutup
      </a>      
      <!-- Left navbar links -->
      <ul id="nav-menu" class="navbar-nav">
            
        @php
            $hakakses = json_decode(Auth::user()->role->menu);
        @endphp
                @foreach (App\menu::where("sub",null)
                ->whereIn('id',$hakakses)
                ->orderBy("urutan","ASC")
                ->get() as $item)
                      @php
                        $submenu = App\menu::where("sub",$item->id)->whereIn('id',$hakakses)->get();                      
                      @endphp
                 @if ($submenu->count() > 0)
                 
                    <!-- Level two dropdown-->
                    <li class="nav-item dropdown dropdown-hover">

                    <a id="dropdownSubMenu2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle" href="{{ $item->url }}">    
                            <i class="{{ $item->icon }}"></i>
                              {{ $item->nama }}
                      </a>
                        
                        
                        @if ($submenu->count() > 0)
                            <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow dropdown-submenu dropdown-hover">
                              @foreach ($submenu as $key => $item1) 

                              @php
                                $submenu1 = App\menu::where("sub",$item1->id)->whereIn('id',$hakakses)->get();                      
                              @endphp
                              @if ($submenu1->count() == 0)
                                  <li>
                                    <a href="{{ $item1->url }}" class="dropdown-item">
                                      <i class="{{ $item1->icon }} mr-1"></i>
                                      {{ $item1->nama }}
                                    </a>
                                  </li>
                                @else 
                                  <!-- Level three dropdown-->
                                  <li class="dropdown-submenu">
                                      <a id="dropdownSubMenu3" href="{{ $item1->url }}"
                                           data-toggle="dropdown" 
                                         class="dropdown-item dropdown-toggle">
                                         <i class="{{ $item1->icon }} mr-1"></i>
                                         {{ $item1->nama }}
                                        </a>
                                      
                                      <ul aria-labelledby="dropdownSubMenu3" class="dropdown-menu border-0 shadow">
                                        <li><a href="{{ $item1->url }}" class="dropdown-item">
                                            <i class="{{ $item1->icon }} mr-1"></i>
                                            {{ $item1->nama }}
                                          </a></li>
                                          @foreach ($submenu1 as $item2)
                                            <li><a href="{{ $item2->url }}" 
                                              class="dropdown-item">
                                              <i class="{{ $item2->icon }} mr-1"></i>
                                              {{ $item2->nama }}
                                            </a></li>      
                                          @endforeach
                                        
                                      </ul>
                                    </li>
                                  <!-- End Level three -->
                                  @endif
                                  @endforeach
                              </ul>
                          @endif   
                  
                      </li>
  
                 @else    
                 <li  class="nav-item d-none d-sm-inline-block">   
                        <a href="{{ $item->url }}" class="nav-link">
                          <i class="{{ $item->icon }}"></i>
                          {{ $item->nama }}
                        </a>
                  </li>
                  @endif
                      

                         
                @endforeach
          
      </ul>
    </div>
  </nav>
  <!-- /.navbar -->
</header>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <div class="content-header p-2 text-dark mb-2">
            <div class="container title-wrapper mt-1">
                    @php
                    
                        $menu = App\menu::where("url", '/'.Request::path('profile'))->first();
                        
                        if(isset($menu->sub)){
                          $submenu = App\menu::find($menu->sub);
                          $sub = $submenu->nama;
                          $sub_url = $submenu->url;
                        }else{
                          $sub = "";
                          $sub_url = "";
                        }
                    @endphp 
                    @if (isset($menu->nama))
                    <div class="breadcrumb-title">
                     <h4>{{$sub}}</h4><small class="text-secondary"> {{$menu->nama}}</small>
                    </div>
                    @else
                    <div class="breadcrumb-title">
                    <h4>Selamat Datang di Sistem Informasi Keuangan Pesantren</h4>
                    </div>
                        
                    @endif
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                         <i class="@if(isset($menu->icon)) {{ $menu->icon }} @else fa fa-stream @endif mr-2"></i>  
                      </li>
                   
                   
                      <li class="breadcrumb-item">
                        <a class="text-dark" href="/">Home</a>
                      </li>
                       @if ($sub)
                       <li class="breadcrumb-item">
                        <a class="text-dark" href="{{ $sub_url }}"> {{ $sub }}</a>
                       </li>
                       @endif
                    
                    </li>
                    <li class="breadcrumb-item active text-dark">
                        
                        {{ ($menu) ? $menu->nama : "Home" }}
                      
                    </li>
                  </ol>
            </div><!-- /.container-fluid -->
          </div>
          <!-- /.content-header -->
    
          
   <!-- Main content -->
   <div class="content container" style="min-height:400px">

                <!-- Main content -->
                @yield('content')

    </div>
    <br>
    <br>



<div class="d-block d-lg-none">
<nav class="navigation-tab">
    <a href="/" class="navigation-tab-item">
      <span class="navigation-tab__icon">
          <i class="fa fa-chart-line"></i>
          Home
      </span>
    </a>
    <a href="/input-cash-cicil" class="navigation-tab-item @if(Request::is('input-cash-cicil*')) active @endif">
        <span class="navigation-tab__icon">
            <i class="fa fa-th-large"></i>
            Tagihan
        </span>
    </a>
    <a href="/input-tabungan-siswa" class="navigation-tab-item @if(Request::is('input-tabungan-siswa*')) active @endif">
        <span class="navigation-tab__icon">
            <i class="fa fa-search"></i>
            Tabungan
        </span>
    </a>
    <a href="/keuangan/generate-biaya" class="navigation-tab-item @if(Request::is('keuangan*')) active @endif">
        <span class="navigation-tab__icon">
            <i class="far fa-money-bill-alt"></i>
            Generate
        </span>
    </a>
    <a href="/profile" class="navigation-tab-item @if(Request::is('profile')) active @endif">
        <span class="navigation-tab__icon">
            <i class="fa fa-user"></i>
            Profil
        </span>
    </a>
    <div class="navigation-tab-overlay"
    
    @if(Request::is('input-cash-cicil*')) style="left:20%" @endif
    @if(Request::is('input-tabungan-siswa*')) style="left:40%" @endif
    @if(Request::is('keuangan*')) style="left:60%" @endif
    @if(Request::is('profile')) style="left:80%" @endif
    
    ></div>
  </nav>
</div>
  <!-- Main Footer -->
  <footer class="main-footer" style="position: fixed;bottom: 0;width: 100%; height: 30px; font-size: 12px; padding-top: 5px">
    <!-- Default to the left -->
    @php
      if($info->copyright_after_login) {
        echo $info->copyright_after_login;
      } else {
    @endphp
    <font size="2">Copyright &copy; 2019 - <?=date("Y");?>. Made with <i class="fa fa-coffee" aria-hidden="true"></i> <a href="http://santren.id/" target="_blank">SANTREN.ID</a>.</font>

    @php

      }
    @endphp
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
<script>

$(function() {
  $(".navigation-tab-item").click(function() {
    $(".navigation-tab-item").removeClass("active");
    $(this).addClass("active");
    $(".navigation-tab-overlay").css({
      left: $(this).prevAll().length *  20 + "%"
    });
  });
});

$(".btn-close-menu").hide();
function activeMenu(){
    $(".btn-close-menu").show()
    $("#nav-menu").addClass("d-block");
    $("#nav-menu").addClass("nav-menu-active");
}

function closeMenu(){
    $(".btn-close-menu").hide();
    $("#nav-menu").removeClass("d-block");
    $("#nav-menu").removeClass("nav-menu-active");
}



function rupiah(id){
    var rupiah = $(id);
    rupiah.keyup(function(){
      $(id).val(formatRupiah(this.value));
    })

}

/* Fungsi formatRupiah */
function formatRupiah(angka) {

    if(typeof angka !== 'number'){
      angka = parseInt(angka.replace(/,.*|[^0-9]/g, ''), 10);
    }
    var rupiah = '';		
      var angkarev = angka.toString().split('').reverse().join('');
      for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
      return rupiah.split('',rupiah.length-1).reverse().join('');
  
   
}

function Rupiah(angka) {

    if(typeof angka !== 'number'){
      angka = parseInt(angka.replace(/,.*|[^0-9]/g, ''), 10);
    }
    var rupiah = '';		
      var angkarev = angka.toString().split('').reverse().join('');
      for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
      return "Rp. " + rupiah.split('',rupiah.length-1).reverse().join('');
  
   
}


</script>


@yield('js-after')
</body>
</html>
