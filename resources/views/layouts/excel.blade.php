<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
   @yield('title')
   <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  
   <style>
    *{
      padding: 0;
      margin: 0;
      box-sizing: border-box
    }
  table{
      font-family: Arial,Helvetica Neue,Helvetica,sans-serif; 
      background: #fff;
      padding: 10px 60px;
      /* border-collapse: collapse; */
      border: 1px solid black
  }

table tr:nth-child(even){background-color: #f2f2f2;}

table th {background-color: #ddd;}
table tr:hover {background-color: #ddd;}

  table tr td,table tr th{
      padding: 1px 10px;
      border:1px solid #000;
  }
  table tr td{
    border-left:1px solid #000
  }
  table{
      width: 100%;
  }
  thead{
      text-align: center;
  }
  .logo{
    position: absolute;
    left: 90px;
    width: 60px;
  }
  .header{
    position: relative;
    text-align: center;
    width: 100%;
    padding: 10px;
    padding-top: 80px;
    font-weight: bold
  }
  .sub-header{
    text-align: center;
    width: 100%;
    padding-bottom: 20px;
    font-weight: bold
  }
   </style>
  @yield('css-after')
  
</head>
<body>
    @yield('content')
</body>
</html>
