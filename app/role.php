<?php

namespace App;

// use Jenssegers\Mongodb\Eloquent\Model;
use Illuminate\Database\Eloquent\Model;

class role extends Model
{
    //
    protected $fillable = [
        'nama', 'menu'
    ];
}
