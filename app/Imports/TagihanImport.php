<?php

namespace App\Imports;

use Modules\Siswa\Entities\Siswa;
use Modules\Keuangan\Entities\BiayaSiswa;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class TagihanImport implements  ToCollection
{
    public function collection(Collection $row)
    {

        $skipData = array();
        foreach($row as $r){

           $tagihan = 0;
           foreach($r as $d){
                if (is_numeric($d) AND $d > 1000){
                    $tagihan += $d;
                }
           }

           if($r[1] !== '' && $tagihan > 0){   

                $siswa = Siswa::whereRaw('nama like "%'.$r[1].'%" ')->first();
                if(!$siswa){
                   $nis = $r[1];
                }else{
                    $nis = $siswa->nis;
                }
                        $cek = BiayaSiswa::where("jenis_biaya_id",21)
                        ->where("nis",$nis)
                        ->first();
                        
                        if(!$cek){
                            $bs = new BiayaSiswa;
                            $bs->nis = $nis;
                            $bs->jenis_biaya_id = 21;
                            $bs->nama = 'Tagihan Sebelumnya';
                            $bs->rincian_biaya_id = 0;
                            $bs->satuan = 'sekali';
                            $bs->prioritas = 1;
                            $bs->nominal = $tagihan;
                            $bs->dibayar = 0;
                            $bs->bulan = 11;
                            $bs->tahun_id = 2019;
                            $bs->save();
                        }
           }
           
        }
        // return $skipData;
    }
}

