<?php

namespace App\Imports;

use Modules\Siswa\Entities\Siswa;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use DB;
class SiswaImport implements  ToCollection
{
    /**
     * @param array $row
     *
     * @return Siswa|null
     */
    
    public function collection(Collection $row)
    {

        // dd($row[0][0]);
        if($row[0][0] !== 'Format 1'){

                for ($i=0; $i <= count($row)-1; $i++) { 
            
                    $cek = Siswa::where("nis",$row[$i][4])->first();

                     
                    if(substr($row[$i][2],4,-3) == 10){
                        $jenjang = 'MA';
                        $kelas = $row[$i][36];
                        $cekKelas = DB::table("kelas")->where("nama",$kelas)->first();
                        if($cekKelas){                        
                            $kelas = $cekKelas->nama;                    
                            $id_kelas = $cekKelas->id;
                        }else{
                            $id_kelas = 0;
                        }
                    }else{
                        $jenjang = 'MTS';
                        $kelas = $row[$i][42];
                        $cekKelas = DB::table("kelas")->where("nama",$kelas)->first();
                        
                        if($cekKelas){                        
                            $kelas = $cekKelas->nama;                    
                            $id_kelas = $cekKelas->id;
                        }else{
                            $id_kelas = 0;
                        }
                    }

                    if(!$cek){
                       
                        $daarr  = [
                            'nama'     => $row[$i][1],
                            'nis'    => $row[$i][4],
                            'nipd' => $row[$i][2],
                            'jk'    => $row[$i][3],
                            'nisn'    => $row[$i][4],
                            'alamat_ortu'    => 
                                $row[$i][9] .", RT ".$row[$i][10]
                                .", RW ".$row[$i][11]
                                .", Dusun ".$row[$i][12]
                                .", Kelurahan ".$row[$i][13]
                                .", Kecamatan ".$row[$i][14],
                            'no_hp' => $row[$i][19],
                            'id_kelas' => $id_kelas,
                            'kelas' => $kelas,
                            'status' => 1,
                            'jenjang' => $jenjang,
                            'pin' => rand(100000,999999),
                        ];

                        if( $row[$i][4] !== 'NISN' AND $row[$i][1] !== null){
                            Siswa::insert($daarr);
                        }
                        
                    }else{
                        $cek->id_kelas = $id_kelas;
                        $cek->kelas = $kelas;
                        $cek->update();
                    }

                }

            } else if($row[0][0] == 'Format 1'){

                // Import Format 1
                // dd($row);

               
                
                for ($i=2; $i <= count($row)-1; $i++) { 
                    
                    $cek = Siswa::where("nis",$row[$i][5])->first();

                    // $kelas = $row[$i][8];
                    // $cekKelas = DB::table("kelas")
                    //     ->where("nama",$kelas)
                    //     ->first();
                    
                    if($row[$i][1] == 'MTS'){                        
                        $kelas = 'Kelas 7 Baru';                    
                        $id_kelas = 64;
                    }else{
                        $kelas = 'Kelas 10 Intensif';
                        $id_kelas = 65;
                    }

                    if(!$cek){


                        // dd($row[$i][9]);
                        $daarr  = [
                            'jenjang' => $row[$i][1],
                            'nama'     => $row[$i][2],
                            'nis'    => str_replace(' ', '', $row[$i][5]),
                            'nipd' => $row[$i][3],
                            'jk'    => $row[$i][4],
                            'nisn'    => $row[$i][5],
                            'alamat_ortu' => $row[$i][6],
                            'no_hp' => $row[$i][7],
                            'id_kelas' => $id_kelas,
                            'kelas' => $kelas,
                            'status' => 1,
                            'pin' => rand(100000,999999),
                            'du_dk' => 0,
                            'laundry' => (str_replace(' ', '', $row[$i][10]) == 'YA') ? 1 : 0,
                            'beasiswa' => (str_replace(' ', '', $row[$i][10]) == 'YA') ? 1 : 0,
                            'baru_lama' => 1,
                        ];

                        if($row[$i][5] !== null){
                            Siswa::insert($daarr);
                        }
                        
                    }else{
                        // $cek->id_kelas = $id_kelas;
                        // $cek->kelas = $kelas;
                        // $cek->jenjang = $row[$i][1];
                        // $cek->nama = $row[$i][2];
                        // $cek->nipd = $row[$i][3];
                        // $cek->jk  = $row[$i][4];
                        // $cek->alamat_ortu = $row[$i][6];
                        // $cek->no_hp = $row[$i][7];
                        $cek->du_dk = 0;
                        // $cek->laundry = ($row[$i][10] == 'YA') ? 1 : 0;
                        // $cek->baru_lama = ($row[$i][11] == 'I') ? 1 : 0;
                        $cek->update();
                    }

                }

            }
            
      
    }
}