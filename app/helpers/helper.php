<?php

// Helper

use Illuminate\Http\Request;
use Modules\ApiBris\Entities\TagihanH2h;
use Modules\Keuangan\Entities\BiayaSiswa;

function bulan_text($tanggal){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	return $bulan[ (int)$tanggal ];
}



function rupiah($angka){
	
	$hasil_rupiah = "Rp " . number_format($angka,0,',','.');
	return $hasil_rupiah;
 
}
function nomorin($angka){
	
	return number_format($angka, 0, ',', '.');
 
}

function rp($n, $presisi=1) {
	if ($n < 900) {
		$format_angka = number_format($n, $presisi);
		$simbol = '';
	} else if ($n < 900000) {
		$format_angka = number_format($n / 1000, $presisi);
		$simbol = 'rb';
	} else if ($n < 900000000) {
		$format_angka = number_format($n / 1000000, $presisi);
		$simbol = 'jt';
	} else if ($n < 900000000000) {
		$format_angka = number_format($n / 1000000000, $presisi);
		$simbol = 'M';
	} else {
		$format_angka = number_format($n / 1000000000000, $presisi);
		$simbol = 'T';
	}
 
	if ( $presisi > 0 ) {
		$pisah = '.' . str_repeat( '0', $presisi );
		$format_angka = str_replace( $pisah, '', $format_angka );
	}
	
	return $format_angka . $simbol;
}

function tgl_indo($tanggal){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
 
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
function tgl_indo_dt($tgl){
    $time = strtotime($tgl);
    $tanggal = date('d-m-Y',$time);
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
 
	return $pecahkan[0] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[2];
}

function jamber($tanggal){
	
	$pecahkan = explode(' ', $tanggal);
 
	return $pecahkan[1];
}
 
function tgl_indo_p($tanggal,$param){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
	if($param == 'M'){
		return $bulan[ (int)$pecahkan[1] ];

	}else if($param == 'Y'){
		return $pecahkan[0];

	}else if($param == 'D'){
		return $pecahkan[2];

	}else if($param == 'M-Y'){
		return $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
	}
	else{
		return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];

	}
 
}

	function sejarahVA($req){
        // return response()->json($req, 200);
        if ( !$req->awal || !$req->akhir) {
            return response()->json([
                "status" => false,
                "pesan" => 'Field Kosong'
            ], 200);
        }
        $client_id = config('bank-bri.client_id');
        $secret_id = config('bank-bri.client_secret');
        $timestamp = config('bank-bri.timestamp');
        $secret = $secret_id;
        $token = BRIVAgenerateToken($client_id, $secret_id);

        $institutionCode = config('bank-bri.institution_code');
        $brivaNo = config('bank-bri.briva_no');
        // $custCode = $req->va;
        $startDate = $req->awal;
        $endDate = $req->akhir;

        $payload = null;
        $url = config('bank-bri.briva.report');
        $verb = config('bank-bri.briva.report_verb');
        $path = $url.'/'.$institutionCode."/".$brivaNo."/".$startDate."/".$endDate;
        $base64sign = BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret);

        $request_headers = array(
            "Authorization:Bearer " . $token,
            "BRI-Timestamp:" . $timestamp,
            "BRI-Signature:" . $base64sign,
        );

        $urlPost = config('bank-bri.api_url_extra').$url."/".$institutionCode."/".$brivaNo."/".$startDate."/".$endDate;
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_URL, $urlPost);
        curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, "GET"); 
        curl_setopt($chPost, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);

        $resultPost = curl_exec($chPost);
        $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
        curl_close($chPost);

        return json_decode($resultPost, true);
    }

    function infoVA($nis){
        $h2h = TagihanH2h::where('nis',$nis)->first();
        if (!$h2h) {
            return response()->json([
                "status" => false,
                "pesan" => 'Data Tagihan tidak ditemukan'
            ], 200);
        }

        $tgl = date('Ymd', strtotime($h2h->created_at));
        $req = new stdClass();
        $req->awal = $tgl;
        $req->akhir = $tgl;
        $getReport = sejarahVA($req);

        if ($getReport['status'] === true) {
            $report = collect($getReport['data']);
            $isDibayar = $report->where('custCode', $h2h->nis)->first();
            if ($isDibayar) {
                $h2h->update(['status' => 2]);
                foreach(explode(",",$h2h->biaya_id) as $ta_id){
                        $item = BiayaSiswa::find($ta_id);
                        $item->update(['dibayar' => $item->nominal]);
                        
                    }
                return response()->json([
                    "status" => true,
                    "pesan" => 'Tagihan Sudah dibayar',
                    "data" => $isDibayar
                ], 200);
            }
            return response()->json([
                    "status" => false,
                    "pesan" => 'Tagihan Belum dibayar'
                ], 200);
            // return response()->json($isDibayar, 200);
        } 
        return response()->json([
                    "status" => false,
                    "pesan" => 'Tagihan tidak ditemukan'
                ], 200);
        // return response()->json($getReport, 200);

        
    }
	function hapusVA($req){
        if (!$req->va) {
            return response()->json([
                "status" => false,
                "pesan" => 'Field VA Kosong'
            ], 200);
        }
        


        $client_id = config('bank-bri.client_id');
        $secret_id = config('bank-bri.client_secret');
        $timestamp = config('bank-bri.timestamp');
        $secret = $secret_id;
        $token = BRIVAgenerateToken($client_id, $secret_id);

        $institutionCode = config('bank-bri.institution_code');
        $brivaNo = config('bank-bri.briva_no');
        $custCode = $req->va;


        $payload = "institutionCode=".$institutionCode."&brivaNo=".$brivaNo."&custCode=".$custCode;
        $path = config('bank-bri.briva.destroy');
        $verb = config('bank-bri.briva.destroy_verb');
        $base64sign = BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret);

        $request_headers = array(
            "Authorization:Bearer " . $token,
            "BRI-Timestamp:" . $timestamp,
            "BRI-Signature:" . $base64sign,
        );

        $urlPost = config('bank-bri.api_url_extra').$path;
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_URL, $urlPost);
        curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chPost, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
        curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
        
        $resultPost = curl_exec($chPost);
        $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
        curl_close($chPost);
        return json_decode($resultPost, true);
    }

    function updateVA($req){
        if ( !$req->va || !$req->nama || !$req->amount || !$req->keterangan) {
            return response()->json([
                "status" => false,
                "pesan" => 'Field Kosong'
            ], 200);
        }
        $client_id = config('bank-bri.client_id');
        $secret_id = config('bank-bri.client_secret');
        $timestamp = config('bank-bri.timestamp');
        $secret = $secret_id;
        $token = BRIVAgenerateToken($client_id, $secret_id);

        $institutionCode = config('bank-bri.institution_code');
        $brivaNo = config('bank-bri.briva_no');

        $datas = array(
            'institutionCode' => $institutionCode ,
            'brivaNo' => $brivaNo,
            'custCode' => $req->va,
            'nama' => $req->nama,
            'amount' => $req->amount,
            'keterangan' => $req->keterangan,
            'expiredDate' =>  Carbon::now()->addMonth()->format('Y-m-d H:i:s')
        );

        $payload = json_encode($datas, true);
        $path = config('bank-bri.briva.update');
        $verb = config('bank-bri.briva.update_verb');
        $base64sign = BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret);

        $request_headers = array(
            "Content-Type:"."application/json",
            "Authorization:Bearer " . $token,
            "BRI-Timestamp:" . $timestamp,
            "BRI-Signature:" . $base64sign,
        );

        $urlPost = config('bank-bri.api_url_extra').$path;
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_URL, $urlPost);
        curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, "PUT"); 
        curl_setopt($chPost, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);

        $resultPost = curl_exec($chPost);
        $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
        curl_close($chPost);

        return json_decode($resultPost, true);
    }
	function BRIVAgenerateToken($client_id, $secret_id) {
        $url = config('bank-bri.api_url_extra').config('bank-bri.get_token');
        $data = "client_id=".$client_id."&client_secret=".$secret_id;
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $data);
        
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $json = json_decode($result, true);
        $accesstoken = $json['access_token'];

        return $accesstoken;
    }

    /* Generate signature */
    function BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret) {
        $payloads = "path=$path&verb=$verb&token=Bearer $token&timestamp=$timestamp&body=$payload";
        $signPayload = hash_hmac('sha256', $payloads, $secret, true);
        return base64_encode($signPayload);
    }

    function sc_report($msg, array $ext = [])
    {
        $msg = date('Y-m-d H:i:s').':'.PHP_EOL.$msg.PHP_EOL;
        if(!in_array('slack', $ext)) {
            if (config('logging.channels.slack.url')) {
                try {
                    \Log::channel('slack')->error($msg);
                } catch(\Throwable $e) {
                    $msg .= $e->getFile().'- Line: '.$e->getLine().PHP_EOL.$e->getMessage().PHP_EOL; 
                }
            }
        }
        \Log::error($msg);
    }

?>