<?php
namespace App\Exports;

use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PerJenisExport implements FromView
{
    public function __construct($req)
    {
        $this->req = $req;
    }

    public function view(): View
    {
        
        $req = $this->req;
        
        return view("excel.perjenis",compact("req"));
    }
}