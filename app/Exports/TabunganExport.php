<?php
namespace App\Exports;

use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TabunganExport implements FromView
{
    public function __construct($req,$res,$user)
    {
        $this->req = $req;
        $this->res = $res;
        $this->user = $user;
    }

    public function view(): View
    {
        
        $req = $this->req;
        $res = $this->res;
        $user = $this->user;
        return view("excel.tabungan-saldo-siswa",compact("req","res","user"));
    }
}