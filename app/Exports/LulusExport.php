<?php
namespace App\Exports;

use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LulusExport implements FromView
{
    public function __construct($req,$res)
    {
        $this->req = $req;
        $this->res = $res;
    }

    public function view(): View
    {
        
        $req = $this->req;
        $res = $this->res;
        return view("excel.tagihan-siswa-lulus",compact("req","res"));
    }
}