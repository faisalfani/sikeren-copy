<?php
namespace App\Exports;

use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DaftarUlangPerJenis implements FromView
{
    public function __construct($req,$req1,$jenis)
    {
        $this->req = $req;
        $this->req1 = $req1;
        $this->jenis = $jenis;
    }

    public function view(): View
    {
        
        $req = $this->req;
        $req1 = $this->req1;
        $jenis = $this->jenis;
        
        return view("excel.daftar-ulang-per-jenis",compact("req","req1","jenis"));
    }
}