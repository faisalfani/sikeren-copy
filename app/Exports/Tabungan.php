<?php
namespace App\Exports;

use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Tabungan implements FromView
{
    public function __construct($req,$nis,$start,$end,$siswa)
    {
        $this->req = $req;
        $this->nis = $nis;
        $this->start = $start;
        $this->end = $end;
        $this->siswa = $siswa;
    }

    public function view(): View
    {
        
        $req = $this->req;
        $nis = $this->nis;
        $start = $this->start;
        $end = $this->end;
        $siswa = $this->siswa;
        
        return view("excel.tabungan",compact("req",'nis','start','end','siswa'));
    }
}