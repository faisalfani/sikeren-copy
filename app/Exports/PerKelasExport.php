<?php
namespace App\Exports;

use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PerKelasExport implements FromView
{
    public function __construct($req,$res,$kelas)
    {
        $this->req = $req;
        $this->res = $res;
        $this->kelas = $kelas;
    }

    public function view(): View
    {
        
        $req = $this->req;
        $res = $this->res;
        $kelas = $this->kelas;
        return view("excel.perkelas",compact("req","res","kelas"));
    }
}