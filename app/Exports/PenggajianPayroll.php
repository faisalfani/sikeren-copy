<?php
namespace App\Exports;

use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PenggajianPayroll implements FromView
{
    public function __construct($req,$jumlah, $header, $res)
    {
        $this->req = $req;
        $this->header = $header;
        $this->jumlah = $jumlah;
        $this->res = $res;
    }

    public function view(): View
    {
        
        $req = $this->req;
        $header = $this->header;
        $jumlah = $this->jumlah;
        $res = $this->res;
        
        return view("excel.penggajian-payroll",compact("req","header", "res", "jumlah"));
    }
}