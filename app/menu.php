<?php

namespace App;

// use Jenssegers\Mongodb\Eloquent\Model;
use Illuminate\Database\Eloquent\Model;

class menu extends Model
{
    protected $fillable = [
        'nama', 'url', 'icon','sub'
    ];
    //
}
