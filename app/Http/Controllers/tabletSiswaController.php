<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Siswa\Entities\Siswa;

class tabletSiswaController extends Controller
{
    //
    public function index(Request $req){
        return view("tabletSiswa",compact("req"));
    }

    public function searchNis(Request $req)
    {
        if ($req->has('q')) {
            $cari = $req->q;
            $data = Siswa::select('nis', 'nama')
            ->whereRaw('nama LIKE "%'.$cari.'%" OR nis LIKE "%'.$cari.'%" ')
            ->get();

            return $data;
        }
    }

}
