<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class LoginPegawai extends Controller
{
    public function login(Request $request)
    {

        // return response()->json($request, 200);
        $credentials = request(['nik', 'pin']);
        $validator = Validator::make($request->all(), [
            'nik' => 'required|string',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (! $token = auth('pegawai')->attempt($validator->validated(), ['exp' => Carbon::now()->addDays(1)->timestamp])) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'type' =>'success',
            'token' => $token,
            'token_type' => 'bearer'
        ]);
    }
}
