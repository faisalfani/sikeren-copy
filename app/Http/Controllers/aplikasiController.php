<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Siswa\Entities\Siswa;
use Modules\Keuangan\Entities\BiayaSiswa;
use Modules\Keuangan\Entities\BayarSiswa;
use Modules\Keuangan\Entities\Jurnal;
use Modules\Keuangan\Entities\PemasukanTabunganSiswa;

class aplikasiController extends Controller
{
    //


    public function updateAvatar(Request $req, $nis, $pin)
    {


        $folderPath = public_path('images/avatar/');

        $img  = $req->avatar;

        if ($req->avatar) {

            $user = Siswa::where("nis", $nis)->where("pin", $pin)->first();


            $image_parts = explode(";base64,", $img);

            $image_type_aux = explode("image/", $image_parts[0]);

            $image_type = $image_type_aux[1];

            $image_base64 = base64_decode($image_parts[1]);


            $fileLama = public_path() . $user->avatar;

            // return $fileLama;
            @unlink($fileLama);

            $nama = uniqid() . '.png';

            $file = $folderPath . $nama;

            file_put_contents($file, $image_base64);



            $user->avatar = '/images/avatar/' . $nama;

            $user->update();

            return $user;
        } else {
            return $user;
        }
    }
    public function updatePin(Request $req, $nis, $pin)
    {

        $user = Siswa::where("nis", $nis)->where("pin", $pin)->first();

        if ($req->pin) {
            $user->pin = $req->pinbaru;
        }
        $nohape = substr($req->nohp, 0, 2);
        $realhp = substr($req->nohp, 1);
        if ($nohape == '08') {
            $nohpindo = '+62' . $realhp;
        } else {
            $nohpindo = $req->nohp;
        }
        //        $nohpindo = $req->nohp;
        $user->no_hp = $nohpindo;
        $user->alamat_ortu = $req->alamat;
        $user->update();

        return $user;
    }
    public function index($nis, $pin)
    {

        $siswa = Siswa::where("nis", $nis)->where("pin", $pin)->first();

        if ($siswa) {

            $tagihan = BiayaSiswa::where("nis", $nis)->whereRaw("nominal != dibayar")
                ->orderBy("bulan", "ASC");

            $total_tagihan = BiayaSiswa::where("nis", $nis)->sum("nominal") -
                BiayaSiswa::where("nis", $nis)->sum("dibayar");

            $hisoryPembayaran = Jurnal::where("type", 1)
                ->with("tagihan")
                ->where("id_jenis_biaya", "!=", 4)
                ->where("nis", $nis)
                ->orderBy("created_at", "DESC")
                ->limit(10);

            $total_pembayaran = Jurnal::where("type", 1)
                ->where("id_jenis_biaya", "!=", 4)
                ->where("nis", $nis)->sum("jumlah");

            $idTabungan = DB::table('jenis_biayas')->where('nama', 'TABUNGAN')->first();
            // return $idTabungan;
            $tabungan =
                Jurnal::where(function ($q) use ($idTabungan, $nis) {

                    $q->where("id_jenis_biaya", 4);
                    if ($idTabungan) {
                        $q->orWhere("id_jenis_biaya", $idTabungan->id);
                    }
                })->where("nis", $nis)
                ->orderBy("created_at", "DESC")
                ->limit(10);
            // return response()->json($tabungan, 200);



            $total_tabungan = Jurnal::where("nis", $nis)
                ->where("id_jenis_biaya", 4)
                ->where("type", 1)
                ->sum("jumlah")
                - Jurnal::where("nis", $nis)
                ->where("id_jenis_biaya", 4)
                ->where("type", 0)
                ->sum('jumlah');


            $res = array([
                "siswa" => $siswa,
                "tagihan" => $tagihan->get(),
                "history_pembayaran" => $hisoryPembayaran->get(),
                "tabungan" => $tabungan->get(),
                "total_tagihan" => $total_tagihan,
                "total_pembayaran" => $total_pembayaran,
                "total_tabungan" => $total_tabungan
            ]);
            return $res;
        } else {
            return $res = array([
                "gagal" => "Gagal Login, Cek NIS dan PIN",
            ]);
        }
    }

    public function tabungan($nis, $pin)
    {
        $siswa = Siswa::where("nis", $nis)->where("pin", $pin)->first();

        if ($siswa) {

            $tabungan =
                Jurnal::where("id_jenis_biaya", 4)
                ->where("nis", $nis)
                ->orderBy("created_at", "DESC")
                ->paginate(10);

            return $tabungan;
        } else {
            return $res = array([
                "gagal" => "Gagal Login, Cek NIS dan PIN",
            ]);
        }
    }
    public function pembayaran($nis, $pin)
    {
        $siswa = Siswa::where("nis", $nis)->where("pin", $pin)->first();

        if ($siswa) {

            $hisoryPembayaran = Jurnal::where("type", 1)
                ->with("tagihan")
                ->where("id_jenis_biaya", "!=", 4)
                ->where("nis", $nis)
                ->orderBy("created_at", "DESC")
                ->paginate(10);


            return $hisoryPembayaran;
        } else {
            return $res = array([
                "gagal" => "Gagal Login, Cek NIS dan PIN",
            ]);
        }
    }
}
