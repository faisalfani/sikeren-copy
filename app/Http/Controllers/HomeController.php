<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;

use Validator;
use Excel;
use App\Imports\TagihanImport;
use Illuminate\Support\Facades\DB;
use Modules\Core\Entities\CoreInfo;
use Modules\Keuangan\Entities\BiayaSiswa;
use Modules\Keuangan\Entities\JenisBiaya;
use Modules\Master\Entities\Kelas;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function baseHome(Request $req) {
        $info = CoreInfo::first();
        // return view('home',compact('req'));
        if ($info->type_dashboard == '1') {
            # code...
            return view('home',compact('req'));
        }
        // return 2;
        $tahun_akademik = BiayaSiswa::select('tahun_ajaran', DB::raw('concat(tahun_ajaran,"/", tahun_ajaran+1) as tahun_akademik'))
            ->where('tahun_ajaran','!=', '0')
            ->groupBy('tahun_ajaran')
            ->orderBy('tahun_ajaran','ASC')
            ->get();

        $data_kelas = DB::table('biaya_siswas as bs')
                      ->leftJoin('siswas as s', 's.nis', 'bs.nis')
                      ->leftJoin('kelas as k', 'k.id', 's.id_kelas')
                      ->select('s.id_kelas','k.nama')
                      ->groupBy('s.id_kelas')
                      ->where('id_kelas','!=', '0')
                      ->where('id_kelas','!=', NULL)
                      ->get();

        $data_jumlah = DB::table('biaya_siswas as bs')
                      ->leftJoin('siswas as s', 's.nis', 'bs.nis')
                    //   ->leftJoin('kelas as k', 'k.id', 's.id_kelas')
                      ->select(
                          DB::raw('COUNT(DISTINCT(CASE WHEN s.jk = "L"  THEN s.nis END)) as laki'),
                          DB::raw('COUNT(DISTINCT(CASE WHEN s.jk = "P"  THEN s.nis END)) as perempuan'),
                          DB::raw('Count(DISTINCT(s.nis)) total'),
                          's.status'
                      )
                    //   ->groupBy('s.nis')
                      ->groupBy('s.status')
                      ->where('s.id_kelas','!=', '0')
                      ->where('s.id_kelas','!=', NULL)
                      ->get();
        $data_aktif = $data_jumlah->where('status', '1')->first();
        $data_alumni = $data_jumlah->where('status', '2')->first();
        $data_berhenti = $data_jumlah->where('status', '0')->first();

        $data_siswa = [
            "aktif" => $data_aktif,
            "alumni" => $data_alumni,
            "berhenti" => $data_berhenti
        ];


        $jenis_biaya = JenisBiaya::select('id', 'nama')->get();
        
        // return response()->json($data_siswa, 200);
        return view('home2',compact('req', 'tahun_akademik', 'data_kelas', 'data_siswa', 'jenis_biaya'));
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('profile');
    }

    public function editProfile(Request $req){
        
        $user = User::find(Auth::user()->id);
        
        // dd($req);
        
        $user->name = $req->name;

        $cek = User::where("email",$req->email)->first();
        
        if($cek){
            if($cek->email != $user->email){
                return back()->with("error","Email sudah digunakan");
            }else{
                $user->email = $req->email;
            }
        }else{
            $user->email = $req->email;
        }


        $cek1 = User::where("username",$req->username)->first();
        if($cek1){
            if($cek1->username != $user->username){
                return back()->with("error","Username sudah digunakan");
            }else{
                $user->username = $req->username;
            }
        }else{
            $user->username = $req->username;
        }

        $user->email = $req->email;
        if($req->password){
        $user->password = Hash::make($req->password);
        }
        $user->update();
        if($user){
            
            return back()->with("success","Profile berhasil diedit");

        }
    }


    public function importTagihan(Request $req)
    {
        // dd($req);
            $validator = Validator::make($req->all(), [
                'file'  => 'required|mimes:xlsx,xls,cvs'
            ]);

            if ($validator->fails()) {
                // dd($validator);
                // return back()->with('errors', ['File Tidak Boleh Kosong']);
            }

             
            
            $file = $req->file('file'); //GET FILE
            // Excel::import(new SiswaImport, $file); //IMPORT FILE 
            Excel::import(new TagihanImport, request()->file('file'));
            
            // $data = Excel::import(new SiswaImport, request()->file('file-import'));
        
            return back()->with('success', 'Data Berhasil di import!');
            // dd($data);

            }

    
}
