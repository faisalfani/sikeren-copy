<?php

namespace App\Http\Controllers;

use PDF;
use Excel;
use stdClass;
use App\Exports\LkExport;
use App\Exports\Tabungan;
use App\Exports\DaftarUlang;
use App\Exports\LulusExport;
use Illuminate\Http\Request;
use App\Exports\JurnalExport;
use Illuminate\Support\Carbon;
use App\Exports\PerJenisExport;
use App\Exports\PerKelasExport;
use App\Exports\PerSiswaExport;
use App\Exports\DUPerKelasExport;
use Modules\Siswa\Entities\Siswa;
use App\Exports\PenggajianLaporan;
use App\Exports\PenggajianPayroll;
use App\Exports\UangSetoranExport;
use Illuminate\Support\Facades\DB;
use Modules\Master\Entities\Kelas;
use Illuminate\Support\Facades\App;
use App\Exports\DaftarUlangPerJenis;
use Modules\Pegawai\Entities\Pegawai;
use App\Exports\PenggajianPayrollCopy;
use App\Exports\TabunganExport;
use Modules\Pegawai\Entities\Penggajian;
use Modules\Keuangan\Entities\BiayaSiswa;
use Modules\Keuangan\Entities\JenisBiaya;
use Modules\Pegawai\Entities\TipePegawai;
use Modules\Pegawai\Entities\JenisPegawai;
use Modules\Pegawai\Entities\IhsanDanPotongan;
use Modules\Pegawai\Entities\MasterPenggajian;
use App\Exports\UangSetoranSiswaExport;
use Illuminate\Support\Facades\Auth;

class LaporanController extends Controller
{
    //
    public function resume($bulan, $tahun, Request $req){

        $dataPenggajian = Penggajian::where('bulan', $bulan)->where('tahun', $tahun)->get();
        $data = [
            "tanggal" => $today = Carbon::today()->toDateString()
        ];

        $res = $dataPenggajian->map(function($q) {
            $dataPendapatan = [];
            if ($q->ihsan_pokok_id == 0) {
               $pokok_nama = "IHSAN POKOK"; 
               $pokok_jumlah = 0;
            } else {
                $dataPokok = MasterPenggajian::with("itp")->find($q->ihsan_pokok_id);
                $pokok_nama = $dataPokok->itp->nama;
                $pokok_jumlah = $dataPokok->jumlah_ihsan * $q->masa_kerja;
            }
            
            $pokok = [
                "nama" => $pokok_nama,
                "jumlah" => $pokok_jumlah
            ];
            array_push($dataPendapatan, $pokok);

            // $dataJamKerja = MasterPenggajian::with("itp")->find($q->ihsan_jk_id);
            if ($q->ihsan_jk_id == 0) {
                $jk_nama = "IHSAN JAM KERJA"; 
                $jk_jumlah = 0;
             } else {
                 $datajk = MasterPenggajian::with("itp")->find($q->ihsan_jk_id);
                 $jk_nama = $datajk->itp->nama;
                 $jk_jumlah = $datajk->jumlah_ihsan * $q->ihsan_jk_jp;
             }
            $jamKerja = [
                "nama" => $jk_nama,
                "jumlah" => $jk_jumlah
            ];
            array_push($dataPendapatan, $jamKerja);

            $tunjangan_id = explode(",", $q->tunjangan_id);
            $dataTunjangan = MasterPenggajian::with("itp")->whereIn("id", $tunjangan_id)->get();

            $dataTunjangan->map(function($z) use(&$dataPendapatan) {
                $tunjangan = [
                    "nama" => $z->itp->nama,
                    "jumlah" => $z->jumlah_ihsan
                ];
                array_push($dataPendapatan, $tunjangan);
            });

            $dataPotongan = [];
            $potongan_id = explode(",", $q->potongan_id);
            $potongan = MasterPenggajian::with("itp")->whereIn("id", $potongan_id)->get();

            $potongan->map(function($z) use(&$dataPotongan,  $pokok_jumlah) {
                $potongan = [
                    "nama" => $z->itp->nama,
                    "jumlah" => $z->is_persen ? $z->jumlah_ihsan * $pokok_jumlah /100 : $z->jumlah_ihsan
                ];
                array_push($dataPotongan, $potongan);
            });

            $pegawai = Pegawai::find($q->pegawai_id);
            $jabatan = DB::table('master_penggajians as mp')
                        ->leftJoin('ihsan_dan_potongans as ip', 'ip.id', 'mp.itp_id')
                        ->select('ip.id', 'ip.nama')
                        ->whereIn("mp.id", $tunjangan_id)
                        ->where('ip.is_jabatan','1')->first();
            $kepegawaian = TipePegawai::find(@$pegawai->tipe_pegawai_id);
            $jenisPegawai = JenisPegawai::find(@$pegawai->jenis_pegawai_id);

            $year = new Carbon(@$pegawai->tgl_masuk);

            $res = [
                "nama" => @$pegawai->nama,
                "maksimal"=>(collect($dataPendapatan)->count() > collect($dataPotongan)->count()) ? collect($dataPendapatan)->count() : collect($dataPotongan)->count(),
                "jabatan" => ($jabatan) ? $jabatan->nama : '-',
                "mk" => $year->year,
                "kepegawaian" => ($kepegawaian) ? $kepegawaian->kode ." - ". $jenisPegawai->nama : '?',
                "pendapatan" => $dataPendapatan,
                "potongan" => $dataPotongan,
                "jumlah_pendapatan" => collect($dataPendapatan)->sum('jumlah'),
                "jumlah_potongan" => collect($dataPotongan)->sum('jumlah'),
                "total" => collect($dataPendapatan)->sum('jumlah') - collect($dataPotongan)->sum('jumlah')
            ];

            return $res;

        });

        // return response()->json($res, 200);

        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.penggajian-resume',compact("data", "req", "res"))->setPaper('a4', 'potrait');;
            return $pdf->stream();
            
        }

        $req->print = false;

        return view("pdf.penggajian-resume",compact("data", "req", "res"));
    }

    public function resumeId($bulan, $tahun, $idPegawai, Request $req){

        $dataPenggajian = Penggajian::where('bulan', $bulan)->where('tahun', $tahun)->where('pegawai_id', $idPegawai)->get();
        $data = [
            "tanggal" => $today = Carbon::today()->toDateString()
        ];

        $res = $dataPenggajian->map(function($q) {
            $dataPendapatan = [];
            if ($q->ihsan_pokok_id == 0) {
               $pokok_nama = "IHSAN POKOK"; 
               $pokok_jumlah = 0;
            } else {
                $dataPokok = MasterPenggajian::with("itp")->find($q->ihsan_pokok_id);
                $pokok_nama = $dataPokok->itp->nama;
                $pokok_jumlah = $dataPokok->jumlah_ihsan * $q->masa_kerja;
            }
            
            $pokok = [
                "nama" => $pokok_nama,
                "jumlah" => $pokok_jumlah
            ];
            array_push($dataPendapatan, $pokok);

            // $dataJamKerja = MasterPenggajian::with("itp")->find($q->ihsan_jk_id);
            if ($q->ihsan_jk_id == 0) {
                $jk_nama = "IHSAN JAM KERJA"; 
                $jk_jumlah = 0;
             } else {
                 $datajk = MasterPenggajian::with("itp")->find($q->ihsan_jk_id);
                 $jk_nama = $datajk->itp->nama;
                 $jk_jumlah = $datajk->jumlah_ihsan * $q->ihsan_jk_jp;
             }
            $jamKerja = [
                "nama" => $jk_nama,
                "jumlah" => $jk_jumlah
            ];
            array_push($dataPendapatan, $jamKerja);

            $tunjangan_id = explode(",", $q->tunjangan_id);
            $dataTunjangan = MasterPenggajian::with("itp")->whereIn("id", $tunjangan_id)->get();

            $dataTunjangan->map(function($z) use(&$dataPendapatan) {
                $tunjangan = [
                    "nama" => $z->itp->nama,
                    "jumlah" => $z->jumlah_ihsan
                ];
                array_push($dataPendapatan, $tunjangan);
            });

            $dataPotongan = [];
            $potongan_id = explode(",", $q->potongan_id);
            $potongan = MasterPenggajian::with("itp")->whereIn("id", $potongan_id)->get();

            $potongan->map(function($z) use(&$dataPotongan, $pokok_jumlah) {
                $potongan = [
                    "nama" => $z->itp->nama,
                    "jumlah" => $z->is_persen ? $z->jumlah_ihsan * $pokok_jumlah /100 : $z->jumlah_ihsan
                ];
                array_push($dataPotongan, $potongan);
            });

            $pegawai = Pegawai::find($q->pegawai_id);
            $jabatan = DB::table('master_penggajians as mp')
                        ->leftJoin('ihsan_dan_potongans as ip', 'ip.id', 'mp.itp_id')
                        ->select('ip.id', 'ip.nama')
                        ->whereIn("mp.id", $tunjangan_id)
                        ->where('ip.is_jabatan','1')->first();
            $kepegawaian = TipePegawai::find($pegawai->tipe_pegawai_id);
            $year = new Carbon($pegawai->tgl_masuk);
            $res = [
                "nama" => $pegawai->nama,
                "maksimal"=>(collect($dataPendapatan)->count() > collect($dataPotongan)->count()) ? collect($dataPendapatan)->count() : collect($dataPotongan)->count(),
                "jabatan" => ($jabatan) ? $jabatan->nama : '-',
                "mk" => $year->year,
                "kepegawaian" => ($kepegawaian) ? $kepegawaian->kode : '?',
                "pendapatan" => $dataPendapatan,
                "potongan" => $dataPotongan,
                "jumlah_pendapatan" => collect($dataPendapatan)->sum('jumlah'),
                "jumlah_potongan" => collect($dataPotongan)->sum('jumlah'),
                "total" => collect($dataPendapatan)->sum('jumlah') - collect($dataPotongan)->sum('jumlah')
            ];

            return $res;

        });

        // return response()->json($res, 200);

        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.penggajian-resume',compact("data", "req", "res"))->setPaper('a4', 'potrait');;
            return $pdf->stream();
            
        }

        $req->print = false;

        return view("pdf.penggajian-resume",compact("data", "req", "res"));
    }

    public function laporanPenggajian($bulan, $tahun, Request $req){

        $idJabatan = IhsanDanPotongan::where('is_jabatan', '1')->get()->pluck("id");
        $headerTunjangan = [];
        $dataPenggajian = DB::table('penggajians as gaji')
                            ->leftJoin('pegawais as pg','gaji.pegawai_id', 'pg.id')
                            ->leftJoin("jenis_pegawais as jp", "jp.id", "pg.jenis_pegawai_id")
                            ->where("gaji.bulan", $bulan)
                            ->where("gaji.tahun", $tahun)
                            ->where(function($q) use($req){
                                if ($req->tipe) {
                                    $q->where('pg.tipe_pegawai_id', $req->tipe);
                                }
                                if ($req->jenis) {
                                    $q->where('pg.jenis_pegawai_id', $req->jenis);
                                }
                            })
                            ->select("pg.*", "gaji.*", "jp.id as jp_id", "jp.nama as jp_nama")
                            ->orderBy("jp.nama","ASC")
                            ->orderBy('pg.nama', "ASC")
                            ->get();
        // return response()->json($dataPenggajian, 200);
        $dataPenggajian->map(function($q) use(&$headerTunjangan, $idJabatan ) {
            $cek = explode(",", $q->tunjangan_id);
            collect($cek)->map(function($x) use(&$headerTunjangan, $idJabatan ) {
                $is_jabatan = MasterPenggajian::where('id', $x)->whereIn('itp_id', $idJabatan)->first();
                if ($is_jabatan) {
                    # code...
                } else {
                    
                    $is_header = array_search($x, $headerTunjangan);
                    if ($is_header === false) {
                        array_push($headerTunjangan, $x);
                    } 
                }
            });
        });

        $headerPotongan = [];
        $dataHeaderPotongan = Penggajian::where("bulan", $bulan)
                        ->where("tahun", $tahun)
                        ->select('potongan_id')
                        ->get();
        $dataHeaderPotongan->map(function($q) use(&$headerPotongan, $idJabatan ) {
            $cek = explode(",", $q->potongan_id);
            collect($cek)->map(function($x) use(&$headerPotongan, $idJabatan ) {
                $is_jabatan = MasterPenggajian::where('id', $x)->whereIn('itp_id', $idJabatan)->first();
                if ($is_jabatan ) {
                    # code...
                } else {
                    
                    $is_header = array_search($x, $headerPotongan);
                    if ($is_header === false) {
                        array_push($headerPotongan, $x);
                    } 
                }
            });
        });
        // $a = array_search("18", $headerTunjangan);

        $h_tunjangan = DB::table('master_penggajians as pg')
                        ->leftJoin("ihsan_dan_potongans as ip", "ip.id", "pg.itp_id")
                        ->whereIn("pg.id", $headerTunjangan)
                        ->select("pg.id", "ip.nama")
                        ->orderBy("pg.id")
                        ->groupBy("ip.nama")
                        ->get();
        $h_potongan = DB::table('master_penggajians as pg')
                        ->leftJoin("ihsan_dan_potongans as ip", "ip.id", "pg.itp_id")
                        ->whereIn("pg.id", $headerPotongan)
                        ->select("pg.id", "ip.nama", "pg.is_persen")
                        ->groupBy("ip.nama")
                        ->orderBy("pg.id")
                        ->get();
        

        if ($req->tipe) {
            $tipeHeader = TipePegawai::find($req->tipe);
        } else {
            $tipeHeader = NULL;
        }
        if ($h_potongan->count() < 1) {
            $h_potongan = [];
            $pot = new stdClass();
            $pot->id = 0;
            $pot->nama = "-";
            array_push($h_potongan, $pot);
            // $h_potongan = collect($pot);

            // return $h_potongan;
        }
        $header = [
            "tunjangan" => $h_tunjangan,
            "potongan" => $h_potongan,
            "tipe" => $tipeHeader
        ];

        // return response()->json($h_tunjangan, 200);
        $dataJumlahTunjangan = [];
        foreach ($h_tunjangan as $key => $value) {
            $dataJumlahTunjangan[$key] = 0;
        }
        $dataJumlahPotongan = [];
        foreach ($h_potongan as $key => $value) {
            $dataJumlahPotongan[$key] = 0;
        }

        // return response()->json($h_potongan, 200);
        $b = [
            'Lol',
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'];
            if ($buln = array_search($req->bulan, $b)) {
                // $buln = $buln+1;
            } else {
                $buln = $req->bulan;
            }
        $terakhir = Carbon::create($req->tahun, $buln)->lastOfMonth()->format('Y-m-d'); 
        // return response()->json($dataPenggajian, 200);
        $res = $dataPenggajian->map(function($q) use(&$dataBody, $idJabatan, $terakhir, $h_tunjangan, $h_potongan, &$dataJumlahTunjangan, &$dataJumlahPotongan) {
            // $pegawai = Pegawai::find($q->pegawai_id);
            // $res = [
            //     "id" => $q->pegawai_id,
            //     "status" => $pegawai ? 'true' : 'false'
            // ];
            // return $res;
            if ($q->ihsan_pokok_id == 0) {
                $ip_jumlah = 0;
            } else {
                $ip_jumlah = MasterPenggajian::find($q->ihsan_pokok_id)->jumlah_ihsan * $q->masa_kerja;
            }
            if ($q->ihsan_jk_id == 0) {
                $ijk_jumlah = 0;
            } else {
                $ijk_jumlah = MasterPenggajian::find($q->ihsan_jk_id)->jumlah_ihsan * $q->ihsan_jk_jp;
            }
            
            $cek = explode(",", $q->tunjangan_id);
            $tunjangan_jabatan_id = '';
            collect($cek)->map(function($x) use($idJabatan, &$tunjangan_jabatan_id ) {
                $is_jabatan = MasterPenggajian::where('id', $x)->whereIn('itp_id', $idJabatan)->first();
                if ($is_jabatan) {
                    $tunjangan_jabatan_id = $x;
                }
            });

            if ($tunjangan_jabatan_id) {
                $tunjangan_jabatan = MasterPenggajian::find($tunjangan_jabatan_id)->jumlah_ihsan;
            } else {
                $tunjangan_jabatan = 0;
            }
            $tunjangan = [];
            // $tunjangan = MasterPenggajian::whereIn("id",  $h_tunjangan)->select("id", "jumlah_ihsan")->orderBy("id")->get();
            foreach ($h_tunjangan as $key => $hp) {
                $p = 
                    DB::table('master_penggajians as mg')
                    ->leftJoin('ihsan_dan_potongans as ip', 'mg.itp_id', 'ip.id')
                    ->where('mg.tipe', 1)
                    ->where(function($dt) use($q) {
                        $dt->where('mg.tipe_pegawai_id', $q->tipe_pegawai_id)
                        ->orWhereNull('mg.tipe_pegawai_id');
                    })
                    ->where(function($dt) use($q) {
                        $dt->where('mg.jenis_pegawai_id', $q->jenis_pegawai_id)
                        ->orWhereNull('mg.jenis_pegawai_id');
                    })
                    ->where(function($dt) use($q) {
                        $dt->where(function($z) use($q) {
                            $z->where('mg.pegawai_id', 'LIKE', "%,".$q->pegawai_id.",%")
                            ->orWhere('mg.pegawai_id', 'LIKE', $q->pegawai_id)
                            ->orWhere('mg.pegawai_id', 'LIKE', $q->pegawai_id.",%")
                            ->orWhere('mg.pegawai_id', 'LIKE', "%,".$q->pegawai_id);
                        })
                        ->orWhere('mg.pegawai_id', '')
                        ->orWhereNull('mg.pegawai_id');
                    })
                    ->where(function($q) use($terakhir) {
                        $q->whereDate('mg.berlaku_sampai','>=', $terakhir)->orWhereNull('mg.berlaku_sampai');
                    })
                    ->where("mg.status", 1)
                    // ->where("mg.id", $hp->id)
                    ->where("ip.nama", $hp->nama)
                    // ->where("ip.nama", $hp->nama)
                    ->select('mg.id','mg.jumlah_ihsan', 'ip.nama', 'mg.is_persen')->get();
                
                if ($p->count() > 0) {
                    $z = new stdClass();
                    $z->id = $hp->id;
                    $z->jumlah_ihsan = $p->sum('jumlah_ihsan');
                    array_push($tunjangan, $z);
                    $dataJumlahTunjangan[$key] = $dataJumlahTunjangan[$key] + $z->jumlah_ihsan;
                } else {
                    $z = new stdClass();
                    $z->id = $hp->id;
                    $z->jumlah_ihsan = 0;
                    array_push($tunjangan, $z);
                    $dataJumlahTunjangan[$key] = $dataJumlahTunjangan[$key] + 0;
                }
            }

            $potongan = [];
            
            foreach ($h_potongan as $key => $hp) {
                $p = 
                    DB::table('master_penggajians as mg')
                    ->leftJoin('ihsan_dan_potongans as ip', 'mg.itp_id', 'ip.id')
                    ->where('mg.tipe', 0)
                    ->where(function($dt) use($q) {
                        $dt->where('mg.tipe_pegawai_id', $q->tipe_pegawai_id)
                        ->orWhereNull('mg.tipe_pegawai_id');
                    })
                    ->where(function($dt) use($q) {
                        $dt->where('mg.jenis_pegawai_id', $q->jenis_pegawai_id)
                        ->orWhereNull('mg.jenis_pegawai_id');
                    })
                    ->where(function($dt) use($q) {
                        $dt->where(function($z) use($q) {
                            $z->where('mg.pegawai_id', 'LIKE', "%,".$q->pegawai_id.",%")
                            ->orWhere('mg.pegawai_id', 'LIKE', $q->pegawai_id)
                            ->orWhere('mg.pegawai_id', 'LIKE', $q->pegawai_id.",%")
                            ->orWhere('mg.pegawai_id', 'LIKE', "%,".$q->pegawai_id);
                        })
                        ->orWhere('mg.pegawai_id', '')
                        ->orWhereNull('mg.pegawai_id');
                    })
                    ->where(function($q) use($terakhir) {
                        $q->whereDate('mg.berlaku_sampai','>=', $terakhir)->orWhereNull('mg.berlaku_sampai');
                    })
                    ->where("mg.status", 1)
                    // ->where("mg.id", $hp->id)
                    ->where("ip.nama", $hp->nama)
                    // ->where("ip.nama", $hp->nama)
                    ->select('mg.id','mg.jumlah_ihsan', 'ip.nama', 'mg.is_persen')->get();
                
                if ($p->count() > 0) {
                    $pArray = $p->map(function($pa) use($ip_jumlah) {
                        if ($pa->is_persen) {
                            $pa->jumlah_ihsan = $ip_jumlah * $pa->jumlah_ihsan /100;
                        }
                        return $pa;
                    });
                    $z = new stdClass();
                    $z->id = $hp->id;
                    $z->jumlah_ihsan = $pArray->sum('jumlah_ihsan');
                    
                    array_push($potongan, $z);
                    $dataJumlahPotongan[$key] = $dataJumlahPotongan[$key] + $z->jumlah_ihsan;
                } else {
                    $z = new stdClass();
                    $z->id = $hp->id;
                    $z->jumlah_ihsan = 0;
                    array_push($potongan, $z);
                    $dataJumlahPotongan[$key] = $dataJumlahPotongan[$key] + 0;
                }
                
            }
            
            $pegawai = Pegawai::find($q->pegawai_id);
            $year = new Carbon($pegawai->tgl_masuk);
            $total =  $ip_jumlah + $ijk_jumlah + $tunjangan_jabatan + collect($tunjangan)->sum("jumlah_ihsan") - collect($potongan)->sum("jumlah_ihsan");
            
            
            $res = [
                "nama" => $pegawai->nama,
                "jenis" => $q->jp_nama,
                "tahun" => $year->year,
                "sertifikasi" => $pegawai->sertifikasi,
                "mk" => $q->masa_kerja,
                "ip_jumlah" => $ip_jumlah,
                "jp" => $q->ihsan_jk_jp,
                "ijk_jumlah" => $ijk_jumlah,
                "tunjangan_jabatan" => $tunjangan_jabatan,
                "tunjangan" => $tunjangan,
                "potongan" => $potongan,
                "total" => $total
            ];

            return $res;


        });

        // return response()->json($res, 200);
        $jumlah = new stdClass();
        $jumlah->ip_jumlah = $res->sum("ip_jumlah");
        $jumlah->ijk_jumlah = $res->sum("ijk_jumlah");
        $jumlah->tunjangan_jabatan = $res->sum("tunjangan_jabatan");
        $jumlah->data_jumlah_tunjangan = $dataJumlahTunjangan;
        $jumlah->data_jumlah_potongan = $dataJumlahPotongan;
        $jumlah->total = $res->sum("total");
        
        $req->tanggal = Carbon::today()->toDateString();
        $res = $res->groupBy('jenis');
        $res = collect($res);
        // return response()->json($res, 200);
        if($req->print == 'true'){
            ini_set("max_execution_time", "800");
            ini_set('memory_limit', '-1');
            $pdF = App::make('dompdf.wrapper');
            // // $pdF->setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]);
            $pdF->loadView('pdf.penggajian-laporan', compact("jumlah","header", "req", "res"));
            // (Optional) Setup the paper size and orientation
            $pdF->setPaper('A4', 'landscape');
            $pdF->setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]);

            // Render the HTML as PDF
            // $pdF->render();
            return $pdF->stream('Laporan '. Carbon::now()->format('d F Y') . '.pdf');
            PDF::setOption('isHtml5ParserEnabled', true);
            $pdf = PDF::loadView('pdf.penggajian-laporan', compact("jumlah","header", "req", "res"))->setIsHtml5ParserEnabled(true)->setPaper('a4', 'landscape');
            return $pdf->stream();
            
        } else if($req->print == 'excel'){
            
            $nama_file = 'Laporan_Penggajian_'. $bulan.'_'.$tahun . date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new PenggajianLaporan($req, $jumlah, $header, $res), $nama_file);
            
        }

        $req->print = false;
        
        // return response()->json($res, 200);
        return view("pdf.penggajian-laporan", compact("jumlah","header", "req", "res"));

    }

    public function payrollPenggajianLama($bulan, $tahun, Request $req){

        $idJabatan = IhsanDanPotongan::where('is_jabatan', '1')->get()->pluck("id");
        $dataPenggajian = DB::table('penggajians as ps')
                ->leftJoin('pegawais as pe', 'pe.id', 'ps.pegawai_id')
                ->where("bulan", $bulan)
                ->where("tahun", $tahun)
                ->select('ps.*', 'pe.tipe_pegawai_id', 'pe.jenis_pegawai_id')
                ->orderBy('pe.bank', 'ASC')
                ->orderBy('pe.nama', 'ASC')
                ->get();
        
        $headerPotongan = [];
        $dataPenggajian->map(function($q) use(&$headerPotongan, $idJabatan ) {
            $cek = explode(",", $q->potongan_id);
            collect($cek)->map(function($x) use(&$headerPotongan, $idJabatan ) {
                $is_jabatan = MasterPenggajian::where('id', $x)->whereIn('itp_id', $idJabatan)->first();
                if ($is_jabatan ) {
                    # code...
                } else {
                    
                    $is_header = array_search($x, $headerPotongan);
                    if ($is_header === false) {
                        array_push($headerPotongan, $x);
                    } 
                }
            });
        });

        
        
        $h_potongan = DB::table('master_penggajians as pg')
                        ->leftJoin("ihsan_dan_potongans as ip", "ip.id", "pg.itp_id")
                        ->whereIn("pg.id", $headerPotongan)
                        ->select("pg.id", "ip.nama")
                        ->orderBy("pg.id")
                        ->groupBy("ip.nama")
                        ->get();
        
        $header = [
            "potongan" => $h_potongan
        ];

        $dataJumlahPotongan = [];
        foreach ($h_potongan as $key => $value) {
            $dataJumlahPotongan[$key] = 0;
        }
        $b = [
            'Lol',
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'];
            if ($buln = array_search($req->bulan, $b)) {
                // $buln = $buln+1;
            } else {
                $buln = $req->bulan;
            }
        $terakhir = Carbon::create($req->tahun, $buln)->lastOfMonth()->format('Y-m-d'); 
        $res = $dataPenggajian->map(function($q) use(&$dataJumlahPotongan, $h_potongan, $terakhir) {
            // $ip_jumlah = MasterPenggajian::find($q->ihsan_pokok_id)->jumlah_ihsan * $q->masa_kerja;
            // $ijk_jumlah = MasterPenggajian::find($q->ihsan_jk_id)->jumlah_ihsan * $q->ihsan_jk_jp;
            if ($q->ihsan_pokok_id == 0) {
                $ip_jumlah = 0;
            } else {
                $ip_jumlah = MasterPenggajian::find($q->ihsan_pokok_id)->jumlah_ihsan * $q->masa_kerja;
            }
            if ($q->ihsan_jk_id == 0) {
                $ijk_jumlah = 0;
            } else {
                $ijk_jumlah = MasterPenggajian::find($q->ihsan_jk_id)->jumlah_ihsan * $q->ihsan_jk_jp;
            }
            $cekTunjangan = explode(",", $q->tunjangan_id);
           
            
            $tunjangan = MasterPenggajian::whereIn("id", $cekTunjangan)->select("id", "jumlah_ihsan")->orderBy("id")->get();
            $potongan = [];
            foreach ($h_potongan as $key => $hp) {
                $p = DB::table('master_penggajians as mg')
                ->leftJoin('ihsan_dan_potongans as ip', 'mg.itp_id', 'ip.id')
                ->where('mg.tipe', 0)
                ->where(function($dt) use($q) {
                    $dt->where('mg.tipe_pegawai_id', $q->tipe_pegawai_id)
                    ->orWhereNull('mg.tipe_pegawai_id');
                })
                ->where(function($dt) use($q) {
                    $dt->where('mg.jenis_pegawai_id', $q->jenis_pegawai_id)
                    ->orWhereNull('mg.jenis_pegawai_id');
                })
                ->where(function($dt) use($q) {
                    $dt->where(function($z) use($q) {
                        $z->where('mg.pegawai_id', 'LIKE', "%,".$q->pegawai_id.",%")
                        ->orWhere('mg.pegawai_id', 'LIKE', $q->pegawai_id)
                        ->orWhere('mg.pegawai_id', 'LIKE', $q->pegawai_id.",%")
                        ->orWhere('mg.pegawai_id', 'LIKE', "%,".$q->pegawai_id);
                    })
                    ->orWhere('mg.pegawai_id', '')
                    ->orWhereNull('mg.pegawai_id');
                })
                ->where(function($q) use($terakhir) {
                    $q->whereDate('mg.berlaku_sampai','>=', $terakhir)->orWhereNull('mg.berlaku_sampai');
                })
                ->where("mg.status", 1)
                ->where("ip.nama", $hp->nama)
                ->select('mg.id','mg.jumlah_ihsan', 'ip.nama', 'mg.is_persen')->get();
                    
                if ($p->count() > 0) {
                    $pArray = $p->map(function($pa) use($ip_jumlah) {
                        if ($pa->is_persen) {
                            $pa->jumlah_ihsan = $ip_jumlah * $pa->jumlah_ihsan /100;
                        }
                        return $pa;
                    });
                    $z = new stdClass();
                    $z->id = $hp->id;
                    $z->jumlah_ihsan = $pArray->sum('jumlah_ihsan');
                    
                    array_push($potongan, $z);
                    $dataJumlahPotongan[$key] = $dataJumlahPotongan[$key] + $z->jumlah_ihsan;
                    // if ($p->is_persen) {
                    //     $p->jumlah_ihsan = $ip_jumlah * $p->jumlah_ihsan /100;
                    // }
                    // array_push($potongan, $p);
                    // $dataJumlahPotongan[$key] = $dataJumlahPotongan[$key] + $p->jumlah_ihsan;
                } else {
                    $z = new stdClass();
                    $z->id = $hp->id;
                    $z->jumlah_ihsan = 0;
                    array_push($potongan, $z);
                    $dataJumlahPotongan[$key] = $dataJumlahPotongan[$key] + 0;
                }
                
            }
            $pegawai = Pegawai::find($q->pegawai_id);
            $year = new Carbon(@$pegawai->tgl_masuk);
            $total =  $ip_jumlah + $ijk_jumlah + $tunjangan->sum("jumlah_ihsan");
            $diterima = $total - collect($potongan)->sum("jumlah_ihsan");
            $res = [
                "nama" => @$pegawai->nama,
                // "tahun" => $year->year,
                "no_rek" => @$pegawai->no_rek,
                "bank" => @$pegawai->bank,
                // "sertifikasi" => $pegawai->sertifikasi,
                // "mk" => $q->masa_kerja,
                "pokok" => $ip_jumlah,
                // "jp" => $q->ihsan_jk_jp,
                // "ijk_jumlah" => $ijk_jumlah,
                "lain2" => $tunjangan->sum('jumlah_ihsan') +  $ijk_jumlah,
                "total" => $total,
                "potongan" => $potongan,
                "diterima" => $diterima
            ];

            return $res;


        });
        
        $req->bulan = $bulan;
        $req->tahun = $tahun;
        $jumlah = new stdClass();
        $jumlah->pokok = $res->sum("pokok");
        $jumlah->lain2 = $res->sum("lain2");
        $jumlah->total = $res->sum("total");
        $jumlah->potongan = $dataJumlahPotongan;
        $jumlah->diterima = $res->sum("diterima");
        // return response()->json($res->groupBy('bank'), 200);
        if($req->print == 'true'){
            ini_set("max_execution_time", "800");
            ini_set('memory_limit', '-1');
            $res = $res->groupBy('bank');
            $pdf = PDF::loadView('pdf.penggajian-payroll-copy', compact("jumlah","header", "req", "res"))->setPaper('a4', 'landscape');;
            return $pdf->stream();
            
        } else if($req->print == 'excel'){
            // return response()->json($res, 200);
            $nama_file = 'Penggajian_Payroll'. $bulan.'_'.$tahun . date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new PenggajianPayrollCopy($req, $jumlah, $header, $res), $nama_file);
            
        }
        $res = $res->groupBy('bank');
        // return response()->json($res, 200);
        $req->print = false;
        // return response()->json($res, 200);
        return view("pdf.penggajian-payroll-copy", compact("jumlah", "header", "req", "res"));
    }

    public function payrollPenggajian($bulan, $tahun, Request $req){

        $idJabatan = IhsanDanPotongan::where('is_jabatan', '1')->get()->pluck("id");
        $dataPenggajian = DB::table('penggajians as ps')
                        ->leftJoin('pegawais as pe', 'pe.id', 'ps.pegawai_id')
                        ->where("bulan", $bulan)
                        ->where("tahun", $tahun)
                        ->select('ps.*', 'pe.tipe_pegawai_id', 'pe.jenis_pegawai_id')
                        ->orderBy('pe.nama', 'ASC')
                        ->get();
        // $dataPenggajian = Penggajian::where("bulan", $bulan)
        // ->where("tahun", $tahun)->get();
        // return response()->json($dataPenggajian, 200);
        
        $headerPotongan = [];
        $dataPenggajian->map(function($q) use(&$headerPotongan, $idJabatan ) {
            $cek = explode(",", $q->potongan_id);
            collect($cek)->map(function($x) use(&$headerPotongan, $idJabatan ) {
                $is_jabatan = MasterPenggajian::where('id', $x)->whereIn('itp_id', $idJabatan)->first();
                if ($is_jabatan ) {
                    # code...
                } else {
                    
                    $is_header = array_search($x, $headerPotongan);
                    if ($is_header === false) {
                        array_push($headerPotongan, $x);
                    } 
                }
            });
        });

        // return response()->json($dataPenggajian, 200);
        
        $h_potongan = DB::table('master_penggajians as pg')
                        ->leftJoin("ihsan_dan_potongans as ip", "ip.id", "pg.itp_id")
                        ->whereIn("pg.id", $headerPotongan)
                        ->select("pg.id", "ip.nama")
                        ->orderBy("pg.id")
                        ->groupBy('ip.nama')
                        ->get();
        
        $header = [
            "potongan" => $h_potongan
        ];

        //  return response()->json($header, 200);

        $dataJumlahPotongan = [];
        foreach ($h_potongan as $key => $value) {
            $dataJumlahPotongan[$key] = 0;
        }
        $b = [
            'Lol',
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'];
            if ($buln = array_search($req->bulan, $b)) {
                // $buln = $buln+1;
            } else {
                $buln = $req->bulan;
            }
        $terakhir = Carbon::create($req->tahun, $buln)->lastOfMonth()->format('Y-m-d'); 
        // return response()->json($dataPenggajian, 200);
        $res = $dataPenggajian->map(function($q) use(&$dataJumlahPotongan, $h_potongan, $terakhir) {
            // $ip_jumlah = MasterPenggajian::find($q->ihsan_pokok_id)->jumlah_ihsan * $q->masa_kerja;
            // $ijk_jumlah = MasterPenggajian::find($q->ihsan_jk_id)->jumlah_ihsan * $q->ihsan_jk_jp;
            if ($q->ihsan_pokok_id == 0) {
                $ip_jumlah = 0;
            } else {
                $ip_jumlah = MasterPenggajian::find($q->ihsan_pokok_id)->jumlah_ihsan * $q->masa_kerja;
            }
            if ($q->ihsan_jk_id == 0) {
                $ijk_jumlah = 0;
            } else {
                $ijk_jumlah = MasterPenggajian::find($q->ihsan_jk_id)->jumlah_ihsan * $q->ihsan_jk_jp;
            }
            $cekTunjangan = explode(",", $q->tunjangan_id);
           
            
            $tunjangan = MasterPenggajian::whereIn("id", $cekTunjangan)->select("id", "jumlah_ihsan")->orderBy("id")->get();
            $potongan = [];
            foreach ($h_potongan as $key => $hp) {
                $p = 
                    DB::table('master_penggajians as mg')
                    ->leftJoin('ihsan_dan_potongans as ip', 'mg.itp_id', 'ip.id')
                    ->where('mg.tipe', 0)
                    ->where(function($dt) use($q) {
                        $dt->where('mg.tipe_pegawai_id', $q->tipe_pegawai_id)
                        ->orWhereNull('mg.tipe_pegawai_id');
                    })
                    ->where(function($dt) use($q) {
                        $dt->where('mg.jenis_pegawai_id', $q->jenis_pegawai_id)
                        ->orWhereNull('mg.jenis_pegawai_id');
                    })
                    ->where(function($dt) use($q) {
                        $dt->where(function($z) use($q) {
                            $z->where('mg.pegawai_id', 'LIKE', "%,".$q->pegawai_id.",%")
                            ->orWhere('mg.pegawai_id', 'LIKE', $q->pegawai_id)
                            ->orWhere('mg.pegawai_id', 'LIKE', $q->pegawai_id.",%")
                            ->orWhere('mg.pegawai_id', 'LIKE', "%,".$q->pegawai_id);
                        })
                        ->orWhere('mg.pegawai_id', '')
                        ->orWhereNull('mg.pegawai_id');
                    })
                    ->where(function($q) use($terakhir) {
                        $q->whereDate('mg.berlaku_sampai','>=', $terakhir)->orWhereNull('mg.berlaku_sampai');
                    })
                    ->where("mg.status", 1)
                    // ->where("mg.id", $hp->id)
                    ->where("ip.nama", $hp->nama)
                    // ->where("ip.nama", $hp->nama)
                    ->select('mg.id','mg.jumlah_ihsan', 'ip.nama', 'mg.is_persen')->get();
                
                if ($p->count() > 0) {
                    $pArray = $p->map(function($pa) use($ip_jumlah) {
                        if ($pa->is_persen) {
                            $pa->jumlah_ihsan = $ip_jumlah * $pa->jumlah_ihsan /100;
                        }
                        return $pa;
                    });
                    $z = new stdClass();
                    $z->id = $hp->id;
                    $z->jumlah_ihsan = $pArray->sum('jumlah_ihsan');
                    
                    array_push($potongan, $z);
                    $dataJumlahPotongan[$key] = $dataJumlahPotongan[$key] + $z->jumlah_ihsan;
                } else {
                    $z = new stdClass();
                    $z->id = $hp->id;
                    $z->jumlah_ihsan = 0;
                    array_push($potongan, $z);
                    $dataJumlahPotongan[$key] = $dataJumlahPotongan[$key] + 0;
                }
                
            }
            $pegawai = Pegawai::find($q->pegawai_id);
            $year = new Carbon($pegawai->tgl_masuk);
            
            $total =  $ip_jumlah + $ijk_jumlah + $tunjangan->sum("jumlah_ihsan");
            $diterima = $total - collect($potongan)->sum("jumlah_ihsan");
            $res = [
                "nama" => $pegawai->nama,
                "tunjangan" => $tunjangan,
                "no_rek" => $pegawai->no_rek,
                // "sertifikasi" => $pegawai->sertifikasi,
                // "mk" => $q->masa_kerja,
                "pokok" => $ip_jumlah,
                // "jp" => $q->ihsan_jk_jp,
                // "ijk_jumlah" => $ijk_jumlah,
                "lain2" => $tunjangan->sum('jumlah_ihsan') +  $ijk_jumlah,
                "total" => $total,
                "potongan" => $potongan,
                "diterima" => $diterima
            ];

            return $res;


        });
        
        $req->bulan = $bulan;
        $req->tahun = $tahun;
        $jumlah = new stdClass();
        $jumlah->pokok = $res->sum("pokok");
        $jumlah->lain2 = $res->sum("lain2");
        $jumlah->total = $res->sum("total");
        $jumlah->potongan = $dataJumlahPotongan;
        $jumlah->diterima = $res->sum("diterima");
        // return response()->json($res, 200);
        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.penggajian-payroll', compact("jumlah","header", "req", "res"))->setPaper('a4', 'potrait');;
            return $pdf->stream();
            
        } else if($req->print == 'excel'){
            
            $nama_file = 'Penggajian_Payroll'. $bulan.'_'.$tahun . date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new PenggajianPayroll($req, $jumlah, $header, $res), $nama_file);
            
        }

        $req->print = false;
        // return response()->json($res, 200);
        return view("pdf.penggajian-payroll", compact("jumlah", "header", "req", "res"));
    }

    public function index(Request $req){

        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.lk',compact("req"));
            return $pdf->stream();
            
        }else if($req->print == 'excel'){
           
            $nama_file = 'laporan_LK_'.date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new LkExport($req), $nama_file);
            
        }else{
           
            return view("pdf.lk",compact("req"));
        }
    }
    
    function cetakperJenis(Request $req){
        // dd($req);
        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.laporan-perjenis',compact("req"));
            return $pdf->stream();
            
        }else if($req->print == 'excel'){
            $nama_file = 'laporan_PerJenis_'.date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new PerJenisExport($req), $nama_file);
            
        }else{
            return view("pdf.laporan-perjenis",compact("req"));
        }
    }

    
    public function cetakperKelas(Request $req,$id){

        $kelas = Kelas::find($id);
        $siswa = Siswa::where("id_kelas",$id)->get();
        $res = array();
        foreach($siswa as $s){
            $tagihan = BiayaSiswa::where("nis",$s->nis)
                ->where("nominal","!=","dibayar")
                ->get();
            $sisa = 0;

            foreach($tagihan as $t){
                $sisa += $t->nominal - $t->dibayar;    
            }
            $data = [
                    "nis" => $s->nis, 
                    "nama" => $s->nama,
                "jk" => $s->jk,
                    "tagihan" => $sisa,
                ];
            array_push($res,$data);
        }

        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.laporan-perkelas',compact('req','res','kelas'));
            return $pdf->stream();
            
        }else if($req->print == 'excel'){
          
            $nama_file = 'laporan_PerKelas_'.date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new PerKelasExport($req,$res,$kelas), $nama_file);
            
        }else{
            return view('pdf.laporan-perkelas',compact('req','res','kelas'));
        }
       
        
     }
    public function lk(Request $req){
        return view("laporan.lk",compact('req'));
    }

    public function perJenis(Request $req){
        return view("laporan.perjenis",compact('req'));
    }

  

    public function jurnal(Request $req){
        return view("laporan.jurnal",compact('req'));
    }

       //
    public function pdfjurnal(Request $req){

        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.laporan-jurnal',compact("req"));
            return $pdf->stream();
        }else if($req->print == 'excel'){
            $nama_file = 'laporan_Jurnal_'.date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new JurnalExport($req), $nama_file);
            
        }else{
            return view("pdf.laporan-jurnal",compact("req"));
        }
    }
    

    // Pemasukan
       
        public function pemasukan(Request $req){
            return view("laporan.pemasukan",compact('req'));
        }
       public function pdfpemasukan(Request $req){

        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.pemasukan',compact("req"));
            return $pdf->stream();
        }else if($req->print == 'excel'){
            
            $nama_file = 'Uang_Setoran_'.date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new UangSetoranExport($req), $nama_file);
            
        }
        else{
            return view("pdf.pemasukan",compact("req"));
        }
    }

    // Setoran
       
    public function setoran(Request $req){
            return view("laporan.setoran",compact('req'));
    }
    public function pdfsetoran(Request $req){
        

        // return response()->json($data, 200);
        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.setoran',compact("req"));
            return $pdf->stream();
        }else if($req->print == 'excel'){
            
            $nama_file = 'Uang_Setoran_Siswa_'.date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new UangSetoranSiswaExport($req), $nama_file);
            
        }
        else{
            return view("pdf.setoran",compact("req"));
        }
    }

    public function dataKelas($id){
        return Kelas::where('jenjang_id', $id)->orderBy('nama', 'ASC')->get();
    }

     // Persiswa
       
    public function persiswa(Request $req){
        return view("laporan.persiswa",compact('req'));
    }
    public function pdfpersiswa(Request $req){

        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.persiswa',compact("req"));
            return $pdf->stream();
            
        }else if($req->print == 'excel'){
            
            $nama_file = 'Laporan_PerSIswa_'.date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new PerSiswaExport($req), $nama_file);
            
        }
        else{
            return view("pdf.persiswa",compact("req"));
        }
    }


    public function pdftabungan(Request $req,$nis,$start,$end){

        $siswa = Siswa::where("nis",$nis)->first();
        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.tabungan',compact("req","nis","start","end","siswa"));
            return $pdf->stream();
            
        }else if($req->print == 'excel'){
            
            $nama_file = 'Laporan_Tabungan_Siswa_'. $nis . date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new Tabungan($req,$nis,$start,$end,$siswa), $nama_file);
            
        }
        else{
            return view("pdf.tabungan",compact("req","nis","start","end","siswa"));
        }
    }


    public function DaftarUlang(Request $req){
        return view("laporan.daftar-ulang",compact('req'));
    }
    public function CetakDaftarUlang(Request $req){
        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.laporan-daftar-ulang',compact("req"));
            return $pdf->stream();
        }else if($req->print == 'excel'){
            $nama_file = 'laporan_daftar_ulang_'.date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new DaftarUlang($req), $nama_file);
        }else{
            return view("pdf.laporan-daftar-ulang",compact("req"));
        }
    }

    public function CetakDaftarUlangPerJenis(Request $req,$id){

        $req1 = json_decode($req->data);

        $jenis = JenisBiaya::find($id);

        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.laporan-daftar-ulang-per-jenis',compact("req","req1","jenis"));
            return $pdf->stream();
        }else if($req->print == 'excel'){
            $nama_file = 'laporan_daftar_ulang_per_jenis'.date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new DaftarUlangPerJenis($req,$req1,$jenis), $nama_file);
        }else{
            return view("pdf.laporan-daftar-ulang-per-jenis",compact("req","req1",'jenis'));
        }
    }

    public function DUKelas(Request $req){
        return view("laporan.daftar-ulang-kelas",compact('req'));
    }

    public function cetakDUKelas(Request $req,$id){

        
            $kelas = Kelas::find($id);
            
            
            if($req->gender !== 'SEMUA'){
                $siswa = Siswa::
                where("id_kelas",$id)
                ->where("jk",$req->gender)
                ->orderBy("nama","ASC")
                ->get();
            }else{
                $siswa = Siswa::
                    where("id_kelas",$id)
                    ->orderBy("nama","ASC")
                    ->get();
            }
            $res = array();
            foreach($siswa as $s){
                $tagihan = BiayaSiswa::
                    where("nis",$s->nis)
                    ->where("bulan",7)
                    ->where("satuan","Daftar Ulang")
                    ->where("tahun_ajaran",$req->du_tahun_ajaran)
                    ->get();
                $sisa = 0;
                $dibayar = 0;
                $nominal = 0;

                foreach($tagihan as $t){
                    $sisa += $t->nominal - $t->dibayar;    
                    $dibayar += $t->dibayar;    
                    $nominal += $t->nominal;    
                }
                $data = [
                        "nis" => $s->nis, 
                        "nama" => $s->nama,
                        "jk" => $s->jk,
                        "tagihan" => $sisa,
                        "dibayar" => $dibayar,
                        "nominal" => $nominal,
                    ];
                array_push($res,$data);
            }

            if($req->print == 'true'){
                $pdf = PDF::loadView('pdf.du-kelas',compact('req','res','kelas'));
                return $pdf->stream();
                
            }else if($req->print == 'excel'){
            
                $nama_file = 'laporan_DU_Kelas_'.date('Y-m-d_H-i-s').'.xlsx';
                return Excel::download(new DUPerKelasExport($req,$res,$kelas), $nama_file);
                
            }else{
                return view('pdf.du-kelas',compact('req','res','kelas'));
            }
    }

    public function cetakTagihanSiswaLulus(Request $req){
    

        $siswa = DB::table("siswas as s")
            ->leftJoin('biaya_siswas as bs', 'bs.nis', 's.nis')
            ->leftJoin('riwayat_kelas as rk', 'rk.nis', 's.nis')
            ->leftJoin('kelas as k', 'rk.id_kelas', 'k.id')
            ->where("s.id_kelas",0)
            ->where("s.status",2)
            ->where("bs.nominal","!=","bs.dibayar")
            ->where("rk.kelas_tujuan", 0)
            ->select(
                "s.nis","s.nama","s.jk",
                DB::raw('SUM((bs.nominal - bs.dibayar)) AS tagihan'),
                "k.nama as kelas")
            ->having('tagihan', '>', 0)
            ->groupBy('s.nis')
            ->orderBy('s.nis','ASC')
            ->get();

        $res = $siswa->map(function($s) use($req) {
            $s->kelas = $s->kelas ? $s->kelas : '-';
            if($s->tagihan > 0){
                return $s;
            }else{
                if($req->filter !== "tagihan"){
                    return $s;
                }
            }
            return false;
        })->reject(function ($value) {
            return $value === false;
        });

        // return count($siswa);
        

        // return response()->json($res, 200);
        $res  = json_decode($res, true);
        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.laporan-tagihan-siswa-lulus',compact('req','res'));
            return $pdf->stream();
            
        }else if($req->print == 'excel'){
          
            $nama_file = 'laporan_Tagihan_Siswa_Lulus_'.date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new LulusExport($req,$res), $nama_file);
            
        }else{
            return view('pdf.laporan-tagihan-siswa-lulus',compact('req','res'));
        }
    }


    public function saldoTabunganSiswa(Request $req){
    
        $res = DB::table("jurnals as j")
            ->leftJoin('siswas as s', 'j.nis', 's.nis')
            ->select(
                "j.nis", 
                's.nama',
                DB::raw('SUM(case when j.type = 1 then j.jumlah else 0 end) as masuk,
                SUM(case when j.type = 0 then j.jumlah else 0 end) as keluar,
                SUM(case when j.type = 1 then j.jumlah else 0 end)-SUM(case when j.type = 0 then j.jumlah else 0 end) as saldo')
                )
            ->where("j.id_jenis_biaya", "=", 4)
            ->whereNotNull("j.nis")
            ->groupBy("j.nis")
            ->orderBy('j.nis','ASC')
            // ->take(799)
            ->get();
        
        
        $user = Auth::user()->name;
        // dd($a);
        // return response()->json($data, 200);
        
        if($req->print == 'true'){
            $pdf = PDF::loadView('pdf.laporan-saldo-tabungan-siswa',compact('req','res', 'user'));
            return $pdf->stream();
            
        }else if($req->print == 'excel'){
          
            $nama_file = 'laporan_Saldo_Tabungan_Siswa'.date('Y-m-d_H-i-s').'.xlsx';
            return Excel::download(new TabunganExport($req,$res, $user), $nama_file);
            
        }else{
            return view('pdf.laporan-saldo-tabungan-siswa',compact('req','res', 'user'));
        }
    }
}
