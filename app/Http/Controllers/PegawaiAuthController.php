<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Pegawai\Entities\Pegawai;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Modules\Pegawai\Entities\Penggajian;
use Modules\Pegawai\Entities\MasterPenggajian;

class PegawaiAuthController extends Controller
{
    /**
     * Create a new PegawaiAuthController instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        // $this->middleware('pegawai', ['except' => ['login']]);
        Config::set('jwt.user', \Modules\Pegawai\Entities\Pegawai::class);
        Config::set('auth.providers', ['users' => [
                'driver' => 'eloquent',
                'model' => \Modules\Pegawai\Entities\Pegawai::class,
            ]]);
        
    }
    public function username()
    {
        return 'nik';
    }
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $id = auth()->id();
        $data = DB::table('pegawais as pg')
                    ->leftJoin('tipe_pegawais as tp', 'tp.id', 'pg.tipe_pegawai_id')
                    ->leftJoin('jenis_pegawais as jp', 'jp.id', 'pg.jenis_pegawai_id')
                    ->select("pg.*", "tp.nama as tipe_nama", "tp.kode as tipe_kode", "jp.nama as jenis_nama")
                    ->where("pg.id", $id)
                    // ->exclude(["pg.password"])
                    ->first();
        $data->password = "";
        
        $res = [
            "data" => $data
        ];
        return response()->json($res);
    }

    public function updatePegawai(Request $req){
        $id = auth()->id();
        $user = Pegawai::find($id);
        $user->nama = $req->nama;
        $user->nik = $req->niy;
        $user->no_rek = $req->rek;
        $user->bank = $req->bank;
        $user->no_hp = $req->hp;
        $user->pin = $req->pin;
        $user->alamat = $req->alamat;
        if ($req->password) {
            $pass = bcrypt($req->password);
            $user->password = $pass;
        }
        $user->update();
        return response()->json($user, 200);


    }

    public function pendapatanBulanIni(){
        $id = auth()->id();
        $now = Carbon::now();
        $bulan  = $now->month;
        $tahun  = $now->year;
        // return $bulan;
        $dataGajian = Penggajian::where("pegawai_id", $id)->where('bulan',$bulan)->where('tahun',$tahun)->first();
        if ($dataGajian) {
            $pokok = $dataGajian->ihsan_pokok_id ? MasterPenggajian::find($dataGajian->ihsan_pokok_id)->jumlah_ihsan * $dataGajian->masa_kerja : 0;
            $dataGajian->pokok = rupiah($pokok);
            $jamker = $dataGajian->ihsan_jk_id ? MasterPenggajian::find($dataGajian->ihsan_jk_id)->jumlah_ihsan * $dataGajian->ihsan_jk_jp : 0;
            $dataGajian->jamker = rupiah($jamker);
            $potongan_id = explode(",", $dataGajian->potongan_id);
            $potongan = DB::table('master_penggajians as mp')
                            ->leftJoin("ihsan_dan_potongans as ip", "ip.id", "mp.itp_id")
                            ->whereIn("mp.id", $potongan_id)
                            ->select("mp.id","mp.jumlah_ihsan","ip.nama", "mp.is_persen")
                            ->get();
            $potongan_rp = $potongan->map(function($q) use($pokok) {
                        if ($q->is_persen) {
                            $q->rupiah = rupiah($pokok * $q->jumlah_ihsan/100);
                            $q->jumlah_ihsan = $pokok * $q->jumlah_ihsan/100;
                        } else {

                            $q->rupiah = rupiah($q->jumlah_ihsan);
                        }
                        return $q;
            });
            $dataGajian->potongan = $potongan_rp;
            $tunjangan_id = explode(",", $dataGajian->tunjangan_id);
            $tunjangan = DB::table('master_penggajians as mp')
                            ->leftJoin("ihsan_dan_potongans as ip", "ip.id", "mp.itp_id")
                            ->whereIn("mp.id", $tunjangan_id)
                            ->select("mp.id","mp.jumlah_ihsan","ip.nama")
                            ->get();
            $tunjangan_rp = $tunjangan->map(function($q) {
                        $q->rupiah = rupiah($q->jumlah_ihsan);
                        return $q;
            });
            $dataGajian->tunjangan = $tunjangan_rp;
            $jumlah_potongan = $potongan->count() > 0 ? $potongan->sum("jumlah_ihsan") : 0;
            $jumlah_tunjangan = $tunjangan->count() > 0 ? $tunjangan->sum("jumlah_ihsan") : 0;
            $jumlah = ($pokok) + ($jamker ) + $jumlah_tunjangan - $jumlah_potongan;
            $dataGajian->jumlah = rupiah($jumlah);
            $dataGajian->bulan_nama = bulan_text($dataGajian->bulan);
            $dataGajian->tahun_2 = substr ($dataGajian->tahun, -2);
            $res = [
                "status" => true,
                "data" => $dataGajian
            ];
        } else {
            $res = [
                "status" => false,
                "pesan" => "Belum ada data penggajian untuk bulan ini"
            ];
        }
        

        return response()->json($res, 200);
    }

    public function uploadFoto(Request $request){

        // $image_64 = $request->file('file'); //your base64 encoded data
        // // return response()->json($image_64, 200);
        $id = auth()->id();

        $file = $request->file('avatar');
		$nama_file = time()."_".$id."_.".$file->getClientOriginalExtension();
        Storage::disk('pegawai')->put($nama_file,file_get_contents($file));

        $user = Pegawai::find($id);
        if ($user->foto) {
            # code...
            Storage::disk("pegawai")->delete($user->foto);
        }
        $user->foto = $nama_file;
        $user->update();
        if ($user) {
            $res = [
                "status" => true,
                "data" => $user
            ];
        } else {
            $res = [
                "status" => false,
                "pesan" => "Belum ada data penggajian untuk bulan ini"
            ];
        }
        

        return response()->json($res, 200);

        // return response()->json($nama_file, 200);
    }

    public function sejarahPenggajian(){
        $id = auth()->id();
        $dataGajian = Penggajian::where("pegawai_id", $id)->orderBy('tahun','DESC')->orderBy('bulan','DESC')->get();

        if ($dataGajian->count() > 0) {
            $data = $dataGajian->map(function($q) {
                $pokok = $q->ihsan_pokok_id ? MasterPenggajian::find($q->ihsan_pokok_id)->jumlah_ihsan * $q->masa_kerja : 0;;
                $q->pokok = rupiah($pokok);
                $jamker = $q->ihsan_jk_id ? MasterPenggajian::find($q->ihsan_jk_id)->jumlah_ihsan * $q->ihsan_jk_jp : 0;
                $q->jamker = rupiah($jamker);
                $potongan_id = explode(",", $q->potongan_id);
                $potongan = DB::table('master_penggajians as mp')
                                ->leftJoin("ihsan_dan_potongans as ip", "ip.id", "mp.itp_id")
                                ->whereIn("mp.id", $potongan_id)
                                ->select("mp.id","mp.jumlah_ihsan","ip.nama", 'mp.is_persen')
                                ->get();
                $potongan_rp = $potongan->map(function($q) use($pokok){
                    if ($q->is_persen) {
                        $q->rupiah = rupiah($pokok * $q->jumlah_ihsan/100);
                        $q->jumlah_ihsan = $pokok * $q->jumlah_ihsan/100;
                    } else {
                        $q->rupiah = rupiah($q->jumlah_ihsan);
                    }
                            // $q->rupiah = rupiah($q->jumlah_ihsan);
                            return $q;
                });
                $q->potongan = $potongan_rp;
                $tunjangan_id = explode(",", $q->tunjangan_id);
                $tunjangan = DB::table('master_penggajians as mp')
                                ->leftJoin("ihsan_dan_potongans as ip", "ip.id", "mp.itp_id")
                                ->whereIn("mp.id", $tunjangan_id)
                                ->select("mp.id","mp.jumlah_ihsan","ip.nama")
                                ->get();
                $tunjangan_rp = $tunjangan->map(function($q) {
                            $q->rupiah = rupiah($q->jumlah_ihsan);
                            return $q;
                });
                $q->tunjangan = $tunjangan_rp;
                $jumlah_potongan = $potongan->count() > 0 ? $potongan->sum("jumlah_ihsan") : 0;
                $jumlah_tunjangan = $tunjangan->count() > 0 ? $tunjangan->sum("jumlah_ihsan") : 0;
                $jumlah = ($pokok) + ($jamker ) + $jumlah_tunjangan - $jumlah_potongan;
                $q->jumlah = rupiah($jumlah);
                $q->bulan_nama = bulan_text($q->bulan);
                $q->tahun_2 = substr ($q->tahun, -2);
                return $q;
            });
        } else {
            $data = $dataGajian;
        }



        return response()->json($data, 200);
    }

    public function statusDiterima(Request $req) {
        $penggajian = Penggajian::find($req->id);
        $penggajian->status = 1;
        $penggajian->update();
        if ($penggajian) {
            $res = [
                "status" => true,
                "data" => $penggajian
            ];
        } else {
            $res = [
                "status" => false,
                "pesan" => "Belum ada data penggajian untuk bulan ini"
            ];
        }

        return response()->json($res, 200);
    }

    public function home(){
        $data = [
            "a" => "ASDASD"
        ];
        return response()->json($data, 200);
    }
    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'type' =>'success',
            'token' => $token,
            'token_type' => 'bearer'
        ]);
    }
}