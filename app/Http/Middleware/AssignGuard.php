<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Config;

class AssignGuard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Config::set('jwt.user', \Modules\Pegawai\Entities\Pegawai::class);
        Config::set('auth.providers', ['users' => [
                'driver' => 'eloquent',
                'model' => \Modules\Pegawai\Entities\Pegawai::class,
            ]]);
        try {
            $user = JWTAuth::parseToken()->authenticate();
            // return auth()->shouldUse('pegawai');
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json(['status' => 'Token is Invalid']);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->json(['status' => 'Token is Expired']);
            }else{
                return response()->json(['status' => 'Authorization Token not found']);
            }
        }
        return $next($request, $user);
    }
}
