import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    state: {
      login: localStorage.getItem("data") ? true : false,
      url: "https://sikeren.alfurqon.id",
      api: "https://sikeren.alfurqon.id/api",
      data: {
        history_pembayaran: [],
        siswa: {},
        tabungan: [],
        tagihan: [],
        total_pembayaran: 0,
        total_tabungan: 0,
        total_tagihan: 0,
      },
      checkout: [],
      h: {
        loadPembayaran: [],
        loading: false,
        page: 1,
      },
      settingan: {},
    },
    mutations: {
      setHistoryPembayaran(state) {
        state.h.loading = true;
        let nis = state.data.siswa.nis;
        let pin = state.data.siswa.pin;

        fetch(
          state.api +
            "/" +
            nis +
            "/" +
            pin +
            "/pembayaran?page=" +
            (parseInt(state.h.page) + 1)
        )
          .then((res) => {
            res.json().then(function (d) {
              if (d) {
                state.h.loadPembayaran = state.h.loadPembayaran.concat(d.data);
              }
              state.h.loading = false;
            });
          })
          .catch((e) => {
            alert(e);
          });
      },
      change(state, data) {
        let save = JSON.stringify(data);
        state.data = data;
        localStorage.setItem("data", save);
      },
      resetCheckout(state) {
        this.state.checkout = [];
      },
      addCheckout(state, data) {
        let d = state.checkout;
        if (d.indexOf(data) == -1) {
          d.push(data);
          state.checkout = d;
        } else {
          d.splice(d.indexOf(data), 1);
        }
      },
      deleteCheckout(state, data) {
        const array = state.checkout;
        const index = array.indexOf(data);
        if (index > -1) {
          array.splice(index, 1);
        }
        state.checkout = array;

        // console.log(state.checkout)
      },
      bayarCheckout(state) {
        let dat = "";
        for (let x in state.checkout) {
          if (dat == "") {
            dat = state.checkout[x].id;
          } else {
            dat = dat + "-" + state.checkout[x].id;
          }
        }
        let url =
          state.api +
          "/v1/buatTagihanBris/" +
          state.data.siswa.nis +
          "/" +
          state.data.siswa.pin +
          "/" +
          dat;

        axios
          .get(url)
          .then(function (response) {
            // handle success
            // let save = JSON.stringify(response.data.data)
            // state.tagihan_h2h = response.data.data
            state.checkout = [];
            // localStorage.setItem('tagihan_h2h', save);
          })
          .catch(function (error) {
            // handle error
            console.log(error);
          })
          .then(function () {
            // always executed
          });
      },
      defaultSettings(state) {
        axios.get(state.url + "/setting-json").then((response) => {
          state.settings = response.data;
          console.log("settings", state.settings);
        });
      },
      batalkanPembayaran(state, id) {
        // console.log("batalkan pembayaran ")

        let url =
          state.api +
          "/v1/batalkanTagihanBris/" +
          state.data.siswa.nis +
          "/" +
          state.data.siswa.pin +
          "/" +
          id;
        axios
          .get(url)
          .then(function (res) {
            console.log(res);
            state.checkout = [];
            localStorage.removeItem("tagihan_h2h");
          })
          .catch(function (error) {
            // handle error
            console.log(error);
          })
          .then(function () {
            // always executed
          });
      },
      bayarBRI(state) {
        let dat = "";
        for (let x in state.checkout) {
          if (dat == "") {
            dat = state.checkout[x].id;
          } else {
            dat = dat + "-" + state.checkout[x].id;
          }
        }
        let form = new FormData();
        form.append("nis", state.data.siswa.nis);
        form.append("pin", state.data.siswa.pin);
        form.append("tagihan", dat);

        axios.post(state.url + "/briva/buat-va", form).then((res) => {
          console.log("briva", res.data);
        });
      },
      batalBRI(state, id) {
        let form = new FormData();
        form.append("id", id);
        axios.post(state.url + "/briva/hapus-va", form).then((res) => {
          state.checkout = [];
          console.log("berhasil", res.data);
          localStorage.removeItem("tagihan_h2h");
        });
      },
      cekStatusBri(state, id) {
        axios
          .get(state.url + "/briva/cek-status-pembayaran/" + id)
          .then((res) => {
            // state.checkout = [];
            console.log("berhasil", res.data);
            // localStorage.removeItem("tagihan_h2h");
          });
      },
    },
    getters: {
      data: (state) => {
        return state.data;
      },
      h: (state) => {
        return state.h;
      },
      checkout: (state) => {
        return state.checkout;
      },
    },
    modules: {
      // example
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV,
  });

  return Store;
}
