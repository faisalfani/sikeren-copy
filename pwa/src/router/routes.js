import Login from '../views/Login.vue'

import Tagihan from '../views/Tagihan.vue'
import History from '../views/History.vue'

import Tabungan from '../views/Tabungan.vue'

import Home from '../views/Home.vue'
import Profil from '../views/Profil.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },{
    path: '/tagihan',
    name: 'tagihan',
    component: Tagihan
  },{
    path: '/history-pembayaran',
    name: 'history',
    component: History
  },{
    path: '/tabungan',
    name: 'tabungan',
    component: Tabungan
  },
  {
    path: '/profil',
    name: 'profil',
    component: Profil
  }
  // ,{
  //   path: '/tabungan-keluar',
  //   name: 'tabunganKeluar',
  //   component: Tabungankeluar
  // }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
